%Prends une sortie de dynamicmatch en entr�e
%Renvoie la matrice des notes qui matchent UNIQUEMENT ! 
function [dynamat_croche, dynamat_blanche] = GetMatchingNotes()
	
blanche  =  load('datablanche.mat','match');
croche   =  load('data_croche','match');
    

    for ii = 1 : length(croche.match)
        temp = [];
        for i = 1:length(croche.match{ii}.M)
            if( strcmp(croche.match{ii}.M{i,1},'M')==1 ||...
                strcmp(croche.match{ii}.M{i,1},'sub')==1)
                temp = [temp;croche.match{ii}.M(i,:)];
            end
        end

        dynamat_croche{ii} = temp;
    end
    
    dynamat_croche = dynamat_croche';
    
    for ii = 1 : length(blanche.match)
        temp = [];
        for i = 1:length(blanche.match{ii}.M)
            if( strcmp(blanche.match{ii}.M{i,1},'M')==1 ||...
                strcmp(blanche.match{ii}.M{i,1},'sub')==1)
                temp = [temp;blanche.match{ii}.M(i,:)];
            end
        end

        dynamat_blanche{ii} = SupprTrillNotes(SupprAccords(temp));
    end
    
    dynamat_blanche = dynamat_blanche';
end