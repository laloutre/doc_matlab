%prends un M en entr�e et supprime les trilles (uniquement pour le cas
%sp�cifique du premier pr�lude de Couperin

function out = SupprTrillNotes(in)

out = [];
for i=1:length(in)
	if(~isnan(in{i,2}) ...
		&& in{i,2}~=2 && in{i,2}~=3 ...
		&& in{i,2}~=6 && in{i,2}~=7 ...
		&& in{i,2}~=24 && in{i,2}~=25 ...
		&& in{i,2}~=28 && in{i,2}~=29 ...
		&& in{i,2}~=51 && in{i,2}~=52 ...
		&& in{i,2}~=47 && in{i,2}~=48 )
	
	out=[out;in(i,:)];
	end
end

end