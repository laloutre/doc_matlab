%fonction qui supprime les quatre accords dans les matchings du pr�lude de
%Couperin. 
%Pour etre plus pr�cis les �v�nements midi : 12/19/39/42

function out = SupprAccords(in)

vect =[];
out =[];
	for i=1:length(in)
		if(sum((vect==in{i,2}))==0 )
			vect = [vect,in{i,2}];
			out = [out ; in(i,:)];
		end
	end
end