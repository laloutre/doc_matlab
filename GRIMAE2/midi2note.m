function note = midi2note(midi_number)

notes = {'C','C#','D','D#','E','F','F#','G','G#','A','A#','B'};


modulo = mod(midi_number,12);

note = strcat(notes{modulo+1},num2str(floor(midi_number/12)-1));
end