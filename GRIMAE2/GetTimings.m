function [out_croches, out_blanches] = GetTimings()

[matches_croches, matches_blanches]  = GetMatchingNotes();



%blanches
for ii = 1:length(matches_blanches)
    out_blanches{ii} = [];
    temps = 0;
    notes = midi2note(matches_blanches{ii}{1,4});
    
     for i = 1:length(matches_blanches{ii})-1
                temps = (60/(matches_blanches{ii}{i+1,7}-matches_blanches{ii}{i,7}));
                notes = {midi2note(matches_blanches{ii}{i,4})};
                out_blanches{ii} = [out_blanches{ii};temps notes];
     end
    out_blanches{ii} = [[out_blanches{ii}; {0} midi2note(matches_blanches{ii}{1,4})] ];
end

%croches
for ii = 1:length(matches_croches)
    out_croches{ii} = [];
    temps = 0;
    notes = midi2note(matches_croches{ii}{1,4});
    
     for i = 1:length(matches_croches{ii})-1
                temps = (15/(matches_croches{ii}{i+1,7}-matches_croches{ii}{i,7}));
                notes = {midi2note(matches_croches{ii}{i,4})};
                out_croches{ii} = [out_croches{ii};temps notes];
     end
    out_croches{ii} = [out_croches{ii} ;[{0} midi2note(matches_croches{ii}{end,4})]];     
end

%modifications pour le vecteur croches (temps manquants repris dans le
%vecteur blanche)
for ii = 1:length(matches_croches)
    

    out_croches{ii} = [...
        repmat(out_blanches{ii}(1,:),4,1);...
        out_croches{ii}(1:36,:);...
        repmat(out_blanches{ii}(11,:),4,1);...
        out_croches{ii}(37:112,:);...
        repmat(out_blanches{ii}(31,:),4,1);...
        out_croches{ii}(113:144,:);...
        repmat(out_blanches{ii}(end,:),4,1)]; 
    
     out_croches{ii}(40,1) = {cell2mat(out_croches{ii}(40,1))*5};
     out_croches{ii}(120,1) = {cell2mat(out_croches{ii}(120,1))*5};
end
end