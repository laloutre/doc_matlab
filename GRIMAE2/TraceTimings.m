%Prends en entr�e un tableau de timings et une couleur et trace la fonction
%correspondant.
%exemple : 
%			rouge = TraceTimings(Timings_7,'r');
function out = TraceTimings()
	
load('data_blanche','timings')


colors{1} = [57 106 177]/255
colors{2} = [218 124 48]/255;
colors{3} = [62 150 81]/255;
colors{4} = [204 37 41]/255;
colors{5} = [107 76 154]/255;
colors{6} = [146 36 40]/255;
colors{7} = [148 139 61]/255;

figure(1);
hold on;

axeX= [0:2:100];

for i=1:length(timings)
	timings{i} = timings{i}(2:end);
out = plot(axeX(1:length(timings{i})),timings{i}(1:end),'Color',colors{i},'LineWidth',1.5);
end

set(gca,'XTick',[0:4:100000]);
set(gca,'XMinorGrid','on');
set(gca,'XTickLabel',[1:1:100]);
set(get(gca,'XLabel'),'String','Mesures');
set(get(gca,'YLabel'),'String','Tempo (en BpM)');

legend('Performer 1','Performer 2','Performer 3','Performer 4','Performer 5','Performer 6','Performer 7');
end
%Avec le set de data de Sylvain : 
%TraceTimings(TIMINGS_1,[57 106 177]/255);TraceTimings(TIMINGS_2,[218 124 48]/255);TraceTimings(TIMINGS_3,[62 150 81]/255);TraceTimings(TIMINGS_4,[204 37 41]/255);TraceTimings(TIMINGS_5,[107 76 154]/255);TraceTimings(TIMINGS_6,[146 36 40]/255);TraceTimings(TIMINGS_7,[148 139 61]/255);
