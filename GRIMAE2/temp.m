TAILLE_LIGNES       = 0.4;
TAILLES_MARQUEURS   = 4.5;

%load('data_croche','timings_blanches');
[timings_recomposes,timings_blanches] = GetTimings();

close all;

colors{1} = [57 106 177]/255;
colors{2} = [218 124 48]/255;
colors{3} = [62 150 81]/255;
colors{4} = [204 37 41]/255;
colors{5} = [107 76 154]/255;
colors{6} = [255 0 255]/255;
colors{7} = [148 139 61]/255;

figure(1);
hold on;

set(gca,'XGrid','off');
set(gca,'XMinorGrid','off');
xg = [0:1:1000];
xg(1:8:1000)=[];
yg = [0 1000];
xx = reshape([xg;xg;NaN(1,length(xg))],1,length(xg)*3);
yy = repmat([yg NaN],1,length(xg));
h_minorgrid = plot(xx,yy,':k');

xg = [0:8:1000];

yg = [0 1000];
xx = reshape([xg;xg;NaN(1,length(xg))],1,length(xg)*3);
yy = repmat([yg NaN],1,length(xg));
h_minorgrid = plot(xx,yy,'-k');


axis([0 160 0 140]);



temp= [0:1:100000];
forme_des_points =repmat({'square','diamond','o','diamond','o','diamond','o','diamond'},1,50)';
remplissage_des_points = repmat([0 1 1 1 0 1 1 1],1,50);
remplissage_des_points = remplissage_des_points(1:end);

for i=1:length(timings_blanches)
	%timings_blanches{i} = timings_blanches{i}(4:end);
    %timings_blanches{i}(1) = timings_blanches{i}(2);
    Axex = temp(1:length(timings_blanches{i})+0);
    AxeMarkers = forme_des_points(1:length(Axex)+0);
    
    tempos{i} = cell2mat(timings_blanches{i}(:,1));
    lignes{i} = plot(Axex,tempos{i}(1:end),'-','Color',colors{i},'LineWidth',TAILLE_LIGNES);
    %lignes{i} = plot(Axex,timings_blanches{i}(1:end),'-','Color',colors{i},'LineWidth',TAILLE_LIGNES);
end  

performer1 = strcat('Performer 1 :',num2str(round(median(tempos{1}))));
performer2 = strcat('Performer 2 :',num2str(round(median(tempos{2}))));
performer3 = strcat('Performer 3 :',num2str(round(median(tempos{3}))));
performer4 = strcat('Performer 4 :',num2str(round(median(tempos{4}))));
performer5 = strcat('Performer 5 :',num2str(round(median(tempos{5}))));
performer6 = strcat('Performer 6 :',num2str(round(median(tempos{6}))));
performer7 = strcat('Performer 7 :',num2str(round(median(tempos{7}))));
legend(...
    [lignes{1} lignes{2} lignes{3} lignes{4} lignes{5} lignes{6} lignes{7}],...
    {performer1,performer2,performer3,performer4,performer5,performer6,performer7});

for i=1:length(timings_blanches)
    tempos = cell2mat(timings_blanches{i}(:,1));
    note = timings_blanches{i}(:,2);
    for ii = 1:length(tempos)
        if(remplissage_des_points(ii))
            out = plot(Axex(ii),tempos(ii),...
                'Marker',AxeMarkers{ii},...
                'MarkerFaceColor',colors{i},...
                'MarkerEdgeColor',colors{i},...
                'MarkerSize',TAILLES_MARQUEURS);
                
                text(Axex(ii)+0.1,tempos(ii)+0.1,...
                note(ii),...
                'Fontsize',7);
        else
            out = plot(Axex(ii),tempos(ii),...
                'Marker',AxeMarkers{ii},...
                'MarkerFaceColor','none',...
                'MarkerEdgeColor',colors{i},...
                'MarkerSize',TAILLES_MARQUEURS);
                
                text(Axex(ii)+0.1,tempos(ii)+0.1,...
                note(ii),...
                'FontSize',7);
        end
    end
end


set(gca,'XTick',[0:8:100000]);
set(gca,'XTickLabel',[1:1:100]);
set(get(gca,'XLabel'),'String','Mesures');
set(get(gca,'YLabel'),'String','Tempo (en BpM)');

%impression wide 
axis([ 0 21*8 0 140]);
set(legend,'Position',[0.8459 0.6270 0.04 0.2889])
set(figure(1),'PaperSize',[20 9],'PaperPosition', [-2.5 0.0001 24.4 9])
print(figure(1),'-dpdf','vue_large')

%impression courte
axis([ 0*8 10*8 0 140])
set(legend,'Position',[0.8459 0.1170 0.04 0.2889])
set(figure(1),'PaperSize',[20 9],'PaperPosition', [-2.5 0.0001 24.4 9])
print(figure(1),'-dpdf','1-10_croches')

axis([ 10*8 20*8 0 140])
set(legend,'Position',[0.8459 0.6270 0.04 0.2889])
set(figure(1),'PaperSize',[20 9],'PaperPosition', [-2.5 0.0001 24.4 9])
print(figure(1),'-dpdf','10-20_croches')


axis([ 0 21*8 0 140]);
%moyennes etc....
for i=1:length(timings_blanches)
    %timings_blanches{i}((8*(num_mesure_deb-1))+1:(8*num_mesure_fin))
%moyennes (sans 1 et 20)
    moyenne{i} = mean(timings_blanches{i}((8*( 2 -1))+1:(8* 19 )));
%variance (sans 1,7,10,16,20)
    variance{i} = std([...
                   timings_blanches{i}((8*( 2 -1))+1:(8* 6 ));...
                   timings_blanches{i}((8*( 8 -1))+1:(8* 9 ));...
                   timings_blanches{i}((8*( 11 -1))+1:(8* 15 ));...
                   timings_blanches{i}((8*( 17 -1))+1:(8* 19 ))   ]);
%Tableau avec uniquement tempo des mes 1, 7, 10, 16, 20
    tab_tempo_moyen{i} = [...
    mean(timings_blanches{i}((8*( 1 -1))+1:(8* 1 )));...
    mean(timings_blanches{i}((8*( 7 -1))+1:(8* 7 )));...
    mean(timings_blanches{i}((8*( 10 -1))+1:(8* 10 )));...
    mean(timings_blanches{i}((8*( 16 -1))+1:(8* 16 )))
    mean(timings_blanches{i}((8*( 20 -1))+1:end))   ];
%Tableau avec uniquement tempo des phrases 2-> 5, 7->8, 10->15, 17->19
    tab_tempo_phrases{i} = [...
    mean(timings_blanches{i}((8*( 2 -1))+1:(8* 5 )));...
    mean(timings_blanches{i}((8*( 7 -1))+1:(8* 8 )));...
    mean(timings_blanches{i}((8*( 10 -1))+1:(8* 15 )));...
    mean(timings_blanches{i}((8*( 17 -1))+1:(8* 19 )))
    ];
end