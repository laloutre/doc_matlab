function v = mesuresJ(nb_mesures,signature,espace)

v='';
if(espace==0)
for i=0:nb_mesures
    for j=1:signature
        v = strcat(v,num2str(i+1),'.',num2str(j),';');
    end
end

else
v = '';
compteur =0;
for i=0:nb_mesures
    for j=1:signature
        if(j==1||compteur==espace)
            v = strcat(v,num2str(i+1),'.',num2str(j),';');
            compteur =0;
        else
            v = strcat(v,' ;');
            compteur = compteur+1;
        end
    end
end
       
end

end