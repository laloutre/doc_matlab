%usage : transforme(repertoire, nb_temps ,mesures)
function transforme(repertoire, nb_temps ,mesures)  

cd(repertoire);
    
fichiers = dir('*.warp');
nbfichiers = length(fichiers);
   parse = '%d,'; 
   parse2 = '';
for i =1 : nb_temps
    parse  = strcat(parse, '%f');
    parse2 = strcat(parse2,' %f');
end
    parse2 = strcat(parse2,'\n');

    %parse = {parse};
    %parse2 = {parse2};
    
    for i=1:nbfichiers 
        data = zeros(1,nb_temps);
        ftemp = fopen(fichiers(i).name);
        
        while feof(ftemp) == 0
            templ = fgetl(ftemp);
            temps= sscanf(templ,parse);
            temps = temps';
            data = [data;temps(1,2:end)];
        end
        data = data(2:end,:);
        %data = data[2:end];
        fclose(ftemp);
        delete(fichiers(i).name)
        
        %data = cell2mat(data);
        
        f = fopen(fichiers(i).name,'w');
        for i=1:mesures
        fprintf(f,parse2 ,data(i,1:end));
        
        end
        fclose(f)
    end
     cd ..
end