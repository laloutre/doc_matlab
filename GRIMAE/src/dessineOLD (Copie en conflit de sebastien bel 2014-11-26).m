 %dessine(X, REP) 
 %
 %Cr�e une interface graphique permettant d'afficher les tempos(contenus 
 %dans les fichiers textes pr�sents dans le repertoire REP) selon des mesures 
 %contenant X temps . Ces fichiers peuvent �tre obtenus en identifiant 
 %la position des temps dans Ableton live avec le patch GRIMAE.
 %Les fichiers doivent �tre nomm�s comme suit :
 %
 %interprete_instrument_auteurdumarquage_numerodeversion.txt
 
    function dessine(inputsignature,repertoire)  
          
    %% DECLARATIONS

    %initialisations globales
    close all;
    clear global;
    warning('OFF', 'MATLAB:legend:PlotEmpty');
    
    %declarations
    global  data  figure1 axes1 liste vecteur_interprete vecteur_version...
        vecteur_instrument calcul signature data_figure menu;
    signature = inputsignature;
    figure1  = figure('Name','Etude de timing','Visible','off','Color',...
        [1,1,1],'Position',[0,0,1000,900],'NumberTitle','off','Resize','on');

    %% INITIALISATION
    % On va loader les fichiers, cr�er des listes pour les menus, cr�er les
    % menus et la toolbar
    
    %on load les fichiers
    cd(repertoire);
    fichiers = dir('*.txt');
    nbfichiers = length(fichiers);
        %colormap(hsv); %pour reset la map de couleurs
    couleurs = hsv(nbfichiers+1);
        %couleurs = rand(length(fichiers), 3);
    marqueurs = 'oo<*sdv^>ph+.';
    lignes = cellstr(['- ';'- ';'-.';': ';'--']);
    
    %une liste pour savoir quel instrument/version/interpretes sont cliqu�s
    liste.interprete = '';
    liste.version = '';
    liste.instrument = '';
    liste.calcul{1} =0;
    liste.calcul{2} =0;
    tempmarqueur = 1;
    tempcouleur = 1;
    templigne = 1;
    
    for i=1:nbfichiers 
        temp= strrep(fichiers(i).name,'.txt','');
        
        [data{i}.interprete,temp]=strtok(temp,'_');
        [data{i}.instrument,temp]=strtok(temp,'_');
        [data{i}.version,temp]=strtok(temp,'_');
       
        data{i}.deja_trace = 0;
        data{i}.data = importdata(fichiers(i).name);
        data{i}.plot1 = 'rien';
        data{i}.plot2 = 'rien';
        data{i}.plot3 = 'rien';
        data{i}.plot4 = 'rien';
        data{i}.moyennecalculok = 0;
        data{i}.moyenne = data{i}.data;
        data{i}.moyenneinst = data{i}.data;
        data{i}.min = data{i}.data;
        data{i}.max = data{i}.data;
    %data{i}.interprete = strcat(data{i}.interprete,'--' ,data{i}.instrument);
        
        for j=1:length(data)
        if(strcmp(data{i}.interprete,data{j}.interprete) ==1 && j~=i)
            data{i}.couleur = data{j}.couleur; break;
        else data{i}.couleur = couleurs(mod(tempcouleur,nbfichiers+1)+1,:);
        end
        end
        
        for j=1:length(data)
        if(strcmp(data{i}.version,data{j}.version) ==1 && j~=i)
            data{i}.ligne = data{j}.ligne; break;
        else data{i}.ligne = lignes{mod(templigne,4)+1}; 
        end
        end
        
        for j=1:length(data)
        if(strcmp(data{i}.instrument,data{j}.instrument) ==1 && j~=i)
            data{i}.marqueur = data{j}.marqueur;break;
        else data{i}.marqueur = marqueurs(mod(tempmarqueur,10)+1);
        end
        end
        templigne       =   templigne+1;
        tempcouleur     =   tempcouleur+1;
        tempmarqueur    =   tempmarqueur+1;
        
    end
    
    cd ..;

    %% Initialisation de la toolbar
    set(figure1,'Toolbar', 'figure');
    hOpen = findall(figure1, 'tooltipstring', 'Open File');
    hOpen2 = findall(figure1, 'tooltipstring', 'New Figure');
    hOpen3 = ...
        findall(figure1, 'tooltipstring', 'Show Plot Tools and Dock Figure'); 
    hOpen4 = findall(figure1, 'tooltipstring', 'Link Plot');   
    hOpen5 = findall(figure1, 'tooltipstring', 'Rotate 3D');
    hOpen6 = findall(figure1, 'tooltipstring', 'Edit Plot');
    hOpen7 = findall(figure1, 'tooltipstring', 'Hide Plot Tools');
    hOpen8 = findall(figure1, 'tooltipstring', 'Insert Colorbar');
    delete(hOpen4);
    delete(hOpen5);
    delete(hOpen);
    delete(hOpen2);
    delete(hOpen3);
    delete(hOpen6);
    delete(hOpen7);
    delete(hOpen8);
    
    %% Initialisation du menu
    set(figure1,'MenuBar','none');
    menu = uimenu(figure1,'Label','Interpretes');
    menu2 = uimenu(figure1,'Label','Versions');
        %menu3 = uimenu(figure1,'Label','Instruments');
    menu4 = uimenu(figure1,'Label','Type de calcul');
    menu5 = uimenu(figure1,'Label','Calculer','Callback',{@calculer});
    menu6 = uimenu(figure1,'Label','Mesures','Callback',{@Redo_Ticks});
    menu7 = uimenu(figure1,'Label','Zoom','Callback',{@Re_Zoom});
    marq_couleurs = 1;
    marq_marqueur = 1;
    marq_ligne = 1;
    vecteur_interprete = [];
    vecteur_version = [];
    vecteur_instrument = [];
    
    %on va cr�er des vecteurs avec les interpretes pour faire les menuus
    temp = '';
    temp2 = [];  
    for i=1 : length(data)
        if(~strcmp(vecteur_interprete,data{i}.interprete))
            vecteur_interprete{length(vecteur_interprete)+1} =...
                data{i}.interprete;
        end       
    end
    
    %on va cr�er des vecteurs avec les instruments pour faire les menus 
    temp = '';
    temp2 = [];
    for i=1 : length(data)
        temp = strfind(vecteur_instrument,data{i}.instrument);
        for j=1:length(temp) 
            temp2 = find(temp{j});
            if(temp2==1)
                break;
            end
        end
        if(isempty(temp2))
            vecteur_instrument{length(vecteur_instrument)+1}=...
                data{i}.instrument;
        end   
    end
    
    %on va cr�er des vecteurs avec les versions pour faire les menus
    temp = '';
    temp2 = [];
    for i=1 : length(data)
        temp = strfind(vecteur_version,data{i}.version);
        for j=1:length(temp)
            temp2 = find(temp{j});
            if(temp2==1)
                break;
            end
        end
        if(isempty(temp2))
            vecteur_version{length(vecteur_version)+1} =...
                data{i}.version;
        end   
    end

    %On fait les menus
    for i=1:length(vecteur_interprete)
    sousmenu{1} = uimenu(menu,'Label',vecteur_interprete{i},'Callback',...
         {@clickseb},'Checked','off');
    end
    for i=1:length(vecteur_version)
    sousmenu2{1} = uimenu(menu2,'Label',vecteur_version{i},'Callback',...
         {@clickseb},'Checked','off');
    end
%      for i=1:length(vecteur_instrument)
%     sousmenu3{1} = uimenu(menu3,'Label',vecteur_instrument{i},'Callback',...
%          {@clickseb},'Checked','off');
%      end
    sousmenu4{1} = uimenu(menu4,'Label','Moyenne','Callback',...
         {@clickseb},'Checked','off');
    sousmenu4{2} = uimenu(menu4,'Label','Moyenne instantann�e','Callback',...
         {@clickseb},'Checked','off');
    
    %% On r�gle les axes de la figure.
    %g = get(figure1);
    ticks = [1.0 : 10 :((length(data{1}.data)/signature+2))+0.1];
    labels_ticks =  num2str(ticks);
    labels_ticks =     str2num(strrep(num2str(labels_ticks),'.0','.1')); 
    labels_ticks =     str2num(strrep(num2str(labels_ticks),'.25','.2 '));
    labels_ticks =     str2num(strrep(num2str(labels_ticks),'.50','.3 '));
    labels_ticks =     str2num(strrep(num2str(labels_ticks),'.75','.4 '));
    set(figure1,'Units','normalized','Visible','on');
            axes1 = axes('Parent',figure1,...
                'YGrid','on','YLimMode','auto',...
            'XMinorTick','off','XTick',ticks,'XTickLabel',labels_ticks,...
            'XGrid','on','TickDir','in','XLim',[1,...
            length(data{1}.data)/signature+2]);
    ylabel(axes1, 'Tempo en BPM');
    xlabel(axes1, 'Mesures');
    hold(axes1,'all');
    
    %initialisation de l'impression
    set(gcf,'PaperPositionMode', 'manual', 'PaperUnits','centimeters',...
    'PaperOrientation', 'LandScape', 'PaperSize',[25 20],...
    'Paperposition',[1 1 25 15]);
    end
    
    function trace(z)

    %% DECLARATIONS
    global data  figure1 axes1 liste signature vect;
    
    %% RESET AFFICHAGE (on efface tout).
    for i = 1 :length(data)
           if( ishandle(data{i}.plot1))
               delete(data{i}.plot1);
           end
           if(ishandle(data{i}.plot2))
               delete(data{i}.plot2);
           end
           if(ishandle(data{i}.plot3))
               delete(data{i}.plot3);
           end
           if(ishandle(data{i}.plot4))
               delete(data{i}.plot4);
           end
           data{i}.deja_trace =0;
    end
   
    %% TRACAGE
    % On retrace ce qui est s�l�ctionn� dans les menus seulement
    for i = 1 : length(data)
    for j = 1 :length(liste.interprete)
    for k = 1:length(liste.version)
        if((strcmp(data{i}.interprete ,liste.interprete{j})==1) &&...
                (strcmp(data{i}.version ,liste.version{k})==1) &&...
                (data{i}.deja_trace==0))
            
            %regle les axes
            data{i}.deja_trace =1;
            %vect = 1:0.25:length(data{i}.data(:,1))/4+0.75;
            vect = 1:(1/signature):length(data{i}.data(:,1))/signature+...
                (1-1/signature);

             data{i}.plot1 = plot(vect,data{i}.data);
            
             legende = char(strcat(char(data{i}.interprete),{' '},...
                char(data{i}.version),{' '},char(data{i}.instrument)));
            %regles lesproprietes de la ligne
            set(data{i}.plot1,...
            'Parent',axes1,...
            'Color',data{i}.couleur,...
            'MarkerFaceColor',data{i}.couleur,...
            'LineStyle',data{i}.ligne,...
            'LineWidth',1,...
            'Marker',data{i}.marqueur,...
            'MarkerSize',3, ... %RAJOUT�
            'DisplayName',legende);
            
            if(ishandle(data{i}.plot2))
                set(data{i}.plot2,...
            'Parent',axes1,...
            'Color',data{i}.couleur,...
            'MarkerFaceColor',data{i}.couleur,...
            'MarkerSize',3, ... %RAJOUT�
            'LineStyle',data{i}.ligne,...
            'LineWidth',1,...
            'Marker',data{i}.marqueur,...
            'MarkerSize',3, ... %RAJOUT�
            'DisplayName',strcat('Max',char(data{i}.interprete),'/',...
                char(data{i}.version),'/',char(data{i}.instrument)));
            end
            if(ishandle(data{i}.plot3))
            set(data{i}.plot3,...
            'Parent',axes1,...
            'Color',data{i}.couleur,...
            'MarkerSize',3, ... %RAJOUT�            
            'MarkerFaceColor',data{i}.couleur,...
            'LineStyle',data{i}.ligne,...
            'LineWidth',1,...
            'Marker',data{i}.marqueur,...
            'MarkerSize',3, ... %RAJOUT�
            'DisplayName',strcat('Min',char(data{i}.interprete),'/',...
                char(data{i}.version),'/',char(data{i}.instrument)));
            end
            
            if(strcmp(data{i}.marqueur, '.')==1)...
                    set(data{i}.plot1,'MarkerSize',3);end
        end
    end
    end
    end
        ylim('auto');
        
    %% LEGENDE
    legend(axes1,'off');
    legend(axes1,'show');
    end
    
    function trace_moyenne(z)

    %% DECLARATIONS
    global data  figure1 axes1 liste signature;
     
    %% RESET AFFICHAGE (on efface tout).
    for i = 1 :length(data)
           if(ishandle(data{i}.plot1))delete(data{i}.plot1);end
           if(ishandle(data{i}.plot2))delete(data{i}.plot2);end
           if(ishandle(data{i}.plot3))delete(data{i}.plot3);end
           if(ishandle(data{i}.plot4))delete(data{i}.plot4);end
           data{i}.deja_trace =0;
    end
   
    %% TRACAGE
    for i = 1 : length(data)
    for j = 1 :length(liste.interprete)
    for k = 1:length(liste.version)
        %Si c'est � tracer on le trace
        if((strcmp(data{i}.interprete ,liste.interprete{j})==1) &&...
                (strcmp(data{i}.version ,liste.version{k})==1) &&...
                (data{i}.deja_trace==0))
            %regle les axes
            data{i}.deja_trace =1;
            vect = 1:(1/signature):length(data{i}.data(:,1))/signature+...
                (1-1/signature);
            %dessine la ligne ou la moyenne
            if(liste.calcul{1}==1)
                %delete(data{i}.plot1); 
                 data{i}.plot1 = plot(vect,data{i}.moyenne);
                 %data{i}.plot2 = plot(vect,data{i}.max);
                 %data{i}.plot3 = plot(vect,data{i}.min);
                 data{i}.plot4 = plot(vect,data{i}.data);
                 
            elseif(liste.calcul{2}==1)
              vect2  = 1:0.01:length(data{i}.data(:,1))/signature+...
                (1-1/signature);
             test = interp1(vect,data{i}.moyenneinst,vect2,'spline');
                data{i}.plot1 = plot(vect2,test);
                %data{i}.plot1 = plot(vect,data{i}.moyenneinst);
                %data{i}.plot2 = plot(vect,data{i}.max);
                %data{i}.plot3 = plot(vect,data{i}.min);
                data{i}.plot4 = plot(vect,data{i}.data);
            
            
            elseif(liste.calcul{2}==0&&liste.calcul{1}==0) data{i}.plot1 =...
                    plot(vect,data{i}.data);
            end
            legende = char(strcat('Moyenne ',{' '},char(data{i}.interprete),{' '},...
                char(data{i}.version),{' '},char(data{i}.instrument)));
            %regles lesproprietes de la ligne
            if(ishandle(data{i}.plot1))
            set(data{i}.plot1,...
            'Parent',axes1,...
            'Color',data{i}.couleur,...%'MarkerFaceColor',data{i}.couleur,...
            'LineStyle',data{i}.ligne,...
            'LineWidth',3,...
            'Marker',data{i}.marqueur,...
            'MarkerSize',0.1, ... %RAJOUT�
            'DisplayName',legende);
             end
            if(ishandle(data{i}.plot2))
            legende2 = char(strcat('Max ',{' '},char(data{i}.interprete),{' '},...
                char(data{i}.version),{' '},char(data{i}.instrument)));
                set(data{i}.plot2,...
            'Parent',axes1,...
            'Color',data{i}.couleur,...
            'MarkerFaceColor',data{i}.couleur,...
            'MarkerSize',3, ... %RAJOUT�            
            'LineStyle',data{i}.ligne,...
            'LineWidth',1,...'Marker',data{i}.marqueur,...
            'DisplayName',legende2);
            end
            if(ishandle(data{i}.plot3))
            set(data{i}.plot3,...
            'Parent',axes1,...
            'Color',data{i}.couleur,...            
            'MarkerFaceColor',data{i}.couleur,...
            'MarkerSize',3, ... %RAJOUT�
            'LineStyle',data{i}.ligne,...
            'LineWidth',1,...'Marker',data{i}.marqueur,...
            'DisplayName',strcat('Min',char(data{i}.interprete),'/',...
                char(data{i}.version),'/',char(data{i}.instrument)));
            end
            if(ishandle(data{i}.plot4))
            legende4 = char(strcat(char(data{i}.interprete),{' '},...
            char(data{i}.version),{' '},char(data{i}.instrument)));
            set(data{i}.plot4,...
            'Parent',axes1,...
            'Color',data{i}.couleur,...
            'MarkerFaceColor',data{i}.couleur,...
            'MarkerSize',3, ... %RAJOUT�            
            'LineStyle',data{i}.ligne,...
            'LineWidth',1,...
            'Marker',data{i}.marqueur,...
            'DisplayName',legende4);
            end
            if(strcmp(data{i}.marqueur, '.')==1)set(data{i}.plot1,...
                    'MarkerSize',3);end
        end
    end
    end
    end
        %set(axes1,'YLim','auto');
    
    %% LEGENDE
    legend(axes1,'off');
    
    legend(axes1,'show');
    end
    
    function clickseb(hObject, EventData, handles)
    
     %% DECLARATIONS
        global axes1 data liste;
        
     %% RECUPERATION DU LABEL DU SOUS MENU CLIQU�
        menuclique = get(get(hObject,'Parent'),'Label');
        
     %% ACTION POUR LE SOUS MENU ET CHANGEMENT D'�TAT DU SOUS MENU 
     %(CHECK/UNCHECK)
        if(strcmp(get(hObject,'Checked'),'on')==1)
            
                set(hObject,'Checked','off');
                
                if(strcmp(menuclique,'Interpretes')==1)
                    for i=1 :length(liste.interprete)
                        if(strcmp(liste.interprete{i},...
                                get(hObject,'Label'))==1)
                            liste.interprete(i) = '';
                            break
                        end
                    end
                    trace();
                end
                if(strcmp(menuclique,'Versions')==1)
                    for i=1 :length(liste.version)
                        if(strcmp(liste.version{i},...
                                get(hObject,'Label'))==1)
                            liste.version(i) = '';
                            break
                        end
                    end
                    trace();
                end
                if(strcmp(menuclique,'Instruments')==1)
                    for i=1 :length(liste.instrument)
                        if(strcmp(liste.instrument{i},...
                                get(hObject,'Label'))==1)
                            liste.instrument(i) = '';
                            break
                        end
                    end
                    trace();
                end
                if(strcmp(menuclique,'Type de calcul')==1)
                    if(strcmp(get(hObject,'Label'),'Moyenne')==1)
                    liste.calcul{1} =0;end
                    if(strcmp(get(hObject,'Label'),'Moyenne instantann�e')==1)
                    liste.calcul{2} =0;end
                    trace();
                end
        else
            
                set(hObject,'Checked','on')
                if(strcmp(menuclique,'Interpretes')==1)
                    liste.interprete{length(liste.interprete)+1} =...
                        get(hObject,'Label');

                trace();
                end
                if(strcmp(menuclique,'Versions')==1)
                   liste.version{length(liste.version)+1} =...
                       get(hObject,'Label');
                   trace();
                end
                if(strcmp(menuclique,'Instruments')==1)
                    liste.instrument{length(liste.instrument)+1} =...
                        get(hObject,'Label');
                    trace();
                end
                if(strcmp(menuclique,'Type de calcul')==1)
                    if(strcmp(get(hObject,'Label'),'Moyenne')==1)
                    liste.calcul{1} =1;end
                    %calculer();
                    %trace_moyenne();
                    if(strcmp(get(hObject,'Label'),'Moyenne instantann�e')==1)
                    liste.calcul{2} =1;end
                    %calculer();
                    %trace_moyenne();
                end
        end
    end
    
    function calculer(hObject, EventData, handles)
      
        %% D�CLARATIONS
        global data axes1 liste signature vect menu;
        
         temp = get(axes1,'XTick');
         
                
        indices = test_data_affiche();
        
        %% CALCUL DES MOYENNES
        %CAS 1 : Moyenne simple
        if(liste.calcul{1}==1)
        for j = 1 : length(indices)
            for i =1:(length(temp)-1)
                % R�CUP�RATION DES LIMITES DU ZOOM
                limites = [find(vect==temp(i)),find(vect==temp(i+1))];
                if(limites(1)<=0)limites(1)=1;end
                if(limites(end)>length(data{1}.data))
                    limites(2)=length(data{1}.data);
                end
                data{indices(j)}.moyenne(limites(1): limites(2)) = ...
                    mean(data{indices(j)}.data(limites(1): limites(2)));
                
                data{indices(j)}.max(limites(1): limites(2)) = ...
                    max(data{indices(j)}.data(limites(1): limites(2)));
                
                data{indices(j)}.min(limites(1): limites(2)) ...
                    = min(data{indices(j)}.data(limites(1): limites(2)));
              
            end
        end
        end
           
        % CAS 2 : Moyenne liss�e
        if(liste.calcul{2}==1)
            for j = 1 : length(indices)
            for i =1:(length(temp)-1)
                % R�CUP�RATION DES LIMITES DU ZOOM
                limites = [find(vect==temp(i)),find(vect==temp(i+1))];
                if(limites(1)<=0)limites(1)=1;end
                if(limites(end)>length(data{1}.data))
                    limites(2)=length(data{1}.data);
                end
                data{indices(j)}.moyenneinst(limites(1): limites(2)) = ...
                    smooth(data{indices(j)}.data(limites(1): limites(2)));
                
                data{indices(j)}.max(limites(1): limites(2)) = ...
                    max(data{indices(j)}.data(limites(1): limites(2)));
                
                data{indices(j)}.min(limites(1): limites(2)) ...
                    = min(data{indices(j)}.data(limites(1): limites(2)));
              
            end
            end
        end        
        trace_moyenne();
    end
    
    function Redo_Ticks(hObject, EventData, handles)

        %%declarations

        global figure1 axes1 data signature
        temp = inputdlg;
        temp2 = str2num(temp{1});
        temp3 = strsplit(temp{1});
        
        %% FAIRE EN FONCTION DE LA SIGNATURE !!!!!!!!!!
        temp2 =     str2num(strrep(num2str(temp2),'.1','')); 
        temp2 =     str2num(strrep(num2str(temp2),'.2','.25 '));
        temp2 =     str2num(strrep(num2str(temp2),'.3','.5 '));
        temp2 =     str2num(strrep(num2str(temp2),'.4','.75 '));
        
        for i = 1:size(temp2,2)
            if(round(temp2(i))==temp2(i))
            temp3{i}    = num2str(temp2(i)+0.1);
            end
        end
    
    %set(figure1,'Units','normalized','Visible','on');
            set(axes1,'Parent',figure1,...
                'YGrid','on','YLimMode','auto',...
            'XMinorTick','off','XTick',temp2,'XTickLabel',temp3,...
            'XGrid','on','TickDir','in');
        
    ylabel(axes1, 'Tempo en BPM');
    hold(axes1,'all');
    end
    
    function Re_Zoom(hObject, EventData, handles)
          %%declarations
        global figure1 axes1 data signature
        temp = inputdlg;
        temp2 = str2num(temp{1});
        
        set(axes1,'XLim',[temp2(1) temp2(2)]);
        zoom('on')
        zoom reset;
    end
    
    function indice = test_data_affiche()
        global liste data;
        indice =[];
        for i = 1 : length(data)
            for j = 1 :length(liste.interprete)
                for k = 1:length(liste.version)
                    if((strcmp(data{i}.interprete ,liste.interprete{j})==1) &&...
                        (strcmp(data{i}.version ,liste.version{k})==1) )
                    indice = [indice,i];
                    end
                end
            end
        end
    end