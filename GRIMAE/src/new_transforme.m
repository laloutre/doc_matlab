function [] = new_transforme(rep,sig)


cd(rep);
fichiers = dir('*.asd');

nb_fich = length(fichiers);

for i = 1 : nb_fich
    nom = fichiers(i).name;
    dat = getwarpmarkers(nom,sig);
    nom = strrep(nom,'.mp3.asd','_piano_sylvain.warp');
    nom = strrep(nom,'.wav.asd','_piano_sylvain.warp');
    export(dat,'file',nom);
    dat = '';
end

cd ..;
end