 %dessine(X, REP) 
 %
 %Cr�e une interface graphique permettant d'afficher les tempos(contenus 
 %dans les fichiers textes pr�sents dans le repertoire REP) selon des mesures 
 %contenant X temps . Ces fichiers peuvent �tre obtenus en identifiant 
 %la position des temps dans Ableton live avec le patch GRIMAE.
 %Les fichiers doivent �tre nomm�s comme suit :
 %
 %interprete_instrument_auteurdumarquage_numerodeversion.txt
 
    function dessine(repertoire,ecart,mes)  
          
    %% DECLARATIONS

    %initialisations globales
    close all;
    clear global;
    warning('OFF', 'MATLAB:legend:PlotEmpty');
    
    %declarations
    global  data  figure1 axes1 liste vecteur_interprete ...
    vecteur_version vecteur_instrument calcul signature data_figure...
    menu sousmenu labels_ticks ticks mesures M;
    
    figure1  = figure('Name','Etude de timing','Visible','off','Color',...
        [1,1,1],'Position',[0,0,1000,900],'NumberTitle','off','Resize','on');

    %% INITIALISATION
    % On va loader les fichiers, cr�er des listes pour les menus, cr�er les
    % menus et la toolbar
    
    %on load les fichiers
    cd(repertoire);
    fichiers = dir('*.warp');
    nbfichiers = length(fichiers);
    %COULEURS
        %colormap(hsv); %pour reset la map de couleurs
    %couleurs = hsv(nbfichiers+1);
    couleurs = rgbconv('a6cee3');
    couleurs = [couleurs;rgbconv('1f78b4')];
    couleurs = [couleurs;rgbconv('b2df8a')];
    couleurs = [couleurs;rgbconv('33a02c')];
    couleurs = [couleurs;rgbconv('a6cee3')];
    couleurs = [couleurs;rgbconv('fdbf6f')];
    couleurs = [couleurs;rgbconv('6a3d9a')];
    couleurs = [couleurs;rgbconv('000000')];
    couleurs = [couleurs;rgbconv('998c8c')];
    couleurs = [couleurs;rgbconv('e31a1c')];
    couleurs = [couleurs;rgbconv('b15928')];
    couleurs = [couleurs;rgbconv('ff7f00')];
    couleurs = [couleurs;rgbconv('cab2d6')];
    couleurs = [couleurs;rgbconv('c994c7')];
    couleurs = [couleurs;rgbconv('8dd3c7')];
    couleurs = [couleurs;rgbconv('b15928')];
    couleurs = [couleurs;rgbconv('b15928')];
    couleurs = [couleurs;rgbconv('b15928')];
    couleurs = [couleurs;rgbconv('b15928')];
    couleurs = [couleurs;rgbconv('b15928')];
    couleurs = [couleurs;rgbconv('b15928')];
    
    rgb2hsv(couleurs);
        %couleurs = rand(length(fichiers), 3);
    marqueurs = 'oosdv^p>+.';
    tailles = [2,3,3,5,3,3,3,3,3,3,3];
    lignes = cellstr(['--';'- ';'-.';': ';'--']);
    
    %une liste pour savoir quel instrument/version/interpretes sont cliqu�s
    liste.interprete = '';
    liste.version = '';
    liste.instrument = '';
    tempmarqueur = 1;
    tempcouleur = 1;
    templigne = 1;
    
    mesures = mes;
    %% INITIALISATION DE LA STRUCTURE !
    for i=1:nbfichiers 
        temp = strrep(fichiers(i).name,'.warp','');
        
        [data{i}.interprete,temp]=strtok(temp,'_');
        [data{i}.instrument,temp]=strtok(temp,'_');
        [data{i}.version,temp]=strtok(temp,'_');
        data{i}.data = '';
        data{i}.deja_trace = 0;
        tempdata = importdata(fichiers(i).name);
        
        xi = tempdata.data(:,1);
        yi= tempdata.data(:,4);
        data{i}.ticks = [1:0.05:mesures];
        data{i}.data = interp1(xi, yi, data{i}.ticks , 'linear');
        
        
        signature =tempdata.data(1,5);
        data{i}.plotdata = 'rien';
        data{i}.plotmoyenne = 'rien';
        data{i}.plotmoyenne2 = 'rien';
        data{i}.moyenne = 0;
        data{i}.moyenneinst = 0;
        data{i}.premiers_temps = 'rien';
        %Calcul des INTER ONSET INTERVALS
        Ioi(i);
        data{i}.moyenneIoi = 0;
        data{i}.checked = 'off';
        
        for j=1:length(data)
        if(strcmp(data{i}.interprete,data{j}.interprete) ==1 && j~=i)
            data{i}.couleur = data{j}.couleur; break;
        else data{i}.couleur = couleurs(mod(tempcouleur,nbfichiers+1)+1,:);
        end
        end
        
        for j=1:length(data)
        if(strcmp(data{i}.version,data{j}.version) ==1 && j~=i)
            data{i}.ligne = data{j}.ligne; break;
        else data{i}.ligne = lignes{mod(templigne,4)+1}; 
        end
        end
        
        for j=1:length(data)
        if(strcmp(data{i}.instrument,data{j}.instrument)==1 && j~=i)
            data{i}.marqueur = data{j}.marqueur;
            data{i}.taille   = data{j}.taille;
            break;
            
        else
            data{i}.marqueur = marqueurs(mod(tempmarqueur,10)+1);
            data{i}.taille   = tailles(mod(tempmarqueur,10)+1);
            tempmarqueur    =   tempmarqueur+1;
        end
        end
        templigne       =   templigne+1;
        tempcouleur     =   tempcouleur+1;
        
        
    end
    
    for i=1:nbfichiers 
%         difference(i);
    end
    
    
    cd ..;

    %% Initialisation de la toolbar
    set(figure1,'Toolbar', 'figure');
    hOpen = findall(figure1, 'tooltipstring', 'Open File');
    hOpen2 = findall(figure1, 'tooltipstring', 'New Figure');
    hOpen3 = ...
        findall(figure1, 'tooltipstring', 'Show Plot Tools and Dock Figure'); 
    hOpen4 = findall(figure1, 'tooltipstring', 'Link Plot');   
    hOpen5 = findall(figure1, 'tooltipstring', 'Rotate 3D');
    hOpen6 = findall(figure1, 'tooltipstring', 'Edit Plot');
    hOpen7 = findall(figure1, 'tooltipstring', 'Hide Plot Tools');
    hOpen8 = findall(figure1, 'tooltipstring', 'Insert Colorbar');
    delete(hOpen4);
    delete(hOpen5);
    delete(hOpen);
    delete(hOpen2);
    delete(hOpen3);
    delete(hOpen6);
    delete(hOpen7);
    delete(hOpen8);
    
    %% Initialisation des menus
    set(figure1,'MenuBar','none');
    menu{1} = uimenu(figure1,'Label','Interpretes');
    menu{4} = uimenu(figure1,'Label','Courbes');
    menu{5} = uimenu(figure1,'Label','Affichage');
    menu{8} = uimenu(figure1,'Label','Imprimer','Callback',{@Imprimer});
    menu{9} = uimenu(figure1,'Label','BPM/IOI');
    menu{10}= uimenu(figure1,'Label','Autoprint','Callback',{@autoPrintInitiate});
    
    marq_couleurs = 1;
    marq_marqueur = 1;
    marq_ligne = 1;
    vecteur_interprete = [];
    vecteur_version = [];
    vecteur_instrument = [];
    
    %% Vecteurs 
    %on va cr�er des vecteurs avec les interpretes pour faire les menuus
    temp = '';
    temp2 = [];  
    for i=1 : length(data)
        if(~strcmp(vecteur_interprete,data{i}.interprete))
            vecteur_interprete{length(vecteur_interprete)+1} =...
                data{i}.interprete;
        end       
    end
    
    %% Sous Menus
    %SOUS MENU 1 pour les INTERPRETES
    for i=1:length(vecteur_interprete)
    sousmenu{1}(i) = uimenu(menu{1},'Label',vecteur_interprete{i},'Callback',...
         {@clickseb},'Checked','off');
    end
    
    %SOUS MENU 3 pour BPM/IOI
    sousmenu{3}(1) = uimenu(menu{9},'Label','IOI','Callback',...
         {@clickseb},'Checked','off');
    sousmenu{3}(2) = uimenu(menu{9},'Label','BPM','Callback',...
         {@clickseb},'Checked','off');
     set(sousmenu{3}(2),'Checked','on');
     
    %SOUS MENU 4 MOYENNES
    sousmenu{4}(1) = uimenu(menu{4},'Label','Moyenne','Callback',...
         {@clickseb},'Checked','off');
    sousmenu{4}(2) = uimenu(menu{4},'Label','Boite a moustaches','Callback',...
         {@clickseb},'Checked','off');
    sousmenu{4}(3) = uimenu(menu{4},'Label','Tempo','Callback',...
         {@clickseb},'Checked','on'); 
     
    sousmenu{2}(1) = uimenu(menu{5},'Label','Limites','Callback',...
         {@clickseb},'Checked','off');
    sousmenu{2}(2)  = uimenu(menu{5},'Label','Mesures','Callback',{@Redo_Ticks});
    sousmenu{2}(3)  = uimenu(menu{5},'Label','Zoom','Callback',{@Re_Zoom});
     
    %% On cr�e les axes de la figure.
    ticks =  [0:ecart:mesures];
    set(figure1,'Units','normalized','Visible','on');
    axes1 = axes('Parent',figure1,...
    'YGrid','on','YLimMode','auto',...
    'XMinorTick','off','XTick',ticks,...%'XTickLabel',labels_ticks,...
    'XGrid','on','TickDir','in','XLim',[1,mesures]);
    ylabel(axes1, 'Tempo in BPM');
    xlabel(axes1, 'Bar number');
    hold(axes1,'all');
    
    %% Initialise la moyenne
    M.lim = [1,mesures]
    
    %% Initialisation de l'impression
    set(gcf,'PaperPositionMode', 'manual', 'PaperUnits','centimeters',...
    'PaperOrientation', 'LandScape', 'PaperSize',[25 20],...
    'Paperposition',[1 1 25 15]);
    %clickseb(sousmenu{1}(1)); %pour les tests !
    %clickseb(sousmenu{2}(1));
%    clickseb(sousmenu{3}{1});
    %clickseb(sousmenu{2}{2});
%    clickseb(sousmenu{1}{1});
    end
    
    function reset()
    global data;
    %% RESET AFFICHAGE (on efface tout).
    for i = 1 :length(data)
           if( ishandle(data{i}.plotdata))
               delete(data{i}.plotdata);
           end
            if(ishandle(data{i}.plotmoyenne))
               delete(data{i}.plotmoyenne);
            end
            if(ishandle(data{i}.plotmoyenne2))
               delete(data{i}.plotmoyenne2);
            end
            if(ishandle(data{i}.premiers_temps))
                delete(data{i}.premiers_temps);
                delete(data{i}.sec_temps);
            end
    end
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function trace(z)

    %% DECLARATIONS
    global data M figure1 ticks axes1 liste signature menu sousmenu ...
        mesures labels_ticks;
    
    reset();
   
    %% TRACAGE
    % On retrace ce qui est s�l�ctionn� dans les menus seulement
    for i = 1 : length(data)
            if(strcmp(data{i}.checked,'on')==1)
               
    %On regarde si on est en mode BPM ou IOI
     Child = get(menu{9},'Children');
     Child2 = get(menu{4},'Children');
%PLOT !              
     if((strcmp(get(Child(1),'Checked'),'on')==1 ) && ...
             (strcmp(get(Child2(1),'Checked'),'on')==1 ) )
     %dessine les donn�es   
        data{i}.plotdata = plot(data{i}.ticks,data{i}.data);
        %data{i}.plotdata = plot(data{i}.ticks,data{i}.data');
        %Legende 
         legende1 = char(strcat(char(data{i}.interprete),{' '},...
            char(data{i}.instrument),...
            {' (Mea : '},num2str(roundn(mean(data{i}.data'))),{','},...
            {' Dev : '},num2str(roundn(std(data{i}.data),2)),{','},...
            {' Med : '},num2str(roundn(median(data{i}.data),2)),{')'}));
     elseif((strcmp(get(Child(2),'Checked'),'on')==1 ) && ...
             (strcmp(get(Child2(1),'Checked'),'on')==1 ) )
    %dessine les IOI
        data{i}.plotdata = plot(data{i}.ticks,data{i}.Ioi');
        %Legende                     
         legende1 = char(strcat(char(data{i}.interprete),{' '},...
            char(data{i}.instrument),...
            {' (Moy : '},num2str(roundn(1/mean(data{i}.data'),2)),{','},...
            {' Dev : '},num2str(roundn(1/std(data{i}.data'),2)),{','},...
            {' Med : '},num2str(roundn(median(data{i}.data'),2)),{')'}));
     end
                 data{i}.deja_trace =1;

        %dessine la moyenne (deux cas)
        if(strcmp(get(sousmenu{4}(1),'Checked'),'on')==1)
            calculer(i,1);
            if(strcmp(get(Child(1),'Checked'),'on')==1)
             data{i}.plotmoyenne = stairs(data{i}.ticks(1:length(data{i}.moyenne))...
                 ,data{i}.moyenne);
%                      Pour effacer les courbes pendant qu'on trace les
%                      moyennes (JUSTINE)
                     if( ishandle(data{i}.plotdata))
%                        delete(data{i}.plotdata);
                     end
             else
             data{i}.plotmoyenne = stairs(data{i}.ticks(1:length(data{i}.moyenneIoi))...
                 ,data{i}.moyenneIoi);

            end
        end
                

                %regles lesproprietes des lignes 
                if(ishandle(data{i}.plotmoyenne))
                legende = char(strcat('Mean ',{' '},char(data{i}.interprete),{' '},...
                    char(data{i}.instrument)));
                set(data{i}.plotmoyenne,...
                'Parent',axes1,...
                'Color',data{i}.couleur,...
                'MarkerFaceColor',data{i}.couleur,...
                'MarkerSize',1, ...             
                'LineStyle',data{i}.ligne,...
                'LineWidth',2,...
                'Marker',data{i}.marqueur,...
                'DisplayName',legende);
                end
                if(ishandle(data{i}.plotmoyenne2))
                legende = char(strcat('Moyenne2 ',{' '},char(data{i}.interprete),{' '},...
                    char(data{i}.instrument)));                
                set(data{i}.plotmoyenne2,...
                'Parent',axes1,...
                'Color',data{i}.couleur,...
                'LineStyle',data{i}.ligne,...
                'LineWidth',0.9,...
                'Marker',data{i}.marqueur,...
                'MarkerSize',1.5, ... 
                'MarkerFaceColor',data{i}.couleur,...            
                'DisplayName',legende);
                end
                if(ishandle(data{i}.plotdata))

                set(data{i}.plotdata,...
                'Parent',axes1,...
                'Color',data{i}.couleur,...
                'LineStyle','-',...%data{i}.ligne,...
                'LineWidth',1,...%'Marker',data{i}.marqueur,...'MarkerSize',data{i}.taille, ...'MarkerFaceColor',data{i}.couleur,...
                'DisplayName',legende1);
            
                %On affiche un gros point si c'est le d�but d'une
                %mesure
                 if(strcmp(get(Child(1),'Checked'),'on')==1)
                 v = data{i}.data(1:20:end)';
                 v2=data{i}.data(1:5:end)';
                 c = [1:1:mesures];
                 c2 = [1:0.25:mesures];
                 else
                 v = data{i}.Ioi(1:20:end)';
                 v2=data{i}.Ioi(1:5:end)';
                 c = [1:1:mesures];
                 c2 = [1:0.25:mesures];
                 end
                %il y avait 40 avant Justine!
               data{i}.premiers_temps = scatter(c,v,40,...
                    data{i}.couleur,data{i}.marqueur,'fill'); 
                data{i}.sec_temps = scatter(c2,v2,20,...
                    data{i}.couleur,data{i}.marqueur,'fill');
                hAnnotation = get(data{i}.premiers_temps,'Annotation');
                hLegendEntry = get(hAnnotation','LegendInformation');
                set(hLegendEntry,'IconDisplayStyle','off');
                hAnnotation2 = get(data{i}.sec_temps,'Annotation');
                hLegendEntry2 = get(hAnnotation2','LegendInformation');
                set(hLegendEntry2,'IconDisplayStyle','off');
                end

            end
        

    end
            %ylim('auto');
            legend(axes1,'off');
            legend(axes1,'show');
            %hold(axes1,'all');
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    function clickseb(hObject, EventData, handles)
    
     %% DECLARATIONS
        global axes1 data liste M mesures signature;
        
     %% RECUPERATION DU LABEL DU SOUS MENU CLIQU�
        menuclique = get(get(hObject,'Parent'),'Label');
        
     %% ACTION POUR LE SOUS MENU ET CHANGEMENT D'�TAT DU SOUS MENU 
     %(CHECK/UNCHECK)
        if(strcmp(get(hObject,'Checked'),'on')==1)
            
                set(hObject,'Checked','off');
                if(strcmp(menuclique,'Interpretes')==1)
                    for i=1 :length(data)
                        if(strcmp(data{i}.interprete,...
                                get(hObject,'Label'))==1)
                            data{i}.checked = 'off';
                            break
                        end
                    end
                    trace();
                end
                if(strcmp(get(hObject,'Label'),'Tempo')==1)
                set(hObject,'Checked','off');
                    trace();
                end
                if(strcmp(menuclique,'BPM/IOI')==1)
                    set(hObject,'Checked','on');
                    
                end
        else
            
                set(hObject,'Checked','on');
                if(strcmp(get(hObject,'Label'),'Tempo')==1)
                set(hObject,'Checked','on');
                    trace();
                end
                if(strcmp(menuclique,'Interpretes')==1)
                    for i=1 :length(data)
                        if(strcmp(data{i}.interprete,...
                                get(hObject,'Label'))==1)
                            data{i}.checked = 'on';
                            break
                        end
                    end

                trace();
                end
                if(strcmp(get(hObject,'Label'),'Moyenne')==1) 
                    trace();
                end
                if(strcmp(get(hObject,'Label'),'Boite a moustaches')==1)
                    traceboxplot();
                end
                if(strcmp(get(hObject,'Label'),'Limites')==1)
                        set(hObject,'Checked','off');
                             temp = inputdlg;
                             Redo_lims(temp);
                    trace();
                end
                if(strcmp(menuclique,'BPM/IOI')==1)
                    if(strcmp(get(hObject,'Label'),'BPM')==1)
                        temp = get(get(hObject,'Parent'),'Children');
                        set(temp(2,1),'Checked','off');
                        ylabel(axes1, 'Tempo en BPM');
                        trace();
                         
                    else (strcmp(get(hObject,'Label'),'IOI')==1)
                       temp = get(get(hObject,'Parent'),'Children');
                        set(temp(1,1),'Checked','off');
                        ylabel(axes1, 'Inter-beat interval (IBI) in ms');
                        trace();
                    end
                end
        end
    end
    
    function calculer(indice,type)
      
        %% D�CLARATIONS
        global data axes1 signature mesures ticks M;
         
        %% CALCUL DES MOYENNES
        
        %CAS 1 : Moyenne simple
        if(type==1)
        dat = data{indice}.data';
        datIoi = data{indice}.Ioi';
            for j =1:(length(M.lim)-1)
            % R�CUP�RATION DES LIMITES
            limtemp1  = M.lim(j);
            limtemp2 = (M.lim(j+1));
            limites = [find(abs((data{indice}.ticks-limtemp1))<0.00000001),...
                 find(abs((data{indice}.ticks-limtemp2))<0.00000001)-1];

             data{indice}.moyenne(limites(1): limites(2)) = ...
                mean(dat(limites(1):5: limites(2)));

            data{indice}.moyenneIoi(limites(1): limites(2)) = ...
                mean(datIoi(limites(1):5: limites(2)));
                

              
            end
        end
           
        % CAS 2 : Moyenne liss�e
        if(type==2)

            for i =1:(length(temp)-1)
                % R�CUP�RATION DES LIMITES DU ZOOM
                limites = [find(get(axes1,'XTick')==temp(i)),find(get(axes1,'XTick')==temp(i+1))];
                if(limites(1)<=0)
                    limites(1)=1;
                end
                if(limites(end)>length(data{1}.data))
                    limites(2)=length(data{1}.data);
                end
                
                data{i}.moyenneinst(limites(1): limites(2)) = ...
                    smooth(data{i}.data(limites(1): limites(2)));
                
                data{i}.moyenneinstIoi(limites(1): limites(2)) = ...
                    smooth(data{i}.Ioi(limites(1): limites(2)));
                
                data{i}.max(limites(1): limites(2)) = ...
                    max(data{i}.data(limites(1): limites(2)));
                
                data{i}.min(limites(1): limites(2)) ...
                    = min(data{i}.data(limites(1): limites(2)));
              
            end
        end        
    end

    function drawRedo_Ticks(ticks)
    XTicks =0;
    global figure1 axes1 data signature
        temp = strsplit(ticks{1});
        
        for i = 1:length(temp)
        XTicks = [XTicks, str2num(temp{i})];
        end
        XTicks = XTicks(2:end)
           set(axes1,'Parent',figure1,...
                'YGrid','on','YLimMode','auto',...
            'XMinorTick','off','XTick',XTicks,'XTickLabel',temp,...
            'XGrid','on','TickDir','in');
        
    hold(axes1,'all');
    end
    function Redo_lims(temp)
    global M mesures
                                 M.lim =0;
                             if(isempty(temp{1}))
                                 temp = 0;
                             else
                                 temp = strsplit(temp{1});
                                 for i = 1:length(temp)
                                    M.lim = [M.lim,str2num(temp{i})];    
                                 end
                             end
                             M.lim = [M.lim(2:end),mesures];
    end
    function Redo_Ticks(hObject, EventData, handles)

        %%declarations
        temp = inputdlg;
        drawRedo_Ticks(temp)
        
    end
    
    function tic = mes2ticks(mes)
    
    global signature labels_ticks ticks
    
    splt = strsplit(mes,'.');
    
        if(length(splt)==1)
            a = str2num(splt{1});
            b = 0;
        else a = str2num(splt{1});
             b = str2num(splt{2})-1;
        end
        
        tic = ((a-1)*signature+ (b));
        
        if(tic ==-1)
            tic =0;
        end
    end
    function temp = tick2mes(tick)
    global signature
        temp1 = mod(tick,signature);
        temp2 = floor(tick/signature)+1;
        
        temp = strcat(num2str(temp2),'.',num2str(temp1));
    
    end
    
    function draw_Re_Zoom(entree)
    global figure1 axes1 data signature ticks labels_ticks;
        temp = strsplit(entree{1});
        
        set(axes1,'XLim',[str2num(temp{1}) str2num(temp{2})]);
        zoom('on')
        %zoom reset;
    end
    function Re_Zoom(hObject, EventData, handles)
          %%declarations
        global figure1 axes1 data signature;
        temp = inputdlg;
        draw_Re_Zoom(temp);
    end
    
    function Imprimer(hObject, EventData, handles)
    global data axes1 liste menu sousmenu signature;
    temp = get(axes1,'XLim');
    nom = '';
    for i=1:length(data)
        if(strcmp(data{i}.checked,'on')==1)
            nom = strcat(nom,data{i}.interprete,'-');
        end
    end
     if(strcmp(get(sousmenu{4}(1),'Checked'),'on')==1)
    nom = strcat('Moy_',nom);
    end
     if(strcmp(get(sousmenu{4}(2),'Checked'),'on')==1)
    nom = strcat('Liss_',nom);
    end
     Child = get(menu{9},'Children');
     Child2 = get(menu{4},'Children');
    if(strcmp(get(Child(2),'Checked'),'on')==1 &&...
        (strcmp(get(Child2(1),'Checked'),'on')==1 ))
         nom = strcat('IOI_',nom);
    elseif (strcmp(get(Child(2),'Checked'),'on')==1 &&...
        (strcmp(get(Child2(1),'Checked'),'on')==1 ))
    nom = strcat('BPM_',nom);
    else
    end
    

   nom = strcat(nom ,num2str(num2str(temp(1))),'-',...
            num2str(temp(end)));
    Sprint(nom);
    end
   
    function autoPrintInitiate(hObject, EventData, handles)
    
    global sousmenu M menu
    %%
    %clickseb(sousmenu{4}(1)); %IOI
    %clickseb(sousmenu{2}(1)); %MOYENNES
    tcksSylvain= {'1 5 8 10.5 14 15 19 22 24 28 29 35 39 40 46 50 51'};

%     ZoomJustine1 = {'1.01 8.07'};   
%     ZoomJustine2 = {'9.01 13.01'};
%     ZoomJustine3 = {'25.01 32.01'};
%     ZoomJustine4 = {'2.01 2.12'};
%     ZoomJustine5 = {'4.01 4.12'};
%     ZoomJustine6 = {'12.01 13.01'};
%     ZoomJustine7 = {'26.01 28.7'};
%     ZoomJustine8 = {'29.01 32.01'};
%     
%     tcksJustine1= {'1.01 1.04 1.07 1.10 2.01 2.04 2.07 2.10 3.01 3.04 3.07 3.10 4.01 4.04 4.07 4.10 5.01 5.04 5.07 5.10 6.01 6.04 6.07 6.10 7.01 7.04 7.07 7.10 8.01 8.04 8.07 8.10'};
%     tcksJustine2= {'9.01         9.04         9.07          9.10        10.01        10.04        10.07         10.10        11.01        11.04        11.07         11.10        12.01        12.04        12.07         12.10        13.01        13.04        13.07         13.10'};
%     tcksJustine3= {'25.01        25.04        25.07         25.10        26.01        26.04        26.07         26.10        27.01        27.04        27.07         27.10        28.01        28.04        28.07         28.10        29.01        29.04        29.07         29.10        30.01        30.04        30.07         30.10        31.01        31.04        31.07         31.10        32.01        32.04        32.07         32.10'};
%     tcksJustine4= {'2.01        2.02        2.03        2.04        2.05        2.06        2.07        2.08        2.09         2.10        2.11        2.12'}
%     tcksJustine5= {'4.01        4.02        4.03        4.04        4.05        4.06        4.07        4.08        4.09         4.10        4.11        4.12'};
%     tcksJustine6= {'12.01        12.02        12.03        12.04        12.05        12.06        12.07        12.08        12.09         12.10        12.11        12.12'};
%     tcksJustine7= {'26.01 26.02 26.03 26.04 26.05 26.06 26.07        26.08        26.09         26.10        26.11        26.12        27.01        27.02        27.03        27.04        27.05        27.06        27.07 27.08 27.09 27.10 27.11 27.12 28.01 28.02 28.03 28.04 28.05 28.06 28.07 28.08 28.09'};
%     tcksJustine8= {'29.01        29.02        29.03        29.04        29.05        29.06        29.07        29.08        29.09         29.10        29.11        29.12        30.01        30.02        30.03        30.04        30.05        30.06        30.07        30.08        30.09         30.10        30.11        30.12        31.01        31.02        31.03        31.04        31.05        31.06        31.07        31.08        31.09         31.10        31.11        31.12        32.01        32.02        32.03        32.04        32.05        32.06        32.07        32.08        32.09         32.10        32.11        32.12'};
%     
    %% Justine1
%     clickseb(sousmenu{2}(1));
%     clickseb(sousmenu{4}(1)); %MOYENNES
% 
%     draw_Re_Zoom(ZoomJustine3);
%     drawRedo_Ticks(ZoomJustine3);
%         clickseb(sousmenu{4}(1)); %MOYENNES
%         drawnow
%         clickseb(sousmenu{4}(1)); %MOYENNES
%         drawnow
%     clickseb(sousmenu{1}(3));%interprete
%     clickseb(sousmenu{1}(10));%interprete
% 
%     drawRedo_Ticks(tcksJustine3);
%     Imprimer();
%     clickseb(sousmenu{1}(3));%interprete
%     clickseb(sousmenu{1}(10));%interprete
%     
%     %%%%%%%%%%%%
%     draw_Re_Zoom(ZoomJustine3);
%     drawRedo_Ticks(ZoomJustine3);
%         clickseb(sousmenu{4}(1)); %MOYENNES
%         drawnow
%         clickseb(sousmenu{4}(1)); %MOYENNES
%         drawnow
%     clickseb(sousmenu{1}(7));%interprete
%     clickseb(sousmenu{1}(9));%interprete
%     clickseb(sousmenu{1}(5));%interprete
%     clickseb(sousmenu{1}(2));%interprete
% 
%     drawRedo_Ticks(tcksJustine3);
%     Imprimer();
%     clickseb(sousmenu{1}(7));%interprete
%     clickseb(sousmenu{1}(9));%interprete
%     clickseb(sousmenu{1}(5));%interprete
%     clickseb(sousmenu{1}(2));%interprete
%         %%%%%%%%%%%%
%     draw_Re_Zoom(ZoomJustine3);
%     drawRedo_Ticks(ZoomJustine3);
%         clickseb(sousmenu{4}(1)); %MOYENNES
%         drawnow
%         clickseb(sousmenu{4}(1)); %MOYENNES
%         drawnow
%     clickseb(sousmenu{1}(6));%interprete
%     clickseb(sousmenu{1}(2));%interprete
% 
% 
%     drawRedo_Ticks(tcksJustine3);
%     Imprimer();
%     clickseb(sousmenu{1}(6));%interprete
%     clickseb(sousmenu{1}(2));%interprete

    %% justine2
%     clickseb(sousmenu{2}(1));
%     clickseb(sousmenu{4}(1)); %MOYENNES
%         for i=1:length(sousmenu{1})
% 
%                 
% 
%                 draw_Re_Zoom(ZoomJustine3);
%                 drawRedo_Ticks(ZoomJustine3);
%                     clickseb(sousmenu{4}(1)); %MOYENNES
%                     drawnow
%                     clickseb(sousmenu{4}(1)); %MOYENNES
%                     drawnow
%                 clickseb(sousmenu{1}(i));%interpretes
% 
%                 drawRedo_Ticks(tcksJustine3);
%                 Imprimer();
%                 clickseb(sousmenu{1}(i));%interpretes
%             
%         end
%     clickseb(sousmenu{1}(i));%on d�clique celui en cours pour passer � 
%                              %un autre
    
    %% Justine3
%     clickseb(sousmenu{2}(1));
%     draw_Re_Zoom(ZoomJustine7);
%     drawRedo_Ticks(ZoomJustine7);
%     drawnow
%     for i=1:length(sousmenu{1}+1)
%         clickseb(sousmenu{1}(i));%interpretes
%     end
%         drawRedo_Ticks(tcksJustine7);
%         Imprimer();
%         %clickseb(sousmenu{1}(i));%interpretes
%         
   %% Sylvain
     Redo_lims(tcksSylvain);
    set(gca,'YLim',[0.0006,0.0025]);
    autoPrint();
    draw_Re_Zoom({'1 15'});
    autoPrint();
    draw_Re_Zoom({'15 29'});
    autoPrint();
    draw_Re_Zoom({'29 40'});
    autoPrint();
    draw_Re_Zoom({'40 51'});

%     clickseb(sousmenu{4}(1));%Moyenne ON !
%     clickseb(sousmenu{3}(1));%BPM ON!
%     Redo_lims(tcksSylvain);
%     set(gca,'YLim',[0.0006,0.0025]);
%     draw_Re_Zoom({'1 51'});
%     Imprimer();
%     draw_Re_Zoom({'1 15'});
%     Imprimer();
%     draw_Re_Zoom({'15 29'});
%     Imprimer();
%     draw_Re_Zoom({'29 40'});
%     Imprimer();
%     draw_Re_Zoom({'40 51'});
%     Imprimer();
    end
    
    function autoPrint()
        global sousmenu;
    
    for i=1:length(sousmenu{1})
        clickseb(sousmenu{1}(i));%interpretes
%                 set(gca,'YLim',[25,100]);
%                 clickseb(sousmenu{3}(2));%BPM
%                 Imprimer();
                %clickseb(sousmenu{3}(1));%BPM
                
                Imprimer();
    clickseb(sousmenu{1}(i));%on d�clique celui en cours pour passer � 
                             %un autre
    end
    
    
    
    
    
        
    end
    
    function Ioi(i)
    global data signature;

    data{i}.Ioi = 60 ./(data{i}.data*1000);
    data{i}.Ioi(1:5) = 0;
    end
    
    function difference(i)
    global data signature sumi suma;
    
       if(data{i}.version =='MG')
        for j = 1:length(data) 
           if(strcmp(data{j}.interprete,data{i}.interprete) && strcmp(data{j}.version , 'MD'))
            MG = reshape(data{i}.Ioi',1,signature*size(data{1}.data,1));
            MD = reshape(data{j}.Ioi',1,signature*size(data{1}.data,1));
                    suma  = 0;
        sumi = 0;
        for j =1: length(MG)
            suma     = [suma,sum(MG(1:j))];
            sumi    = [sumi,sum(MD(1:j))];
        end
        
        %nosub = [2,3,4,8,9,11,14,17,20,21,23,26,27,29,32,33,35,36,44,45,46,47,50,51,52,57,58,68,69,71,74,75,80,81,83,84,88,92,93,94];
        nosub = [];
        for k = 1:length(suma)
            if(sum(ismember(nosub,k)))
                data{i}.Ioi(k) = 0;
            else
                data{i}.Ioi(k) = sumi(k)-suma(k);
            end
        end
        
           end
        end
        data{i}.Ioi = data{i}.Ioi(1:end);
        data{i}.Ioi = data{i}.Ioi(1:end-1);
        end
    end
    
    function traceboxplot()
    global data sousmenu vecteur_version labels_ticks
    compteur =1;
    moyboxplot = zeros(length(data{1}.data),1);
    
    for i = 1:length(data)    
        if(strcmp(get(sousmenu{1}(i),'Checked'),'on')==1)
            delete(data{i}.plotdata);
            delete(data{i}.premiers_temps);
            delete(data{i}.sec_temps);
            moyboxplot = moyboxplot+ data{i}.data;
            compteur = compteur+1;
        end
    end
    moyboxplot = moyboxplot/(compteur-1);
    
    vectbox = zeros(1,length(data{1}.data));
    for i = 1 :3: length(data{1}.data)
        if((i)<length(data{1}.data))
            vectbox(i:i+3) = i;

        end
    end
    pos = 1:3:length(data{1}.data)-1;
    lab = labels_ticks(1:3:end);
    plotbox = boxplot(moyboxplot',vectbox,'positions',pos,...
        'labels',lab(1:length(pos)),...
        'plotstyle','traditional');
    end