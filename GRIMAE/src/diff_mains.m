function [diff,g,G] = diff_mains(diff,signature,nom)

SAUT = 6
%diff = MD - MG;

[a,V,v]=cree_vecteur(1,8.07,signature,0.01);

g = plot(v,diff);
G = get(g,'Parent');

set(G,'XTick',v(1:SAUT:end));
set(G,'XTickLabel',V(1:SAUT:end));
set(G,'XGrid','on');
set(g,'Marker','o');
legend(g,nom);
xlabel(G,'Mesures');
ylabel(G,'Temps en secondes');

ptx = diff(1:signature:end);
pty = v(1:signature:end);
hold on;
scatter(pty,ptx,60,get(g,'color'),'d','fill');
end