%retourne un vecteur en double de taille d�but-fin qui est sous la forme de
%mesures avec 'mesure' beats, avance d'intervalle en intervalle
%
%b = vecteur en double
%c = vecteur en char
%z = vecteur d'axe X pour le plot

function [b,c,z] =cree_vecteur(debut, fin, mesure,intervalle)

if(fin-fix(fin)== 0)
fin = fin+1;
end

a = [0.01:intervalle:mesure/100];
z = [0:1/mesure:fin];

b = a+debut;

for i = debut+1:(fin-1)
   
    b = [b,a+i];
    
end

r = fin-fix(fin);
ind = find(abs(a-r)<0.0001);
b = [b,fix(fin)+a(1:ind)];

z=z(1:size(b,2));
c = num2str(b);

pat = '(\d.1) ';
c=regexprep(c, pat, '$10');
c
c= strsplit(c,' ');
end