%[verif ]= verifie_( FICHIER , X,Y)
%Prends un fichier FICHIER en entr�e, un nombre de mesures X, et un nombre
%de temps par mesures 
%et cherche les erreurs engendr�es par le patch max4live de r�cup�ration des timings.
%Les vides sont combl�s avec la moyenne des valeurs directement pr�cedente
%et directement suivante.
function [verif ]= verifie_(fichier,nb_mes,beat_mes)

handle = fopen(fichier);
i=1;
vect1 = [];
vect2 = [];
while(~feof(handle))
   temp = fgets(handle);
   [u,d]=strtok(temp,',; ');
   [t]=strtok(d,',; ');
   
   
   vect1 = [vect1;str2double(u)];
   vect2 = [vect2;str2double(t)];
   i = i+1;
   
end

fclose(handle);
delete(fichier);
z=1;
verif = [];
for i=1:nb_mes
   for j=1:beat_mes
    verif = [verif;i*10+j];
    z = z+1;
   end
   
end


nouveau = zeros(length(verif),1);
nouveau2 = zeros(length(verif),1);

for i = 1:length(nouveau)
   temp = find(vect1==verif(i));
    if(~isempty(temp))
        nouveau(i) = vect1(temp);
        nouveau2(i) = vect2(temp);
    else
        nouveau(i) = verif(i);
        
   end
end

ind = find(nouveau2==0);
for i=1:length(ind)
    if(ind(i)>1)
        if(ind(i)~=length(nouveau) && ind(i)~=1)
            nouveau2(ind(i)) = (nouveau2(ind(i)+1)+nouveau2(ind(i)-1))/2;
        end
    end
end
verif = [verif,nouveau,nouveau2];
f = fopen(strrep(fichier,'.txt','_v2.txt'),'w');
for i=1:length(nouveau2)
fprintf(f,'%f\r\n',nouveau2(i));
end
fclose all;
