 %dessine(X, REP) 
 %
 %Cr�e une interface graphique permettant d'afficher les tempos(contenus 
 %dans les fichiers textes pr�sents dans le repertoire REP) selon des mesures 
 %contenant X temps . Ces fichiers peuvent �tre obtenus en identifiant 
 %la position des temps dans Ableton live avec le patch GRIMAE.
 %Les fichiers doivent �tre nomm�s comme suit :
 %
 %interprete_instrument_auteurdumarquage_numerodeversion.txt
 
    function dessine(inputsignature,repertoire)  
          
    %% DECLARATIONS

    %initialisations globales
    close all;
    clear global;
    warning('OFF', 'MATLAB:legend:PlotEmpty');
    
    %declarations
    global  data  figure1 axes1 liste vecteur_interprete vecteur_version...
        vecteur_instrument calcul signature data_figure menu vect;
    signature = inputsignature;
    figure1  = figure('Name','Etude de timing','Visible','off','Color',...
        [1,1,1],'Position',[0,0,1000,900],'NumberTitle','off','Resize','on');

    %% INITIALISATION
    % On va loader les fichiers, cr�er des listes pour les menus, cr�er les
    % menus et la toolbar
    
    %on load les fichiers
    cd(repertoire);
    fichiers = dir('*.txt');
    nbfichiers = length(fichiers);
    %COULEURS
        %colormap(hsv); %pour reset la map de couleurs
    %couleurs = hsv(nbfichiers+1);
    couleurs = rgbconv('66c2a5');
    couleurs = [couleurs;rgbconv('fc8d62')];
    couleurs = [couleurs;rgbconv('8da0cb')];
    couleurs = [couleurs;rgbconv('e78ac3')];
    rgb2hsv(couleurs);
        %couleurs = rand(length(fichiers), 3);
    marqueurs = 'oo<*sdv^>ph+.';
    lignes = cellstr(['- ';'- ';'-.';': ';'--']);
    
    %une liste pour savoir quel instrument/version/interpretes sont cliqu�s
    liste.interprete = '';
    liste.version = '';
    liste.instrument = '';
    liste.calcul{1} =0;
    liste.calcul{2} =0;
    tempmarqueur = 1;
    tempcouleur = 1;
    templigne = 1;
    
    %initialisation de la structure
    for i=1:nbfichiers 
        temp= strrep(fichiers(i).name,'.txt','');
        
        [data{i}.interprete,temp]=strtok(temp,'_');
        [data{i}.instrument,temp]=strtok(temp,'_');
        [data{i}.version,temp]=strtok(temp,'_');
       
        data{i}.deja_trace = 0;
        data{i}.data = importdata(fichiers(i).name);
        data{i}.plotdata = 'rien';
        data{i}.plotmax = 'rien';
        data{i}.plotmin = 'rien';
        data{i}.plotmoyenne = 'rien';
        data{i}.plotmoyenne2 = 'rien';
        data{i}.moyennecalculok = 0;
        data{i}.moyenne = data{i}.data;
        data{i}.moyenneinst = data{i}.data;
        data{i}.min = data{i}.data;
        data{i}.max = data{i}.data;
        data{i}.premiers_temps = 'rien';
    %data{i}.interprete = strcat(data{i}.interprete,'--' ,data{i}.instrument);
        
        for j=1:length(data)
        if(strcmp(data{i}.interprete,data{j}.interprete) ==1 && j~=i)
            data{i}.couleur = data{j}.couleur; break;
        else data{i}.couleur = couleurs(mod(tempcouleur,nbfichiers+1)+1,:);
        end
        end
        
        for j=1:length(data)
        if(strcmp(data{i}.version,data{j}.version) ==1 && j~=i)
            data{i}.ligne = data{j}.ligne; break;
        else data{i}.ligne = lignes{mod(templigne,4)+1}; 
        end
        end
        
        for j=1:length(data)
        if(strcmp(data{i}.instrument,data{j}.instrument) ==1 && j~=i)
            data{i}.marqueur = data{j}.marqueur;break;
        else data{i}.marqueur = marqueurs(mod(tempmarqueur,10)+1);
        end
        end
        templigne       =   templigne+1;
        tempcouleur     =   tempcouleur+1;
        tempmarqueur    =   tempmarqueur+1;
        
    end
    
    cd ..;

    %% Initialisation de la toolbar
    set(figure1,'Toolbar', 'figure');
    hOpen = findall(figure1, 'tooltipstring', 'Open File');
    hOpen2 = findall(figure1, 'tooltipstring', 'New Figure');
    hOpen3 = ...
        findall(figure1, 'tooltipstring', 'Show Plot Tools and Dock Figure'); 
    hOpen4 = findall(figure1, 'tooltipstring', 'Link Plot');   
    hOpen5 = findall(figure1, 'tooltipstring', 'Rotate 3D');
    hOpen6 = findall(figure1, 'tooltipstring', 'Edit Plot');
    hOpen7 = findall(figure1, 'tooltipstring', 'Hide Plot Tools');
    hOpen8 = findall(figure1, 'tooltipstring', 'Insert Colorbar');
    delete(hOpen4);
    delete(hOpen5);
    delete(hOpen);
    delete(hOpen2);
    delete(hOpen3);
    delete(hOpen6);
    delete(hOpen7);
    delete(hOpen8);
    
    %% Initialisation du menu
    set(figure1,'MenuBar','none');
    menu = uimenu(figure1,'Label','Interpretes');
    menu2 = uimenu(figure1,'Label','Versions');
    menu4 = uimenu(figure1,'Label','Type de calcul');
    menu6 = uimenu(figure1,'Label','Mesures','Callback',{@Redo_Ticks});
    menu7 = uimenu(figure1,'Label','Zoom','Callback',{@Re_Zoom});
    menu8 = uimenu(figure1,'Label','Imprimer','Callback',{@Imprimer});
    marq_couleurs = 1;
    marq_marqueur = 1;
    marq_ligne = 1;
    vecteur_interprete = [];
    vecteur_version = [];
    vecteur_instrument = [];
    
    %on va cr�er des vecteurs avec les interpretes pour faire les menuus
    temp = '';
    temp2 = [];  
    for i=1 : length(data)
        if(~strcmp(vecteur_interprete,data{i}.interprete))
            vecteur_interprete{length(vecteur_interprete)+1} =...
                data{i}.interprete;
        end       
    end
    
    %on va cr�er des vecteurs avec les instruments pour faire les menus 
    temp = '';
    temp2 = [];
    for i=1 : length(data)
        temp = strfind(vecteur_instrument,data{i}.instrument);
        for j=1:length(temp) 
            temp2 = find(temp{j});
            if(temp2==1)
                break;
            end
        end
        if(isempty(temp2))
            vecteur_instrument{length(vecteur_instrument)+1}=...
                data{i}.instrument;
        end   
    end
    
    %on va cr�er des vecteurs avec les versions pour faire les menus
    temp = '';
    temp2 = [];
    for i=1 : length(data)
        temp = strfind(vecteur_version,data{i}.version);
        for j=1:length(temp)
            temp2 = find(temp{j});
            if(temp2==1)
                break;
            end
        end
        if(isempty(temp2))
            vecteur_version{length(vecteur_version)+1} =...
                data{i}.version;
        end   
    end

    %On fait les menus
    for i=1:length(vecteur_interprete)
    sousmenu{1} = uimenu(menu,'Label',vecteur_interprete{i},'Callback',...
         {@clickseb},'Checked','off');
    end
    for i=1:length(vecteur_version)
    sousmenu2{1} = uimenu(menu2,'Label',vecteur_version{i},'Callback',...
         {@clickseb},'Checked','off');
    end
%      for i=1:length(vecteur_instrument)
%     sousmenu3{1} = uimenu(menu3,'Label',vecteur_instrument{i},'Callback',...
%          {@clickseb},'Checked','off');
%      end
    sousmenu4{1} = uimenu(menu4,'Label','Moyenne','Callback',...
         {@clickseb},'Checked','off');
    sousmenu4{2} = uimenu(menu4,'Label','Moyenne instantann�e','Callback',...
         {@clickseb},'Checked','off');
    
    %% On r�gle les axes de la figure.
    %g = get(figure1);
    ticks = [1.0 : 10 :((length(data{1}.data)/signature+2))+0.1];
    labels_ticks =  num2str(ticks);
    vect = 1:(1/signature):length(data{1}.data(:,1))/...
                        signature+(1-1/signature);
    labels_ticks =     str2num(strrep(num2str(labels_ticks),'.0','.1')); 
    labels_ticks =     str2num(strrep(num2str(labels_ticks),'.25','.2 '));
    labels_ticks =     str2num(strrep(num2str(labels_ticks),'.50','.3 '));
    labels_ticks =     str2num(strrep(num2str(labels_ticks),'.75','.4 '));
    set(figure1,'Units','normalized','Visible','on');
            axes1 = axes('Parent',figure1,...
                'YGrid','on','YLimMode','auto',...
            'XMinorTick','off','XTick',ticks,'XTickLabel',labels_ticks,...
            'XGrid','on','TickDir','in','XLim',[1,...
            length(data{1}.data)/signature+2]);
    ylabel(axes1, 'Tempo en BPM');
    xlabel(axes1, 'Mesures');
    hold(axes1,'all');
    
    %initialisation de l'impression
    set(gcf,'PaperPositionMode', 'manual', 'PaperUnits','centimeters',...
    'PaperOrientation', 'LandScape', 'PaperSize',[25 20],...
    'Paperposition',[1 1 25 15]);
    end
    
    function trace(z)

    %% DECLARATIONS
    global data  figure1 axes1 liste signature vect;
    
    %% RESET AFFICHAGE (on efface tout).
    for i = 1 :length(data)
           if( ishandle(data{i}.plotdata))
               delete(data{i}.plotdata);

           end
            if(ishandle(data{i}.plotmax))
               delete(data{i}.plotmax);
            end
            if(ishandle(data{i}.plotmin))
               delete(data{i}.plotmin);
            end
            if(ishandle(data{i}.plotmoyenne))
               delete(data{i}.plotmoyenne);
            end
            if(ishandle(data{i}.plotmoyenne2))
               delete(data{i}.plotmoyenne2);
            end
            if(ishandle(data{i}.premiers_temps))
                delete(data{i}.premiers_temps);
            end
    end
   
    %% TRACAGE
    % On retrace ce qui est s�l�ctionn� dans les menus seulement
    for i = 1 : length(data)
        for j = 1 :length(liste.interprete)
        for k = 1:length(liste.version)
            if((strcmp(data{i}.interprete ,liste.interprete{j})==1) &&...
                    (strcmp(data{i}.version ,liste.version{k})==1) )

                %regle les axes
                vect = 1:(1/signature):length(data{i}.data(:,1))/...
                    signature+(1-1/signature);

                %dessine les donn�es
                 data{i}.plotdata = plot(vect,data{i}.data);
                 data{i}.deja_trace =1;

                %dessine la moyenne (deux cas)
                if(liste.calcul{1}==1)
                    calculer(i,1);
                     data{i}.plotmoyenne = plot(vect,data{i}.moyenne);
                end
                if(liste.calcul{2}==1)
                    calculer(i,2);
                  vect2  = 1:0.01:length(data{i}.data(:,1))/signature+...
                    (1-1/signature);
                 test = interp1(vect,data{i}.moyenneinst,vect2,'spline');
                    data{i}.plotmoyenne2 = plot(vect2,test);
                end

                %regles lesproprietes des lignes 
                if(ishandle(data{i}.plotmax))
                legende2 = char(strcat('Max ',{' '},char(data{i}.interprete),{' '},...
                    char(data{i}.version),{' '},char(data{i}.instrument)));
                    set(data{i}.plotmax,...
                'Parent',axes1,...
                'Color',data{i}.couleur,...
                'MarkerFaceColor',data{i}.couleur,...
                'MarkerSize',3, ... %RAJOUT�            
                'LineStyle',data{i}.ligne,...
                'LineWidth',1,...'Marker',data{i}.marqueur,...
                'DisplayName',legende2);
                end
                if(ishandle(data{i}.plotmin))
                set(data{i}.plotmin,...
                'Parent',axes1,...
                'Color',data{i}.couleur,...            
                'MarkerFaceColor',data{i}.couleur,...
                'MarkerSize',3, ... %RAJOUT�
                'LineStyle',data{i}.ligne,...
                'LineWidth',1,...'Marker',data{i}.marqueur,...
                'DisplayName',strcat('Min',char(data{i}.interprete),'/',...
                    char(data{i}.version),'/',char(data{i}.instrument)));
                end
                if(ishandle(data{i}.plotmoyenne))
                legende = char(strcat('Moyenne ',{' '},char(data{i}.interprete),{' '},...
                    char(data{i}.version),{' '},char(data{i}.instrument)));
                set(data{i}.plotmoyenne,...
                'Parent',axes1,...
                'Color',data{i}.couleur,...
                'MarkerFaceColor',data{i}.couleur,...
                'MarkerSize',1, ...             
                'LineStyle',data{i}.ligne,...
                'LineWidth',2,...
                'Marker',data{i}.marqueur,...
                'DisplayName',legende);
                end
                if(ishandle(data{i}.plotmoyenne2))
                legende = char(strcat('Moyenne2 ',{' '},char(data{i}.interprete),{' '},...
                    char(data{i}.version),{' '},char(data{i}.instrument)));                
                set(data{i}.plotmoyenne2,...
                'Parent',axes1,...
                'Color',data{i}.couleur,...
                'LineStyle',data{i}.ligne,...
                'LineWidth',0.9,...
                'Marker',data{i}.marqueur,...
                'MarkerSize',1.5, ... 
                'MarkerFaceColor',data{i}.couleur,...            
                'DisplayName',legende);
                end
                if(ishandle(data{i}.plotdata))
                legende1 = char(strcat(char(data{i}.interprete),{' '},...
                    char(data{i}.version),{' '},char(data{i}.instrument),...
                    {' (Moy : '},num2str(roundn(mean(data{i}.data),-1)),{','},...
                    {' Dev : '},num2str(roundn(std(data{i}.data),-1)),{','},...
                    {' Med : '},num2str(roundn(median(data{i}.data),-1)),{')'}));
                set(data{i}.plotdata,...
                'Parent',axes1,...
                'Color',data{i}.couleur,...
                'LineStyle',data{i}.ligne,...
                'LineWidth',1,...
                'Marker',data{i}.marqueur,...
                'MarkerSize',2, ... 
                'MarkerFaceColor',data{i}.couleur,...
                'DisplayName',legende1);

                %On affiche un gros point si c'est le d�but d'une
                %mesure
                v = data{i}.data(1:4:end);
                c = [1:4:length(data{i}.data)];
                data{i}.premiers_temps = scatter((c/4)+0.75,v,40,...
                    data{i}.couleur,'d','fill');
                hAnnotation = get(data{i}.premiers_temps,'Annotation');
                hLegendEntry = get(hAnnotation','LegendInformation');
                set(hLegendEntry,'IconDisplayStyle','off');
                end

            end
        end
        end

    end
                    %ylim('auto');
            legend(axes1,'off');
            legend(axes1,'show');
            hold(axes1,'all');
    end
    
    function clickseb(hObject, EventData, handles)
    
     %% DECLARATIONS
        global axes1 data liste;
        
     %% RECUPERATION DU LABEL DU SOUS MENU CLIQU�
        menuclique = get(get(hObject,'Parent'),'Label');
        
     %% ACTION POUR LE SOUS MENU ET CHANGEMENT D'�TAT DU SOUS MENU 
     %(CHECK/UNCHECK)
        if(strcmp(get(hObject,'Checked'),'on')==1)
            
                set(hObject,'Checked','off');
                
                if(strcmp(menuclique,'Interpretes')==1)
                    for i=1 :length(liste.interprete)
                        if(strcmp(liste.interprete{i},...
                                get(hObject,'Label'))==1)
                            liste.interprete(i) = '';
                            break
                        end
                    end
                    trace();
                end
                if(strcmp(menuclique,'Versions')==1)
                    for i=1 :length(liste.version)
                        if(strcmp(liste.version{i},...
                                get(hObject,'Label'))==1)
                            liste.version(i) = '';
                            break
                        end
                    end
                    trace();
                end
                if(strcmp(menuclique,'Instruments')==1)
                    for i=1 :length(liste.instrument)
                        if(strcmp(liste.instrument{i},...
                                get(hObject,'Label'))==1)
                            liste.instrument(i) = '';
                            break
                        end
                    end
                    trace();
                end
                if(strcmp(menuclique,'Type de calcul')==1)
                    if(strcmp(get(hObject,'Label'),'Moyenne')==1)
                    liste.calcul{1} =0;end
                    if(strcmp(get(hObject,'Label'),'Moyenne instantann�e')==1)
                    liste.calcul{2} =0;end
                    trace();
                end
        else
            
                set(hObject,'Checked','on')
                if(strcmp(menuclique,'Interpretes')==1)
                    liste.interprete{length(liste.interprete)+1} =...
                        get(hObject,'Label');

                trace();
                end
                if(strcmp(menuclique,'Versions')==1)
                   liste.version{length(liste.version)+1} =...
                       get(hObject,'Label');
                   trace();
                end
                if(strcmp(menuclique,'Instruments')==1)
                    liste.instrument{length(liste.instrument)+1} =...
                        get(hObject,'Label');
                    trace();
                end
                if(strcmp(menuclique,'Type de calcul')==1)
                    if(strcmp(get(hObject,'Label'),'Moyenne')==1)
                    liste.calcul{1} =1;end
                    trace();
                    if(strcmp(get(hObject,'Label'),'Moyenne instantann�e')==1)
                    liste.calcul{2} =1;end
                    trace();
                end
        end
    end
    
    function calculer(indices,type)
      
        %% D�CLARATIONS
        global data axes1 vect;
        
         temp = get(axes1,'XTick');
        %% CALCUL DES MOYENNES
        %CAS 1 : Moyenne simple
        if(type==1)
        for j = 1 : length(indices)
            for i =1:(length(temp)-1)
                % R�CUP�RATION DES LIMITES DU ZOOM
                limites = [find(vect==temp(i)),find(vect==temp(i+1))-1];
                %if(limites(1)<=0)limites(1)=1;end
                %if(limites(end)>length(data{1}.data))
                %    limites(2)=length(data{1}.data);
                %end
                
                data{indices(j)}.moyenne(limites(1): limites(2)) = ...
                    mean(data{indices(j)}.data(limites(1): limites(2)));
                
                data{indices(j)}.max(limites(1): limites(2)) = ...
                    max(data{indices(j)}.data(limites(1): limites(2)));
                
                data{indices(j)}.min(limites(1): limites(2)) ...
                    = min(data{indices(j)}.data(limites(1): limites(2)));
              
            end
        end
        end
           
        % CAS 2 : Moyenne liss�e
        if(type==2)

            for i =1:(length(temp)-1)
                % R�CUP�RATION DES LIMITES DU ZOOM
                limites = [find(vect==temp(i)),find(vect==temp(i+1))];
                if(limites(1)<=0)
                    limites(1)=1;
                end
                if(limites(end)>length(data{1}.data))
                    limites(2)=length(data{1}.data);
                end
                
                data{indices}.moyenneinst(limites(1): limites(2)) = ...
                    smooth(data{indices}.data(limites(1): limites(2)));
                
                data{indices}.max(limites(1): limites(2)) = ...
                    max(data{indices}.data(limites(1): limites(2)));
                
                data{indices}.min(limites(1): limites(2)) ...
                    = min(data{indices}.data(limites(1): limites(2)));
              
            end
        end        
    end
    
    function Redo_Ticks(hObject, EventData, handles)

        %%declarations

        global figure1 axes1 data signature
        temp = inputdlg;
        temp2 = str2num(temp{1});
        temp3 = strsplit(temp{1});
        
        %% FAIRE EN FONCTION DE LA SIGNATURE !!!!!!!!!!
        temp2 =     str2num(strrep(num2str(temp2),'.1','')); 
        temp2 =     str2num(strrep(num2str(temp2),'.2','.25 '));
        temp2 =     str2num(strrep(num2str(temp2),'.3','.5 '));
        temp2 =     str2num(strrep(num2str(temp2),'.4','.75 '));
        
        for i = 1:size(temp2,2)
            if(round(temp2(i))==temp2(i))
            temp3{i}    = num2str(temp2(i)+0.1);
            end
        end
    
    %set(figure1,'Units','normalized','Visible','on');
            set(axes1,'Parent',figure1,...
                'YGrid','on','YLimMode','auto',...
            'XMinorTick','off','XTick',temp2,'XTickLabel',temp3,...
            'XGrid','on','TickDir','in');
        
    ylabel(axes1, 'Tempo en BPM');
    hold(axes1,'all');
    end
    
    function Re_Zoom(hObject, EventData, handles)
          %%declarations
        global figure1 axes1 data signature
        temp = inputdlg;
        temp2 = str2num(temp{1});
        temp2 =     str2num(strrep(num2str(temp2),'.1','')); 
        temp2 =     str2num(strrep(num2str(temp2),'.2','.25 '));
        temp2 =     str2num(strrep(num2str(temp2),'.3','.5 '));
        temp2 =     str2num(strrep(num2str(temp2),'.4','.75 '));
        
        set(axes1,'XLim',[temp2(1) temp2(2)]);
        zoom('on')
        zoom reset;
    end
    
    function Imprimer(hObject, EventData, handles)
    global data vect axes1 liste;
    temp = get(axes1,'XLim');
    nom = strcat(liste.interprete{:},num2str(temp(1)),'-',num2str(temp(end)));
    if(liste.calcul{1})
    nom = strcat('Moy_',nom);
    end
    if(liste.calcul{2})
    nom = strcat('Liss_',nom);
    end
    Sprint(nom);
    end
    