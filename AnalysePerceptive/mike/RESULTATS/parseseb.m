function [out] = parseseb(rep)

mat = struct;
if(isdir(rep))
    fichiers = dir(strcat(rep,'/*.txt'));

    i = 1;
    for i=1:size(fichiers)
        nom = regexp(fichiers(i).name,'\w+','match');
        bloc = traitement(fichiers(i).name);
        mat = setfield(mat,char(nom(1)),bloc);
    end
else out = traitement(rep);
end

mat = statseb(mat);
out = mat;

end

function out = traitement(rep)
pat = '"REC(?<REC>\d\d\d)\s(?<heuu>\w\w)\sp(?<piece>\d\d)\s(?<timbre>\w+)\.wav"\s(?<reponse>\w+)';
file = fopen(rep,'r');
mat = [];
test = struct;

tline = fgetl(file);
out = tline;
i = 0;
ligne = 1;
while( ischar( tline))
    i= regexp(tline,pat,'names');
    mat = setfield(mat,strcat('ecran',num2str(ligne)),i);
    tline = fgetl(file);
    ligne = ligne+1;
end


fclose(file);
out = mat;
end

function out = statseb(mat)

i =  1;
j =1;
bigbonus = 0;
test = fieldnames(mat);
a = {'sec','brillant','sombre','veloute', 'rond'};

for i =1 : size(test)
    matrice = zeros(5,5);
    ecran = fieldnames(mat.(char(test(i))));
    for j = 1:size(ecran)

        for k = 1:5

    matrice(strmatch(mat.(char(test(i) )).(char(ecran(j) ))(1,k).timbre,...
        a,'exact'),strmatch(mat.(char(test(i) )).(char(ecran(j) ))(1,k).reponse,a,'exact')) = ...
        matrice(strmatch(mat.(char(test(i) )).(char(ecran(j) ))(1,k).timbre,...
        a,'exact'),strmatch(mat.(char(test(i) )).(char(ecran(j) ))(1,k).reponse,a,'exact'))+1;

        end
    end
    matriceend = dataset({matrice(:,1),'sec'},{matrice(:,2),'brillant'},...
        {matrice(:,3),'sombre'},{matrice(:,4),'rond'},{matrice(:,5),'veloute'},...
        'obsnames',a);
    mat.( char(test(i))) = setfield(mat.( char(test(i))),'bonus',matriceend);
end

out = mat;
end
