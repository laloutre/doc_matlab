function [out reps] = PerceptionResults(list,varargin)

% Input: Text file of answers
% Options:      'Pieces' for piece labels (and number thereof)
%               'PieceNum' for number of pieces
%               'Pianists' for pianists names (and number thereof)
%               'PianistNum' for number of pianists
%               'Pages' for number of pages
%               'Timbres' for timbre labels (and number thereof)
%               'TimbreNum' for number of timbres
%
%
% Output structure with fields:
%       Pieces (12*1 vector)
%       Pianists (12*1 vector)
%       Confusion matrices (5*5*12) (targets*answers*pages)
%
%       Answers (12*5) (pages*timbre targets, timbre answered)
%       FA (12*5) (pages*timbres answered, timbre target)
%       Scores (12*5, binary) (pages*timbre targets, right or wrong)
%       ScoresPerPage (12*1) (sum of scores)
%
%       PieceScores (4*5)
%       PianistScores (3*5)
%       TotalScores (5*1) (sum of scores, per timbre target)
%


%%%%%%% PARAMETERS %%%%%%%

cpued = 0;
np = 12; % Number of pages
ntim = 5; % Number of timbres
npiece = 4; % Number of pieces
npianist = 3; % Number of pianists

timbres = {'sec','brillant','rond','sombre','veloute'};
pianistnames = {'RB','BB','FP'};
piecenames = {'p01','p02','p03','p04'};


%%%%%%% OPTIONS %%%%%%%
ivar=1;
while ivar<=length(varargin),
    if strcmp(varargin{ivar},'Pieces') && ivar<length(varargin),
        piecenames = varargin{ivar+1};
        npiece = length(piecenames);
        ivar = ivar+1;
    elseif strcmp(varargin{ivar},'PieceNum') && ivar<length(varargin),
        npiece = varargin{ivar+1};
        ivar = ivar+1;
    elseif strcmp(varargin{ivar},'Pianists') && ivar<length(varargin),
        pianistnames = varargin{ivar+1};
        npianist = length(pianistnames);
        ivar = ivar+1;
    elseif strcmp(varargin{ivar},'PianistNum') && ivar<length(varargin),
        npianist = varargin{ivar+1};
        ivar = ivar+1;
    elseif strcmp(varargin{ivar},'Pages') && ivar<length(varargin),
        np = varargin{ivar+1};
    elseif strcmp(varargin{ivar},'Timbres') && ivar<length(varargin),
        timbres = varargin{ivar+1};
        ntim = length(timbres);
        ivar = ivar+1;
    elseif strcmp(varargin{ivar},'TimbreNum') && ivar<length(varargin),
        ntim = varargin{ivar+1};
        ivar = ivar+1;
    end
    ivar = ivar+1;
end




%%%%%%% TEXT PARSER %%%%%%%

%%%% ini %%%
pieces = zeros(np,1);
pianists = zeros(np,1);
confu = zeros(ntim,ntim,np);
reps = zeros(np,ntim);

%%% Results in %%%
fid=fopen(list);
pos=1; nf=0;
while(~feof(fid)),
    pages(pos)=textscan(fid,'%s',1,'Delimiter','\n');
    if ~isempty(pages{pos}), nf=nf+1; end % nf: number of lines
    pos=pos+1;
end
fclose(fid);

if nf~=np, disp('Warning: the number of lines in the input text does not correspond to the number of pages/tests intended; wrong input?'); end

for i=1:nf,
    page = cell2mat(pages{i});
    guil = find(page=='"');
    if length(guil)~= 2*ntim, fprintf('\n Warning: missing information on line %d of input file.\n',i);
    else
        
        % Pianist
        for k=1:length(pianistnames),
            if strcmp(page(guil(1)+8:guil(1)+7+length(pianistnames{k})),pianistnames{k}), pianists(i,1)=k; end
        end
        
        % Piece
        for k=1:length(piecenames),
            if strcmp( page( guil(1)+7+length(pianistnames{pianists(i,1)})+2 : guil(1)+7+length(pianistnames{pianists(i,1)})+1+length(piecenames{k}) ) , piecenames{k}), pieces(i,1)=k; end
        end
        
        if pianists(i,1)==0 || pieces(i,1)==0
            if pianists(i,1)==0, fprintf('\n Something went wrong on line %d; could note identify the pianist.\n',i); end
            if pieces(i,1)==0, fprintf('\n Something went wrong on line %d; could note identify the piece.\n',i); end
        else
            
            for t=1:ntim,
                % Check pianist
                if ~strcmp(pianistnames{pianists(i,1)} , page(guil(2*(t-1)+1)+8:guil(2*(t-1)+1)+7+length(pianistnames{pianists(i,1)})) ),
                    fprintf('\n Warning!: mismatch in pianists names on the same line (line %d); fail!\n',i);
                    
                elseif ~strcmp(piecenames{pieces(i,1)} , page( guil(2*(t-1)+1)+7+length(pianistnames{pianists(i,1)})+2 : guil(2*(t-1)+1)+7+length(pianistnames{pianists(i,1)})+1+length(piecenames{pieces(i,1)}))),
                    fprintf('\n Warning!: mismatch in piece names on the same line (line %d); fail!\n',i);
                else
                    
                    timstart = guil(2*(t-1)+1)+7+length(pianistnames{pianists(i,1)})+1+length(piecenames{pieces(i,1)})+2;
                    
                    for k=1:length(timbres), 
                        if strcmp(timbres{k} , page(timstart:timstart-1+length(timbres{k}))), ranki = k; end % Target, row in confusion, col. in answer
                        if strcmp( timbres{k} , page( guil(2*t)+2 : min(guil(2*t)+1+length(timbres{k}),length(page)) ) ), rankj = k; end % Answer, column in confusion, answer in answer
                    end
                    
                    if ranki && rankj, 
                        confu(ranki,rankj,i) = 1;
                        reps(i,ranki) = rankj;
                        cpued=1;
                    end
                end
            end
        end
    end
end





%%%%%%% OUTPUT %%%%%%%

if cpued,
    
    %%% ini %%%
    % confu = zeros(ntim,ntim,np);
    scores = zeros(np,ntim);
    fa = zeros(np,ntim);

    piecescores = zeros(npiece,ntim);
    pianistscores = zeros(npianist,ntim);
    
    
    %%% Affectation %%%
    
    % % Confusion matrices
    % for p=1:np,
    %     for i=1:ntim, confu(i,reps(i,p),p) = 1; end
    % end
    
    % Scores from answers
    for p=1:np,
        for i=1:ntim,
            if reps(p,i) == i, scores(p,i) = 1; end
        end
    end
    
    % FA
    for p=1:np,
        for i=1:ntim, fa(p,reps(p,i)) = i; end
    end
    
    % Per piece
    for p=1:npiece,
        piecescores(p,:) = sum(scores(pieces==p,:));
    end
    
    
    
    
    % Per pianist
    for p=1:npianist,
        pianistscores(p,:) = sum(scores(pianists==p,:));
    end
    
    
    
    
    %%% Build struct %%%
    
%     out = struct('Timbres',timbres,...
%         'PieceLabels',piecenames,...
%         'Pieces', pieces,...
%         'PianistsNames',pianistnames,...
%         'Pianists', pianists,...
%         'Confusion',confu,...
%         'Answers',reps,...
%         'FA',fa,...
%         'Scores',scores,...
%         'ScoresPerPage',sum(scores,2),...
%         'PieceScores',piecescores,...
%         'PianistScores',pianistscores,...
%         'TotalScores',sum(scores));
    
    
    out.Timbres = timbres;
    out.PieceNames = piecenames;
    out.Pieces = pieces;
    out.PianistNames = pianistnames;
    out.Pianists = pianists;
    out.Confusion = confu;
    out.Answers = reps;
    out.FA = fa;
    out.Scores = scores;
    out.ScoresPerPage = sum(scores,2);
    out.PieceScores = piecescores;
    out.PianistScores = pianistscores;
    out.ScoresPerTimbre = sum(scores);
    out.TotalConfusion = sum(confu,3);
    out.TotalScore = sum(sum(scores));
    
else out = []; reps = [];
end


