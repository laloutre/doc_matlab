% ================================================================
% Full report
% ================================================================
function M = matchReport(M)

realIndN = [];
realIndP = [];

% Step 1. Create chord-based match (Gin -> realGIp)
% =================================================
for nc = 1:size(M.GIn,1)    % for each notated chord
  ii = find(M.indN  == nc);
  if ~isempty(ii)                          
    pc = find(M.realGIp(:,1) == M.indP(ii(1)));  % find performed chord that matches
    if ~isempty(pc) % redundant, ii would be empty if there wasn't a match
      realIndN = [realIndN; nc];
      realIndP = [realIndP; pc];
    end
  end
end

realGIn = M.GIn;
cmatch = [realIndN, realIndP];

% Step 2. Match notes of each chord
% =================================
M.M = [];
nprev = 0;
pprev = 0;
cm = 1;

while cm <= size(cmatch,1)    % for each group (chord) in notation
  m=zeros(1,11);
  nc = cmatch(cm, 1);
  pc = cmatch(cm, 2);

  nn = M.GIn    (nc, find(M.GIn    (nc,:)));
  pp = M.realGIp(pc, find(M.realGIp(pc,:)));

  if     nn(1) > nprev+1; % if the chord match skipped a notated note
    nn = nprev+1;         % don't advance cm, do nest time through
    nc = nc - 1;
    pp = [];
    nprev = nn;
    nm = [1];
  elseif pp(1) > pprev+1 % if the chord match skipped a performed note
    nn = [];              % don't advance cm, do nest time through
    nc = NaN;
    pp = pprev+1;
    pprev = pp;
    nm = NaN;
  else                    % otherwise move on with chord match
    nm = noteMatch(M.Nn(nn,4)', M.Np(pp,4)');
    nprev = nn(end);
    pprev = pp(end);
    cm = cm + 1;
  end

  % Write chord match information into array
  for n = 1:max(length(nn),length(pp))   % list out the chord
    if ~isnan(nm(n)) & nm(n) <= length(nn) 
      m([2 4 6 8 10]) = [nc, M.Nn(nn(nm(n)), [4 1 5 2])];
    else
      m([2 4 6 8 10]) = [nc, NaN, NaN, NaN, NaN];
    end
    if n <= length(pp)
      m([3 5 7 9 11]) = [pp(n), M.Np(pp(n), [4 6 5 7])];
    else
      m([3 5 7 9 11]) = NaN;
    end
    M.M = [M.M; m];
  end

end

% Step 2. Determine: m, s, a, or d
% =================================
M.M = num2cell(M.M);
for nn = 1:length(M.M)
  if     M.M{nn,4} == M.M{nn,5}
    M.M{nn,1} = 'M';
  elseif isnan(M.M{nn,4})
    M.M{nn,1} = 'add';
  elseif isnan(M.M{nn,5})
    M.M{nn,1} = 'del';
  else
    M.M{nn,1} = 'sub';
  end
end

% =====================================================================
function nn = noteMatch(N, P)
N = N(:)';
P = P(:)';

diffl = length(N)-length(P);
if     diffl > 0
  P = [P, 100+zeros(1, diffl)];
elseif diffl < 0
  N = [N, NaN+zeros(1,-diffl)];
end

nn = []; Pnot = [];
for pp = 1:length(P)
  n = find(N == P(pp));
  if ~isempty(n)
    N(n) = NaN;
    Pnot = [Pnot,NaN];
    nn = [nn,n];
  else
    Pnot = [Pnot,P(pp)];
    nn = [nn, NaN];
  end
end

for pp = 1:length(Pnot)
  n = find((abs(N-Pnot(pp)) ==  min(abs(N-Pnot(pp)))), 1, 'first');
  if ~isempty(n)
    N(n)  = NaN;
    nn(pp) = n;
  end
end
