function ts = tscore(GTp, GTn, i, j, tempoRatio)

if j < size(GTn,1)
   ioiN = GTn(j+1,1)-GTn(j,1);		% normal case
else
   ioiN = GTn(j,1)-GTn(j-1,1);		% last chord
end

if i > 1
   ioiP = GTp(i,1)-GTp(i-1,1);		% normal case
else
   ioiP = 0.000;			% first note
end

ts = scorec(ioiP, ioiN/4);		% 3 also works well
