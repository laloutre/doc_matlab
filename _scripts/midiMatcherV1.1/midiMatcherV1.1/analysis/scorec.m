function c = cscore(ioi, thresh)

c = round(14*(1-sigmoid(ioi, thresh, thresh/5)))-7;