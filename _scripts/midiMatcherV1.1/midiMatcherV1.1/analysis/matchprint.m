function [] = matchprint(CTi, CPi, indI, CT, CP, indP)
%MATCHPRINT
% Prints the matching produced by match to the window in a more-or-less
% readable form.
%
% See also: MATCH, MATCHFIGURE
%
fid=fopen('musicout','w');
for k = 1:length(indP)
	
   i = indP(k);
   j = indI(k);
   fprintf(1,' %2d, %6.3f; %2d, %6.3f : ', i, CT(i), j, CTi(j)); 
   fprintf(fid,' %2d, %6.3f; %2d, %6.3f : ', i, CT(i), j, CTi(j)); 
   fprintf(1,' %d ', sort(nonzeros(CP(i,:)))'); 
   fprintf(fid,' %d ', sort(nonzeros(CP(i,:)))');
   fprintf(1, ' <--> ');
   fprintf(fid, ' <--> ');
   fprintf(1,' %d ', sort(nonzeros(CPi(j,:)))'); fprintf(1, '\n'); 
   fprintf(fid,' %d ', sort(nonzeros(CPi(j,:)))'); fprintf(fid, '\n');

end

status=fclose(fid);
i = indP(k);
j = indI(k);
%
%fprintf(' %d, %6.3f; %d, %6.3f : ', i, CT(i), j, CTi(j)); 
%fprintf(' %d ', sort(nonzeros(CP(i,:)))'); 
%fprintf(1, ' <--> ');
%fprintf(' %d ', sort(nonzeros(CPi(j,:)))'); fprintf(1, '\n'); 

