function [GIp, Tn, Bn, Tp, Bp, tempoMap, periodMap, velocMap] = realGroup(M)

GIp = M.GIp;

[Mn Mi] = unique(M.indN);
Mi = [1; 1+Mi(1:length(Mi)-1)]';

Bn = M.Nn(M.GIn(:,1),1);
Bn = Bn(M.indN(Mi));
Tn = M.Nn(M.GIn(:,1),6);
Tn = Tn(M.indN(Mi));

mm = 0;
for nn = unique(M.indN)
   mm = mm+1;
   ii = find(M.indN == nn);
   GIp(mm, 1:length(ii)) = M.indP(ii);
end

GIp = GIp(1:mm,:);
Tp = zeros(size(GIp,1),1);
velocMap = zeros(size(GIp,1),1);

for pp = 1:size(GIp,1)
   ii = GIp(pp,find(GIp(pp,:) ~= 0));
   Tp(pp) = mean(M.Np(ii,6));
   velocMap(pp) = mean(M.Np(ii,5));
end 
Bp = Bn;

tempoMap  = diff(Bn) ./ diff(Tp); tempoMap  = [tempoMap(1);  tempoMap ];
periodMap = diff(Tp) ./ diff(Bn); periodMap = [periodMap(1); periodMap];

% the old way
% TnScaled = Tn * (Tp(length(Tp))/Tn(length(Tn)));
%tempoMap = [1; diff(Tp) ./ diff(TnScaled)];

