function [pts, matched] = score(I, P)
%SCORE(I, P)
%

lnI = length(nonzeros(I));
lnP = length(nonzeros(P));
pts = 0;
matched = 0;

if find(I == P(1))
   pts = 10;
else
   pts = 3;
end
matched = P(1);
