function y = signmoid(x, theta, T)

if nargin < 3
   T = 1;
end
if nargin < 2
   theta = 0;
end


y = 1 ./ (1 + exp(-(x - theta) / T));