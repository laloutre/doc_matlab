nmat = readmidi('Files/bachNotation.mid');
pmat = readmidi('Files/bachPerformance.mid');

M = dynamicmatch(pmat, nmat, 'Goldberg Variations Aria');
gmw('create', M);
