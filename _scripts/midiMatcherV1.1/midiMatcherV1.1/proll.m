function [PAT] = proll(N, tunit, c, fill)
%[PAT] = proll(N, color, fill)
%
% Plots a note matrix of the type returned by scoread. PROLL returns
% a column vector of handles to patch objects. Fill (optional)
% specifies whether or not patches are drawn filled. C, if specified,
% determines the fill color, otherwise colors denote velocity.


%---------------------
% Parse calling syntax
%---------------------
if nargin < 2; tunit    = 'beat'; end;
if nargin < 3; c        = '';     end;
if nargin < 4; fill     =  1;     end;

t = onset(N, tunit); d = dur (N, 'sec'); p = pitch(N); a = floor(velocity(N)/2)+1;

% -------------------------------------
% Try to behave like the 'plot' command
% -------------------------------------
state = get(gca, 'NextPlot');
if state(1:3) == 'rep'
   delete(get(gca, 'Children'));
end

% ----------------------------
% Draw one patch for each note
% ----------------------------
if nargout > 0
  PAT = [];
end
h = flipud(hot);
for i= 1:length(N(:,1))
   x = [t(i) t(i) t(i)+d(i) t(i)+d(i)];
   y = [p(i)-.5 p(i)+.5 p(i)+.5 p(i)-.5];

   if ~isempty(c); color = c; else; 
       color = h(a(i), :);
   end;
   if fill
      fcolor = color;
      ecolor = 'k';
   else
      fcolor = 'none';
      ecolor = 'k';
   end

   if color == 't';
      % df = [0; diff(round(1000*N(:,1)))];
      pat= text(t(i), p(i), num2str(i), 'FontSize', 7);
   else
      fn = sprintf('lightUp(%d)', i);
      pat= patch('Xdata', x, 'YData', y, ...
	    'FaceColor', fcolor, 'EdgeColor', ecolor, 'ButtonDownFcn', fn);
   end

   if nargout > 0
      PAT = [PAT; pat];
   end
end

colormap(flipud(hot));
