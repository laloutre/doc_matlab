function [M] = dynamicmatch(pmat, nmat, name, offdiag);
% Wrapper function for the matcher. It reads the files,
% runs the matcher, groups the performance according to
% the optimal match, and returns the necessary information 
% in a structure

if nargin < 4
    offdiag = 80;
end

if nargin < 3
    M.title = '';
else
    M.title = (name);
end
M.Nn = nmat;
M.Np = pmat;

% Calculate the match
max_notes = 10; % maximum number of notes in a chord
[M.GIn M.GIp M.indN M.indP tbl pscr tscr] = matchsum(M.Nn, M.Np, max_notes, offdiag);

% Then present the results in nice formats
% 1. The match table
mtbl = zeros(length(M.GIp), length(M.GIn));
for ii = 1:length(M.indP);
  mtbl(M.indP(ii), M.indN(ii)) = pscr(M.indP(ii), M.indN(ii));
end
M.mtbl = sparse(mtbl);

% 2.Group the performance into chords, and calculate the maps
[M.realGIp M.Tn M.Bn M.Tp M.Bp M.tempoMap M.periodMap, M.velocMap] = realGroup(M);

% 3. The note matrix for the match
M.Nm = [];
m=0;
for ip = 1:length(M.indP)
    n = M.indP(ip);
    c = M.indN(ip);
    m = m+1;
    M.Nm = [M.Nm; M.Np(n,:)];
    M.Nm(m,1:2) = M.Nn(M.GIn(c,1),1:2); % doesn't set the duration (beats)
                                        % right for every note (only for 
                                        % the first)
end

% 4. The full report, with matches, substitutions, additions and deletions
M = matchReport(M);
