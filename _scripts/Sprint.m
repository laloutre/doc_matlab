    %script pour r�gler l'impression et imprimer correctement.
	%usage Sprint(num,Nom_figure,extention)
    function Sprint(num,Nom_figure,extention)

    set(figure(num),'PaperPositionMode', 'manual', 'PaperUnits','centimeters',...
    'PaperOrientation', 'LandScape', 'PaperSize',[27 17],...
    'Paperposition',[1 1 25 15]);
%saveas(gcf, 'test', 'pdf') %Save figure
%print;
%print(gcf,'filename','-dpdf','-r0')
set(gcf,'units','normalized','outerposition',[0 0 1 1])
if(isdir('figures'))
    cd figures;
    export_fig(strcat(Nom_figure,extention));
    cd ..;

else
    mkdir figures;
    cd figures;
    export_fig(strcat(Nom_figure,extention));
    cd ..;
end

