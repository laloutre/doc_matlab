function [dat ,signature] = getwarpmarkers(fichier,sig)

f = fopen(fichier);
a = fread(f);
a=a';
fclose(f);
pos = strfind(a,'WarpMarker');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%signaturen = dec2hex(a((pos(2)-49):-1:(pos(2)-52)));
%signaturen = reshape(signaturen',1,...
%    size(signaturen',1)*size(signaturen',2));
%signaturen = hex2dec(signaturen);
%sign = bitget(signaturen,32);                            
%exponent = bitget(signaturen,24:31)*2.^(0:7).';           
%fraction = bitget(signaturen,1:23)*2.^(-23:-1).';         
%signaturen = (-1)^sign*(1+fraction)*2^(exponent-127);  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% signatured = dec2hex(a((pos(2)-45):-1:(pos(2)-48)));
% signatured = reshape(signatured',1,...
%     size(signatured',1)*size(signatured',2));
% signatured = hex2dec(signatured);
% sign = bitget(signatured,32);                            
% exponent = bitget(signatured,24:31)*2.^(0:7).';           
% fraction = bitget(signatured,1:23)*2.^(-23:-1).';         
% signatured = (-1)^sign*(1+fraction)*2^(exponent-127);

signature = 4%strcat(num2str(signaturen),'/',num2str(signatured));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
temps = 0;
mesures = 0;
for i=2:length(pos)-1
temps =[temps; hexget(a,10,4,8,pos(i))];

mesures= [mesures;hexget(a,10,12,8,pos(i))];
end
temps = temps(2:end);
mesures = mesures(2:end);

%%%%%%%%%%%%%Fin du parsing%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dat = dataset;
dat.No_mesure = (mesures./sig)+1;
dat.No_tic= mesures;
dat.temps_ms = temps;

delta = 0;
deltatps =0;
for i=2:length(dat)
delta = [delta;dat.temps_ms(i)-dat.temps_ms(i-1)];
deltatps = [deltatps;dat.No_tic(i)-dat.No_tic(i-1)];
end

tempo = (deltatps./delta).*60;
tempo(isnan(tempo)) = 0 ;
dat.tempo = tempo;
dat.signature = signature;
end