function create_all(list,initials,varargin)

% list = list of raw files' names; names must be standardized: RECXXX MM-DD HHmm JD pYY timbre.raw
% Create:
%  - notetimes
%  - pianoroll figs
%  - notes xls
%  - chords mats & xls
%  - chords.only + chords.notes mats & xls
%  - pianoroll.chords figs

if nargin<2, initials = list; initials = initials(length(initials)-5:length(initials)-4);
end


nt=1; proll=1; nowtes=1; chwords=1; chwordsep=1;prollch=1; chwordmean=1;

for i=1:length(varargin)
    if strcmp(varargin(i),'none'), nt=0; proll=0; nowtes=0; chwords=0; chwordsep=0;prollch=0; chwordmean=0;
    elseif strcmp(varargin(i),'all'), nt=1; proll=1; nowtes=1; chwords=1; chwordsep=1;prollch=1; chwordmean=1;
    elseif strcmp(varargin(i),'nt'), nt=1;
    elseif strcmp(varargin(i),'nont'), nt=0;
    elseif strcmp(varargin(i),'proll') || strcmp(varargin(i),'pianoroll'), proll=1;
    elseif strcmp(varargin(i),'noproll') || strcmp(varargin(i),'nopianoroll'), proll=0;
    elseif strcmp(varargin(i),'notes'), nowtes=1;
    elseif strcmp(varargin(i),'nonotes'), nowtes=0;
    elseif strcmp(varargin(i),'chords'), chwords=1;
    elseif strcmp(varargin(i),'nochords'), chwords=0;
    elseif strcmp(varargin(i),'chordsep'), chwordsep=1;
    elseif strcmp(varargin(i),'nochordsep'), chwordsep=0;
    elseif strcmp(varargin(i),'prollch') || strcmp(varargin(i),'prollchords') || strcmp(varargin(i),'pianorollch') || strcmp(varargin(i),'pianorollchords'), prollch=1;
    elseif strcmp(varargin(i),'noprollch') || strcmp(varargin(i),'noprollchords') || strcmp(varargin(i),'nopianorollch') || strcmp(varargin(i),'nopianorollchords'), prollch=0;
    elseif strcmp(varargin(i),'chordsmeans'), chwordmean=1;
    elseif strcmp(varargin(i),'nochordsmeans'), chwordmean=0;
    end
end
if ~chwords, prollch=0; chwordmean=0; end


% --- raw in ---
fid=fopen(list);
pos=1; nf=0;
while(~feof(fid)),
    files(pos)=textscan(fid,'%s',1,'Delimiter','\n');
    if ~isempty(files{pos}), nf=nf+1; end % nf: number of files
    pos=pos+1;
end
fclose(fid);

if chwordmean,
    npar = 46;
    npar2 = npar + 10;
    chpar = 56;
    
    chall = zeros(nf,2*chpar+4*npar2);
    chleft = zeros(nf,2*chpar+4*npar2);
    chright=zeros(nf,2*chpar+4*npar2);
    chdiff=zeros(nf,2*chpar+4*npar2);
end

% Directories tree
mkdir(initials);
cd(initials);
mkdir('MATs');
if nt, mkdir('notetimes'); end
if proll, mkdir('pianorolls'); end
if nowtes, mkdir('notes'); end
if chwords, mkdir('chords'); end
if chwordsep, mkdir('chords.only'); mkdir('chords.notes'); end
if prollch, mkdir('pianorolls.chords'); end


for i=1:nf
    disp(i)
    rawname = cell2mat(files{i});
    nameof = [rawname(1:7),rawname(19:length(rawname)-4)];
    
    % NOTETIMES
    if nt,
        cd('notetimes');
        ntmat = notetime(rawname);
        ntmatname=['nt' rawname(4:6)];
        eval([ntmatname '=ntmat;']);
        ntname = ['nt ' nameof];
        cd('..');
    else
        cd('..');
        ntname = ['nt ' nameof];
        ntmat = dlmread(ntname);
        cd(initials);
    end
    
    
    % PIANOROLLS
    if proll,
        cd('pianorolls');
        pianoroll(ntmat);
        fname = ['proll ' nameof '.fig'];
        hgsave(fname);
        cd('..');
    end
    
    % NOTES
    if nowtes,
        cd('notes');
        notesmat = notes(ntmat,'printout','nameout',ntname);
        notesmatname = ['notesperkey' rawname(4:6)];
        eval([notesmatname '=notesmat;']);
        cd('..');
    end
    
    % CHORDS
    if chwords,
        cd('chords');
        chordmat = chords(ntmat,'printout','nameout',ntname);
        chordsmatname = ['chords' rawname(4:6)];
        eval([chordsmatname '=chordmat;']);
        cd('..');
    end
    
    % CHORDS SEPARATED
    if chwordsep,
        cd('chords.only');
        [chomat chnmat] = chords(ntmat,'separate','printout','nameout',ntname);
        chomatname = ['chordsonly' rawname(4:6)];
        chnmatname = ['chordsnotes' rawname(4:6)];
        eval([chomatname '=chomat;']);
        eval([chnmatname '=chnmat;']);
        
        %         % Move 'chords.notes' to rightful folder
        %         movefile('chords.notes*','../chords.notes');
        %         % See at the end of the loop
        cd('..');
    end
    
    % PIANOROLLS WITH CHORDS
    if prollch,
        cd('pianorolls.chords');
        pianoroll(ntmat,'chords',chordmat);
        ffname = ['proll.chords ' nameof '.fig'];
        hgsave(ffname);
        cd('..');
    end
    
    % MATRIX OF CHORDS MEANS PER PERFORMANCE
    if chwordmean,
        chall(i,1)=str2double(rawname(4:6)); % Recording #
        chall(i,2:chpar) = chordmat(1,1,2:chpar); % Mean of chords features
        chall(i,chpar+1:2*chpar) = chordmat(1,4,1:chpar); % SD of chords features
        chall(i,2*chpar+1:2*chpar+npar2) = chordmat(1,2,1:npar2); % Mean of mean.per.chord of notes features
        chall(i,2*chpar+npar2+1:2*chpar+2*npar2) = chordmat(1,5,1:npar2); % SD of mean.per.chord of notes features
        chall(i,2*chpar+2*npar2+1:2*chpar+3*npar2) = chordmat(1,3,1:npar2); % Mean of SD.per.chord of notes features
        chall(i,2*chpar+3*npar2+1:2*chpar+4*npar2) = chordmat(1,6,1:npar2); % SD of SD.per.chord of notes features
        
        
        chleft(i,1)=str2double(rawname(4:6)); % Recording #
        chleft(i,2:chpar) = chordmat(2,1,2:chpar); % Mean of chords features
        chleft(i,chpar+1:2*chpar) = chordmat(2,4,1:chpar); % SD of chords features
        chleft(i,2*chpar+1:2*chpar+npar2) = chordmat(2,2,1:npar2); % Mean of mean.per.chord of notes features
        chleft(i,2*chpar+npar2+1:2*chpar+2*npar2) = chordmat(2,5,1:npar2); % SD of mean.per.chord of notes features
        chleft(i,2*chpar+2*npar2+1:2*chpar+3*npar2) = chordmat(2,3,1:npar2); % Mean of SD.per.chord of notes features
        chleft(i,2*chpar+3*npar2+1:2*chpar+4*npar2) = chordmat(2,6,1:npar2); % SD of SD.per.chord of notes features
        
        
        chright(i,1)=str2double(rawname(4:6)); % Recording #
        chright(i,2:chpar) = chordmat(3,1,2:chpar); % Mean of chords features
        chright(i,chpar+1:2*chpar) = chordmat(3,4,1:chpar); % SD of chords features
        chright(i,2*chpar+1:2*chpar+npar2) = chordmat(3,2,1:npar2); % Mean of mean.per.chord of notes features
        chright(i,2*chpar+npar2+1:2*chpar+2*npar2) = chordmat(3,5,1:npar2); % SD of mean.per.chord of notes features
        chright(i,2*chpar+2*npar2+1:2*chpar+3*npar2) = chordmat(3,3,1:npar2); % Mean of SD.per.chord of notes features
        chright(i,2*chpar+3*npar2+1:2*chpar+4*npar2) = chordmat(3,6,1:npar2); % SD of SD.per.chord of notes features
        
        
        chdiff(i,1)=str2double(rawname(4:6)); % Recording #
        chdiff(i,2:chpar) = chordmat(4,1,2:chpar); % Mean of chords features
        chdiff(i,chpar+1:2*chpar) = chordmat(4,4,1:chpar); % SD of chords features
        chdiff(i,2*chpar+1:2*chpar+npar2) = chordmat(4,2,1:npar2); % Mean of mean.per.chord of notes features
        chdiff(i,2*chpar+npar2+1:2*chpar+2*npar2) = chordmat(4,5,1:npar2); % SD of mean.per.chord of notes features
        chdiff(i,2*chpar+2*npar2+1:2*chpar+3*npar2) = chordmat(4,3,1:npar2); % Mean of SD.per.chord of notes features
        chdiff(i,2*chpar+3*npar2+1:2*chpar+4*npar2) = chordmat(4,6,1:npar2); % SD of SD.per.chord of notes features
    end
    
end

% Move 'chords.notes' to rightful folder
movefile('chords.only/chords.notes*','chords.notes');


% SAVES
cd('MATs');

clear ntmat notesmat chordmat chomat chnmat;
clear fid pos nf rawname nameof ntmatname fname notesmatname chordmatsname chomatname chnmatname ffname;
clear  ans chordsmatname chpar list npar npar2 ntname varargin;

w=whos;

% Separate saves
if chwordmean, save(['Means_' initials],'chall','chdiff','chleft','chright','files'); end


if nt, % Notetimes
    ntvars=cell(1,1);
    rank=1;
    for i=1:length(w),
        if length(w(i).name)>2 && strcmp(w(i).name(1:2),'nt'), ntvars{rank} = w(i).name; rank=rank+1; end
    end
    if ~isempty(ntvars), save(['Notetimes_' initials], ntvars{:}); end
end

if nowtes, % Notes
    notesvars=cell(1,1);
    rank=1;
    for i=1:length(w),
        if length(w(i).name)>10 && strcmp(w(i).name(1:11),'notesperkey'), notesvars{rank} = w(i).name; rank=rank+1; end
    end
    if ~isempty(notesvars), save(['Notesperkey_' initials], notesvars{:}); end
end

if chwords, % Chords
    cwhovars=cell(1,1);
    rank=1;
    for i=1:length(w),
        if length(w(i).name)>6 && strcmp(w(i).name(1:6),'chords') && ~strcmp(w(i).name(7),'o')  && ~strcmp(w(i).name(7),'n'),
            cwhovars{rank} = w(i).name;
            rank=rank+1;
        end
    end
    if ~isempty(cwhovars), save(['Chords_' initials], cwhovars{:}); end
end

if chwordsep, % Chords.only & Chords.notes
    conlyvars=cell(1,1);
    cnotesvars=cell(1,1);
    ranko=1;
    rankn=1;
    for i=1:length(w),
        if length(w(i).name)>9 && strcmp(w(i).name(1:10),'chordsonly'), conlyvars{ranko} = w(i).name; ranko=ranko+1; end
        if length(w(i).name)>10 && strcmp(w(i).name(1:11),'chordsnotes'), cnotesvars{rankn} = w(i).name; rankn=rankn+1; end
    end
    if ~isempty(conlyvars), save(['Chordsonly_' initials], conlyvars{:}); end
    if ~isempty(cnotesvars), save(['Chordsnotes_' initials], cnotesvars{:}); end
end

clear nt proll nowtes chwords chwordsep prollch cnotesvars conlyvars chwovars i notesvars ntvars rankrankn ranko w;

if ~chwordmean,
    % Save all
    clear chwordmean;
    eval(['save Performances_' initials]);
    cd('..');
    
else
    % Save all    
    clear chwordmean;
    eval(['save Performances_' initials]);
    cd('..');
    
    % ALL MEANS PRINTOUT
    mkdir('Performances');
    cd('Performances');
    
    titles={'#','Number of chords','','Chord duration','max Amax', 'max MHV','Melody lead','Melody lead rate','Last-note trail','Last-note trail rate',...
        'Soft pedal depression/chord','Soft pedal duration/chord','Soft pedal full-depression length/chord','Soft pedal mid-depression length/chord','Soft pedal mid-depression level/chord',...
        'Sustain pedal depression/chord','Sustain pedal duration/chord','Sustain pedal full-depression length/chord','Sustain pedal mid-depression length/chord','Sustain pedal mid-depression level/chord',...
        'Soft pedal @onset','Soft pedal bin. @onset','Soft pedal lead @onset','Sustain pedal @onset','Sustain pedal bin. @onset','Sustain pedal lead @onset',...
        'Soft pedal @offset','Soft pedal bin. @offset','Soft pedal trail @offset','Sustain pedal @offset','Sustain pedal bin. @offset','Sustain pedal trail @offset',...
        'IOI','OffOnI','OffOnI direction','IOI same-hand','OffOnI same-hand','OffOnI direction same-hand','IOI other-hand','OffOnI other-hand','OffOnI direction other-hand',...
        'Overlap length','Overlap length rate','Overlap amount','Overlap amount rate','Number of overlaps',...
        'Overlap length same-hand','Overlap length rate same-hand','Overlap amount same-hand','Overlap amount rate same-hand','Number of overlaps same-hand',...
        'Overlap length other-hand','Overlap length rate other-hand','Overlap amount other-hand','Overlap amount rate other-hand','Number of overlaps other-hand',...
        '','','','Chord duration  SDc','max Amax  SDc', 'max MHV  SDc','Melody lead  SDc','Melody lead rate  SDc','Last-note trail  SDc','Last-note trail rate  SDc',...
        'Soft pedal depression/chord  SDc','Soft pedal duration/chord  SDc','Soft pedal full-depression length/chord  SDc','Soft pedal mid-depression length/chord  SDc','Soft pedal mid-depression level/chord  SDc',...
        'Sustain pedal depression/chord  SDc','Sustain pedal duration/chord  SDc','Sustain pedal full-depression length/chord  SDc','Sustain pedal mid-depression length/chord  SDc','Sustain pedal mid-depression level/chord  SDc',...
        'Soft pedal @onset  SDc','Soft pedal bin. @onset  SDc','Soft pedal lead @onset  SDc','Sustain pedal @onset  SDc','Sustain pedal bin. @onset  SDc','Sustain pedal lead @onset  SDc',...
        'Soft pedal @offset  SDc','Soft pedal bin. @offset  SDc','Soft pedal trail @offset  SDc','Sustain pedal @offset  SDc','Sustain pedal bin. @offset  SDc','Sustain pedal trail @offset  SDc',...
        'IOI  SDc','OffOnI  SDc','OffOnI direction  SDc','IOI same-hand  SDc','OffOnI same-hand  SDc','OffOnI direction same-hand  SDc','IOI other-hand  SDc','OffOnI other-hand  SDc','OffOnI direction other-hand  SDc',...
        'Overlap length  SDc','Overlap length rate  SDc','Overlap amount  SDc','Overlap amount rate  SDc','Number of overlaps  SDc',...
        'Overlap length same-hand  SDc','Overlap length rate same-hand  SDc','Overlap amount same-hand  SDc','Overlap amount rate same-hand  SDc','Number of overlaps same-hand  SDc',...
        'Overlap length other-hand  SDc','Overlap length rate other-hand  SDc','Overlap amount other-hand  SDc','Overlap amount rate other-hand  SDc','Number of overlaps other-hand  SDc',...
        '','Total number of notes','Mean number of notes per chord','Note duration','Amoy','Amax','t.Amax','MHV','t.MHV',...
        'Speed of attack','Slope of attack','Speed of key attack','Slope of key attack','Directness MHV-Amax','MHV/Amax','Sharpness of key attack slope','Sharpness of attack slope',...
        'FKD start','FKD end','FKD duration','FKD rate','Pre-FKD duration','Pre-FKD rate','Post-FKD duration','Post-FKD rate',...
        't.attack.end','t.decay.end','t.release.start','Attack duration','Sustain duration','Release duration','Attack rate','Sustain rate','Release rate',...
        'Soft pedal depression', 'Sustain pedal depression', 'Soft pedal @note.onset','Sustain pedal @note.onset', 'Soft pedal @MHV', 'Sustain pedal @MHV', 'Soft pedal @note.offset', 'Sustain pedal @note.offset',...
        'Soft pedal depression length','Sustain pedal depression length','Key attack mean depression','Key attack mean depression rate',...
        'Note onset lag','Note onset lag rate','Note onset lag amount','Note onset lag amount rate','Note offset lead','Note offset lead rate','Note offset lead amount','Note offset lead amount rate','Sync rate','Sync amount rate',...
        '','','Number of notes per chord SDc','Note duration SDc','Amoy SDc','Amax SDc','t.Amax SDc','MHV SDc','t.MHV SDc',...
        'Speed of attack SDc','Slope of attack SDc','Speed of key attack SDc','Slope of key attack SDc','Directness MHV-Amax SDc','MHV/Amax SDc','Sharpness of key attack slope SDc','Sharpness of attack slope SDc',...
        'FKD start SDc','FKD end SDc','FKD duration SDc','FKD rate SDc','Pre-FKD duration SDc','Pre-FKD rate SDc','Post-FKD duration SDc','Post-FKD rate SDc',...
        't.attack.end SDc','t.decay.end SDc','t.release.start SDc','Attack duration SDc','Sustain duration SDc','Release duration SDc','Attack rate SDc','Sustain rate SDc','Release rate SDc',...
        'Soft pedal depression SDc', 'Sustain pedal depression SDc', 'Soft pedal @note.onset SDc','Sustain pedal @note.onset SDc', 'Soft pedal @MHV SDc', 'Sustain pedal @MHV SDc', 'Soft pedal @note.offset SDc', 'Sustain pedal @note.offset SDc',...
        'Soft pedal depression length SDc','Sustain pedal depression length SDc','Key attack mean depression SDc','Key attack mean depression rate SDc',...
        'Note onset lag SDc','Note onset lag rate SDc','Note onset lag amount SDc','Note onset lag amount rate SDc',...
        'Note offset lead SDc','Note offset lead rate SDc','Note offset lead amount SDc','Note offset lead amount rate SDc','Sync rate SDc','Sync amount rate SDc',...
        '','','','Note duration SDn','Amoy SDn','Amax SDn','t.Amax SDn','MHV SDn','t.MHV SDn',...
        'Speed of attack SDn','Slope of attack SDn','Speed of key attack SDn','Slope of key attack SDn','Directness MHV-Amax SDn','MHV/Amax SDn','Sharpness of key attack slope SDn','Sharpness of attack slope SDn',...
        'FKD start SDn','FKD end SDn','FKD duration SDn','FKD rate SDn','Pre-FKD duration SDn','Pre-FKD rate SDn','Post-FKD duration SDn','Post-FKD rate SDn',...
        't.attack.end SDn','t.decay.end SDn','t.release.start SDn','Attack duration SDn','Sustain duration SDn','Release duration SDn','Attack rate SDn','Sustain rate SDn','Release rate SDn',...
        'Soft pedal depression SDn', 'Sustain pedal depression SDn', 'Soft pedal @note.onset SDn','Sustain pedal @note.onset SDn', 'Soft pedal @MHV SDn', 'Sustain pedal @MHV SDn', 'Soft pedal @note.offset SDn', 'Sustain pedal @note.offset SDn',...
        'Soft pedal depression length SDn','Sustain pedal depression length SDn','Key attack mean depression SDn','Key attack mean depression rate SDn',...
        'Note onset lag SDn','Note onset lag rate SDn','Note onset lag amount SDn','Note onset lag amount rate SDn',...
        'Note offset lead SDn','Note offset lead rate SDn','Note offset lead amount SDn','Note offset lead amount rate SDn','Sync rate SDn','Sync amount rate SDn',...
        '','','','Note duration SDnxc','Amoy SDnxc','Amax SDnxc','t.Amax SDnxc','MHV SDnxc','t.MHV SDnxc',...
        'Speed of attack SDnxc','Slope of attack SDnxc','Speed of key attack SDnxc','Slope of key attack SDnxc','Directness MHV-Amax SDnxc','MHV/Amax SDnxc','Sharpness of key attack slope SDnxc','Sharpness of attack slope SDnxc',...
        'FKD start SDnxc','FKD end SDnxc','FKD duration SDnxc','FKD rate SDnxc','Pre-FKD duration SDnxc','Pre-FKD rate SDnxc','Post-FKD duration SDnxc','Post-FKD rate SDnxc',...
        't.attack.end SDnxc','t.decay.end SDnxc','t.release.start SDnxc','Attack duration SDnxc','Sustain duration SDnxc','Release duration SDnxc','Attack rate SDnxc','Sustain rate SDnxc','Release rate SDnxc',...
        'Soft pedal depression SDnxc', 'Sustain pedal depression SDnxc', 'Soft pedal @note.onset SDnxc','Sustain pedal @note.onset SDnxc',...
        'Soft pedal @MHV SDnxc', 'Sustain pedal @MHV SDnxc', 'Soft pedal @note.offset SDnxc', 'Sustain pedal @note.offset SDnxc',...
        'Soft pedal depression length SDnxc','Sustain pedal depression length SDnxc','Key attack mean depression SDnxc','Key attack mean depression rate SDnxc',...
        'Note onset lag SDnxc','Note onset lag rate SDnxc','Note onset lag amount SDnxc','Note onset lag amount rate SDnxc',...
        'Note offset lead SDnxc','Note offset lead rate SDnxc','Note offset lead amount SDnxc','Note offset lead amount rate SDnxc','Sync rate SDnxc','Sync amount rate SDnxc'};
    
    
    fidall = fopen(['Performances_all_chords_' initials '.xls'],'w+');
    for i=1:length(titles), fprintf(fidall, '%s \t', cell2mat(titles(i))); end
    for i=1:size(chall,1),
        fprintf(fidall,'\n');
        for j=1:size(chall,2), fprintf(fidall,'%.4f\t',chall(i,j)); end
    end
    fclose(fidall);
    
    fidleft = fopen(['Performances_lefthand_chords_' initials '.xls'],'w+');
    for i=1:length(titles), fprintf(fidleft, '%s \t', cell2mat(titles(i))); end
    for i=1:size(chleft,1),
        fprintf(fidleft,'\n');
        for j=1:size(chleft,2), fprintf(fidleft,'%.4f\t',chleft(i,j)); end
    end
    fclose(fidleft);
    
    fidright = fopen(['Performances_righthand_chords_' initials '.xls'],'w+');
    for i=1:length(titles), fprintf(fidright, '%s \t', cell2mat(titles(i))); end
    for i=1:size(chright,1),
        fprintf(fidright,'\n');
        for j=1:size(chright,2), fprintf(fidright,'%.4f\t',chright(i,j)); end
    end
    fclose(fidright);
    
    fiddiff = fopen(['Performances_handsdiff_chords_' initials '.xls'],'w+');
    for i=1:length(titles), fprintf(fiddiff, '%s \t', cell2mat(titles(i))); end
    for i=1:size(chdiff,1),
        fprintf(fiddiff,'\n');
        for j=1:size(chdiff,2), fprintf(fiddiff,'%.4f\t',chdiff(i,j)); end
    end
    fclose(fiddiff);
    cd('..');
end
cd('..');






















