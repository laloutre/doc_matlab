function ext = extract(file)

% boe parser
% INPUT : boe data file name
% OUTPUT : Matrix of events at each timestamp
%

THRESHOLD=2; % parameter : time increment

fidt = fopen(file,'r');
test = fscanf(fidt,'%2x',1);
fclose(fidt);

if ~isempty(test), % Hex file format
    ext = extract_hex(file,THRESHOLD);
else % bin file format
    ext = extract_bin(file,THRESHOLD);
end

% dlmwrite('events.txt', ext)
end


function ext = extract_hex(file,THRESHOLD)
%open file
fid = fopen(file,'r');

%Initialization
eventnum=0;
bitnum=0;
key = 0;
hex2bit=0;

%Event Matrix
events=[];

%read the first 2 characters
hex2bit = fscanf(fid,'%2x',1);

%loop
while ~feof(fid)
    %If to make sure you read the whole file
    if hex2bit ~= 255
        hex2bit = fscanf(fid,'%2x',1);
    end

    %Conditions that determines if we go in the while loop
    if hex2bit == 255
        %Enter the loop
        eventLoop = 1;

        %Write  first 2 bits to the events matrix
        eventnum = eventnum + 1;
        bitnum = bitnum + 1;
        events(eventnum,bitnum) = hex2bit;

        %Write timestamp to the events matrix
        time = fscanf(fid,'%6x',1);
        bitnum = bitnum + 1;
        events(eventnum,bitnum) = time;


    else
        %Don't enter the loop
        eventLoop=0;
    end

    validevent = 0;

    while eventLoop==1
        hex2bit = fscanf(fid,'%2x',1);
        if hex2bit==255
            %Break the loop
            bitnum=0;
            eventLoop=0;

            %Write subsequent bits to the event matrix
        else
            %Write values from even columns (# key pressed) to a tmp variable
            modulo = mod(bitnum,2);
            if modulo==0
                key = hex2bit;
                bitnum = bitnum + 1;

                %Determines if values of odd columns (value of the key) are > 2
                %If yes, both key # and key value get written to the matrix
            elseif (modulo==1) && (hex2bit>THRESHOLD)
                bitnum = bitnum + 1;
                events(eventnum,(bitnum-1)) = key;
                events(eventnum,bitnum) = hex2bit;
                validevent = validevent+1;
                key = 0;

                %If not in the first three columns or as a value > 2, then
                %clear tmp and redo the loop
            elseif (modulo==1)
                key = 0;
                bitnum = bitnum - 1;
            end

            %Make sure we stay in the loop
            eventLoop=1;

            if feof(fid)

                break;
            end

        end
    end
    if validevent==0
        eventnum = eventnum - 1;
    end
end
fclose(fid);

ext = events;

end


function ext = extract_bin(file,THRESHOLD)

fid = fopen(file,'r'); % open file

dat = fread(fid); % read & store bin data

% Init
events=[]; % Event Matrix
iev=0; % Storing row in events - timestamp
i=1; % index

while i<length(dat) % loop
    
    % To not get stuck (avoid everything except 255)
    while dat(i)~=255 && i<length(dat), i=i+1; end

    if dat(i)==255,
        if dat(i+1)==255, break; % End of data - start of metadata

        elseif dat(i+1)==0 && i+2<length(dat), % New timestamp
            iev=iev+1;
            events(iev,1)=dat(i); % 255 FTW (first column)

            if dat(i+2)==255 || dat(i+3)==255, % Something went wrong, discard the series
                i=i+1;
                events(iev,:) = [];
                iev=iev-1;

            else % Whence A-OK

                events(iev,2) = dat(i+2)*256 + dat(i+3); % timestamp
                jev=3; % Storing column in events
                i=i+4;

                while i<length(dat) && dat(i)~=255 && dat(i+1)~=255, % Still within current timestamp
                    if dat(i+1)>THRESHOLD, % Relevant event
                        events(iev,jev) = dat(i);
                        events(iev,jev+1) = dat(i+1);
                        jev = jev+2;
                    end
                    i=i+2;
                end

                if jev==3, % No relevant event at this timestamp
                    events(iev,:) = [];
                    iev = iev-1;
                end
            end

        else i=i+1;
        end
        
    end

end

fclose(fid);
ext = events;

end