function [score tau midmat1 midmat2] = score_matching(matin1,type1,matin2,type2)

% Using Ed Large's Score matching function to evaluate to correspondence between performances' notes and score instructions, 
% or between 2 performances
%
% IN: chords mat or notes mat or midi file (for score)
%       First input to be displayed in lower window ('Notation')
%       Second input to be displayed in upper window ('Performance')
%
% OUT:
%

midmat1 = go2pmat(matin1,type1);
midmat2 = go2pmat(matin2,type2);

if type1, nam1='Performance'; else nam1='Score'; end
if type2, nam2='Performance'; else nam2='Score'; end

nameout = [nam1 ' Vs ' nam2];


score = dynamicmatch(midmat1, midmat2, nameout);

tau = taux(score);

warning off;
gmw('create', score);

end


function midmat = go2pmat(matin,type)

% Input type
if ischar(type),
    if strcmp(type,'chords') || strcmp(type,'chord'), type=1;
    elseif strcmp(type,'notes') || strcmp(type,'note'), type=2;
    elseif strcmp(type,'midi') || strcmp(type,'mid'), type=0;
    end
end

if type==1, % Chords mat input
    dmeans=4; % Number of different means and SDs calculated over the whole file (first rows)

    midmat = zeros(1,7);
    midrank=1;
    
    for i=dmeans+1:size(matin,1) % Scans all chords
        for j=4:matin(i,1,1)+3 % Scan all of chord's notes
            
            midmat(midrank,1) = matin(i,j,2)*2e-3; % Onset time in beats
            midmat(midrank,6) = matin(i,j,2)*1e-3; % Onset time in sec
            midmat(midrank,2) = matin(i,j,4)*2e-3; % Duration in beats
            midmat(midrank,7) = matin(i,j,4)*1e-3; % Duration in sec
            midmat(midrank,3) = 1; % Channel
            midmat(midrank,4) = matin(i,j,1); % Pitch = key #
            if matin(i,j,8)>0,
                midmat(midrank,5) = round(matin(i,j,8) *127/250); % Velocity = MHV
            else midmat(midrank,5) = round(matin(i,j,6) *127/250); % Velocity = Amax if none better
            end
            midrank=midrank+1;
        end
    end
    
    midmat = sortrows(midmat,1);
       
    
elseif type==2, % Notes mat input
    midmat = zeros(1,7);
    midrank=1;
    
    for i=2:size(matin,1) % Scans all keys
        if matin(i,1,1)<109,
            for j=3:matin(i,1,2)+2 % Scan all notes on key i
                
                midmat(midrank,1) = matin(i,j,2)*2e-3; % Onset time in beats
                midmat(midrank,6) = matin(i,j,2)*1e-3; % Onset time in sec
                midmat(midrank,2) = matin(i,j,4)*2e-3; % Duration in beats
                midmat(midrank,7) = matin(i,j,4)*1e-3; % Duration in sec
                midmat(midrank,3) = 1; % Channel
                midmat(midrank,4) = matin(i,1,1); % Pitch = key #
                if matin(i,j,8)>0,
                    midmat(midrank,5) = round(matin(i,j,8) *127/250); % Velocity = MHV
                else midmat(midrank,5) = round(matin(i,j,6) *127/250); % Velocity = Amax if none better
                end
                midrank=midrank+1;
            end
        end
    end
    
    midmat = sortrows(midmat,1);
    
    
elseif type==0, % MIDI input,
    midmat = midi2nmat(matin);

end

end


function yo = taux (M)

[m n] = size(M.mtbl);

correlation=0;
for i=1:m
    for j=1:n
        if (M.mtbl(i,j) == 10)
             correlation = correlation +1;
        end
    end
end


anti_correlation=0;
for i=1:m
    for j=1:n
        if (M.mtbl(i,j) == 3)
             anti_correlation = anti_correlation +1;
        end
    end
end



matching = correlation / (correlation + anti_correlation) ;      %taux matching
erreur = anti_correlation / (correlation + anti_correlation);   % taux erreur

yo(1)=matching;
yo(2)=erreur;

fprintf('Matching rate = %.4f\n',matching);
fprintf('Error rate = %.4f\n',erreur);

end


%% Ed Large's functions follow

function [M] = dynamicmatch(pmat, nmat, name)
% Wrapper function fo the matcher. It reads the files,
% runs the matcher, groups the performance according to
% th optimal match, and returns the necessary information 
% in a structure

if nargin < 3
    M.title = '';
else
    M.title = (name);
end
M.Nn = nmat;
M.Np = pmat;

% Calculate the match
max_notes = 10; % maximum number of notes in a chord
[M.GIn M.GIp M.indN M.indP tbl pscr tscr] = matchsum(M.Nn, M.Np, max_notes);

% Then present the results in nice foramts
% 1. The match table
mtbl = zeros(length(M.GIp), length(M.GIn));
for ii = 1:length(M.indP);
  mtbl(M.indP(ii), M.indN(ii)) = pscr(M.indP(ii), M.indN(ii));
end
M.mtbl = sparse(mtbl);

% 2.Group the performance into chords, and calculate the maps
[M.realGIp M.Tn M.Bn M.Tp M.Bp M.tempoMap M.periodMap, M.velocMap] = realGroup(M);

% 3. The note matrix for the match
M.Nm = [];
m=0;
for ip = 1:length(M.indP)
    n = M.indP(ip);
    c = M.indN(ip);
    m = m+1;
    M.Nm = [M.Nm; M.Np(n,:)];
    M.Nm(m,1:2) = M.Nn(M.GIn(c,1),1:2); % doesn't set the duration (beats)
                                        % right for every note (only for 
                                        % the first)
end

% 4. The full report, with matches, substitutions, additions and deletions
M = matchReport(M);

end


function [GIn, GIp, indN, indP, tbl, pscr, tscr] = matchsum(Nn, Np, max_notes)

%Nn is a matrix variable that holds seven pieces of information for each event in the notated piece
%(time, duration,midi channel, note number, velocity, onset(sec), duration (sec)) 
%Np holds this same information for the performed  piece

%the p or n at the end of these variables denotes performed or notated

%max-notes is the maximum number of notes in a chord

% if isempty(Nn) return; end
% if isempty(Np) return; end

% Extract vectors corresponding to onset Times, Pitches,
% and Amplitudes of each note for readability;
    if ~isempty(Nn), Tn = Nn(:,1); Pn = Nn(:,4); An = Nn(:,5); else Tn=[]; Pn=[]; An=[]; end
    if ~isempty(Np), Tp = Np(:,1); Pp = Np(:,4); Ap = Nn(:,5); else Tp=[]; Pp=[]; Ap=[]; end


%GP is the vector of chords containing pitches
%GT is the vector of chords containing times
%GI is the vector of chords containing indices

[GPp GTp GIp] = group(Tp, Pp, max_notes, -1); sz= size(GPp); cp = sz(1);
[GPn GTn GIn] = group(Tn, Pn, max_notes, 0); sz= size(GPn); cn = sz(1);

% Here's the Dynamic Programming part
%
[tbl pscr tscr] = table7(GPn, cn, GPp, cp, GTn, GTp);

%
% Read the solution from the table
%
indP = [];
indN = [];

i = find(tbl(:,1) == max(tbl(:,1)));		% Find Column Max
j = 1;

while i <= cp && j <= cn

   indP = [indP i];
   indN = [indN j];

   % Find CURRENT COLUMN Max (cur) if IOI is short enough to be a chord
   tbl(i+1,j  ) = tbl(i+1,j  ) + tscr(i+1,j);
   cur             = tbl(i+1,j  );
   [curmax nextc] = max(cur);
   if isempty(nextc), nextc = cp+1; else nextc = i+nextc(1); end;

   % Find NEXTROW Max
   tbl(i+1,j+1:cn) = tbl(i+1,j+1:cn) - tscr(i+1,j+1:cn);
   row    = tbl(i+1,j+1:cn);
   [rowmax nextj] = max(row);
   if isempty(nextj), nextj = cn+1; else nextj = j+nextj(1); end;

   % Find NEXT COLUMN Max (note: mistake in the paper)
   col    = tbl(i+1:cp,j+1) - tscr(i+1:cp,j+1);
   if ~isempty(col), col(1) = col(1) + tscr(i+1,j+1); end;

   [colmax nexti] = max(col);

   if isempty(nexti)
      nexti = cp+1; 
   else
      nexti = i+nexti(1); 
   end;

   if ~isempty(colmax)
      if colmax >= curmax && colmax >= rowmax 
	 tbl(i+1:nexti,j+1) = tbl(i+1:nexti,j+1) - tscr(i+1:nexti,j+1);
	 tbl(i+1,j+1) = tbl(i+1,j+1) + tscr(i+1,j+1);
      end

      if colmax < curmax
	 colmax = curmax;
	 nexti = nextc;
	 jplus = j;
      else
	 jplus = j+1;
      end
   end
   
   if isempty(colmax); colmax = -1; end;
   if isempty(rowmax); rowmax = -1; end;

   if ~isempty(colmax) && ~isempty(rowmax)
      if colmax >= rowmax
	 i = nexti;
	 j = jplus;
      else
	 i = i+1;
	 j = nextj;
      end
   end

end

% Sort the indices in GIp and GIn into their original order
% which may have been screwed up above

for i = 1:size(GIp,1)
   notei = find(GIp(i,:));
   GIp(i,notei) = sort(GIp(i,notei));
end
for i = 1:size(GIn,1)
   notei = find(GIn(i,:));
   GIn(i,notei) = sort(GIn(i,notei));
end

end


function [GP, GT, GI] = group(T, P, max_notes, gap)

%This function groups chords using fixed gap size. Gap size should be chosen
%according to the tempo of the performance
%T is the times extracted from the music vector for each event
%P is the pitches extracted from the music vector for each event
%max_notes is the maximum number of notes in a chord
%This function returns the pitches (in GP), the times (in GT),
%and the indices (in GI) in chords for the entire piece

n = length(T);
GP = zeros(n, max_notes); GP(1,1) = P(1);
GT = zeros(n, max_notes); GT(1,1) = T(1);
GI = zeros(n, max_notes); GI(1,1) = 1;


j = 1; k = 1;
for i = 2:n
   if T(i) - T(i-1) > gap
      j = j + 1;
      k = 1;
   else
      k = k + 1;
  end
  if k > max_notes
	  error(sprintf('There are more than max_notes near time %f', T(i)));
  end

   GP(j, k) = P(i);
   GT(j, k) = T(i);
   GI(j, k) = i;
      
end



GP = GP(1:j,:);
GT = GT(1:j,:);
GI = GI(1:j,:);

end


function [tbl, pscr, tscr] = table7(GPn, nn, GPp, np, GTn, GTp)

tbl  = sparse(zeros(np+1, nn+1));
pscr = sparse(zeros(np+1, nn+1));
tscr = sparse(zeros(np+1, nn+1));

% Equate mean tempi
%tempoRatio finds the ratio of the performed tempo to the notated tempo
%the time matrix of the notated is then multiplied by the ratio to change
%the average tempo of the notated to match that of the performed - they
%now begin and end at the same time
tempoRatio = GTp(size(GTp,1),1)/GTn(size(GTn,1),1);
GTn = GTn * tempoRatio;

% Range limit for efficiency
slope = nn/np;
range = 40;				% this should be a parameter

for i = np:-1:1
    nn_upper = min(nn, round(slope*i+range));
    nn_lower = max( 1, round(slope*i-range));
    for j = nn_upper:-1:nn_lower

        %  pitch score
        pscr(i,j) = scorep(GPn(j,:), GPp(i,:));

        %  interval time score
        tscr(i,j) = scoret(GTp, GTn, i, j);


        curmax  = tbl(i+1, j) + tscr(i+1, j);

        col = tbl(i+1:np,j+1);
        if isempty(col)
            col  = 0;
        else
            col(1)  = col(1) - tscr(i+1,j+1);
        end
        colmax  = max(col);

        row = tbl(i+1,j+1:nn);
        if isempty(row)
            row = 0;
        else
            row = row - tscr(i+1,j+1:nn);
        end
        rowmax  = max(row);

        tbl(i,j) = pscr(i,j) + max([curmax, colmax, rowmax]);

    end
end
end


function [pts, matched] = scorep(I, P)
%SCORE(I, P)
%

lnI = length(nonzeros(I));
lnP = length(nonzeros(P));
pts = 0;
matched = 0;

if find(I == P(1))
   pts = 10;
else
   pts = 3;
end
matched = P(1);
end


function ts = scoret(GTp, GTn, i, j)

if j < size(GTn,1)
   ioiN = GTn(j+1,1)-GTn(j,1);		% normal case
else
   ioiN = GTn(j,1)-GTn(j-1,1);		% last chord
end

if i > 1
   ioiP = GTp(i,1)-GTp(i-1,1);		% normal case
else
   ioiP = 0.000;			% first note
end

ts = round(14*(1-( 1 ./ (1 + exp(-(ioiP - ioiN/4) / ioiN/20)) )))-7;
end


function [GIp, Tn, Bn, Tp, Bp, tempoMap, periodMap, velocMap] = realGroup(M)

GIp = M.GIp;

[Mn Mi] = unique(M.indN);
Mi = [1, 1+Mi(1:length(Mi)-1)]';

Bn = M.Nn(M.GIn(:,1),1);
Bn = Bn(M.indN(Mi));
Tn = M.Nn(M.GIn(:,1),6);
Tn = Tn(M.indN(Mi));

mm = 0;
for nn = unique(M.indN)
   mm = mm+1;
   ii = find(M.indN == nn);
   GIp(mm, 1:length(ii)) = M.indP(ii);
end

GIp = GIp(1:mm,:);
Tp = zeros(size(GIp,1),1);
velocMap = zeros(size(GIp,1),1);

for pp = 1:size(GIp,1)
   ii = GIp(pp,find(GIp(pp,:) ~= 0));
   Tp(pp) = mean(M.Np(ii,6));
   velocMap(pp) = mean(M.Np(ii,5));
end 
Bp = Bn;

tempoMap  = diff(Bn) ./ diff(Tp); tempoMap  = [tempoMap(1);  tempoMap ];
periodMap = diff(Tp) ./ diff(Bn); periodMap = [periodMap(1); periodMap];

end


function M = matchReport(M)

% Full report
% ================================================================
realIndN = [];
realIndP = [];

% Step 1. Create chord-based match (Gin -> realGIp)
% =================================================
for nc = 1:size(M.GIn,1)    % for each notated chord
  ii = find(M.indN  == nc);
  if ~isempty(ii)                          
    pc = find(M.realGIp(:,1) == M.indP(ii(1)));  % find performed chord that matches
    if ~isempty(pc) % redundant, ii would be empty if there wasn't a match
      realIndN = [realIndN; nc];
      realIndP = [realIndP; pc];
    end
  end
end

realGIn = M.GIn;
cmatch = [realIndN, realIndP];

% Step 2. Match notes of each chord
% =================================
M.M = [];
nprev = 0;
pprev = 0;
cm = 1;

while cm <= size(cmatch,1)    % for each group (chord) in notation
  m=zeros(1,11);
  nc = cmatch(cm, 1);
  pc = cmatch(cm, 2);

  nn = M.GIn    (nc, find(M.GIn    (nc,:)));
  pp = M.realGIp(pc, find(M.realGIp(pc,:)));

  if     nn(1) > nprev+1; % if the chord match skipped a notated note
    nn = nprev+1;         % don't advance cm, do nest time through
    nc = nc - 1;
    pp = [];
    nprev = nn;
    nm = [1];
  elseif pp(1) > pprev+1 % if the chord match skipped a performed note
    nn = [];              % don't advance cm, do nest time through
    nc = NaN;
    pp = pprev+1;
    pprev = pp;
    nm = NaN;
  else                    % otherwise move on with chord match
    nm = noteMatch(M.Nn(nn,4)', M.Np(pp,4)');
    nprev = nn(end);
    pprev = pp(end);
    cm = cm + 1;
  end

  % Write chord match information into array
  for n = 1:max(length(nn),length(pp))   % list out the chord
    if ~isnan(nm(n)) & nm(n) <= length(nn) 
      m([2 4 6 8 10]) = [nc, M.Nn(nn(nm(n)), [4 1 5 2])];
    else
      m([2 4 6 8 10]) = [nc, NaN, NaN, NaN, NaN];
    end
    if n <= length(pp)
      m([3 5 7 9 11]) = [pp(n), M.Np(pp(n), [4 6 5 7])];
    else
      m([3 5 7 9 11]) = NaN;
    end
    M.M = [M.M; m];
  end

end

% Step 2. Determine: m, s, a, or d
% =================================
M.M = num2cell(M.M);
for nn = 1:length(M.M)
  if     M.M{nn,4} == M.M{nn,5}
    M.M{nn,1} = 'M';
  elseif isnan(M.M{nn,4})
    M.M{nn,1} = 'add';
  elseif isnan(M.M{nn,5})
    M.M{nn,1} = 'del';
  else
    M.M{nn,1} = 'sub';
  end
end

end


function nn = noteMatch(N, P)
% =====================================================================
N = N(:)';
P = P(:)';

diffl = length(N)-length(P);
if     diffl > 0
  P = [P, 100+zeros(1, diffl)];
elseif diffl < 0
  N = [N, NaN+zeros(1,-diffl)];
end

nn = []; Pnot = [];
for pp = 1:length(P)
  n = find(N == P(pp));
  if ~isempty(n)
    N(n) = NaN;
    Pnot = [Pnot,NaN];
    nn = [nn,n];
  else
    Pnot = [Pnot,P(pp)];
    nn = [nn, NaN];
  end
end

for pp = 1:length(Pnot)
  n = find((abs(N-Pnot(pp)) ==  min(abs(N-Pnot(pp)))));
  if ~isempty(n)
    N(n)  = NaN;
    nn(pp) = n;
  end
end

end
