function [chords note] = chords(filename,varargin)

%% Identifies chords and single notes in discrete notetime files
% Analyzes those chords and single notes
% IN: notetime files or mats
% Processed through notes.m to identify and analyze each note
% OUT: 3D matrix:
%  Dim.1: 1: all chords mean, 2: left-hand chords mean, 3: right-hand chords mean, 4: mean diff. between hands, + rest: chords
%  Dim.2: 1: chord features, 2&3: mean/SD of chord's notes features, + each note 
%  Dim.3: descriptive features
%
% varargins: 'printout' for outputting formatted text/table file
%            'separate' outputs: chords and notes resp. in separate 2D matrices
%            piece number from 1 to 4 if automatic detection fails (non-standard file name or matrix input)
%
% ------------------------------------------
%
% chout(1,1,1:3) = [0, total number of chords, 0];
% chout(1,1,4:chpar) = [mean of chords features];
% chout(1,2,1:3) = [0, total number of notes, mean number of notes per chord];
% chout(1,3,1:3) = [0, 0, 0];
% chout(1,2:3,4:npar2) = [mean of mean/SD of chords' notes features];
% chout(1,4,1:3) = [0, 0, 0];
% chout(1,4,4:chpar) = [SD of chords features];
% chout(1,5,1:3) = [0, 0, SD of number of notes per chord];
% chout(1,6,1:3) = [0, 0, 0];
% chout(1,5:6,4:npar2) = [SD of mean/SD of chords' notes features];
% 
% chout(2:4,1:6,:) = the same but for resp. left-hand chords only, right-hand chords only and diff. between hands
%
% chout(i,1,1:chpar) = chord's descriptive features;
% chout(i,2,1:npar2) = mean of chord's notes features;
% chout(i,3,1:npar2) = SD of chord's notes features;
% 
% ------------------------------------------
% 
% ------- NOTE FEATURES ------- 
%
% BASICS:
% 1 - Key #
% 2 - Note onset
% 3 - Note offset
% 4 - Note duration
% 5 - Amoy
% 6 - Amax
% 7 - t.Amax
% 8 - MHV
% 9 - t.MHV
%  
% ATTACK:
% 10 - speed of attack: dt(MHV-onset)
% 11 - slope of attack: MHV/speed.of.attack
% 12 - speed of key attack: dt(Amax-onset)
% 13 - slope of key attack
% 14  directness: dt(MHV-Amax)
% 15 - MHV/Amax
% 16 - sharpness of key attack slope
% 17 - sharpness of attack slope
% 
% DEPRESSION PROFILE:
% FKD
% 18  fkd start
% 19  fkd end
% 20  fkd duration
% 21  fkd rate
% 22 - pre-fkd duration
% 23 - pre-fkd rate
% 24 - post-fkd duration
% 25 - post-fkd rate
% ADSR 
% 26  t.attack.end 
% 27  t.decay.end 
% 28  t.release.start 
% 29 - attack duration
% 30 - sustain duration 
% 31 - release duration
% 32 - attack rate
% 33 - sustain rate
% 34 - release rate
% 
% PEDALS:
% 35 - pedal 109 Amoy/note
% 36 - pedal 111 Amoy/note
% 37 - pedal 109 @onset
% 38 - pedal 111 @onset
% 39 - pedal 109 @t.mhv
% 40 - pedal 111 @t.mhv
% 41 - pedal 109 @offset
% 42 - pedal 111 @offset
% 43 - pedal 109 depression duration
% 44 - pedal 111 depression duration
%
% BONUS:
% 45 - key attack mean depression
% 46 - key attack mean depression rate (/Amax)
% 
% COMPARISON WITH CHORDS OTHER NOTES:
% npar+1 - onset lag (on chord onset)
% npar+2 - onset lag rate (/chord duration)
% npar+3 - onset lag amount (sum(A.other.notes.bef.on))
% npar+4 - onset lag amount rate (/ mean(Amoy.other.ns))
% npar+5 - offset lead (on chord offset)
% npar+6 - offset lead rate (/chord duration)
% npar+7 - offset lead amount
% npar+8 - offset lead amount rate 
% npar+9 - sync rate (note.dur/chord.dur)
% npar+10 - sync amount rate (sum(A.other.notes.when.note.on)/mean(Amoy.other.notes))
%
% ------------------------------------------
%
% ------- CHORDS FEATURES -------
%
% BASICS:
% 1  Number of notes in the chord
% 2  Chord onset (min(note.onsets))
% 3  Chord offset (max(note.offsets))
% 4  Chord duration (3-2)
% 
% RETAKES:
% 5 - max Amax
% 6 - max MHV
% 7 - min onset lag (melody lead)
% 8 - min onset lag rate
% 9 - min offset lead (last-note-trail)
% 10 - min offset lead rate
% 
% 
% WITHIN-CHORD ADD:
% 
% PEDALS:
% 11 - Pedal 109 Amoy /chord
% 12 - Pedal 109 A.duration /chord
% 13 - Pedal 109 full-depression /chord
% 14 - Pedal 109 mid-depression /chord
% 15 - Pedal 109 mid-depression value /chord
% 16 - Pedal 111 Amoy /chord
% 17 - Pedal 111 A.duration /chord
% 18 - Pedal 111 full-depression /chord
% 19 - Pedal 111 mid-depression /chord
% 20 - Pedal 111 mid-depression value /chord
% 
% 21 - Pedal 109 value @chord onset
% 22 - Pedal 109 bin. @chord onset
% 23 - Pedal 109 lead/lag /onset
% 24 - Pedal 111 value @chord onset
% 25 - Pedal 111 bin. @chord onset
% 26 - Pedal 111 lead/lag /onset
% 
% 27 - Pedal 109 value @chord offset
% 28 - Pedal 109 bin. @chord offset
% 29 - Pedal 109 trail/lead /offset
% 30 - Pedal 111 value @chord offset
% 31 - Pedal 111 bin. @chord offset
% 32 - Pedal 111 trail/lead /offset
% 
% 
%  
% BETWEEN-CHORDS, ONE CHORD / THE OTHERS
% 
% INTERVALS:
% 33 - IOI: onset from next chord
% 34 - OffOnI: IOI-chord.duration
% 35 - OffOnI direction: <0 => -1, >0 => +1
% 
% 36 - IOI within hand
% 37 - OffOnI within hand
% 38  OffOnI direction within hand
% 
% 39 - IOI with other hand
% 40 - OffOnI with other hand
% 41  OffOnI direction with other hand
% 
% 
% OVERLAPS:
% 42 - overlap from other chords
% 43 - overlap rate (/chord duration)
% 44 - overlap amount from other chords: => sum of As (during target chord) for all overlapping chords
% 45 - overlap amount rate (/ chord Amoy)
% 46 - number of overlaps
% 
% 47 - overlap from same hand
% 48 - overlap rate from same hand
% 49 - overlap amount from same hand
% 50 - overlap amount rate from same hand
% 51 - number of overlaps from same hand
% 
% 52 - overlap from the other hand
% 53 - overlap rate from the other hand
% 54 - overlap amount from the other hand
% 55 - overlap amount rate from the other hand
% 56 - number of overlaps from the other hand
%
%% ------------------------------------------


% INI

printout=0; sep=0; piece=[]; nameout=[];
ivar=1;
while ivar<=length(varargin),
    if strcmp(varargin(ivar),'nameout') && ivar<length(varargin), nameout=varargin{ivar+1}; ivar=ivar+1; end
    if strcmp(varargin(ivar),'printout'), printout=1; end
    if strcmp(varargin(ivar),'noprintout'), printout=0; end
    if strcmp(varargin(ivar),'separate') || strcmp(varargin(ivar),'sep'), sep=1; end
    if strcmp(varargin(ivar),'noseparate') || strcmp(varargin(ivar),'nosep'), sep=0; end
    if isnumeric((varargin{ivar})), piece=varargin{ivar}; end
    ivar=ivar+1;
end

if isempty(nameout),
    if ischar(filename) && length(filename)>=16 && strcmp(filename(14),'p') && isempty(piece) % Possible standard nomenclature
        piece = str2double(filename(16));
    end
elseif ischar(nameout) && length(nameout)>=16 && strcmp(nameout(14),'p') && isempty(piece) % Possible standard nomenclature
    piece = str2double(nameout(16));
end

if isempty(piece), disp('The piece identification number is required to follow on, please specify in the input arguments');
elseif piece>4, disp('Invalid piece identification number: must be between 1 and 4');
else
    
    
    % --- SEPARATION BETWEEN HANDS ---
    % Threshand = highest note played by left hand in the piece
    if piece==1, threshand=58; % B-flat 3
    elseif piece==2, threshand=64; % C4
    elseif piece==3, threshand=64; % C4
    elseif piece==4, threshand=57; % A3
    elseif piece==0, threshand=0; % Fake for no-hand-discrimination
    end
    
        
    
    % ------- PARAMETRIC VARS -------
    npar = 46; % Original number of parameters gathered for each note, by itself
    npar2 = npar + 10; % Total number of parameters gathered for each note: by itself + by comparison with other notes in the same chord
    chpar = 56; % Total number of parameters gathered for each chord 
    dmeans = 4; % Number of different means and SDs calculated over the whole file: (mean + SD) of all chords (with some features only multi-notes), left hand, right hand and difference thereof
    
    delay = 101; % Time lapse (msec) to allow fr notes being included in the same chord
    delaytight = round(delay/2)+1; % Short delay to first set closest notes together
    
    pfullthresh=200; % Threshold over which a pedal is deemed fully depressed
    pmidthresh=100; % Threshold over which a pedal is deemed mid-depressed
    
    megatop=1e9; % Use for highest init.
    
    % -------
    
    
    % --- Passing vector [mkey per note (means/SDs per chord and total), mchord per chord (for total mean/SD)] ---
    % 0 = no mean/SD calculated for this parameter; 1 = non-zero mean/SD; 2 = full mean/SD incl. zeros; 3 = non-(9.tMHV=0) mean/SD; 4 = multi-note chords only; 5 = but for last chord (IOIs)
    
    %      [1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 +1 +2 +3 +4 +5 +6 +7 +8 +9 +10]  sheer visual indication of rank index
    mkey = [2  2  2  2  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  2  2  2  2  2  2  1  1  1  1  2  1  1  2  1  2  2  2  2  3  3  2  2  2  2  1  1  1  1  1  1  1  1  1  1  1  1];
    
    %        [1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56]
    mchord = [0  0  0  2  1  1  4  4  4  4  2  2  2  2  1  2  2  2  2  1  2  2  1  2  2  1  2  2  1  2  2  1  5  5  5  1  1  1  1  1  1  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2];
    
    

    % --- Notes computation ---
    nmat = notes(filename);
    [ndim1 ndim2 ndim3] = size(nmat);
    
    % --- Chord matrix output ---
    chout = zeros(dmeans+1,6,max(npar2,chpar));
    
    
%%    % ------- CHORD DETECTION ------------------------------------------
    
    % --- FIRST ROUND: regroup very close notes ---
    for i=2:ndim1
        if nmat(i,1,1)<109 % It's a key, not a pedal
            for j=3:nmat(i,1,2)+2 % Scan all notes of the key
                included = 0; % tag for note inclusion within a chord
                for i2=dmeans+1:size(chout,1) % Scan all previously stored chords
                    if nmat(i,j,2)>0 && (nmat(i,j,2)>chout(i2,1,2)-delaytight) && (nmat(i,j,2)<chout(i2,1,2)+delaytight) && (nmat(i,j,3)>chout(i2,1,2))... % Note onset within (Chord onset ± delay) & note offset > Chord onset
                            && ((chout(i2,4,1)<=threshand && nmat(i,1,1)<=threshand) || (chout(i2,4,1)>threshand && nmat(i,1,1)>threshand)), % Note played by the same hand as the one in the chord

                        if ~included % First chord fitting the note
                            included = i2; % Inclusion of note within the chord

                            % Loop-used chord descriptors
                            chout(i2,1,1) = chout(i2,1,1) +1; % Number of notes in the chord +1
                            chout(i2,1,2) = min(chout(i2,1,2),nmat(i,j,2)); % Chord onset = smallest note onset

                            % Note descriptors
                            chout(i2,chout(i2,1,1)+3,1) = nmat(i,1,1); % Key # as note's first descriptor
                            chout(i2,chout(i2,1,1)+3,2:npar) = nmat(i,j,2:npar); % Store all note descriptors
                            
                        elseif abs(nmat(i,j,2)-chout(i2,1,2)) < abs(nmat(i,j,2)-chout(included,1,2)) % Closer fit of note to this chord
                            % ERASE
                            chout(included,chout(included,1,1)+3,1:npar) = 0; % Clear the note line from previous chord
                            chout(included,1,1)=chout(included,1,1)-1; % One less note in previous chord
                            
                            % LOAD
                            included = i2;
                            % Loop-used chord descriptors
                            chout(i2,1,1) = chout(i2,1,1) +1; % Number of notes in the chord +1
                            chout(i2,1,2) = min(chout(i2,1,2),nmat(i,j,2)); % Chord onset = smallest note onset

                            % Note descriptors
                            chout(i2,chout(i2,1,1)+3,1) = nmat(i,1,1); % Key # as note's first descriptor
                            chout(i2,chout(i2,1,1)+3,2:npar) = nmat(i,j,2:npar); % Store all note descriptors 
                        end
                    end
                end
                
                if ~included, % If the note would not fit in any chords
                    iend = size(chout,1) +1; % rank for one more chord
                    
                    % New chord's loop-used descriptors
                    chout(iend,1,1) = 1; % One note in the new chord
                    chout(iend,1,2) = nmat(i,j,2); % Chord onset = note onset
                    
                    % Note descriptors
                    chout(iend,4,1) = nmat(i,1,1); % Key # as note's first descriptor
                    chout(iend,4,2:npar) = nmat(i,j,2:npar); % Store all note descriptors
                end
            end
        end
    end
    
    % --- SECOND ROUND: regroup chords that fit together in time more loosely yet completely (wrt all their notes) ---
    for i=dmeans+1:size(chout,1)-1
        if chout(i,1,1)>0, % Chord not previously erased
            for i2=i+1:size(chout,1)
                if chout(i,1,1)==0, break; % Chord #i was previously erased, i++ then 
                elseif chout(i2,1,1)>0 && min(chout(i2,4:chout(i2,1,1)+3,2))>max(chout(i,4:chout(i,1,1)+3,2))-delay && max(chout(i2,4:chout(i2,1,1)+3,2))<min(chout(i,4:chout(i,1,1)+3,2))+delay...
                        && ((chout(i,4,1)<=threshand && chout(i2,4,1)<=threshand) || (chout(i,4,1)>threshand && chout(i2,4,1)>threshand)) && chout(i,4,1)>0
                        % Chords not previously erased, and extrema of second chord's note onsets within (larger) delay of first chord's note onsets extrema, and first & second chords in the same hand
                        % => Fusion the chords

                    tmpnotes=zeros(chout(i,1,1)+chout(i2,1,1),npar);
                    tmpnotes(1:chout(i,1,1),:) = chout(i,4:chout(i,1,1)+3,1:npar); % Store all notes and data thereof in tmp matrix
                    tmpnotes(chout(i,1,1)+1:chout(i,1,1)+chout(i2,1,1),:) = chout(i2,4:chout(i2,1,1)+3,1:npar);
                    tmpnotes = sortrows(tmpnotes,1); % Sort notes by increasing key

                    if chout(i,1,2)<=chout(i2,1,2) % First chord starts first: keep it, add second chord in, erase second chord
                        chout(i,1,1)=chout(i,1,1)+chout(i2,1,1); % Number of notes in the fusioned chord
                        chout(i,4:chout(i,1,1)+3,1:npar)=tmpnotes(:,:); % Inject sorted notes in chout
                        chout(i2,1,1)=0; % Clearing tag for the second chord

                    else % Second chord starts first: keep it, add first chord in, erase first chord
                        chout(i2,1,1)=chout(i,1,1)+chout(i2,1,1); % Number of notes in the fusioned chord
                        chout(i2,4:chout(i2,1,1)+3,1:npar)=tmpnotes(:,:); % Inject sorted notes in chout
                        chout(i,1,1)=0; % Clearing tag for the first chord
                        
                    end
                end
            end
        end
    end
    
    % --- CLEAR ---
    ioff=dmeans+1;
    while ioff<=size(chout,1),
        if ~chout(ioff,1,1), chout(ioff,:,:)=[]; ioff=ioff-1; end
        ioff=ioff+1;
    end
    
    % --- SORT BY ONSET ---
    
    sorton = zeros(size(chout,1),2); % Stores chout indexes and onsets
    for i=1:size(chout,1)
        sorton(i,1)=i; % index
        sorton(i,2) = chout(i,1,2); % chord onset
    end
    sorton = sortrows(sorton,2); % sort by increasing onset
    
    for i=dmeans+1:size(chout,1) % Span all chords
        if sorton(i,1)~=sorton(i,2), % If chord not in right place from the start
            chtmp=chout(i,:,:); % tmp storage of this chord's data
            chout(i,:,:) = chout(sorton(i,1),:,:); % Affects to the chord slot 'i' the rightfully-ordered 'sorton(i,1)'th chord
            chout(sorton(i,1),:,:)=chtmp; % The original 'i'-slot chord moves to position 'sorton(i,1)'
            sorton(sorton(:,1)==i,1) = sorton(i,1); % For the displaced chord, update its current index to its new position in place of the rightfully-ordered chord
            sorton(i,1) = i; % In its right place
        end
    end
    
    
    
%%    % ------- ADDITIONAL NOTE STATS ------------------------------------------
    
    for i=dmeans+1:size(chout,1) % Scan all chords
        
        % Chord stats, basics
        chout(i,1,2) = min(chout(i,4:chout(i,1,1)+3,2)); % Chord onset
        chout(i,1,3) = max(chout(i,4:chout(i,1,1)+3,3)); % Chord offset
        chout(i,1,4) = chout(i,1,3) - chout(i,1,2); % Chord duration
        
        for j=4:chout(i,1,1)+3 % Scan all notes as targets
            
            % Syncs
            chout(i,j,npar+1) = chout(i,j,2) - chout(i,1,2); % Onset lag
            chout(i,j,npar+2) = chout(i,j,npar+1)/chout(i,1,4); % Onset lag rate (/chord duration)
            chout(i,j,npar+5) = chout(i,1,3) - chout(i,j,3); % Offset lead
            chout(i,j,npar+6) = chout(i,j,npar+5)/chout(i,1,4); % Offset lead rate (/chord duration)
            chout(i,j,npar+9) = chout(i,j,4)/chout(i,1,4); % Sync rate = note.duration / chord.duration
        end
    end
    
    
    % --- PART DEUX ---
    for i=dmeans+1:size(chout,1) % Scan all chords
        for j=4:chout(i,1,1)+3 % Scan all notes as targets
            tmplagamount=0; % Counter for other notes' contributing to an onset lag amount rate
            tmpleadmount=0; % Counter for other notes' contributing to an offset lead amount rate
            tmpsycamore=0; % Storage of other notes' Amoys when contributing to a sync amount rate
            
            % Amounts
            for j2=4:chout(i,1,1)+3 % Rescan all chord's notes...
                if j2~=j, % ...But for the target one
                    for in1=2:ndim1, 
                        if nmat(in1,1,1)==chout(i,j2,1), % Find the note(i,j2)'s key in nmat
                            for jn1=3:min(nmat(in1,1,2)+2,ndim2)
                                if nmat(in1,jn1,2)==chout(i,j2,2) % Find the note from nmat on key(in1) that matches note(i,j2)-from-chout's onset
                                    if chout(i,j2,2)<chout(i,j,2) % Note onset bef. target note's, therefore an amount to account for
                                        
                                        chout(i,j,npar+3) = chout(i,j,npar+3) + mean(nmat(in1,jn1,npar+1:npar+min((chout(i,j,2)-chout(i,j2,2))/2,ndim3-npar))); 
                                                % Onset lag amount from note(i,j2) to note(i,j): Add mean of key depressions from onset(i,j2) to onset(i,j)
                                        
                                        chout(i,j,npar+4) = chout(i,j,npar+4) + chout(i,j2,5); % Add note(i,j2)'s Amoy to the count of contributing notes
                                        tmplagamount = tmplagamount +1; % Tmp counter for number of chord's notes tributing an onset lag amount
                                    end
                                    
                                    if chout(i,j2,3)>chout(i,j,3) % Note offset after target note's, therefore an amount to account for
                                        
                                        chout(i,j,npar+7) = chout(i,j,npar+7) + mean(nmat(in1,jn1,npar+min((chout(i,j2,3)-chout(i,j,3))/2,ndim3-npar):npar+min((chout(i,j2,3)-chout(i,j2,2))/2+1,ndim3-npar))); 
                                                % Offset lag amount from note(i,j2) to note(i,j): Add mean of key depressions from offset(i,j) to offset(i,j)
                                        
                                        chout(i,j,npar+8) = chout(i,j,npar+8) + chout(i,j2,5); % Add note(i,j2)'s Amoy to the count of contributing notes
                                        tmpleadmount = tmpleadmount +1; % Tmp counter for number of chord's notes tributing an offset lead amount
                                    end
                                    
                                    if (chout(i,j2,2)<=chout(i,j,3) && chout(i,j2,3)>=chout(i,j,2)),
                                        chout(i,j,npar+10) = chout(i,j,npar+10) + mean(nmat(in1,jn1,npar+1+max(0,min((chout(i,j,2)-chout(i,j2,2))/2,ndim3-npar)):npar+max(0,min(ndim3-npar,(chout(i,j2,3)-min(chout(i,j2,2),chout(i,j,2)))/2+1))));
                                                % Sync amount from note(i,j2) to note(i,j): mean of key depressions from j2 for when j is on
                                        
                                        tmpsycamore = tmpsycamore + chout(i,j2,5); % Store the contributing note's Amoy
                                    end
                                    
                                    break;
                                end
                            end
                            break;
                        end
                    end
                end
            end
            
            % Divide amounts by number of contributing notes
            if chout(i,j,npar+4)>0, chout(i,j,npar+4) = chout(i,j,npar+3)*chout(i,j,npar+2)/chout(i,j,npar+4); else chout(i,j,npar+4)=0; end % Onset lag amount rate: sum(onset lag amounts)*lag.rate/sum(Amoys)
            if tmplagamount>0, chout(i,j,npar+3) = chout(i,j,npar+3)/tmplagamount; else chout(i,j,npar+3)=0; end % Onset lag amount
            
            if chout(i,j,npar+8)>0, chout(i,j,npar+8) = chout(i,j,npar+7)*chout(i,j,npar+6)/chout(i,j,npar+8); else chout(i,j,npar+8)=0; end % Offset lead amount rate: sum(offset lead amounts)*lead.rate/sum(Amoys)
            if tmpleadmount>0, chout(i,j,npar+7) = chout(i,j,npar+7)/tmpleadmount; else chout(i,j,npar+7)=0; end % Offset lead amount
            
            if tmpsycamore>0, chout(i,j,npar+10) = chout(i,j,npar+10)/tmpsycamore; else chout(i,j,npar+10)=0; end % Sync amount rate
        end
    end

    
%%    % ------- MEAN/SD OF NOTE STATS PER CHORD and CHORD STATS ------------------------------------------
    
    for i=dmeans+1:size(chout,1) % Scan all chords
        
        % --- Mean/SD of note stats ---
        
        for k=1:npar2 % All note stats
            kmrank=0;
            tmpvect=zeros(1,1);
            if mkey(k)==1, % non-zero
                for j=4:chout(i,1,1)+3, % Each note in the chord
                    if chout(i,j,k)~=0, kmrank=kmrank+1; tmpvect(kmrank)=chout(i,j,k); end % Store non-zero values in tmp vector
                end
                chout(i,2,k) = mean(tmpvect); % Mean of non-zero values
                if isnan(chout(i,2,k)), chout(i,2,k)=0; end
                chout(i,3,k) = std(tmpvect); % SD of non-zero values
                if isnan(chout(i,3,k)), chout(i,3,k)=0; end
                                
            elseif mkey(k)==2, % all
                chout(i,2,k) = mean(chout(i,4:chout(i,1,1)+3,k)); % straight-up mean incl. zeros
                chout(i,3,k) = std(chout(i,4:chout(i,1,1)+3,k)); % straight-up SD incl. zeros
                
            elseif mkey(k)==3, % non-mhv=zero
                for j=4:chout(i,1,1)+3, % Each note in the chord
                    if chout(i,j,9)~=0, kmrank=kmrank+1; tmpvect(kmrank)=chout(i,j,k); end % Store non-mhv=zero values in tmp vector
                end
                chout(i,2,k) = mean(tmpvect); % Mean of non-mhv=zero values
                if isnan(chout(i,2,k)), chout(i,2,k)=0; end
                chout(i,3,k) = std(tmpvect); % SD of non-mhv=zero values
                if isnan(chout(i,3,k)), chout(i,3,k)=0; end
                                
            end
        end
        
        % --- Chord stats ---
        
        % Retakes
        
        chout(i,1,5) = max(chout(i,4:chout(i,1,1)+3,6)); % max Amax
        chout(i,1,6) = max(chout(i,4:chout(i,1,1)+3,8)); % max MHV
        
        chout(i,1,7) = megatop; for j=4:chout(i,1,1)+3, if chout(i,j,npar+1)<chout(i,1,7) && chout(i,j,npar+1)>0, chout(i,1,7)=chout(i,j,npar+1); end; end % Min onset lag, i.e. melody lead
        if chout(i,1,7)==megatop, chout(i,1,7)=0; end
        chout(i,1,8) = chout(i,1,7)/chout(i,1,4); % Min onset lag / melody lead rate
        
        chout(i,1,9) = megatop; for j=4:chout(i,1,1)+3, if chout(i,j,npar+5)<chout(i,1,9) && chout(i,j,npar+5)>0, chout(i,1,9)=chout(i,j,npar+5); end; end % Min offset lead, i.e. last note trail
        if chout(i,1,9)==megatop, chout(i,1,9)=0; end
        chout(i,1,10) = chout(i,1,9)/chout(i,1,4); % Min offset lead / last note trail rate
        
        
        % Pedals
        
        for p=0:1
            if nmat(ndim1-p,1,1)==109 % Soft pedal
                for pn=3:nmat(ndim1-p,1,2)+2, % Scan all pedal "notes"
                    if (nmat(ndim1-p,pn,2)<=chout(i,1,3) && nmat(ndim1-p,pn,3)>=chout(i,1,2)), % Pedal overlapping the chord
%                     if (nmat(ndim1-p,pn,2)>=chout(i,1,2) && nmat(ndim1-p,pn,2)<chout(i,1,3)) || (nmat(ndim1-p,pn,3)<=chout(i,1,3) && nmat(ndim1-p,pn,3)>chout(i,1,2)) % pedal event starts before chord offset or ends after chord onset
                        pkstart = max( npar + (max(chout(i,1,2),nmat(ndim1-p,pn,2))-nmat(ndim1-p,pn,2))/2 +1, npar+1); % rank to start taking the pedal value: onset.diff/2 +1
                        pkend = min(npar + (min(chout(i,1,3),nmat(ndim1-p,pn,3))-nmat(ndim1-p,pn,2))/2 +1, ndim3); % rank of last pedal sample before the first of chord offset or pedal event end
                        
                        % Means
                        chout(i,1,12) = chout(i,1,12) + 2*(pkend-pkstart); % Pedal depression duration during chord (possibly summable)
                        chout(i,1,11) = ( chout(i,1,11)*(chout(i,1,12)-(2*(pkend-pkstart))) + mean(nmat(ndim1-p,pn,pkstart:pkend))*(2*(pkend-pkstart)) ) / chout(i,1,4);
                                % Mean pedal depression during chord prorated to account for duration
                        
                        % Full/mid-depression
                        for d=pkstart:pkend
                            if nmat(ndim1-p,pn,d)>pfullthresh, chout(i,1,13) = chout(i,1,13) + 2; % 2 more msecs of fully-depressed pedal
                            elseif nmat(ndim1-p,pn,d)>pmidthresh,
                                chout(i,1,14) = chout(i,1,14) + 2; % 2 more msecs of fully-depressed pedal
                                chout(i,1,15) = chout(i,1,15) + nmat(ndim1-p,pn,d); % One more angle of pedal mid-depression
                            end
                        end
                        
                        % @ Chord onset
                        if nmat(ndim1-p,pn,2)<=chout(i,1,2), 
                            chout(i,1,21) = nmat(ndim1-p,pn,pkstart); % Pedal value at note onset if pedal already on then
                            chout(i,1,22) = 1; % Pedal on at chord onset
                        end
                        chout(i,1,23) = chout(i,1,2) - nmat(ndim1-p,pn,2); % Pedal lead on chord onset
                        
                        % @ Chord offset
                        if nmat(ndim1-p,pn,3)>=chout(i,1,3),
                            chout(i,1,27) = nmat(ndim1-p,pn,pkend); % Pedal value at note offset if pedal still on then
                            chout(i,1,28) = 1; % Pedal on at chord offset
                        end
                        chout(i,1,29) = nmat(ndim1-p,pn,3) - chout(i,1,3); % Pedal trail on chord offset
                        
                    end
                end
                
                if chout(i,1,14)~=0, chout(i,1,15) = 2*chout(i,1,15)/chout(i,1,14); else chout(i,1,15)=0; end % Mean pedal mid-depression
                chout(i,1,13) = max(0,chout(i,1,13)-2); % for meaningfulness purposes
                chout(i,1,14) = max(0,chout(i,1,14)-2);
                
            elseif nmat(ndim1-p,1,1)==111 % Sustain pedal
                for pn=3:nmat(ndim1-p,1,2)+2, % Same code as pedal 109, with adequate assignments in chout
                    if (nmat(ndim1-p,pn,2)<=chout(i,1,3) && nmat(ndim1-p,pn,3)>=chout(i,1,2)), % Pedal overlapping the chord
%                     if (nmat(ndim1-p,pn,2)>=chout(i,1,2) && nmat(ndim1-p,pn,2)<chout(i,1,3)) || (nmat(ndim1-p,pn,3)<=chout(i,1,3) && nmat(ndim1-p,pn,3)>chout(i,1,2)) % pedal event starts before chord offset or ends after chord onset
                        pkstart = max( npar + (max(chout(i,1,2),nmat(ndim1-p,pn,2))-nmat(ndim1-p,pn,2))/2 +1, npar+1); % rank to start taking the pedal value: onset.diff/2 +1
                        pkend = min(npar + (min(chout(i,1,3),nmat(ndim1-p,pn,3))-nmat(ndim1-p,pn,2))/2 +1, ndim3); % rank of last pedal sample before the first of chord offset or pedal event end
                        
                        % Means
                        chout(i,1,17) = chout(i,1,17) + 2*(pkend-pkstart); % Pedal depression duration during chord (possibly summable)
                        chout(i,1,16) = ( chout(i,1,16)*(chout(i,1,17)-(2*(pkend-pkstart))) + mean(nmat(ndim1-p,pn,pkstart:pkend))*(2*(pkend-pkstart)) ) / chout(i,1,4);
                                % Mean pedal depression during chord prorated to account for duration
                        
                        % Full/mid-depression
                        for d=pkstart:pkend
                            if nmat(ndim1-p,pn,d)>pfullthresh, chout(i,1,18) = chout(i,1,18) + 2; % 2 more msecs of fully-depressed pedal
                            elseif nmat(ndim1-p,pn,d)>pmidthresh,
                                chout(i,1,19) = chout(i,1,19) + 2; % 2 more msecs of fully-depressed pedal
                                chout(i,1,20) = chout(i,1,20) + nmat(ndim1-p,pn,d); % One more angle of pedal mid-depression
                            end
                        end
                        
                        % @ Chord onset
                        if nmat(ndim1-p,pn,2)<=chout(i,1,2),
                            chout(i,1,24) = nmat(ndim1-p,pn,pkstart); % Pedal value at note onset if pedal already on then
                            chout(i,1,25) = 1; % Pedal on at chord onset
                        end
                        chout(i,1,26) = chout(i,1,2) - nmat(ndim1-p,pn,2); % Pedal lead on chord onset
                        
                        % @ Chord offset
                        if nmat(ndim1-p,pn,3)>=chout(i,1,3),
                            chout(i,1,30) = nmat(ndim1-p,pn,pkend); % Pedal value at note offset if pedal still on then
                            chout(i,1,31) = 1; % Pedal on at chord offset
                        end
                        chout(i,1,32) = nmat(ndim1-p,pn,3) - chout(i,1,3); % Pedal trail on chord offset
                        
                    end
                end
                
                if chout(i,1,19)~=0, chout(i,1,20) = 2*chout(i,1,20)/chout(i,1,19); else chout(i,1,20)=0; end % Mean pedal mid-depression
                chout(i,1,18) = max(0,chout(i,1,18)-2); % for meaningfulness purposes
                chout(i,1,19) = max(0,chout(i,1,19)-2);
                
            end
            
        end

        
        
        % Intervals between chords
        
        if i<size(chout,1),
            
            % IOI
            chout(i,1,33) = chout(i+1,1,2) - chout(i,1,2); % IOI = next onset - current onset
            chout(i,1,34) = chout(i,1,33)-chout(i,1,4); % OffOnI = IOI-duration = position of next onset / current offset
            chout(i,1,35) = sign(chout(i,1,34)); % OffOnI direction: +/- 1 for staccato/legato resp.
            
            % IOI within hand
            i2=i+1;
            while (i2<=size(chout,1)) && ( (chout(i,2,1)<=threshand && chout(i2,4,1)>threshand) || (chout(i,2,1)>threshand && chout(i2,4,1)<=threshand) ) % Increment i2 while chord(i2) from different hand
                i2=i2+1;
            end
            if i2<=size(chout,1), % There is a same-hand chord after chord(i)
                chout(i,1,36) = chout(i2,1,2) - chout(i,1,2); % IOI within hand = next same-hand onset - current onset
                chout(i,1,37) = chout(i,1,36)-chout(i,1,4); % OffOnI within hand = IOI.w.h.-duration = position of next onset.w.h. / current offset
                chout(i,1,38) = sign(chout(i,1,37)); % OffOnI.w.h. direction: +/- 1 for staccato/legato resp.
            end
            
            % IOI with other hand
            i2=i+1;
            while (i2<=size(chout,1)) && ( (chout(i,2,1)<=threshand && chout(i2,4,1)<=threshand) || (chout(i,2,1)>threshand && chout(i2,4,1)>threshand) ) % Increment i2 while chord(i2) from same hand
                i2=i2+1;
            end
            if i2<=size(chout,1), % There is an other-hand chord after chord(i)
                chout(i,1,39) = chout(i2,1,2) - chout(i,1,2); % IOI.w.o.h. = next other-hand onset - current onset
                chout(i,1,40) = chout(i,1,39)-chout(i,1,4); % OffOnI.w.o.h. = IOI.w.o.h.-duration = position of next onset.w.o.h. / current offset
                chout(i,1,41) = sign(chout(i,1,40)); % OffOnI.w.o.h. direction: +/- 1 for staccato/legato resp.
            end
        end

        
        
        % Overlaps between chords
        
        tmpon = zeros(3,chout(i,1,4)/2+1); % To mark the overlapped timestamps, for any (line 1), same-hand (line 2) and other-hand (line 3) chords
        
        for i2=dmeans+1:size(chout,1)
            if i2~=i && chout(i2,1,2)<=chout(i,1,3) && chout(i2,1,3)>=chout(i,1,2) % On2<=Off1 && Off2>=On1 <=> there is overlap
                
                % Build temp vector of chord i2 notes' depressions corresponding to chord i
                tmpchord = zeros(chout(i2,1,1)+1,chout(i,1,4)/2+1); % One line per note + mean at line 1; one column for each timestamp corresponding to chord i duration
                for jt=4:chout(i2,1,1)+3 % i.e. for each of chord i2's note
                    for ingo=2:ndim1,
                        if nmat(ingo,1,1)==chout(i2,jt,1) % Key match between nmat and chout(i2)
                            for jngo=3:min(nmat(ingo,1,2)+2,ndim2)
                                if nmat(ingo,jngo,2)==chout(i2,jt,2), % Note onset match
                                    
                                    storemin = max(1,(chout(i2,jt,2)-chout(i,1,2))/2+1);
                                        % Start storing at tmpchord's start if chord(i2)'s note(j2) starts before chord(i), else start at tmpchord's rank corresponding to (i2,j2)'s onset delay on chord(i)'s onset
                                    
                                    getmin = max(1,(chout(i,1,2)-chout(i2,jt,2))/2+1); % Basically the opposite of storemin: delay the values to take if chord(i) is first
                                    
                                    getmax = min(ndim3-npar, min(chout(i2,jt,4)/2+1, (chout(i,1,3)-chout(i2,jt,2))/2+1)); % Get until first offset is reached
%                                     getmax = min(chout(i2,jt,4)/2+1, (chout(i,1,3)-chout(i2,jt,2))/2+1);
                                    
                                    storemax = getmax-getmin+storemin; % Rightful size to avoid mismatches
%                                     storemax = min(size(tmpchord,2),(chout(i2,jt,3)-chout(i,1,2))/2+1);
%                                     storemax = min(ndim3-npar, min(size(tmpchord,2),(chout(i2,jt,3)-chout(i,1,2))/2+1));
                                        % Store until: longer than dataset (for matching purposes), or first of offsets(i,i2jt) is reached

                                    % Fill in tmpchord for note jt
                                    tmpchord(jt-3+1,storemin:storemax) = nmat(ingo,jngo,npar+getmin:npar+getmax);
                                    
                                    break;
                                end
                            end
                            break;
                        end
                    end
                end
                if size(tmpchord,1)==2, tmpchord(1,:)=tmpchord(2,:);
                else tmpchord(1,:) = mean(tmpchord(2:size(tmpchord,1),:)); % tmpchord(1,:) is the mean key depression over all of chord(i2)'s notes along the duration of chord(i)
                end
                
                % Scan and fill
                chout(i,1,46) = chout(i,1,46) +1; % One more overlapping chord
                tmpon(1,:) = tmpon(1,:) + (tmpchord(1,:)>0); % Set the new overlapped timestamps
                chout(i,1,44) = chout(i,1,44) + mean(tmpchord(1,:)); % Overlap amount: sum of the mean depression (during chord(i)) of all other overlapping chords
                if chout(i,2,5)~=0, chout(i,1,45) = chout(i,1,44)/chout(i,2,5); else chout(i,1,45)=0; end % Overlap amount rate (/ chord(i)'s Amoy)
                
                % Hands
                if (chout(i,2,1)<=threshand && chout(i2,4,1)<=threshand) || (chout(i,2,1)>threshand && chout(i2,4,1)>threshand) % Chords i & i2 played with same hand
                    
                    chout(i,1,51) = chout(i,1,51) +1; % One more overlapping same-hand chord
                    tmpon(2,:) = tmpon(2,:) + (tmpchord(1,:)>0); % Set the new same-hand overlapped timestamps
                    chout(i,1,49) = chout(i,1,49) + mean(tmpchord(1,:)); % Overlap same-hand amount: sum of the mean depression (during chord(i)) of same-hand overlapping chords
                    if chout(i,2,5)~=0, chout(i,1,50) = chout(i,1,49)/chout(i,2,5); else chout(i,1,50)=0; end % Overlap same-hand amount rate (/ chord(i)'s Amoy)
                else
                    chout(i,1,56) = chout(i,1,56) +1; % One more overlapping other-hand chord
                    tmpon(3,:) = tmpon(3,:) + (tmpchord(1,:)>0); % Set the new other-hand overlapped timestamps
                    chout(i,1,54) = chout(i,1,54) + mean(tmpchord(1,:)); % Overlap other-hand amount: sum of the mean depression (during chord(i)) of other-hand overlapping chords
                    if chout(i,2,5)~=0, chout(i,1,55) = chout(i,1,54)/chout(i,2,5); else chout(i,1,55)=0; end % Overlap other-hand amount rate (/ chord(i)'s Amoy)
                end
                
            end
        end
        
        % Fill
        chout(i,1,42) = max(0,2*sum(tmpon(1,:)>0)-2); % Overlap duration: all timestamps being overlapped by at least another chord
        chout(i,1,43) = chout(i,1,42)/chout(i,1,4); % Overlap rate
        
        chout(i,1,47) = max(0,2*sum(tmpon(2,:)>0)-2); % Overlap duration from same-hand chords
        chout(i,1,48) = chout(i,1,47)/chout(i,1,4); % Overlap rate from same-hand chords
        
        chout(i,1,52) = max(0,2*sum(tmpon(3,:)>0)-2); % Overlap duration from other-hand chords
        chout(i,1,53) = chout(i,1,52)/chout(i,1,4); % Overlap rate from other-hand chords

    end   
    
    
    
    %% ------- GLOBAL STATS ------------------------------------------
    
    % --- MEANS/SDs of CHORD STATS (1:4,[1 4],:) ---
    
    chout(1,1,2) = size(chout,1) - dmeans; % Total number of chords in the file
    for i=dmeans+1:size(chout,1)
        if chout(i,2,1)<=threshand,  chout(2,1,2) = chout(2,1,2) +1; % One more left-hand chord
        else chout(3,1,2) = chout(3,1,2) +1; % One more right-hand chord
        end
    end
    chout(4,1,2) = chout(3,1,2) - chout(2,1,2); % Difference between hands (right-left)
    
    
    for k=4:chpar
        kmrank=0;
        kmleft=0;
        kmright=0;
        tmpvect=zeros(1,1);
        tmpleft=zeros(1,1);
        tmpright=zeros(1,1);
        
        if mchord(k)==1 % non-zero
            for i=dmeans+1:size(chout,1)
                if chout(i,1,k)~=0, 
                    kmrank=kmrank+1; tmpvect(kmrank)=chout(i,1,k); % Store non-zero values in tmp vector
                    
                    if chout(i,2,1)<=threshand, kmleft=kmleft+1; tmpleft(kmleft)=chout(i,1,k); % non-zero left-hand values
                    else kmright=kmright+1; tmpright(kmright)=chout(i,1,k); % non-zero right-hand values
                    end
                end
            end
            chout(1,1,k) = mean(tmpvect); % Mean of non-zero values
            chout(1,4,k) = std(tmpvect); % SD of non-zero values
            
            chout(2,1,k) = mean(tmpleft); % Mean of left-hand non-zero values
            chout(2,4,k) = std(tmpleft); % SD of left-hand non-zero values
            chout(3,1,k) = mean(tmpright); % Mean of right-hand non-zero values
            chout(3,4,k) = std(tmpright); % SD of right-hand non-zero values
            chout(4,1,k) = chout(3,1,k) - chout(2,1,k); % Difference between hands (right-left)
            chout(4,4,k) = chout(3,4,k) - chout(2,4,k); % Difference between hands (right-left)
            
            
        elseif mchord(k)==2 % all
            chout(1,1,k) = mean(chout(dmeans+1:size(chout,1),1,k)); % straight-up mean incl. zeros
            chout(1,4,k) = std(chout(dmeans+1:size(chout,1),1,k)); % straight-up SD incl. zeros
            
            for i=dmeans+1:size(chout,1)
                if chout(i,2,1)<=threshand, kmleft=kmleft+1; tmpleft(kmleft)=chout(i,1,k); % left-hand values
                else kmright=kmright+1; tmpright(kmright)=chout(i,1,k); % right-hand values
                end
            end
            
            chout(2,1,k) = mean(tmpleft); % Mean of left-hand values
            chout(2,4,k) = std(tmpleft); % SD of left-hand values
            chout(3,1,k) = mean(tmpright); % Mean of right-hand values
            chout(3,4,k) = std(tmpright); % SD of right-hand values
            chout(4,1,k) = chout(3,1,k) - chout(2,1,k); % Difference between hands (right-left)
            chout(4,4,k) = chout(3,4,k) - chout(2,4,k); % Difference between hands (right-left)

            
        elseif mchord(k)==4 % multi-note chords
            for i=dmeans+1:size(chout,1)
                if chout(i,1,1)>1,
                    kmrank=kmrank+1; tmpvect(kmrank)=chout(i,1,k); % Store multi-note chords values in tmp vector
                    
                    if chout(i,2,1)<=threshand, kmleft=kmleft+1; tmpleft(kmleft)=chout(i,1,k); % multi-note chords left-hand values
                    else kmright=kmright+1; tmpright(kmright)=chout(i,1,k); % multi-note chords right-hand values
                    end
                    
                end
            end
            chout(1,1,k) = mean(tmpvect); % Mean of multi-note chords values
            chout(1,4,k) = std(tmpvect); % SD of multi-note chords values
            
            chout(2,1,k) = mean(tmpleft); % Mean of multi-note chords left-hand values
            chout(2,4,k) = std(tmpleft); % SD of multi-note chords left-hand values
            chout(3,1,k) = mean(tmpright); % Mean of multi-note chords right-hand values
            chout(3,4,k) = std(tmpright); % SD of multi-note chords right-hand values
            chout(4,1,k) = chout(3,1,k) - chout(2,1,k); % Difference between hands (right-left)
            chout(4,4,k) = chout(3,4,k) - chout(2,4,k); % Difference between hands (right-left)
            
            
        elseif mchord(k)==5 % but for last chord
            chout(1,1,k) = mean(chout(dmeans+1:size(chout,1)-1,1,k)); % straight-up mean without last chord
            chout(1,4,k) = std(chout(dmeans+1:size(chout,1)-1,1,k)); % straight-up SD without last chord
            
            for i=dmeans+1:size(chout,1)-1 % Without last chord
                if chout(i,2,1)<=threshand, kmleft=kmleft+1; tmpleft(kmleft)=chout(i,1,k); % left-hand values
                else kmright=kmright+1; tmpright(kmright)=chout(i,1,k); % right-hand values
                end
                
                chout(2,1,k) = mean(tmpleft); % Mean of left-hand values
                chout(2,4,k) = std(tmpleft); % SD of left-hand values
                chout(3,1,k) = mean(tmpright); % Mean of right-hand values
                chout(3,4,k) = std(tmpright); % SD of right-hand values
                chout(4,1,k) = chout(3,1,k) - chout(2,1,k); % Difference between hands (right-left)
                chout(4,4,k) = chout(3,4,k) - chout(2,4,k); % Difference between hands (right-left)
                
                
            end
            
        end
    end
    
    
    % --- MEANS/SDs of CHORDS' NOTESTATS (1:4,[2 3 5 6],:) ---
    
    chout(1,2,2) = sum(chout(dmeans+1:size(chout,1),1,1)); % Total number of notes in the file
    chout(1,2,3) = mean(chout(dmeans+1:size(chout,1),1,1)); % Mean number of notes per chord
    chout(1,5,3) = std(chout(dmeans+1:size(chout,1),1,1)); % SD of number of notes per chord
    
    kmleft=0;
    kmright=0;
    tmpleft=zeros(1,1);
    tmpright=zeros(1,1);
    
    for i=dmeans+1:size(chout,1)
        if chout(i,2,1)<=threshand, kmleft=kmleft+1; tmpleft(kmleft)=chout(i,1,1); % Store one more left-hand series of notes            
        else kmright=kmright+1; tmpright(kmright)=chout(i,1,1); % Store one more right-hand series of notes
        end
    end
    
    chout(2,2,2) = sum(tmpleft); % Total number of notes played left-hand
    chout(3,2,2) = sum(tmpright); % Total number of notes played right-hand
    chout(4,2,2) = chout(3,2,2) - chout(2,2,2); % Difference between hands (right-left)
    
    chout(2,2,3) = mean(tmpleft); % Mean number of notes per left-hand chord
    chout(3,2,3) = mean(tmpright); % Mean number of notes per right-hand chord
    chout(4,2,3) = chout(3,2,3) - chout(2,2,3); % Difference between hands (right-left)
    
    chout(2,5,3) = std(tmpleft); % SD of number of notes played left-hand
    chout(3,5,3) = std(tmpright); % SD of number of notes played right-hand
    chout(4,5,3) = chout(3,5,2) - chout(2,5,2); % Difference between hands (right-left)

    
    
    for k=4:npar2
        kmrank=0;
        kmleft=0;
        kmright=0;
        tmpvect=zeros(2,1);
        tmpleft=zeros(2,1);
        tmpright=zeros(2,1);
        
        if mkey(k)==1, % non-zero
            for i=dmeans+1:size(chout,1)
                if chout(i,2,k)~=0,
                    kmrank=kmrank+1; tmpvect(1,kmrank)=chout(i,2,k); tmpvect(2,kmrank)=chout(i,3,k); % Store non-zero values in tmp vector
                    
                    if chout(i,2,1)<=threshand, kmleft=kmleft+1; tmpleft(1,kmleft)=chout(i,2,k); tmpleft(2,kmleft)=chout(i,3,k); % non-zero left-hand values
                    else kmright=kmright+1; tmpright(1,kmright)=chout(i,2,k); tmpright(2,kmright)=chout(i,3,k); % non-zero right-hand values
                    end
                end
            end
            chout(1,2,k) = mean(tmpvect(1,:)); % Mean of non-zero values for notes' mean
            chout(1,3,k) = mean(tmpvect(2,:)); % Mean of non-zero values for notes' SD
            chout(1,5,k) = std(tmpvect(1,:)); % SD of non-zero values for notes' means
            chout(1,6,k) = std(tmpvect(2,:)); % SD of non-zero values for notes' SD
            
            chout(2,2,k) = mean(tmpleft(1,:)); % Mean of left-hand non-zero values for notes' mean
            chout(2,3,k) = mean(tmpleft(2,:)); % Mean of left-hand non-zero values for notes' SD
            chout(2,5,k) = std(tmpleft(1,:)); % SD of left-hand non-zero values for notes' mean
            chout(2,6,k) = std(tmpleft(2,:)); % SD of left-hand non-zero values for notes' SD
            
            chout(3,2,k) = mean(tmpright(1,:)); % Mean of right-hand non-zero values for notes' mean
            chout(3,3,k) = mean(tmpright(2,:)); % Mean of right-hand non-zero values for notes' SD
            chout(3,5,k) = std(tmpright(1,:)); % SD of right-hand non-zero values for notes' mean
            chout(3,6,k) = std(tmpright(2,:)); % SD of right-hand non-zero values for notes' SD
            
            chout(4,2,k) = chout(3,2,k) - chout(2,2,k); % Difference between hands (right-left)
            chout(4,3,k) = chout(3,3,k) - chout(2,3,k); % Difference between hands (right-left)
            chout(4,5,k) = chout(3,5,k) - chout(2,5,k); % Difference between hands (right-left)
            chout(4,6,k) = chout(3,6,k) - chout(2,6,k); % Difference between hands (right-left)
            
            
        elseif mkey(k)==2, % all
            chout(1,2,k) = mean(chout(dmeans+1:size(chout,1),2,k)); % straight-up mean of mean incl. zeros
            chout(1,3,k) = mean(chout(dmeans+1:size(chout,1),3,k)); % straight-up mean of SD incl. zeros
            chout(1,5,k) = std(chout(dmeans+1:size(chout,1),2,k)); % straight-up SD of mean incl. zeros
            chout(1,6,k) = std(chout(dmeans+1:size(chout,1),3,k)); % straight-up SD of SD incl. zeros
            
            for i=dmeans+1:size(chout,1)
                if chout(i,2,1)<=threshand, kmleft=kmleft+1; tmpleft(1,kmleft)=chout(i,2,k); tmpleft(2,kmleft)=chout(i,3,k); % left-hand values
                else kmright=kmright+1; tmpright(1,kmright)=chout(i,2,k); tmpright(2,kmright)=chout(i,3,k); % right-hand values
                end
            end
            
            chout(2,2,k) = mean(tmpleft(1,:)); % Mean of left-hand non-zero values for notes' mean
            chout(2,3,k) = mean(tmpleft(2,:)); % Mean of left-hand non-zero values for notes' SD
            chout(2,5,k) = std(tmpleft(1,:)); % SD of left-hand non-zero values for notes' mean
            chout(2,6,k) = std(tmpleft(2,:)); % SD of left-hand non-zero values for notes' SD
            
            chout(3,2,k) = mean(tmpright(1,:)); % Mean of right-hand non-zero values for notes' mean
            chout(3,3,k) = mean(tmpright(2,:)); % Mean of right-hand non-zero values for notes' SD
            chout(3,5,k) = std(tmpright(1,:)); % SD of right-hand non-zero values for notes' mean
            chout(3,6,k) = std(tmpright(2,:)); % SD of right-hand non-zero values for notes' SD
            
            chout(4,2,k) = chout(3,2,k) - chout(2,2,k); % Difference between hands (right-left)
            chout(4,3,k) = chout(3,3,k) - chout(2,3,k); % Difference between hands (right-left)
            chout(4,5,k) = chout(3,5,k) - chout(2,5,k); % Difference between hands (right-left)
            chout(4,6,k) = chout(3,6,k) - chout(2,6,k); % Difference between hands (right-left)
            
        elseif mkey(k)==3, % non-mhv=zero
            for i=dmeans+1:size(chout,1)
                if chout(i,2,9)~=0,
                    kmrank=kmrank+1; tmpvect(1,kmrank)=chout(i,2,k); tmpvect(2,kmrank)=chout(i,3,k); % Store non-mhv=zero values in tmp vector
                    
                    if chout(i,2,1)<=threshand, kmleft=kmleft+1; tmpleft(1,kmleft)=chout(i,2,k); tmpleft(2,kmleft)=chout(i,3,k); % non-mhv=zero left-hand values
                    else kmright=kmright+1; tmpright(1,kmright)=chout(i,2,k); tmpright(2,kmright)=chout(i,3,k); % non-mhv=zero right-hand values
                    end
                end
            end
            chout(1,2,k) = mean(tmpvect(1,:)); % Mean of non-mhv=zero values for notes' mean
            chout(1,3,k) = mean(tmpvect(2,:)); % Mean of non-mhv=zero values for notes' SD
            chout(1,5,k) = std(tmpvect(1,:)); % SD of non-mhv=zero values for notes' means
            chout(1,6,k) = std(tmpvect(2,:)); % SD of non-mhv=zero values for notes' SD
            
            chout(2,2,k) = mean(tmpleft(1,:)); % Mean of left-hand non-mhv=zero values for notes' mean
            chout(2,3,k) = mean(tmpleft(2,:)); % Mean of left-hand non-mhv=zero values for notes' SD
            chout(2,5,k) = std(tmpleft(1,:)); % SD of left-hand non-mhv=zero values for notes' mean
            chout(2,6,k) = std(tmpleft(2,:)); % SD of left-hand non-mhv=zero values for notes' SD
            
            chout(3,2,k) = mean(tmpright(1,:)); % Mean of right-hand non-mhv=zero values for notes' mean
            chout(3,3,k) = mean(tmpright(2,:)); % Mean of right-hand non-mhv=zero values for notes' SD
            chout(3,5,k) = std(tmpright(1,:)); % SD of right-hand non-mhv=zero values for notes' mean
            chout(3,6,k) = std(tmpright(2,:)); % SD of right-hand non-mhv=zero values for notes' SD
            
            chout(4,2,k) = chout(3,2,k) - chout(2,2,k); % Difference between hands (right-left)
            chout(4,3,k) = chout(3,3,k) - chout(2,3,k); % Difference between hands (right-left)
            chout(4,5,k) = chout(3,5,k) - chout(2,5,k); % Difference between hands (right-left)
            chout(4,6,k) = chout(3,6,k) - chout(2,6,k); % Difference between hands (right-left)
            
            
        end
        
    end
    
    
    
    % ------- OUTPUT ------------------------------------------
    
    % Labels
    % --- Means' lines labels
    til={'All notes','';'Left-hand','';'Right-hand','';'Diff. b/w hands',''};
    tilc={'Chords mean','Chords SD';'Left-hand mean','Left-hand SD';'Right-hand mean','Right-hand SD';'Diff. b/w hands: mean','Diff. b/w hands: SD'};
    
    % --- Define columns' labels: chord features then notes-per-chord means then SD
    titles={'#','Number of notes','Chord onset','Chord offset','Chord duration','max Amax', 'max MHV','Melody lead','Melody lead rate','Last-note trail','Last-note trail rate',...
        'Soft pedal depression/chord','Soft pedal duration/chord','Soft pedal full-depression length/chord','Soft pedal mid-depression length/chord','Soft pedal mid-depression level/chord',...
        'Sustain pedal depression/chord','Sustain pedal duration/chord','Sustain pedal full-depression length/chord','Sustain pedal mid-depression length/chord','Sustain pedal mid-depression level/chord',...
        'Soft pedal @onset','Soft pedal bin. @onset','Soft pedal lead @onset','Sustain pedal @onset','Sustain pedal bin. @onset','Sustain pedal lead @onset',...
        'Soft pedal @offset','Soft pedal bin. @offset','Soft pedal trail @offset','Sustain pedal @offset','Sustain pedal bin. @offset','Sustain pedal trail @offset',...
        'IOI','OffOnI','OffOnI direction','IOI same-hand','OffOnI same-hand','OffOnI direction same-hand','IOI other-hand','OffOnI other-hand','OffOnI direction other-hand',...
        'Overlap length','Overlap length rate','Overlap amount','Overlap amount rate','Number of overlaps',...
        'Overlap length same-hand','Overlap length rate same-hand','Overlap amount same-hand','Overlap amount rate same-hand','Number of overlaps same-hand',...
        'Overlap length other-hand','Overlap length rate other-hand','Overlap amount other-hand','Overlap amount rate other-hand','Number of overlaps other-hand',...
        'Key','Note onset','Note offset','Note duration','Amoy','Amax','t.Amax','MHV','t.MHV',...
        'Speed of attack','Slope of attack','Speed of key attack','Slope of key attack','Directness MHV-Amax','MHV/Amax','Sharpness of key attack slope','Sharpness of attack slope',...
        'FKD start','FKD end','FKD duration','FKD rate','Pre-FKD duration','Pre-FKD rate','Post-FKD duration','Post-FKD rate',...
        't.attack.end','t.decay.end','t.release.start','Attack duration','Sustain duration','Release duration','Attack rate','Sustain rate','Release rate',...
        'Soft pedal depression', 'Sustain pedal depression', 'Soft pedal @note.onset','Sustain pedal @note.onset', 'Soft pedal @MHV', 'Sustain pedal @MHV', 'Soft pedal @note.offset', 'Sustain pedal @note.offset',...
        'Soft pedal depression length','Sustain pedal depression length','Key attack mean depression','Key attack mean depression rate',...
        'Note onset lag','Note onset lag rate','Note onset lag amount','Note onset lag amount rate','Note offset lead','Note offset lead rate','Note offset lead amount','Note offset lead amount rate','Sync rate','Sync amount rate'};
    
    % --- Define notes columns' labels: notes features
    ntitles={'#','Key','Note onset','Note offset','Note duration','Amoy','Amax','t.Amax','MHV','t.MHV',...
        'Speed of attack','Slope of attack','Speed of key attack','Slope of key attack','Directness MHV-Amax','MHV/Amax','Sharpness of key attack slope','Sharpness of attack slope',...
        'FKD start','FKD end','FKD duration','FKD rate','Pre-FKD duration','Pre-FKD rate','Post-FKD duration','Post-FKD rate',...
        't.attack.end','t.decay.end','t.release.start','Attack duration','Sustain duration','Release duration','Attack rate','Sustain rate','Release rate',...
        'Soft pedal depression', 'Sustain pedal depression', 'Soft pedal @note.onset','Sustain pedal @note.onset', 'Soft pedal @MHV', 'Sustain pedal @MHV', 'Soft pedal @note.offset', 'Sustain pedal @note.offset',...
        'Soft pedal depression length','Sustain pedal depression length','Key attack mean depression','Key attack mean depression rate',...
        'Note onset lag','Note onset lag rate','Note onset lag amount','Note onset lag amount rate','Note offset lead','Note offset lead rate','Note offset lead amount','Note offset lead amount rate','Sync rate','Sync amount rate'};


    if sep,
        chords = zeros(size(chout,1)+4,chpar+2*npar2); % (dmeans*2+chords) * features(:chord's + its notes' mean+SD); dmeans*2 => Mean, SD, left-hand mean, left-hand SD, right-hand mean, right-hand SD, hand mean diff., hand SD diff.

        for i=1:4
            chords(2*i-1,1:chpar) = chout(i,1,1:chpar); % Means of chords' features
            chords(2*i-1,chpar+1:chpar+npar2) = chout(i,2,1:npar2); % Means of chords' notes' mean
            chords(2*i-1,chpar+npar2+1:chpar+2*npar2) = chout(i,3,1:npar2); % Means of chords' notes' SD
            
            chords(2*i,1:chpar) = chout(i,4,1:chpar); % SDs of chords' features
            chords(2*i,chpar+1:chpar+npar2) = chout(i,5,1:npar2); % SDs of chords' notes' mean
            chords(2*i,chpar+npar2+1:chpar+2*npar2) = chout(i,5,1:npar2); % SDs of chords' notes' SD
        end
                
        for i=4:size(chout,1), 
            chords(i+4,1:chpar) = chout(i,1,1:chpar);  % Chords' features
            chords(i+4,chpar+1:chpar+npar2) = chout(i,2,1:npar2); % Chord's mean notes features
            chords(i+4,chpar+npar2+1:chpar+2*npar2) = chout(i,3,1:npar2); % SD of chord's notes features
        end
        
                
        note = zeros(chout(1,2,2)+2*dmeans,npar2+1); % (dmeans*2+each note) * ('Chord #' + note features*2[mean+SD])
        
        for i=1:dmeans
            note(2*i-1,2:npar2+1) = chout(i,2,1:npar2); % Mean of notes' mean-per-chord
            note(2*i-1,npar2+2:2*npar2+1) = chout(i,5,1:npar2); % SD of notes' mean-per-chord
            note(2*i,2:npar2+1) = chout(i,3,1:npar2); % Mean of notes' SD-per-chord
            note(2*i,npar2+2:2*npar2+1) = chout(i,6,1:npar2); % SD of notes' SD-per-chord
        end
        
        nrank=0; % rank to store note within 'notes'
        for i=2*dmeans+1:size(chout,1), % Scan chords
            for j=4:chout(i,1,1)+3 % Scan each note within the chord
                nrank=nrank+1;
                note(nrank,1) = i-8; % Chord #
                note(nrank,2:npar2+1) = chout(i,j,1:npar2); % Note's features
            end
        end
        
        if printout,
            
            % --- Name the output files ---
            if isempty(nameout),
            if ~ischar(filename), filename=inputname(1); end
            else filename=nameout;
            end
            
            if strcmp(filename(1:2),'nt'), 
                outchords=['chords.only' filename(3:length(filename)) '.xls'];
                outnotes=['chords.notes' filename(3:length(filename)) '.xls'];
            else
                outchords=['chords.only ' filename '.xls'];
                outnotes=['chords.notes ' filename '.xls'];
            end
            
            
            % --- CHORDS-ONLY OUTPUT ---
            
            fid1 = fopen(outchords,'w+');
            for i=1:length(titles), fprintf(fid1, '%s \t', cell2mat(titles(i))); end
            
            % --- Means and SDs ---
            
            for i=1:dmeans, % 4 diff. means
                fprintf(fid1,'\n %s\t\t',cell2mat(til(i,1))); % Title in empty/useless first column
                for k=2:chpar, fprintf(fid1,'%.4f\t',chout(i,1,k)); end % Chords mean
                fprintf(fid1,'Mean of notes mean\t');
                for k=2:npar2, fprintf(fid1,'%.4f\t',chout(i,2,k)); end
                fprintf(fid1,'\n\t\t');
                for k=2:chpar, fprintf(fid1,'\t'); end
                fprintf(fid1,'Mean of notes SD\t');
                for k=2:npar2, fprintf(fid1,'%.4f\t',chout(i,3,k)); end
                
                fprintf(fid1,'\n %s\t\t',cell2mat(til(i,2)));
                for k=2:chpar, fprintf(fid1,'%.4f\t',chout(i,4,k)); end % Chords SD
                fprintf(fid1,'SD of notes mean\t');
                for k=2:npar2, fprintf(fid1,'%.4f\t',chout(i,5,k)); end
                fprintf(fid1,'\n\t\t');
                for k=2:chpar, fprintf(fid1,'\t'); end
                fprintf(fid1,'SD of notes SD\t');
                for k=2:npar2, fprintf(fid1,'%.4f\t',chout(i,6,k)); end
                fprintf(fid1,'\n');
            end

            % --- All Chords ---
            
            for i=dmeans+1:size(chout,1)
                fprintf(fid1,'\n\n%d\t',i-4); % Chord #
                for k=1:chpar, fprintf(fid1,'%.4f\t',chout(i,1,k)); end % Chord's features
                for k=1:npar2, fprintf(fid1,'%.4f\t',chout(i,2,k)); end % Chord's notes features mean
                fprintf(fid1,'\n\t');
                for k=1:npar2, fprintf(fid1,'%.4f\t',chout(i,3,k)); end % Chord's notes features SD
                for k=1:chpar, fprintf(fid1,'\t'); end
            end
            
            fclose(fid1);
            
            
            % --- NOTES-FROM-CHORDS OUTPUT ---
            
            fid2 = fopen(outnotes,'w+');
            for i=1:length(ntitles), fprintf(fid1, '%s \t', cell2mat(ntitles(i))); end

            % --- Means and SDs ---
            
            for i=1:dmeans, % 4 diff. means
                fprintf(fid2,'\n %s\tMean of mean-per-chord\t',cell2mat(til(i,1)));
                for k=3:npar2+1, fprintf(fid2,'%.4f\t',note(2*i-1,k)); end
                fprintf(fid2,'\n\tSD of mean-per-chord\t');
                for k=npar2+3:2*npar2+1, fprintf(fid2,'%.4f\t',note(2*i-1,k)); end
                
                fprintf(fid2,'\n %s\tMean of SD-per-chord\t',cell2mat(til(i,2)));
                for k=3:npar2+1, fprintf(fid2,'%.4f\t',note(2*i,k)); end
                fprintf(fid2,'\n\tSD of SD-per-chord\t');
                for k=npar2+3:2*npar2+1, fprintf(fid2,'%.4f\t',note(2*i,k)); end
                fprintf(fid2,'\n');
            end
            
            % --- All notes ---
            for i=2*dmeans+1:size(note,1)
                fprintf(fid2,'\n');
                for k=1:npar2+1, fprintf(fid2,'%.4f\t',note(i,k)); end
            end
            
            fclose(fid2);
            
            
            
        end
        
    else chords = chout;
        if printout,
            
            % --- Name the output file
            if isempty(nameout),
            if ~ischar(filename), filename=inputname(1); end
            else filename=nameout;
            end
            
            if strcmp(filename(1:2),'nt'), outname=['chords' filename(3:length(filename)) '.xls'];
            else outname=['chords ' filename '.xls'];
            end
            
            fid = fopen(outname,'w+');
            for i=1:length(titles), fprintf(fid, '%s \t', cell2mat(titles(i))); end
               
            % --- Means and SDs ---

            for i=1:dmeans, % 4 diff. means
                fprintf(fid,'\n %s\t\t',cell2mat(tilc(i,1))); % Title in empty/useless first column
                for k=2:chpar, fprintf(fid,'%.4f\t',chout(i,1,k)); end % Chords mean
                fprintf(fid,'Mean of notes mean\t');
                for k=2:npar2, fprintf(fid,'%.4f\t',chout(i,2,k)); end
                fprintf(fid,'\n\t\t');
                for k=2:chpar, fprintf(fid,'\t'); end
                fprintf(fid,'Mean of notes SD\t');
                for k=2:npar2, fprintf(fid,'%.4f\t',chout(i,3,k)); end
                
                fprintf(fid,'\n %s\t\t',cell2mat(tilc(i,2)));
                for k=2:chpar, fprintf(fid,'%.4f\t',chout(i,4,k)); end % Chords SD
                fprintf(fid,'SD of notes mean\t');
                for k=2:npar2, fprintf(fid,'%.4f\t',chout(i,5,k)); end
                fprintf(fid,'\n\t\t');
                for k=2:chpar, fprintf(fid,'\t'); end
                fprintf(fid,'SD of notes SD\t');
                for k=2:npar2, fprintf(fid,'%.4f\t',chout(i,6,k)); end
                fprintf(fid,'\n');
             end
            
            % --- All Chords ---
            
            for i=dmeans+1:size(chout,1)
                fprintf(fid,'\n\n%d\t',i-4); % Chord #
                for k=1:chpar, fprintf(fid,'%.4f\t',chout(i,1,k)); end % Chord's features
                for k=1:npar2, fprintf(fid,'%.4f\t',chout(i,2,k)); end % Chord's notes features mean
                for j=3:chout(i,1,1)+3 % Notes features SD + each note's features
                    fprintf(fid,'\n\t');
                    for k=1:chpar, fprintf(fid,'\t'); end
                    for k=1:npar2, fprintf(fid,'%.4f\t',chout(i,j,k)); end % Chord's note features
                end
            end
            
            fclose(fid);
        end
    end
    
end