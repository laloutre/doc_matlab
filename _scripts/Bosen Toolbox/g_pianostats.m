function g_pianostats(nmat,chmat)

% GUI Launcher
% One-performance version
% Data visualization : value of selected descriptors for each chord, evolution in time besides pianoroll
% INPUT: notetime matrix or file & correponding chord matrix
%

if nargin<2,chmat=[]; end

% PARAMETERS
npar = 46; % Original number of parameters gathered for each note, by itself
npar2 = npar + 10; % Total number of parameters gathered for each note: by itself + by comparison with other notes in the same chord
chpar = 56; % Total number of parameters gathered for each chord
dmeans = 4; % Number of different means and SDs calculated over the whole file: (mean + SD) of all chords (with some features only multi-notes), left hand, right hand and difference thereof
threshand = [58 64 64 55]; % Note threshold to separate hands for each of the four pieces

[chvars nvars] = varnames;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Features selection GUI

% ScreenSize is a four-element vector: [left, bottom, width, height]:
scrsz = get(0,'ScreenSize');

% Configure display for 4 columns

colw = min(250,fix((scrsz(3)-40)/4)); % Column width
bestwidth = 20 + 4*colw; % Width for 4 columns

nperc = max(fix(chpar/2),fix(npar2/2))+1; % Number of checkboxes per column, i.e. half of chords' or notes' feats
caseheight = min(20,(scrsz(4)-160)/nperc); % Height for one checkbox (20 or less if lack of space)
bestheight = 150 + caseheight*nperc; % Column height

% Figure
h0 = figure('Color',[1,1,1],'Name','Selection','NumberTitle','off','Position',[20 50 bestwidth bestheight]);

% Fill
% Titles
uicontrol('Style','text','String','Please select the features to display','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',16,'Position',[1 bestheight-25 bestwidth 24]);
uicontrol('Style','text','String',' <--- Chord features --->','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',12,'Position',[80 bestheight-120 colw-30 20]);
uicontrol('Style','text','String','||','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',14,'Position',[2*colw-10 bestheight-120 20 20]);
uicontrol('Style','text','String',' <--- Notes features --->','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',12,'Position',[2*colw+80 bestheight-120 colw-30 20]);

% Checkboxes for each feature
for i=1:chpar
    chcol = fix(i/nperc); % # of columns already filled
    chval(i) = uicontrol('Style','checkbox','String',chvars{i+1},'FontSize',round(caseheight/2),'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[10+colw*chcol bestheight-150-caseheight*(i-1-chcol*nperc+(chcol>=1)) colw-10 caseheight],'Callback',{@Click_Callback});
end

for i=1:npar2
    ncol = fix(i/nperc); % # of columns alrady filled
    nval(i) = uicontrol('Style','checkbox','String',nvars{i+1},'FontSize',round(caseheight/2),'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[10+colw*(ncol+2) bestheight-150-caseheight*(i-1-ncol*nperc+(ncol>=1)) colw-10 caseheight],'Callback',{@Click_Callback});
end

% Options
ebar = uicontrol('Style','checkbox','String','Display errorbars','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[3*colw+40 bestheight-40 250 20],'Callback',{@Click_Callback});

piesplit = uicontrol('Style','checkbox','String','Split hands','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[40 bestheight-40 120 20],'Callback',{@Click_Callback});
pienum = uicontrol('Style','popupmenu','String',{'Piece ?','Piece #1','Piece #2','Piece #3','Piece #4'},'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',10,'Position',[160 bestheight-40 100 20],'Callback',{@Pnum_Callback});
leftonly = uicontrol('Style','checkbox','String','Left only','FontSize',10,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[60 bestheight-65 120 20],'Callback',{@Click_Callback});
rightonly = uicontrol('Style','checkbox','String','Right only','FontSize',10,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[60 bestheight-85 120 20],'Callback',{@Click_Callback});

% Go
uicontrol('Style','pushbutton','String','Go','FontSize',16,'Position',[round(bestwidth/2-50) bestheight-80 80 40],'FontWeight','bold','Callback',{@Go_Callback,nmat,chmat,dmeans,chpar,npar2,chvars,nvars,chval,nval,h0,ebar,piesplit,pienum,threshand,leftonly,rightonly});

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% CALLBACKS

% --- For features and options checkboxes ---

function Click_Callback(hObject,eventdata)
end

function Pnum_Callback(hObject,eventdata)
flag_pnum = get(hObject,'Value');
switch flag_pnum;
    case 'Piece #1'
    case 'Piece #2'
    case 'Piece #3'
    case 'Piece #4'
end

end

% --- For launching the plotting function ---

function Go_Callback(src,eventdata,nmat,chmat,dmeans,chpar,npar2,chvars,nvars,chval,nval,h0,ebar,piesplit,pienum,threshand,leftonly,rightonly)

for i=1:length(chval), chsel(i) = get(chval(i),'Value'); end % 1 if feature i is selected (checked box), else 0
for i=1:length(nval), nsel(i) = get(nval(i),'Value'); end

ebarred = get(ebar,'Value');

splitit = get(piesplit,'Value');
piece = get(pienum,'Value') -1; % Minus 1 so as p01 <=> 1, etc.
lefton = get(leftonly,'Value');
righton = get(rightonly,'Value');


%%%%%%
% If to close previously open plots
% fion = get(0,'Children');
% if length(fion)>1, close(find(fion~=1)); end
%%%%%%

if (sum(chsel)+sum(nsel)), % Are any features selected?
    plotit(nmat,chmat,dmeans,chpar,npar2,chvars,nvars,chsel,nsel,h0,ebarred,splitit,piece,threshand,lefton,righton);
else
    disp('Please select at least one feature to plot');
end
end


% --- For going back to selection ---

function Select_Callback(hObject,eventdata,h0,nmat,chmat)

if ishandle(h0), figure(h0); % Selection window open ? Then put forward
    
else g_pianostats(nmat,chmat); % Else reopen it anew
end

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% PLOTTING

function plotit(nmat,chmat,dmeans,chpar,npar2,chvars,nvars,chsel,nsel,h0,ebarred,splitit,piece,threshand,lefton,righton)

if ischar(nmat), nmat = dlmread(nmat); end

% --- New figure ---

scrsz = get(0,'ScreenSize');
h = figure('Color',[1,1,1],'Name','Display','NumberTitle','off','Position',[10 50 scrsz(3)-20 scrsz(4)-150]);
uicontrol('Style','pushbutton','String','Selection','FontSize',12,'Position',[10 10 120 30],'Callback',{@Select_Callback,h0,nmat,chmat});


% --- First: pianoroll ---

h1 = subplot(3,1,1:2);
pianoroll(nmat,'chords',chmat);


% --- Second: stats plot ---

h2 = subplot(3,1,3);

% Design ---

% Define X-axis dimension
xdims = get(h1,'XLim');
set(h2,'XLim',xdims);

% Bind x-zoom between the two subplots
% find all axes handle of type 'axes'
haxes = findobj(h,'type','axes');
linkaxes(haxes,'x');

% Colors
nfeats = sum(chsel)+sum(nsel); % Number of features to display, i.e. non-zero selections in chords and notes feats

color = [0 0 1;0 1 0;1 0 0;0 1 1;1 0 1]; % 5 basic
if nfeats>length(color), % Add colors to fit all features
    for i=length(color)+1:nfeats, color(i,:) = color(i-5,:)./2; end
end

colorank = 1;

hold on;


% Actual plot ---

if splitit && piece % With chords split by hand
    
    % Method: use tmp vector to be then split by hand
    
    pthresh = threshand(piece);
    tmp(:,1) = chmat(dmeans+1:size(chmat,1),1,2); % Chord onsets to recoup later
    tmp(:,3) = chmat(dmeans+1:size(chmat,1),2,1); % Key # means to assign to the righful hand
    
    for i=1:chpar, % General chords features
        
        if chsel(i), % Selected 
            
            tmp(:,2) = zscore(chmat(dmeans+1:size(chmat,1),1,i)); % Set feature z-scored vals in tmp, and now, split:
            
            if ~righton, % Left hand
                plot(tmp(tmp(:,3)<=pthresh,1),tmp(tmp(:,3)<=pthresh,2),'--v','Color',color(colorank,:));
            end
            if ~lefton, % Right hand
                plot(tmp(tmp(:,3)>pthresh,1),tmp(tmp(:,3)>pthresh,2),':^','Color',color(colorank,:));
            end
            
            legendnames{colorank} = chvars{i+1};
            colorank=colorank+1;
        end
    end
    
    for i=1:npar2, % Mean chords' notes features
        
        if nsel(i), % Selected
            
            tmp(:,2) = zscore(chmat(dmeans+1:size(chmat,1),2,i)); % Set feature z-scored vals in tmp, and now, split:
            
            if ~righton, % Left hand
                plot(tmp(tmp(:,3)<=pthresh,1),tmp(tmp(:,3)<=pthresh,2),'--v','Color',color(colorank,:));
            end
            if ~lefton, % Right hand
                plot(tmp(tmp(:,3)>pthresh,1),tmp(tmp(:,3)>pthresh,2),':^','Color',color(colorank,:));
            end
            
            if ebarred, % Option to plot SDs
                
                tmp(:,4) = (zscore(chmat(dmeans+1:size(chmat,1),2,i)+chmat(dmeans+1:size(chmat,1),3,i))-zscore(chmat(dmeans+1:size(chmat,1),2,i)-chmat(dmeans+1:size(chmat,1),3,i)) )./2;
                % Add feat's SD in tmp
                % tmp(:,4) = (zscore(val+SD)-zscore(val-SD))/2, approximation...
                
                if ~righton, % Left hand
                    errorbar(tmp(tmp(:,3)<=pthresh,1),tmp(tmp(:,3)<=pthresh,2),tmp(tmp(:,3)<=pthresh,4),'--','Color',color(colorank,:));
                end
                if ~lefton, % Right hand
                    errorbar(tmp(tmp(:,3)>pthresh,1),tmp(tmp(:,3)>pthresh,2),tmp(tmp(:,3)>pthresh,4),':','Color',color(colorank,:));
                end
            end
            
            legendnames{colorank} = nvars{i+1};
            colorank=colorank+1;
        end
    end
    
    
else % All chords on same plot regardless of the hand (GUI default)
    
    if splitit && ~piece, % In case the piece specifier was forgotten despite hand-splitting intentions
        disp('In order to separate chords between hands, the piece # - thus cutting point - is required. Things will go on without hand discrimination.');
    end
    
    for i=1:chpar, % General chords features
        
        if chsel(i), % Selected 
            
            plot(chmat(dmeans+1:size(chmat,1),1,2),zscore(chmat(dmeans+1:size(chmat,1),1,i)),'-o','Color',color(colorank,:));
            % Plot the i-feature z-scores for each chord, at each chord onset position
            
            legendnames{colorank} = chvars{i+1};
            colorank=colorank+1;
        end
    end
    
    for i=1:npar2, % Mean chords' notes features
        
        if nsel(i), % Selected
            
            if ebarred, % Option to plot SD
                
                z = (zscore(chmat(dmeans+1:size(chmat,1),2,i)+chmat(dmeans+1:size(chmat,1),3,i))-zscore(chmat(dmeans+1:size(chmat,1),2,i)-chmat(dmeans+1:size(chmat,1),3,i)) )./2;
                % z = (zscore(val+SD)-zscore(val-SD))/2, approximation...
                
                errorbar(chmat(dmeans+1:size(chmat,1),1,2),zscore(chmat(dmeans+1:size(chmat,1),2,i)),z,'-o','Color',color(colorank,:));
                
            else
                
                plot(chmat(dmeans+1:size(chmat,1),1,2),zscore(chmat(dmeans+1:size(chmat,1),2,i)),'-o','Color',color(colorank,:));
                % Plot the i-feature mean notes z-scores for each chord, at each chord onset position
                
            end
            
            legendnames{colorank} = nvars{i+1};
            colorank=colorank+1;
        end
    end
    
end


% Legend
for i=1:nfeats, hold on; hplot(i)=plot(0,0,'-','LineWidth',1.5,'Color',color(i,:)); end
if splitit && piece,
    alph=0;
    if ~righton, alph=1; hplot(nfeats+1)=plot(0,0,'--vk'); legendnames{length(legendnames)+1} = 'Left hand'; end
    if ~lefton, hplot(nfeats+alph+1)=plot(0,0,':^k'); legendnames{length(legendnames)+1} = 'Right hand'; end
end
legend(hplot(:),legendnames{:},'Location','SouthWest');


end










function [chv nv] = varnames()

% --- Define columns' labels: chord features then notes-per-chord means then SD

chv={'#','Number of notes','Chord onset','Chord offset','Chord duration','max Amax', 'max MHV','Melody lead','Melody lead rate','Last-note trail','Last-note trail rate',...
    'Soft pedal depression/chord','Soft pedal duration/chord','Soft pedal full-depression length/chord','Soft pedal mid-depression length/chord','Soft pedal mid-depression level/chord',...
    'Sustain pedal depression/chord','Sustain pedal duration/chord','Sustain pedal full-depression length/chord','Sustain pedal mid-depression length/chord','Sustain pedal mid-depression level/chord',...
    'Soft pedal @onset','Soft pedal bin. @onset','Soft pedal lead @onset','Sustain pedal @onset','Sustain pedal bin. @onset','Sustain pedal lead @onset',...
    'Soft pedal @offset','Soft pedal bin. @offset','Soft pedal trail @offset','Sustain pedal @offset','Sustain pedal bin. @offset','Sustain pedal trail @offset',...
    'IOI','OffOnI','OffOnI direction','IOI same-hand','OffOnI same-hand','OffOnI direction same-hand','IOI other-hand','OffOnI other-hand','OffOnI direction other-hand',...
    'Overlap length','Overlap length rate','Overlap amount','Overlap amount rate','Number of overlaps',...
    'Overlap length same-hand','Overlap length rate same-hand','Overlap amount same-hand','Overlap amount rate same-hand','Number of overlaps same-hand',...
    'Overlap length other-hand','Overlap length rate other-hand','Overlap amount other-hand','Overlap amount rate other-hand','Number of overlaps other-hand'};

% --- Define notes columns' labels: notes features

nv={'#','Key','Note onset','Note offset','Note duration','Amoy','Amax','t.Amax','MHV','t.MHV',...
    'Speed of attack','Slope of attack','Speed of key attack','Slope of key attack','Directness MHV-Amax','MHV/Amax','Sharpness of key attack slope','Sharpness of attack slope',...
    'FKD start','FKD end','FKD duration','FKD rate','Pre-FKD duration','Pre-FKD rate','Post-FKD duration','Post-FKD rate',...
    't.attack.end','t.decay.end','t.release.start','Attack duration','Sustain duration','Release duration','Attack rate','Sustain rate','Release rate',...
    'Soft pedal depression', 'Sustain pedal depression', 'Soft pedal @note.onset','Sustain pedal @note.onset', 'Soft pedal @MHV', 'Sustain pedal @MHV', 'Soft pedal @note.offset', 'Sustain pedal @note.offset',...
    'Soft pedal depression length','Sustain pedal depression length','Key attack mean depression','Key attack mean depression rate',...
    'Note onset lag','Note onset lag rate','Note onset lag amount','Note onset lag amount rate','Note offset lead','Note offset lead rate','Note offset lead amount','Note offset lead amount rate','Sync rate','Sync amount rate'};


end