function bosen = boe_gen(filein,source,varargin)

% boe_gen(filein,source,varargin)
%
% filein : txt or midi file
% out : boe file
%
% Text file format:
% Column 1: key #
% Column 2: onset (ms)
% Column 3: dur�e (ms)
% Column 4: MHV
% Column 5: t (MHV) (optional, can be empirically induced from the rest)
%
% Source : txt or MIDI
%
% Varargin params: creation of notetime file ('nt')
%                  write the midi file as txt ('fileout')
%                  choose to compute the MIDI file as is ('flat')
%                  enhance the MIDI file as true-to-form boe ('enhanced')


if strcmp(source,'txt') || strcmp(source,'text') || strcmp(source,'texte'), src='txt';
elseif strcmp(source,'midi') || strcmp(source,'mid') || strcmp(source,'MIDI'), src='midi';
else disp(['Error in file type specification: ' source ' is not a valid parameter'])
end

lengthin = length(varargin);
notetimefile=0;
writefile=0;
enhance=1;
for i=1:lengthin
    if strcmp(varargin(i),'nt') || strcmp(varargin(i),'notetime'), notetimefile=1;
    elseif strcmp(varargin(i),'nont') || strcmp(varargin(i),'nonotetime'), notetimefile=0; end
    if strcmp(varargin(i),'fileout'), writefile=1;
    elseif strcmp(varargin(i),'nofileout'), writefile=0; end
    if strcmp(varargin(i),'enhanced'), enhance=1;
    elseif strcmp(varargin(i),'flat'), enhance=0; end
    
    switch char(varargin(i))
        case {'nt','notetime','nont','nonotetime','fileout','nofileout','enhanced','flat'}
        otherwise disp([char(varargin(i)) ' is not a valid parameter.'])
    end
end

warning off;

shortfilein=filein(1:length(filein)-4); % Remove the extension
if strcmp(shortfilein(length(shortfilein)),'.'), shortfilein=shortfilein(1:length(shortfilein)-1); end % Remove the dot if extension longer than 3 chars (e.g. .midi)

if strcmp(src,'midi'),
    matmid = midi2file(filein);

    if writefile==1,
        fileoutname=['boe.gen ',shortfilein,'.txt'];
        dlmwrite(fileoutname,matmid);
    end
    if enhance==1, notime = file2nt(matmid,'mat');
    else notime = file2nt_flat(matmid);
    end

elseif strcmp(src,'txt')
    if enhance==1, notime = file2nt(filein,'txt');
    else
        mattxt=dlmread(filein);
        notime = file2nt_flat(mattxt);
    end

else disp('Issues in file type spec')
end

if notetimefile==1
    ntout = ['nt ',shortfilein];
    dlmwrite(ntout,notime);
end

% BOE gen from notetime

bosen = nt2boe(notime);

nameout=strcat(shortfilein,'.boe');
dlmwrite(nameout,bosen,'delimiter','');

end


function mat = midi2file(filename)
% Converts MIDI files to custom files

[alpha beta] = midi2nmat(filename); % alpha is kept forward
% alpha columns : onset (beats); duration (beats); midi ch; midi pitch; velocity (/128); onset (sec); duration (sec)
% aim: key #; onset (ms); duration (ms); MHV; t.MHV

mat=zeros(size(alpha,1),5); % tmp input matrix for file2notetime

for i=1:size(mat,1)
    mat(i,1) = alpha(i,4); % key # = midi pitch
    mat(i,2) = 2*round((alpha(i,6)*1000)/2); % onset = onset(sec) * 1000 + round + pair
    mat(i,3) = 2*round((alpha(i,7)*1000)/2); % duration
    % mat(i,4) = round(alpha(i,5)*169/126+81-169/126);
    mat(i,4) = round(alpha(i,5)*250/127); % MHV = velocity *250/127
    mat(i,5) = round(min(-0.8 * mat(i,4) + 200, mat(i,3)/1.9)) + mat(i,2) + 2; % t.MHV extrapoled from MHV: t.MHV = -0.8*MHV + 200 if falls < to  little more than half the note duration (empirical)
end
end


function nt = file2nt(filename,type)

% type: input as text file or matrix
if strcmp(type,'txt'), matin=dlmread(filename);
elseif strcmp(type,'mat') || strcmp(type,'matrix'), matin=filename;
end

mdim1=size(matin,1);

mstin=sortrows(matin,2); % sort in order of increasing onset

ntmp=[];

rank=zeros(256); % stock for each key the rank where to insert further samples
% number of samples = (rank-1)/2

% automatic completion of t.MHV if unspecified
if size(mstin,2)<5, mstin(1,5)=0; end % add the t.MHV column if never specified
for i=1:mdim1
    if mstin(i,5)==0,
        mstin(i,5) = round(min(-0.8 * mstin(i,4) + 200, mstin(i,3)/1.9)) + mstin(i,2); % t.MHV extrapoled from MHV: t.MHV = -0.8*MHV + 200 if falls < to  little more than half the note duration (empirical)
    end
end

% Profile choice => definition of the relevant feature to study
car=zeros(mdim1,1);
for i=1:mdim1
    car(i,1) = carac(mstin(i,5)-mstin(i,2),mstin(i,3));
end


% ini, note 1
ntmp(1,1)=mstin(1,1);
rank(ntmp(1,1))=1;

% Note profile interpolation polynome
p=note_profile(mstin(1,3),mstin(1,4),car(1,1));

% polynome max. on the note, and its order jmax
jmax=0;
for j=1:round(mstin(1,3)),
    if polyval(p,j)==max(polyval(p,1:round(mstin(1,3)))), jmax=j; end
end

if car(1,1)==3,
    tamax = mstin(1,5)-mstin(1,2); % t.Amax = dtMHV
    coeffa = (jmax-mstin(1,3))/(tamax-mstin(1,3));
    coeffb = mstin(1,3)*(tamax-jmax)/(tamax-mstin(1,3)); % coeffs du poly degr� 1 permettant de fitter le warping entre temps de note et j apr�s le max

    for j=0:2:2*(round(mstin(1,3)/2))
        rank(ntmp(1,1))=rank(ntmp(1,1))+2;
        ntmp(1,rank(ntmp(1,1)))=mstin(1,2) + j;
        if j<=tamax, ntmp(1,rank(ntmp(1,1))+1) = round(polyval(p,(jmax/tamax)*j)); % valeur du polynome warp�e pour fitter la synchro tAmax/jmax => au lieu d'aplliquer la valeur en j on l'applique en (jmax/tamax)*j
        else ntmp(1,rank(ntmp(1,1))+1) = round(polyval(p,coeffa*j+coeffb));
        end
        if ntmp(1,rank(ntmp(1,1))+1)<=0, ntmp(1,rank(ntmp(1,1))+1)=0; end
    end

elseif car(1,1)==2,
    tamax = 1.1*(mstin(1,5)-mstin(1,2)); % t.Amax = dtMHV + epsilon ie. 0.1 * dtMHV (� la louche)
    coeffa = (jmax-mstin(1,3))/(tamax-mstin(1,3));
    coeffb = mstin(1,3)*(tamax-jmax)/(tamax-mstin(1,3)); % coeffs du poly degr� 1 permettant de fitter le warping entre temps de note et j apr�s le max

    for j=0:2:2*(round(mstin(1,3)/2))
        rank(ntmp(1,1))=rank(ntmp(1,1))+2;
        ntmp(1,rank(ntmp(1,1)))=mstin(1,2) + j;
        if j<=tamax, ntmp(1,rank(ntmp(1,1))+1) = round(polyval(p,(jmax/tamax)*j)); % valeur du polynome warp�e pour fitter la synchro tAmax/jmax => au lieu d'aplliquer la valeur en j on l'applique en (jmax/tamax)*j
        else ntmp(1,rank(ntmp(1,1))+1) = round(polyval(p,coeffa*j+coeffb));
        end
        if ntmp(1,rank(ntmp(1,1))+1)<=0, ntmp(1,rank(ntmp(1,1))+1)=0; end
    end

else
    tamax = 1.2*(mstin(1,5)-mstin(1,2)); % t.Amax = dtMHV + epsilon ie. 0.1 * dtMHV (� la louche)
    jdecay=(2*jmax + (mstin(1,3)-1.25*jmax))/2;
    coeffa=jmax/tamax;
    coeffb=mstin(1,3)*(1-jmax/tamax);
%     coeffa = (2*jmax-mstin(1,3))/(2*tamax-mstin(1,3));
%     coeffb = 2*mstin(1,3)*(tamax-jmax)/(2*tamax-mstin(1,3)); % coeffs du poly degr� 1 permettant de fitter le warping entre temps de note et j apr�s le max et le sustain

    for j=0:2:2*(round(mstin(1,3)/2))
        rank(ntmp(1,1))=rank(ntmp(1,1))+2;
        ntmp(1,rank(ntmp(1,1)))=mstin(1,2) + j;
        if j<=tamax, ntmp(1,rank(ntmp(1,1))+1) = round(polyval(p,(jmax/tamax)*j)); % valeur du polynome warp�e pour fitter la synchro tAmax/jmax => au lieu d'aplliquer la valeur en j on l'applique en (jmax/tamax)*j
        elseif j<=2*tamax, ntmp(1,rank(ntmp(1,1))+1) = round(polyval(p,(jmax/tamax)*j));
        elseif j<=(mstin(1,3)-1.25*tamax), ntmp(1,rank(ntmp(1,1))+1) = round(polyval(p,jdecay))+round(1.2*(rand-0.5)); % partie stationnaire + fine variation al�atoire
        else ntmp(1,rank(ntmp(1,1))+1) = round(polyval(p,coeffa*j+coeffb));
        end
        if ntmp(1,rank(ntmp(1,1))+1)<=0, ntmp(1,rank(ntmp(1,1))+1)=0; end
    end
end

ntmp(1,2)=(rank(ntmp(1,1))-1)/2; % number of samples



% all the rest

for i2=2:mdim1 % parcours de la mat.in
    % polyn�me d'interpolation de profil
    p=note_profile(mstin(i2,3),mstin(i2,4),car(i2,1));

    % max. du polynome sur la note, et ordre jmax
    jmax=0;
    for j=1:round(mstin(i2,3)),
        if polyval(p,j)==max(polyval(p,1:round(mstin(i2,3)))), jmax=j; end
    end

    if rank(mstin(i2,1))>0,
        sizetmp=size(ntmp,1);
        i1=1;
        while i1<=sizetmp % parcours de la mat.out
            if ntmp(i1,1)==mstin(i2,1)
                if car(i2,1)==3,
                    tamax = mstin(i2,5)-mstin(i2,2); % t.Amax = dtMHV
                    coeffa = (jmax-mstin(i2,3))/(tamax-mstin(i2,3));
                    coeffb = mstin(i2,3)*(tamax-jmax)/(tamax-mstin(i2,3)); % coeffs du poly degr� 1 permettant de fitter le warping entre temps de note et j apr�s le max

                    for j=0:2:2*(round(mstin(i2,3)/2))
                        rank(ntmp(i1,1))=rank(ntmp(i1,1))+2;
                        ntmp(i1,rank(ntmp(i1,1)))=mstin(i2,2) + j;
                        if j<=tamax, ntmp(i1,rank(ntmp(i1,1))+1) = round(polyval(p,(jmax/tamax)*j)); % valeur du polynome warp�e pour fitter la synchro tAmax/jmax => au lieu d'aplliquer la valeur en j on l'applique en (jmax/tamax)*j
                        else ntmp(i1,rank(ntmp(i1,1))+1) = round(polyval(p,coeffa*j+coeffb));
                        end
                        if ntmp(i1,rank(ntmp(i1,1))+1)<=0, ntmp(i1,rank(ntmp(i1,1))+1)=0; end
                    end

                elseif car(i2,1)==2,
                    tamax = 1.1*(mstin(i2,5)-mstin(i2,2)); % t.Amax = dtMHV + epsilon ie. 0.1 * dtMHV (� la louche)
                    coeffa = (jmax-mstin(i2,3))/(tamax-mstin(i2,3));
                    coeffb = mstin(i2,3)*(tamax-jmax)/(tamax-mstin(i2,3)); % coeffs du poly degr� 1 permettant de fitter le warping entre temps de note et j apr�s le max

                    for j=0:2:2*(round(mstin(i2,3)/2))
                        rank(ntmp(i1,1))=rank(ntmp(i1,1))+2;
                        ntmp(i1,rank(ntmp(i1,1)))=mstin(i2,2) + j;
                        if j<=tamax, ntmp(i1,rank(ntmp(i1,1))+1) = round(polyval(p,(jmax/tamax)*j)); % valeur du polynome warp�e pour fitter la synchro tAmax/jmax => au lieu d'aplliquer la valeur en j on l'applique en (jmax/tamax)*j
                        else ntmp(i1,rank(ntmp(i1,1))+1) = round(polyval(p,coeffa*j+coeffb));
                        end
                        if ntmp(i1,rank(ntmp(i1,1))+1)<=0, ntmp(i1,rank(ntmp(i1,1))+1)=0; end
                    end

                else
                    tamax = 1.2*(mstin(i2,5)-mstin(i2,2)); % t.Amax = dtMHV + epsilon ie. 0.2 * dtMHV (� la louche)
                    jdecay=(2*jmax + (mstin(i2,3)-1.25*jmax))/2;
                    coeffa=jmax/tamax;
                    coeffb=mstin(i2,3)*(1-jmax/tamax);
%                     coeffa = (2*jmax-mstin(i2,3))/(2*tamax-mstin(i2,3));
%                     coeffb = 2*mstin(i2,3)*(tamax-jmax)/(2*tamax-mstin(i2,3)); % coeffs du poly degr� 1 permettant de fitter le warping entre temps de note et j apr�s le max et le sustain

                    for j=0:2:2*(round(mstin(i2,3)/2))
                        rank(ntmp(i1,1))=rank(ntmp(i1,1))+2;
                        ntmp(i1,rank(ntmp(i1,1)))=mstin(i2,2) + j;
                        if j<=tamax, ntmp(i1,rank(ntmp(i1,1))+1) = round(polyval(p,(jmax/tamax)*j)); % valeur du polynome warp�e pour fitter la synchro tAmax/jmax => au lieu d'aplliquer la valeur en j on l'applique en (jmax/tamax)*j
                        elseif j<=2*tamax, ntmp(i1,rank(ntmp(i1,1))+1) = round(polyval(p,(jmax/tamax)*j));
                        elseif j<=(mstin(i2,3)-1.25*tamax), ntmp(i1,rank(ntmp(i1,1))+1) = round(polyval(p,jdecay))+round(1.2*(rand-0.5)); % partie stationnaire + fine variation al�atoire
                        else ntmp(i1,rank(ntmp(i1,1))+1) = round(polyval(p,coeffa*j+coeffb));
                        end
                        if ntmp(i1,rank(ntmp(i1,1))+1)<=0, ntmp(i1,rank(ntmp(i1,1))+1)=0; end
                    end
                end

                ntmp(i1,2)=(rank(ntmp(i1,1))-1)/2; % nb d'occs
            end
            i1=i1+1;
        end

    else
        ntmp(size(ntmp,1)+1,1)=mstin(i2,1);
        rank(ntmp(size(ntmp,1),1))=1;

        if car(i2,1)==3,
            tamax = mstin(i2,5)-mstin(i2,2); % t.Amax = dtMHV
            coeffa = (jmax-mstin(i2,3))/(tamax-mstin(i2,3));
            coeffb = mstin(i2,3)*(tamax-jmax)/(tamax-mstin(i2,3)); % coeffs du poly degr� 1 permettant de fitter le warping entre temps de note et j apr�s le max

            for j=0:2:2*(round(mstin(i2,3)/2))
                rank(ntmp(size(ntmp,1),1))=rank(ntmp(size(ntmp,1),1))+2;
                ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1)))=mstin(i2,2) + j;

                if j<=tamax, ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1) = round(polyval(p,(jmax/tamax)*j)); % valeur du polynome warp�e pour fitter la synchro tAmax/jmax => au lieu d'aplliquer la valeur en j on l'applique en (jmax/tamax)*j
                else ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1) = round(polyval(p,coeffa*j+coeffb));
                end
                if ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1)<=0, ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1)=0; end
            end

        elseif car(i2,1)==2,
            tamax = 1.1*(mstin(i2,5)-mstin(i2,2)); % t.Amax = dtMHV + epsilon ie. 0.1 * dtMHV (� la louche)
            coeffa = (jmax-mstin(i2,3))/(tamax-mstin(i2,3));
            coeffb = mstin(i2,3)*(tamax-jmax)/(tamax-mstin(i2,3)); % coeffs du poly degr� 1 permettant de fitter le warping entre temps de note et j apr�s le max

            for j=0:2:2*(round(mstin(i2,3)/2))
                rank(ntmp(size(ntmp,1),1))=rank(ntmp(size(ntmp,1),1))+2;
                ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1)))=mstin(i2,2) + j;

                if j<=tamax, ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1) = round(polyval(p,(jmax/tamax)*j)); % valeur du polynome warp�e pour fitter la synchro tAmax/jmax => au lieu d'aplliquer la valeur en j on l'applique en (jmax/tamax)*j
                else ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1) = round(polyval(p,coeffa*j+coeffb));
                end
                if ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1)<=0, ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1)=0; end
            end

        else
            tamax = 1.2*(mstin(i2,5)-mstin(i2,2)); % t.Amax = dtMHV + epsilon ie. 0.1 * dtMHV (� la louche)
            jdecay=(2*jmax + (mstin(i2,3)-1.25*jmax))/2;
            coeffa=jmax/tamax;
            coeffb=mstin(i2,3)*(1-jmax/tamax);
%             coeffa = (2*jmax-mstin(i2,3))/(2*tamax-mstin(i2,3));
%             coeffb = 2*mstin(i2,3)*(tamax-jmax)/(2*tamax-mstin(i2,3)); % coeffs du poly degr� 1 permettant de fitter le warping entre temps de note et j apr�s le max et le sustain

            for j=0:2:2*(round(mstin(i2,3)/2))
                rank(ntmp(size(ntmp,1),1))=rank(ntmp(size(ntmp,1),1))+2;
                ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1)))=mstin(i2,2) + j;

                if j<=tamax, ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1) = round(polyval(p,(jmax/tamax)*j)); % valeur du polynome warp�e pour fitter la synchro tAmax/jmax => au lieu d'aplliquer la valeur en j on l'applique en (jmax/tamax)*j
                elseif j<=2*tamax, ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1) = round(polyval(p,(jmax/tamax)*j));
                elseif j<=(mstin(i2,3)-1.25*tamax), ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1) = round(polyval(p,jdecay))+round(1.2*(rand-0.5)); % partie stationnaire + fine variation al�atoire
                else ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1) = round(polyval(p,coeffa*j+coeffb));
                end
                if ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1)<=0, ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1)=0; end
            end
        end

        ntmp(size(ntmp,1),2)=(rank(ntmp(size(ntmp,1),1))-1)/2; % nb d'occs

    end
end




% Marteaux
for i2=1:mdim1
    if mstin(i2,1)<109
        if mstin(i2,4)>0
            if rank(mstin(i2,1)+128)>0,
                sizetmp=size(ntmp,1);
                i1=1;
                while i1<=sizetmp
                    if ntmp(i1,1)==mstin(i2,1)+128
                        rank(ntmp(i1,1))=rank(ntmp(i1,1))+2;
                        ntmp(i1,rank(ntmp(i1,1))+1)=mstin(i2,5);
                        ntmp(i1,rank(ntmp(i1,1))+2)=mstin(i2,4);
                        ntmp(i1,2)=(rank(ntmp(i1,1)))/2;
                    end
                    i1=i1+1;
                end
            else
                ntmp(size(ntmp,1)+1,1)=mstin(i2,1)+128;
                rank(ntmp(size(ntmp,1),1))=rank(ntmp(size(ntmp,1),1))+2;
                ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1)=mstin(i2,5);
                ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+2)=mstin(i2,4);
                ntmp(size(ntmp,1),2)=(rank(ntmp(size(ntmp,1),1)))/2;
            end
        end
    end
end

% Trim & filter
msizenew=size(ntmp,1);
itest=0;
for i=1:size(ntmp,1)
    for j=1:size(ntmp,2),
        if isnan(ntmp(i,j)), ntmp(i,j)=0; end
    end
    
    if i<=msizenew
        if ntmp(i-itest,1)==0 || isnan(ntmp(i-itest,1))
            ntmp(i-itest,:)=[];
            msizenew=size(ntmp,1);
            itest=itest+1;
        end
    end
end

nt=sortrows(ntmp,1);

end


function nt = file2nt_flat(matin)
mdim1=size(matin,1);
mstin=sortrows(matin,2); % tri par onset croissant
ntmp=[];


% t.mhv and autogen if required
if size(mstin,2)<5
    for i=1:mdim1, mstin(i,5) = round(min(-0.8 * mstin(i,4) + 200, mstin(i,3)/1.9)) + mstin(i,2) + 2; end
else
    for i=1:mdim1
        if isempty(mstin(i,5)), mstin(i,5) = round(min(-0.8 * mstin(i,4) + 200, mstin(i,3)/1.9)) + mstin(i,2) + 2; end
    end
end


rank=zeros(256); % stockage pour chaque touche du rang o� int�grer les prochaines occs
% nb d'occs=(rank-1)/2

% ini, note 1 
ntmp(1,1)=mstin(1,1);
rank(ntmp(1,1))=1;
for j=0:2:2*(round(mstin(1,3)/2))
    rank(ntmp(1,1))=rank(ntmp(1,1))+2;
    ntmp(1,rank(ntmp(1,1)))=mstin(1,2) + j; %onset
    ntmp(1,rank(ntmp(1,1))+1)= round(-0.004*mstin(1,4)^2 + 2*mstin(1,4)); % key depression inferred from MHV
end
ntmp(1,2)=(rank(ntmp(1,1))-1)/2; % nb d'occs



% all the rest

for i2=2:mdim1 % parcours de la mat.in
    if rank(mstin(i2,1))>0,
        sizetmp=size(ntmp,1);
        i1=1;
        while i1<=sizetmp % parcours de la mat.out
            if ntmp(i1,1)==mstin(i2,1)
                for j=0:2:2*(round(mstin(i2,3)/2))
                    rank(ntmp(i1,1))=rank(ntmp(i1,1))+2;
                    ntmp(i1,rank(ntmp(i1,1)))=mstin(i2,2) + j;
                    ntmp(i1,rank(ntmp(i1,1))+1)= round(-0.004*mstin(i2,4)^2 + 2*mstin(i2,4));
                end
                ntmp(i1,2)=(rank(ntmp(i1,1))-1)/2; % nb d'occs
            end
            i1=i1+1;
        end
    else
        ntmp(size(ntmp,1)+1,1)=mstin(i2,1);
        rank(ntmp(size(ntmp,1),1))=1;
        for j=0:2:2*(round(mstin(i2,3)/2))
            rank(ntmp(size(ntmp,1),1))=rank(ntmp(size(ntmp,1),1))+2;
            ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1)))=mstin(i2,2) + j;
            ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1)= round(-0.004*mstin(i2,4)^2 + 2*mstin(i2,4));
        end
        ntmp(size(ntmp,1),2)=(rank(ntmp(size(ntmp,1),1))-1)/2; % nb d'occs
    end
end

% Marteaux
for i2=1:mdim1
    if mstin(i2,1)<109
        if mstin(i2,4)>0
            if rank(mstin(i2,1)+128)>0,
                sizetmp=size(ntmp,1);
                i1=1;
                while i1<=sizetmp
                    if ntmp(i1,1)==mstin(i2,1)+128
                        rank(ntmp(i1,1))=rank(ntmp(i1,1))+2;
                        ntmp(i1,rank(ntmp(i1,1))+1)=mstin(i2,5);
                        ntmp(i1,rank(ntmp(i1,1))+2)=mstin(i2,4);
                        ntmp(i1,2)=(rank(ntmp(i1,1)))/2;
                    end
                    i1=i1+1;
                end
            else
                ntmp(size(ntmp,1)+1,1)=mstin(i2,1)+128;
                rank(ntmp(size(ntmp,1),1))=rank(ntmp(size(ntmp,1),1))+2;
                ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+1)=mstin(i2,5);
                ntmp(size(ntmp,1),rank(ntmp(size(ntmp,1),1))+2)=mstin(i2,4);
                ntmp(size(ntmp,1),2)=(rank(ntmp(size(ntmp,1),1)))/2;
            end
        end
    end
end

% Trim
msizenew=size(ntmp,1);
itest=0;
for i=1:size(ntmp,1)
    if i<=msizenew
        if ntmp(i-itest,1)==0 || isnan(ntmp(i-itest,1))
            ntmp(i-itest,:)=[];
            msizenew=size(ntmp,1);
            itest=itest+1;
        end
    end
end

nt=sortrows(ntmp,1);
end


function c = carac(dmhv,dur)
if dur>400, c=1;
elseif dur<210
    if dmhv/dur<0.4, c=2; else c=3; end
else
    if dmhv/dur<0.2, c=1; else c=2; end
end
end


function polyn = note_profile(dur,mhv,charac)

% extrapolation de Amax � partir de MHV
amax = round(-0.004*mhv^2 + 2*mhv); % poly degr� 2

if charac==3
    %profil de note type
    x=[0 3 6 9 12 15 18 21 24 27 30 33 36 39 42 45 48 51 54 57 60 63 66 69 72 75 78 81 84 87 90 93 96 99 102 105 108 111 114];
    y=[0 0.024 0.034 0.04 0.06 0.088 0.114 0.144 0.18 0.228 0.28 0.34 0.402 0.472 0.542 0.612 0.698 0.784 0.864 0.852 0.822 0.8 0.776 0.736 0.696 0.652 0.618 0.576 0.53 0.484 0.42 0.348 0.274 0.208 0.154 0.104 0.068 0.036 0];

    %recalcul du profil calibr� sur enfoncement et dur�e moys. de la note cible

    y1 = y.*amax/max(y);
    x1=x.*dur/114;

    %calcul du polynome
    polyn = polyfit(x1,y1,18);

elseif charac==2
    %profil de note type
    x=[0 3 6 9 12 15 18 21 24 27 30 33 36 39 42 45 48 51 54 57 60 63];
    y=[0 0.02 0.04 0.08 0.22 0.33 0.46 0.62 0.77 0.81 0.85 0.82 0.78 0.75 0.73 0.7 0.66 0.53 0.41 0.26 0.12 0];

    %recalcul du profil calibr� sur enfoncement et dur�e moys. de la note cible
    y1 = y.*amax/max(y);
    x1=x.*dur/63;

    %calcul du polynome
    polyn = polyfit(x1,y1,14);

else
    %profil de note type
    x = [0 5 10 15 20 25 30 35 40 45 50 55 60 64 69 74 79 84 89 94 99 104 109 114 119 124 129 134 139 144 149 154 159 164 169 174 179 184 189 194 199 204 209];
    y = [0 0.04 0.05 0.07 0.1 0.15 0.19 0.26 0.32 0.41 0.51 0.62 0.76 0.82 0.77 0.75 0.73 0.72 0.72 0.72 0.72 0.71 0.71 0.71 0.71 0.7 0.7 0.7 0.69 0.67 0.65 0.63 0.61 0.57 0.52 0.46 0.38 0.31 0.24 0.17 0.11 0.06 0];

    %recalcul du profil calibr� sur enfoncement et dur�e moys. de la note cible
    y1 = y.*amax/max(y);
    x1=x.*dur/209;

    %calcul du polynome
    polyn = polyfit(x1,y1,18);

end

end


function Boe = nt2boe (Notetime)

[m n] = size (Notetime);
a=1;
for i=1:m
    for j=3:2:n-1
        if (Notetime(i,j) > 0)
            Mat(a,1)=Notetime(i,j);
            Mat(a,2)=Notetime(i,1);
            Mat(a,3)=Notetime(i,j+1);
            a=a+1;
        end
    end
end

A=sortrows(Mat,1);
k=size(A,1);

i2=1;
for i=1:k-1
    if (i>=i2)
        if (i == 1), Boe = ['FF']; end
        if (i > 1), debut = ['FF']; Boe=horzcat(Boe,debut); end % d�but

        if (A(i,1)<16), int = dec2hex(A(i,1)); temps =['00000' int]; end
        if (A(i,1)>16) && (A(i,1)<256), int = dec2hex(A(i,1)); temps =['0000' int]; end
        if (A(i,1)>256) && (A(i,1)<4096), int = dec2hex(A(i,1)); temps =['000' int]; end
        if (A(i,1)>4096) && (A(i,1)<65536), int = dec2hex(A(i,1)); temps =['00' int]; end
        if (A(i,1)>65536) && (A(i,1)<1048576), int = dec2hex(A(i,1)); temps =['0' int]; end
        if (A(i,1)>1048576), temps = dec2hex(A(i,1)); end
        Boe=horzcat(Boe,temps);
        i2=i;

        while (A(i2,1)==A(i,1))
            if A(i2,2)<16, key=dec2hex(A(i2,2)); touche = ['0' key];
            else touche = dec2hex(A(i2,2));  % touche ou marteau
            end
            Boe=horzcat(Boe,touche);

            if A(i2,3)<16, dep=dec2hex(A(i2,3)); enfoncement = ['0' dep];
            else enfoncement = dec2hex(A(i2,3)); % enfoncement ou vitesse
            end
            Boe=horzcat(Boe,enfoncement);
            
            if(i2 < k), i2=i2+1; end
            if(i2 == k), break; end
        end
    end
end

end

