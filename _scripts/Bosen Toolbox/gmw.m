function M = gmw(command, varargin)

if nargin == 1 && ~ischar(command)
  varargin{1} = command;
  command = 'create';
end

switch command
    case 'create'
        create(varargin{:});   
    case 'lightUp'
        lightUp(varargin{:});
    case 'stepThrough'
        stepThrough(varargin{:});
    case 'matchEdit'      %
        matchEdit(varargin{:});
    case 'matchAccept'      %
        matchAccept(varargin{:});
    case 'xpan'
        xpan(varargin{:});
    case 'xzoomin'
        xzoomin(varargin{:});
    case 'xzoomout'
        xzoomout(varargin{:});
    case 'ypan'
        ypan(varargin{:});
    case 'yzoomin'
        yzoomin(varargin{:});
    case 'yzoomout'
        yzoomout(varargin{:});
    case 'exit'
        exitWindow(varargin{:});
end
end

% ========================================================================
% Creat the gui match window
% ========================================================================
function create(M)

% evalin('base', 'M');

global gw NN
NN = 0;
gw.m = M;

% ======
% Figure
% ======
h0 = figure('Color',[0.8 0.8 0.8], ...
    'PaperOrientation','landscape', ...
    'PaperPosition',[108 90 576 432], ...
    'PaperUnits','points', ...
    'Position',[7 7 1069 726], ...
    'Tag','Fig1', ...
    'DeleteFcn', 'gmw(''exit'')');

% ============
% Figure Title
% ============
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'BackgroundColor',[0.701960784313725 0.701960784313725 0.701960784313725], ...
    'ListboxTop',0, ...
    'Position',[0.3676333021515435 0.9604132231404959 0.25 0.02066115702479339], ...
    'String', gw.m.title, ...
    'Style','text', ...
    'Tag','StaticText1');

% =============================================================================
% Notation Axes 1: time / note number
% =============================================================================
gw.w.notaAxes1 = axes('Parent',h0, ...
    'CameraUpVector',[0 1 0], ...
    'ButtonDownFcn','', ...
    'CameraUpVectorMode','manual', ...
    'Color','none', ...
    'Position',[0.0420954162768943 0.5371900826446281 0.5893358278765202 0.371900826446281], ...
    'Tag','gw.w.notaAxes1', ...
    'GridLineStyle',':', ...
    'XColor',[0 0 0], ...
    'YColor',[0 0 0], ...
    'ZColor',[0 0 0]);
h2 = text('Parent',gw.w.notaAxes1, ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Position',[0.4992050874403816 -0.02973977695167296 9.160254037844386], ...
    'Tag','Axes1Text4', ...
    'VerticalAlignment','cap');
set(get(h2,'Parent'),'XLabel',h2);
h2 = text('Parent',gw.w.notaAxes1, ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Position',[-0.04451510333863275 0.4944237918215613 9.160254037844386], ...
    'Rotation',90, ...
    'String','note number', ...
    'Tag','Axes1Text3', ...
    'VerticalAlignment','baseline');
set(get(h2,'Parent'),'YLabel',h2);
h2 = text('Parent',gw.w.notaAxes1, ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','right', ...
    'Position',[-0.07313195548489666 1.241635687732342 9.160254037844386], ...
    'Tag','Axes1Text2', ...
    'Visible','off');
set(get(h2,'Parent'),'ZLabel',h2);
h2 = text('Parent',gw.w.notaAxes1, ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Position', [0.4992    1.0223    9.1603], ...
    'Tag','Axes1Text1', ...
    'VerticalAlignment','bottom');
set(get(h2,'Parent'),'Title',h2);


% =============================================================================
% Notation Axes 2: beat / pitch
% =============================================================================
gw.w.notaAxes2 = axes('Parent',h0, ...
    'CameraUpVector',[0 1 0], ...
    'CameraUpVectorMode','manual', ...
    'Color', [1 1 1], ...
    'GridLineStyle','-', ...
    'Position',[0.0420954162768943 0.5371900826446281 0.5893358278765202 0.371900826446281], ...
    'Tag','Axes1', ...
    'XAxisLocation','top', ...
    'XColor',[.5 .5 .5], ...
    'XGrid','on', ...
    'YAxisLocation','right', ...
    'YColor','k', ...
    'YTick', [43 47 50 53 57 64 67 71 74 77], ...
    'YTickLabel', ['g'; 'b'; 'd'; 'f'; 'a'; 'e'; 'g'; 'b'; 'd'; 'f';], ...
    'YGrid','on', ...
    'ZColor',[0 0 0]);
h2 = text('Parent',gw.w.notaAxes2, ...
    'ButtonDownFcn','ctlpanel SelectMoveResize', ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Interruptible','off', ...
    'Position',[0.4992050874403816 1.078066914498141 9.160254037844386], ...
    'String','score position (beats)', ...
    'Tag','Axes1Text4', ...
    'VerticalAlignment','bottom');
set(get(h2,'Parent'),'XLabel',h2);
h2 = text('Parent',gw.w.notaAxes2, ...
    'ButtonDownFcn','ctlpanel SelectMoveResize', ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Interruptible','off', ...
    'Position',[1.041335453100159 0.4944237918215613 9.160254037844386], ...
    'Rotation',90, ...
    'String','pitch', ...
    'Tag','Axes1Text3', ...
    'VerticalAlignment','cap');
set(get(h2,'Parent'),'YLabel',h2);
h2 = text('Parent',gw.w.notaAxes2, ...
    'ButtonDownFcn','ctlpanel SelectMoveResize', ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','right', ...
    'Interruptible','off', ...
    'Position',[-0.07313195548489666 1.241635687732342 9.160254037844386], ...
    'Tag','Axes1Text2', ...
    'Visible','off');
set(get(h2,'Parent'),'ZLabel',h2);
h2 = text('Parent',gw.w.notaAxes2, ...
    'ButtonDownFcn','ctlpanel SelectMoveResize', ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Interruptible','off', ...
    'Position',[0.4992    1.1561    9.1603], ...
    'Tag','Axes1Text1', ...
    'VerticalAlignment','bottom');
set(get(h2,'Parent'),'Title',h2);


% ===================
% Notation Axes Title
% ===================
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'BackgroundColor',[0.701960784313725 0.701960784313725 0.701960784313725], ...
    'ListboxTop',0, ...
    'Position',[0.0420954162768943 0.9297520661157025 0.08419083255378859 0.02066115702479339], ...
    'String','Performance', ...
    'Style','text', ...
    'Tag','StaticText2');


% =============================================================================
% Performance Axes 1: time / note number
% =============================================================================
gw.w.perfAxes1 = axes('Parent',h0, ...
    'CameraUpVector',[0 1 0], ...
    'CameraUpVectorMode','manual', ...
    'Color','none', ...
    'Position',[0.0421    0.1033    0.5893    0.3719], ...
    'Tag','Axes1', ...
    'XColor',[0 0 0], ...
    'YColor',[0 0 0], ...
    'ZColor',[0 0 0]);
h2 = text('Parent',gw.w.perfAxes1, ...
    'ButtonDownFcn','ctlpanel SelectMoveResize', ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Interruptible','off', ...
    'Position',[0.4992050874403816 -0.08550185873605942 9.160254037844386], ...
    'String','time (sec)', ...
    'Tag','Axes1Text4', ...
    'VerticalAlignment','cap');
set(get(h2,'Parent'),'XLabel',h2);
h2 = text('Parent',gw.w.perfAxes1, ...
    'ButtonDownFcn','ctlpanel SelectMoveResize', ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Interruptible','off', ...
    'Position',[-0.04451510333863275 0.4944237918215615 9.160254037844386], ...
    'Rotation',90, ...
    'String','note number', ...
    'Tag','Axes1Text3', ...
    'VerticalAlignment','baseline');
set(get(h2,'Parent'),'YLabel',h2);
h2 = text('Parent',gw.w.perfAxes1, ...
    'ButtonDownFcn','ctlpanel SelectMoveResize', ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','right', ...
    'Interruptible','off', ...
    'Position',[-0.07313195548489666 2.412639405204461 9.160254037844386], ...
    'Tag','Axes1Text2', ...
    'Visible','off');
set(get(h2,'Parent'),'ZLabel',h2);
h2 = text('Parent',gw.w.perfAxes1, ...
    'ButtonDownFcn','ctlpanel SelectMoveResize', ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Interruptible','off', ...
    'Position',[0.4992050874403816 1.022304832713755 9.160254037844386], ...
    'Tag','Axes1Text1', ...
    'VerticalAlignment','bottom');
set(get(h2,'Parent'),'Title',h2);

% ============================================================================
% Performace Axes 2: beat / pitch
% ============================================================================
gw.w.perfAxes2 = axes('Parent',h0, ...
    'CameraUpVector',[0 1 0], ...
    'CameraUpVectorMode','manual', ...
    'Color', [1 1 1], ...
    'GridLineStyle','-', ...
    'Position',[0.0421    0.1033    0.5893    0.3719], ...
    'Tag','Axes1', ...
    'XAxisLocation','top', ...
    'XColor',[.5 .5 .5], ...
    'XGrid','on', ...
    'YAxisLocation','right', ...
    'YColor','k', ...
    'YTick', [43 47 50 53 57 64 67 71 74 77], ...
    'YTickLabel', ['g'; 'b'; 'd'; 'f'; 'a'; 'e'; 'g'; 'b'; 'd'; 'f';], ...
    'YGrid','on', ...
    'ZColor',[0 0 0]);
h2 = text('Parent',gw.w.perfAxes2, ...
    'ButtonDownFcn','ctlpanel SelectMoveResize', ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Interruptible','off', ...
    'Position',[0.4992050874403816 1.078066914498141 9.160254037844386], ...
    'Tag','Axes1Text4', ...
    'VerticalAlignment','bottom');
set(get(h2,'Parent'),'XLabel',h2);
h2 = text('Parent',gw.w.perfAxes2, ...
    'ButtonDownFcn','ctlpanel SelectMoveResize', ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Interruptible','off', ...
    'Position',[1.041335453100159 0.4944237918215615 9.160254037844386], ...
    'Rotation',90, ...
    'String','pitch', ...
    'Tag','Axes1Text3', ...
    'VerticalAlignment','cap');
set(get(h2,'Parent'),'YLabel',h2);
h2 = text('Parent',gw.w.perfAxes2, ...
    'ButtonDownFcn','ctlpanel SelectMoveResize', ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','right', ...
    'Interruptible','off', ...
    'Position',[-0.07313195548489666 2.412639405204461 9.160254037844386], ...
    'Tag','Axes1Text2', ...
    'Visible','off');
set(get(h2,'Parent'),'ZLabel',h2);
h2 = text('Parent',gw.w.perfAxes2, ...
    'ButtonDownFcn','ctlpanel SelectMoveResize', ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Interruptible','off', ...
    'Position',[0.4992050874403816 1.100371747211896 9.160254037844386], ...
    'Tag','Axes1Text1', ...
    'VerticalAlignment','bottom');
set(get(h2,'Parent'),'Title',h2);

% ======================
% Performance Axes Title
% ======================
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'BackgroundColor',[0.701960784313725 0.701960784313725 0.701960784313725], ...
    'ListboxTop',0, ...
    'Position',[0.0420954162768943 0.4958677685950413 0.08419083255378859 0.02066115702479339], ...
    'String','Notation', ...
    'Style','text', ...
    'Tag','StaticText3');

h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'BackgroundColor',[0.701960784313725 0.701960784313725 0.701960784313725], ...
    'FontSize',12, ...
    'FontWeight','bold', ...
    'ListboxTop',0, ...
    'Position',[0.30 0.4958677685950413 0.05 0.02066115702479339], ...
    'String','<', ...
    'Tag','Pushbutton1', ...
    'Callback', 'gmw(''stepThrough'', -1)');

h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'BackgroundColor',[0.701960784313725 0.701960784313725 0.701960784313725], ...
    'FontSize',12, ...
    'FontWeight','bold', ...
    'ListboxTop',0, ...
    'Position',[0.35 0.4958677685950413 0.05 0.02066115702479339], ...
    'String','>', ...
    'Tag','Pushbutton1', ...
    'Callback', 'gmw(''stepThrough'', +1)');

% ============================================================================
% Plot to test
% ============================================================================
axes(gw.w.notaAxes1); gw.w.Pn=proll(gw.m.Nn,'sec','');
gw.w.notaAxes1Xlimlim = get(gw.w.notaAxes1, 'XLim');
ylimlim = [20 110];
gw.w.notaAxes1Ylimlim = ylimlim;
set(gw.w.notaAxes1, 'YLim', ylimlim);

xlim1 = get(gw.w.notaAxes1, 'XLim');
xlim2 = xlim1*gettempo(gw.m.Nn)/60;
set(gw.w.notaAxes2, 'XLim', xlim2, 'XGrid', 'on');
set(gw.w.notaAxes2, 'Ylim', ylimlim);

% ---

axes(gw.w.perfAxes1); gw.w.Pp = proll(gw.m.Np,'sec', '');
set(gw.w.perfAxes1, 'XLim', scalePerf(gw.w.notaAxes1Xlimlim));
gw.w.perfAxes1Xlimlim = get(gw.w.perfAxes1, 'XLim');
gw.w.perfAxes1Ylimlim = ylimlim;
set(gw.w.perfAxes1, 'YLim', ylimlim);

set(gw.w.perfAxes1, 'XLim', scalePerf(xlim1));
set(gw.w.perfAxes2, 'XLim', get(gw.w.perfAxes1, 'XLim'), 'XGrid', 'on');
t1 = get(gw.w.notaAxes2, 'XTick');
tlabel = get(gw.w.notaAxes2, 'XTickLabel');
[t2 p2] = scaleTicks(t1);
set(gw.w.perfAxes2, 'XTick', t2, 'XTickLabel', tlabel);
set(gw.w.perfAxes2, 'Ylim', ylimlim);


% =============
% X Axes Slider
% =============
if strcmp(computer, 'MAC2')
    sstep = [0.02 1];
else
    sstep = [0.02, Inf];
end
gw.w.xslide = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'BackgroundColor',[0.701960784313725 0.701960784313725 0.701960784313725], ...
    'ListboxTop',0, ...
    'Position',[0.0420954162768943 0.04132231404958678 0.5893358278765203 0.01928374655647383], ...
    'Style','slider', ...
    'Tag','gw.w.xslide', ...
    'Min', gw.w.notaAxes1Xlimlim(1), 'Max', gw.w.notaAxes1Xlimlim(2), ...
    'Value', 0, ...
    'SliderStep', sstep, ...
    'Callback', 'gmw(''xpan'')');

% =======
% Zoom In
% =======
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'BackgroundColor',[0.701960784313725 0.701960784313725 0.701960784313725], ...
    'FontSize',12, ...
    'FontWeight','bold', ...
    'ListboxTop',0, ...
    'Position',[0.6314312441534145 0.04132231404958678 0.01403180542563143 0.02066115702479339], ...
    'String','+', ...
    'Tag','Pushbutton1', ...
    'Callback', 'gmw(''xzoomin'')');

% =======
% Zoom Out
% =======
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'BackgroundColor',[0.701960784313725 0.701960784313725 0.701960784313725], ...
    'FontSize',12, ...
    'FontWeight','bold', ...
    'ListboxTop',0, ...
    'Position',[0.6454630495790459 0.04132231404958678 0.01403180542563143 0.02066115702479339], ...
    'String','-', ...
    'Tag','Pushbutton1', ...
    'Callback', 'gmw(''xzoomout'')');


% =============
% Y Axes Slider
% =============
gw.w.yslide = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'BackgroundColor',...
    [0.701960784313725 0.701960784313725 0.701960784313725], ...
    'ListboxTop',0, ...
    'Position',[0.6735    0.1033    0.0140    0.8058], ...
    'Style','slider', ...
    'Tag','mx.yslide', ...
    'Min', gw.w.notaAxes1Ylimlim(1), 'Max', gw.w.notaAxes1Ylimlim(2), ...
    'Value', gw.w.notaAxes1Ylimlim(1), ...
    'SliderStep', sstep, ...
    'Callback', 'gmw(''ypan'')');

% =======
% Zoom In
% =======
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'BackgroundColor',[0.701960784313725 0.701960784313725 0.701960784313725], ...
    'FontSize',12, ...
    'FontWeight','bold', ...
    'ListboxTop',0, ...
    'Position',[0.6735266604303087 0.08264462809917356 0.01403180542563143 0.02066115702479339], ...
    'String','+', ...
    'Tag','Pushbutton1', ...
    'Callback', 'gmw(''yzoomin'')');
% ========
% Zoom Out
% ========
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'BackgroundColor',[0.701960784313725 0.701960784313725 0.701960784313725], ...
    'FontSize',12, ...
    'FontWeight','bold', ...
    'ListboxTop',0, ...
    'Position',[0.6735266604303087 0.06198347107438017 0.01403180542563143 0.02066115702479339], ...
    'String','-', ...
    'Tag','Pushbutton1', ...
    'Callback', 'gmw(''yzoomout'')');


% ============================================================================
% Match Axes
% ============================================================================
gw.w.matchAxes = axes('Parent',h0, ...
    'CameraUpVector',[0 1 0], ...
    'CameraUpVectorMode','manual', ...
    'Color','none', ...
    'Position',[0.7437    0.0413    0.2385    0.8884], ...
    'Tag','Axes2', ...
    'XAxisLocation','top', ...
    'XColor',[0 0 0], ...
    'YColor',[0 0 0], ...
    'ZColor',[0 0 0]);

imagesc(gw.m.mtbl)
colormap(flipud(gray))
grid on;

set(gw.w.matchAxes, 'XAxisLocation','top');
set(get(gw.w.matchAxes, 'Children'), 'ButtonDownFcn','gmw(''matchEdit'')');

h2 = text('Parent',gw.w.matchAxes, ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Position',[0.4960629921259843 1.032608695652174 9.160254037844386], ...
    'String','notated event', ...
    'Tag','Axes2Text4', ...
    'VerticalAlignment','bottom');
set(get(h2,'Parent'),'XLabel',h2);
h2 = text('Parent',gw.w.matchAxes, ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Position',[-0.1102    0.4984    9.1603], ...
    'Rotation',90, ...
    'String','performed note', ...
    'Tag','Axes2Text3', ...
    'VerticalAlignment','baseline');
set(get(h2,'Parent'),'YLabel',h2);
h2 = text('Parent',gw.w.matchAxes, ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','right', ...
    'Position',[-3.133858267716535 1.077639751552795 9.160254037844386], ...
    'Tag','Axes2Text2', ...
    'Visible','off');
set(get(h2,'Parent'),'ZLabel',h2);
h2 = text('Parent',gw.w.matchAxes, ...
    'Color',[0 0 0], ...
    'HandleVisibility','off', ...
    'HorizontalAlignment','center', ...
    'Position',[0.4960629921259843 1.065217391304348 9.160254037844386], ...
    'Tag','Axes2Text1', ...
    'VerticalAlignment','bottom');
set(get(h2,'Parent'),'Title',h2);

% ================
% Match Axes Title
% ================
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'BackgroundColor',[0.701960784313725 0.701960784313725 0.701960784313725], ...
    'ListboxTop',0, ...
    'Position',[0.7998    0.0207    0.1179    0.0207], ...
    'String','Match', ...
    'Style','text', ...
    'Tag','StaticText2');
end

% ========================================================================
% Light Up matching notes
% ========================================================================
function lightUp(n)

global NN XDATA YDATA PATCH Ip In InColor IpColor
global gw


if exist('Ip')
    if length(Ip)>1
        for ii = 1:length(Ip)
            set(gw.w.Pp(Ip(ii)), 'FaceColor', IpColor{ii})
        end
    else
        set(gw.w.Pp(Ip), 'FaceColor', IpColor)
    end
    if length(In)>1
        for ii = 1:length(In)
            set(gw.w.Pn(In(ii)), 'FaceColor', InColor{ii})
        end
    else
        set(gw.w.Pn(In), 'FaceColor', InColor)
    end
end

[cc tmp] = find(gw.m.GIn == n);

cc = cc;
In = gw.m.GIn(cc,find(gw.m.GIn(cc,:) ~= 0));
Ip = gw.m.indP(find(gw.m.indN == cc));

% light up both guys
InColor = get(gw.w.Pn(In), 'FaceColor');
IpColor = get(gw.w.Pp(Ip), 'FaceColor');

set(gw.w.Pn(In), 'FaceColor', 'b')
set(gw.w.Pp(Ip), 'FaceColor', 'b')

NN = cc;

%
% Highlight right match
axes(gw.w.matchAxes);
if ~isempty(XDATA) && ~isempty(YDATA)
    set(PATCH, 'FaceColor', 'g', 'EdgeColor', 'g', 'EraseMode', 'xor');
    delete(PATCH);
end

if ~isempty(Ip)
    xdata = [cc-.75 cc+.75 cc+.75 cc-.75];
    ln = length(Ip);
    ydata = [Ip(1)-.75 Ip(1)-.75 Ip(ln)+.75 Ip(ln)+.75];

    PATCH = patch(xdata, ydata, 'g', 'EdgeColor', 'g', 'EraseMode', 'xor');
end

XDATA = xdata;
YDATA = ydata;
disp(PATCH)
end


% ========================================================================
% Step through the match by clicking on notation axis background
% ========================================================================
function stepThrough(direction)

global gw
global NN XDATA YDATA PATCH Ip In InColor IpColor

if NN == 0
  lightUp(1);
  return;
end

if exist('Ip')
    if length(Ip)>1
        for ii = 1:length(Ip)
            set(gw.w.Pp(Ip(ii)), 'FaceColor', IpColor{ii})
        end
    else
        set(gw.w.Pp(Ip), 'FaceColor', IpColor)
    end
    if length(In)>1
        for ii = 1:length(In)
            set(gw.w.Pn(In(ii)), 'FaceColor', InColor{ii})
        end
    else
        set(gw.w.Pn(In), 'FaceColor', InColor)
    end
end

if NN == 0
    xlim = get(gw.w.notaAxes1, 'XLim');
    nn = find(gw.m.Tn >= xlim(1));
    if isempty(nn); nn = 1; end;
    nn = nn(1);
    cc = nn;
else
  if direction > 0
    cc = NN+1;
    if cc > size(gw.m.GIn,1); cc = size(gw.m.GIn,1); end;
  else
    cc = NN-1;
    if cc < 1; cc = 1; end
  end
end

In = gw.m.GIn(cc,find(gw.m.GIn(cc,:) ~= 0));
Ip = gw.m.indP(find(gw.m.indN == cc));

% light up both guys
InColor = get(gw.w.Pn(In), 'FaceColor');
IpColor = get(gw.w.Pp(Ip), 'FaceColor');

set(gw.w.Pn(In), 'FaceColor', 'b')
set(gw.w.Pp(Ip), 'FaceColor', 'b')

NN = cc;
highlightMatch;
end

% ========================================================================
% Edit the match
% ========================================================================
function matchEdit()

global gw

Image = get(gw.w.matchAxes, 'Children');
cdata = get(Image, 'CData');
if iscell(cdata)
    array = sparse(cdata{length(cdata)});
else
    array = sparse(cdata);
end

pt = get(gw.w.matchAxes, 'CurrentPoint');
pt = round(pt(1,1:2));
ii = pt(2);
jj = pt(1);

pn = gw.m.Nn(gw.m.GIn(jj,find(gw.m.GIn(jj,:))),4);
pp = gw.m.Np(gw.m.GIp(ii,find(gw.m.GIp(ii,:))),4);

if array(ii,jj)
    array(ii,jj) = 0;
else
    array(ii,:) = 0;
    if ismember(pp,pn)
        array(ii,jj) = 10;
    else
        array(ii,jj) =  3;
    end
end

set(Image, 'CData', array);

% New, makes match reversion impossible
% does not export match to workspace
matchAccept;
highlightMatch;
end

% =====================
% Highlight right match
% =====================
function highlightMatch()
global gw
global NN XDATA YDATA PATCH Ip In InColor IpColor

axes(gw.w.matchAxes)

if ~isempty(XDATA) & ~isempty(YDATA)
    set(PATCH, 'FaceColor', 'g', 'EdgeColor', 'g', 'EraseMode', 'xor');
    delete(PATCH);
end

if ~isempty(Ip)
    xdata = [NN-.75 NN+.75 NN+.75 NN-.75];
    ln = length(Ip);
    ydata = [Ip(1)-.75 Ip(1)-.75 Ip(ln)+.75 Ip(ln)+.75];
else
    xdata = [0 0 0 0];
    ydata = [0 0 0 0];
end
PATCH = patch(xdata, ydata, 'g', 'EdgeColor', 'g', 'EraseMode', 'xor');
XDATA = xdata;
YDATA = ydata;
end

% ========================================================================
% Accept the match and apply it to the match structure
% ========================================================================
function matchAccept()

global gw

global PATCH XDATA YDATA

Image = get(gw.w.matchAxes, 'Children');
cdata = get(Image, 'CData');
if iscell(cdata)
    array = sparse(cdata{length(cdata)});
else
    array = sparse(cdata);
end

gw.m.mtbl = array;
[gw.m.indP gw.m.indN] = find(gw.m.mtbl);
gw.m.indP = gw.m.indP';
gw.m.indN = gw.m.indN';

[gw.m.realGIp gw.m.Tn gw.m.Bn gw.m.Tp gw.m.Bp gw.m.tempoMap gw.m.periodMap, gw.m.velocMap] = realGroup(gw.m);

gw.m.Nm = [];
m=0;
for ip = 1:length(gw.m.indP)
    n = gw.m.indP(ip);
    c = gw.m.indN(ip);
    m = m+1;
    gw.m.Nm = [gw.m.Nm; gw.m.Np(n,:)];
    gw.m.Nm(m,1:2) = gw.m.Nn(gw.m.GIn(c,1),1:2);
end

gw.m = matchReport(gw.m);

if ~isempty(XDATA) & ~isempty(YDATA)
    set(PATCH, 'FaceColor', 'g', 'EdgeColor', 'g', 'EraseMode', 'xor');
    delete(PATCH);
    PATCH = [];
    XDATA = [];
    YDATA = [];
end

assignin('base', 'M', gw.m);
end

% ========================================================================
% scale the performance axes to display perfomance range that matches
% notation currently in view
% ========================================================================
function xlimP = scalePerf(xlimN)

global gw

nleft = find(gw.m.Tn <= xlimN(1));
nrght = find(gw.m.Tn >= xlimN(2));
if isempty(nrght); nrght = length(gw.m.Tn); end;
if isempty(nleft); nleft = 1; end;

nleft = nleft(length(nleft));
nrght = nrght(1);

ptl = gw.m.Tp(nleft);
ptr = gw.m.Tp(nrght);

ntl = gw.m.Tn(nleft);
ntr = gw.m.Tn(nrght);
if ntl == 0; ntl = 1; end;

xlimP = [xlimN(1)*(ptl/ntl) xlimN(2)*(ptr/ntr)];
end

% ========================================================================
% scale ticks
% ========================================================================
function [TP, PP] = scaleTicks(beats)

global gw

TP = [];
PP = [];
for bb = 1:length(beats);
   pp = find(gw.m.Bp == beats(bb));
   
   if ~isempty(pp)
      pp = pp;
      tp = gw.m.Tp(pp);
   else
      pp1 = find(gw.m.Bp < beats(bb));
      pp2 = find(gw.m.Bp > beats(bb));
      
      if ~isempty(pp2)
	 pp2 = pp2(1);	 
      else
	 pp2 = length(gw.m.Bp);
	 pp1 = pp2-1;
      end
      if ~isempty(pp1)
	 pp1 = pp1(length(pp1));
      else
	 pp1 = 1;
         pp2 = 2;
      end

      db = diff(gw.m.Bp([pp1 pp2]));
      dt = diff(gw.m.Tp([pp1 pp2]));
      
      delta = beats(bb) - gw.m.Bp(pp1);
      
      pp = pp1;
      tp = gw.m.Tp(pp1) + delta * (dt/db);
   end
%modif par seb : 
%    TP = [TP, tp];
%    PP = [PP, pp];
   TP = [TP; tp];
   PP = [PP; pp];
end
end

% ========================================================================
% xpan
% ========================================================================
function [] = xpan()

global gw

na = gw.w.notaAxes1;
pa = gw.w.perfAxes1;
s = gw.w.xslide;
xlimlim = gw.w.notaAxes1Xlimlim;

maxrange = diff(xlimlim);

xlim = get(na, 'XLim');
range = diff(xlim);
value = get(s, 'Value');

sprop = range/maxrange;
n = 1/sprop;
left = xlimlim(1) + ((n-1)/n)*value;
right = left+range;
%if right > xlimlim(2)
%   right = xlimlim(2);
%   left = right-range;
%end

set(na, 'XLim', [left right]);
plim = scalePerf([left right]);
set(pa, 'XLim', plim);

% =============
% second axes 2
% =============
xlim1 = get(gw.w.notaAxes1, 'XLim');
xlim2 = xlim1*gettempo(gw.m.Nn)/60;
set(gw.w.notaAxes2, 'XLim', xlim2, 'XGrid', 'on');
set(gw.w.notaAxes2, 'Ylim', get(gw.w.notaAxes1, 'YLim'));

set(gw.w.perfAxes2, 'Xlim', get(gw.w.perfAxes1, 'XLim'));
t1 = get(gw.w.notaAxes2, 'XTick');
tlabel = get(gw.w.notaAxes2, 'XTickLabel');
t2 = scaleTicks(t1);
set(gw.w.perfAxes2, 'XTick', t2, 'XTickLabel', tlabel);
set(gw.w.perfAxes2, 'Ylim', get(gw.w.perfAxes1, 'YLim'));

% ==========
% match axes
% ==========
ii = find(gw.m.Np(gw.m.GIp(:,1),6) >= plim(1)-1 & gw.m.Np(gw.m.GIp(:,1),6) <= plim(2));
jj = find(gw.m.Nn(gw.m.GIn(:,1),6) >= left-1    & gw.m.Nn(gw.m.GIn(:,1),6) <= right);

iileft = ii(1); iiright = ii(length(ii));
jjleft = jj(1); jjright = jj(length(jj));
set(gw.w.matchAxes, 'YLim', [iileft-.5 iiright+.5], ...
    'XLim', [jjleft-.5 jjright+.5]);
end

% ========================================================================
% xzoomin
% ========================================================================
function [] = xzoomin()

global gw

na = gw.w.notaAxes1;
pa = gw.w.perfAxes1;
s = gw.w.xslide;
xlimlim = gw.w.notaAxes1Xlimlim;

maxrange = diff(xlimlim);

xlim = get(na, 'XLim');
ylim = get(na, 'YLim');
range = diff(xlim);
value = get(s, 'Value');
newrange = range * (3/4);

sprop = newrange/maxrange; 	% this is the value we should be working with
sprop = max(0.02, sprop);
newrange = maxrange*sprop;
if sprop >= 1
    width = Inf;
else
    width = 1 ./ ((1./sprop) - 1);   % but this funny transformation is required
end

left = xlim(1);
right = left+newrange;

set(na, 'XLim', [left right]);
plim = scalePerf([left right]);
set(pa, 'XLim', plim);

n = 1/sprop;
value = ((n-1)/n)*left;

value = max(value, xlimlim(1));
value = min(value, xlimlim(2));

if strcmp(computer, 'MAC2')
    sstep = [0.02 sprop];
else
    sstep = [0.02 width];
end

set(s, 'Value', value, 'SliderStep', sstep);

% =============
% second axes 2
% =============
xlim1 = get(gw.w.notaAxes1, 'XLim');
xlim2 = xlim1*gettempo(gw.m.Nn)/60;
set(gw.w.notaAxes2, 'XLim', xlim2, 'XGrid', 'on');
set(gw.w.notaAxes2, 'Ylim', get(gw.w.notaAxes1, 'YLim'));

set(gw.w.perfAxes2, 'Xlim', get(gw.w.perfAxes1, 'XLim'));
t1 = get(gw.w.notaAxes2, 'XTick');
tlabel = get(gw.w.notaAxes2, 'XTickLabel');
t2 = scaleTicks(t1);
set(gw.w.perfAxes2, 'XTick', t2, 'XTickLabel', tlabel);
set(gw.w.perfAxes2, 'Ylim', get(gw.w.perfAxes1, 'YLim'));

% ==========
% match axes
% ==========
ii = find(gw.m.Np(gw.m.GIp(:,1),6) >= plim(1)-1 & gw.m.Np(gw.m.GIp(:,1),6) <= plim(2));
jj = find(gw.m.Nn(gw.m.GIn(:,1),6) >= left-1    & gw.m.Nn(gw.m.GIn(:,1),6) <= right);

iileft = ii(1); iiright = ii(length(ii));
jjleft = jj(1); jjright = jj(length(jj));
set(gw.w.matchAxes, 'YLim', [iileft-.5 iiright+.5], ...
    'XLim', [jjleft-.5 jjright+.5]);
end

% ========================================================================
% xzoomout
% ========================================================================
function [] = xzoomout()

global gw

na = gw.w.notaAxes1;
pa = gw.w.perfAxes1;
s = gw.w.xslide;
xlimlim = gw.w.notaAxes1Xlimlim;

maxrange = diff(xlimlim);

xlim = get(na, 'XLim');
ylim = get(na, 'YLim');
range = diff(xlim);
newrange = range * (4/3);
newrange = min(maxrange, newrange);

sprop = newrange/maxrange;   	  % this is the value we should be working with
if sprop >= 1
    width = Inf;
else
    width = 1 ./ ((1./sprop) - 1);   % but this funny transformation is required
    if width > 1000
        width = Inf;
    end
end

left = xlim(1);
right = left+newrange;
if right > xlimlim(2)
    right = xlimlim(2);
    left = right - newrange;
    if left < xlimlim + 0.001
        left = xlimlim(1)
    end
end

set(na, 'XLim', [left right]);
plim = scalePerf([left right]);
set(pa, 'XLim', plim);

n = 1/sprop;
if n-1 == 0
    value = 0;
else
    value = (n/(n-1))*left;
end

value = min(value, xlimlim(2));
value = max(value, xlimlim(1));

[left, right, value];

if strcmp(computer, 'MAC2')
    sstep = [0.02 sprop];
else
    sstep = [0.02 width];
end
set(s, 'Value', value, 'SliderStep', sstep);

% =============
% second axes 2
% =============
xlim1 = get(gw.w.notaAxes1, 'XLim');
xlim2 = xlim1*gettempo(gw.m.Nn)/60;
set(gw.w.notaAxes2, 'XLim', xlim2, 'XGrid', 'on');
set(gw.w.notaAxes2, 'Ylim', get(gw.w.notaAxes1, 'YLim'));

set(gw.w.perfAxes2, 'Xlim', get(gw.w.perfAxes1, 'XLim'));
t1 = get(gw.w.notaAxes2, 'XTick');
tlabel = get(gw.w.notaAxes2, 'XTickLabel');
t2 = scaleTicks(t1);
set(gw.w.perfAxes2, 'XTick', t2, 'XTickLabel', tlabel);
set(gw.w.perfAxes2, 'Ylim', get(gw.w.perfAxes1, 'YLim'));

% ==========
% match axes
% ==========
ii = find(gw.m.Np(gw.m.GIp(:,1),6) >= plim(1)-1 & gw.m.Np(gw.m.GIp(:,1),6) <= plim(2));
jj = find(gw.m.Nn(gw.m.GIn(:,1),6) >= left-1    & gw.m.Nn(gw.m.GIn(:,1),6) <= right);

iileft = ii(1); iiright = ii(length(ii));
jjleft = jj(1); jjright = jj(length(jj));
set(gw.w.matchAxes, 'YLim', [iileft-.5 iiright+.5], ...
    'XLim', [jjleft-.5 jjright+.5]);
end

% ========================================================================
% ypan
% ========================================================================
function [] = ypan()

global gw

na = gw.w.notaAxes1;
pa = gw.w.perfAxes1;
s = gw.w.yslide;
ylimlim = gw.w.notaAxes1Ylimlim;

maxrange = diff(ylimlim);
ylim = get(na, 'YLim');
range = diff(ylim);
value = get(s, 'Value');

sprop = range/maxrange;
n = 1/sprop;
bot = ylimlim(1) + ((n-1)/n)*value;
top = bot+range;
if top > ylimlim(2)
    top = ylimlim(2);
    bot = top-range;
end

set(na, 'YLim', [bot top]);
set(pa, 'YLim', [bot top]);
set(gw.w.notaAxes2, 'YLim', [bot top]);
set(gw.w.perfAxes2, 'YLim', [bot top]);
end

% ========================================================================
% yzoomin
% ========================================================================
function [] = yzoomin()

global gw

na = gw.w.notaAxes1;
pa = gw.w.perfAxes1;
s = gw.w.yslide;
ylimlim = gw.w.notaAxes1Ylimlim;

maxrange = diff(ylimlim);

xlim = get(na, 'XLim');
ylim = get(na, 'YLim');
range = diff(ylim);
value = get(s, 'Value');
newrange = range * (3/4);

sprop = newrange/maxrange; 	% this is the value we should be working with
sprop = max(0.05, sprop);
newrange = maxrange*sprop;
if sprop >= 1
    width = Inf;
else
    width = 1 ./ ((1./sprop) - 1);   % but this funny transformation is required
end

bot = ylim(1);
top = bot+newrange;

set(na, 'YLim', [bot top]);
set(pa, 'YLim', [bot top]);
set(gw.w.notaAxes2, 'YLim', [bot top]);
set(gw.w.perfAxes2, 'YLim', [bot top]);

n = 1/sprop;
value = ((n-1)/n)*bot;

value = max(value, ylimlim(1));
value = min(value, ylimlim(2));

if strcmp(computer, 'MAC2')
    sstep = [0.05 sprop];
else
    sstep = [0.05 width];
end
set(s, 'Value', value, 'SliderStep', sstep);
end

% ========================================================================
% yzoomout
% ========================================================================
function [] = yzoomout()

global gw

na = gw.w.notaAxes1;
pa = gw.w.perfAxes1;
s = gw.w.yslide;
ylimlim = gw.w.notaAxes1Ylimlim;

maxrange = diff(ylimlim);

xlim = get(na, 'XLim');
ylim = get(na, 'YLim');
range = diff(ylim);
newrange = range * (4/3);
newrange = min(maxrange, newrange);

sprop = newrange/maxrange;   	  % this is the value we should be working with
if sprop >= 1
    width = Inf;
else
    width = 1 ./ ((1./sprop) - 1);   % but this funny transformation is required
    if width > 1000
        width = Inf;
    end
end

bot = ylim(1);
top = bot+newrange;
if top > ylimlim(2)
    top = ylimlim(2);
    bot = top - newrange;
    if bot < ylimlim + 0.001
        bot = ylimlim(1)
    end
end

set(na, 'YLim', [bot top]);
set(pa, 'YLim', [bot top]);
set(gw.w.notaAxes2, 'YLim', [bot top]);
set(gw.w.perfAxes2, 'YLim', [bot top]);

n = 1/sprop;
if n-1 == 0
    value = 0;
else
    value = (n/(n-1))*top;
end

value = min(value, ylimlim(2));
value = max(value, ylimlim(1));

[top, bot, value];

if strcmp(computer, 'MAC2')
    sstep = [0.05 sprop];
else
    sstep = [0.05 width];
end
set(s, 'Value', value, 'SliderStep', sstep);
end

% =======================================================================
function [PAT] = proll(N, tunit, c, fill)
%[PAT] = proll(N, color, fill)
%
% Plots a note matrix of the type returned by scoread. PROLL returns
% a column vector of handles to patch objects. Fill (optional)
% specifies whether or not patches are drawn filled. C, if specified,
% determines the fill color, otherwise colors denote velocity.


%---------------------
% Parse calling syntax
%---------------------
if nargin < 2; tunit    = 'beat'; end;
if nargin < 3; c        = '';     end;
if nargin < 4; fill     =  1;     end;

t = onset(N, tunit); d = dur (N, 'sec'); p = pitch(N); a = floor(velocity(N)/2)+1;

% -------------------------------------
% Try to behave like the 'plot' command
% -------------------------------------
state = get(gca, 'NextPlot');
if state(1:3) == 'rep'
   delete(get(gca, 'Children'));
end

% ----------------------------
% Draw one patch for each note
% ----------------------------
if nargout > 0
  PAT = [];
end
h = flipud(hot);
for i= 1:length(N(:,1))
   x = [t(i) t(i) t(i)+d(i) t(i)+d(i)];
   y = [p(i)-.5 p(i)+.5 p(i)+.5 p(i)-.5];

   if ~isempty(c); color = c; else; 
       color = h(a(i), :);
   end;
   if fill
      fcolor = color;
      ecolor = 'k';
   else
      fcolor = 'none';
      ecolor = 'k';
   end

   if color == 't';
      % df = [0; diff(round(1000*N(:,1)))];
      pat= text(t(i), p(i), num2str(i), 'FontSize', 7);
   else
      fn = sprintf('gmw(''lightUp'',%d)', i);
      pat= patch('Xdata', x, 'YData', y, ...
	    'FaceColor', fcolor, 'EdgeColor', ecolor, 'ButtonDownFcn', fn);
   end

   if nargout > 0
      PAT = [PAT; pat];
   end
end

colormap(flipud(hot));
end

% =======================
% Clean up before exiting
% =======================
function exitWindow()

global gw

global NN XDATA YDATA PATCH Ip In InColor IpColor

clear NN XDATA YDATA PATCH Ip In InColor IpColor
assignin('base', 'Medit', gw.m);
end





function ob = onset(nmat,timetype)
% Onset times in beats or seconds
% ob = onset(nmat,<timetype>);
% Returns the onset times in beats or seconds of the notes in NMAT
%
% Input argument:
%	NMAT = notematrix
%	TIMETYPE (Optional) = timetype ('BEAT' (default) or SEC (Seconds).
%
% Output:
% 	OB = onset times in beats or seconds of notes in NMAT
%
%  Date		Time	Prog	Note
% 4.6.2002	18:36	TE	Created under MATLAB 5.3 (Mac)
%� Part of the MIDI Toolbox, Copyright � 2004, University of Jyvaskyla, Finland
% See License.txt

if isempty(nmat), ob = []; return; end
if nargin<2, timetype='beat'; end

if strcmp(timetype, 'sec')
	ob = nmat(:,6);
elseif strcmp(timetype, 'beat')
	ob = nmat(:,1);
else
	disp(['Unknown TIMETYPE option:''', timetype,''''])
	disp('     =>''beat'' used instead')
	ob = nmat(:,1);
end
end

function d = dur(nmat,timetype)
% Note durations in beats or seconds
% function d = dur(nmat,<timetype>);
% Returns the durations in beats or seconds of the notes in NMAT
%
% Input argument:
%	NMAT = notematrix
%	TIMETYPE (Optional) = timetype ('BEAT' (default) or SEC (Seconds).
%
% Output:
%	D = note durations in beats or in seconds
%
%  Date		Time	Prog	Note
% 4.6.2002	18:36	TE	Created under MATLAB 5.3 (Mac)
%� Part of the MIDI Toolbox, Copyright � 2004, University of Jyvaskyla, Finland
% See License.txt

if isempty(nmat), d = []; return; end
if nargin<2, timetype='beat'; end

if strcmp(timetype, 'sec')
	d = nmat(:,7);
elseif strcmp(timetype, 'beat')
	d = nmat(:,2);
else
	disp(['Unknown TIMETYPE option:''', timetype,''''])
	disp('     =>''beat'' used instead')
	d = nmat(:,2);
end
end

function p = pitch(nmat)
% MIDI note numbers
% p = pitch(nmat);
% Returns the MIDI note numbers of the events in NMAT
%
% Input argument:
%	NMAT = notematrix
%
% Output:
%	P = MIDI note numbers in NMAT
%  Date		Time	Prog	Note
% 4.6.2002	18:36	PT	Created under MATLAB 5.3 (Mac)
%� Part of the MIDI Toolbox, Copyright � 2004, University of Jyvaskyla, Finland
% See License.txt

if isempty(nmat), return; end
p = nmat(:,4);
end

function v = velocity(nmat)
% Velocities of notes
% v = velocity(nmat);
% Returns the velocities of the notes in NMAT
%
% Input argument:
%	NMAT = notematrix
%
% Output:
%	V = velocity values of notes in NMAT
%
%  Date		Time	Prog	Note
% 4.6.2002	18:36	TE	Created under MATLAB 5.3 (Mac)
%� Part of the MIDI Toolbox, Copyright � 2004, University of Jyvaskyla, Finland
% See License.txt

if isempty(nmat), return; end

v = nmat(:,5);
end

function bpm = gettempo(nmat)
% Get tempo (in BPM)
% bpm = gettempo(nmat)
% Return the tempo of the NMAT. Note that MIDI files 
% can be encoded using any arbitrary tempo.
%
% Input argument:
%	NMAT = notematrix
%
% Output:
%	BPM = Tempo (in beats per minute)
%
% Change History :
% Date		Time	Prog	Note
% 1.7.2003	18:52	TE	Created under MATLAB 5.3 (PC)
%� Part of the MIDI Toolbox, Copyright � 2004, University of Jyvaskyla, Finland
% See License.txt

if isempty(nmat), return; end
beat=nmat(end,1);
beatdur=nmat(end,6); %
bpm = (60/beatdur)*beat;
end


