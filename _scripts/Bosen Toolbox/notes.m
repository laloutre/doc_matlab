function datamat = notes(notetime,varargin)

%% Identifies notes (from onset to offset) in discrete notetime files
% Analyzes those single notes
% IN: notetime files or mats
% OUT: 3D matrix (mean+keys+pedals) * (mean per key + SD per key + each note on this key) * (descriptives)
%
% varargin: 'printout' for outputting formatted text/table file
%
%
% Heuristics used in identying notes:
% - Discontinuity in timestamp sequence: standard, key reaches zero-position which means a note has ended and another starts at next timestamp
%       (The algorithm compensates for possibly missing (or dedundant) timestamps by filling in (resp. erasing))
%       (Noisiness at note onset with extremely low depression values is filtered, and th onset pushed forward)
% - Keep all notes for which key depression is deep enough to possibly allow the hammer to be launched: this compensates for wrongfully missing MHVs, and discards the feeble, unconsequential key depressions
%       (different thresholds depending on whether a MHV is registered: threshold low when MHV, a little higher when not)
% - Detect local minima in case of successive notes relaunched before full key release: new note validated if local minimum low enough to allow for the hammer to be relaunched (escapement point)
%       (local minima also need to be lower by a certain threshold than the max depression before and afterwards)
%
%
% data(1,1,:) = means per key prorated by number of notes (most relevant)
% data(1,2,:) = means of SDs per key prorated by number of notes
% data(1,3,:) = SDs between all notes (most relevant)
% 
% data(1,1,2) = total number of notes in the file
% data(1,1,3) = mean number of notes per key
% data(1,2,2) = SD of number of notes per key
%
% data(x,1,:) = means for key x
% data(x,2,:) = SDs for key x
% data(x,y>2,:) = parameters for note y on key x + each key depression value
%
% Third dimension per note:
%
% Basics:
% 1 - Note #
% 2 - Note onset
% 3 - Note offset
% 4 - Note duration
% 5 - Mean key depression during note (Amoy)
% 6 - Max key depression during note (Amax)
% 7 - t.Amax
% 8 - MHV
% 9 - t.MHV
%
% Attack:
% 10 - speed of attack: dt(MHV-onset)
% 11 - slope of attack: MHV/speed.of.attack
% 12 - speed of key attack: dt(Amax-onset)
% 13 - slope of key attack: Amax/speed.of.key.attack
% 14 - directness / hammer-key attacks match: dt(MHV-Amax)
% 15 - hammer/key correlation: MHV/Amax
% 16 - sharpness of key attack slope: (A@((t.onset+t.amax)/2))/Amax [i.e. A@mid-attack/Amax; >.5: sharp key attack]
% 17 - sharpness of attack slope: (A@((t.onset+t.mhv)/2))/A@(t.mhv) [i.e. A@mid-h.attack/A@t.mhv; >.5: sharp attack]
%
% Depression profile:
% FKD
% 18 - full key-depression start: t(A>maxdthresh for 1st time)
% 19 - full key-depression end: t(A>maxdthresh for last consecutive time)
% 20 - full key-depression duration: fkd start-end
% 21 - full key-depression rate: fkd.duration/note.duration
% 22 - pre- full key-depression duration: t(fkd.start-onset), equiv. to rise time (attack when Amax off)
% 23 - pre- full key-depression rate: pre-fkd.duration/note.duration
% 24 - post- full key-depression duration: t(offset-fkd.end), equiv. to release time
% 25 - post- full key-depression rate: post-fkd.duration/note.duration
%
% ADSR 
% 26 - t(attack.end=decay.start), i.e. slowest rising in increasing angles values sequence (over 5 samples)
% 27 - t(sustain.start or decay.end), i.e. slowest dropping in decreasing angles values sequence (over 5 samples) or first stable point in plateau
% 28 - t(release.start=sustain.end), i.e. fastest dropping in decreasing angles values sequence (over 5 samples)
% 29 - attack duration (t.attack.end-t.onset)
% 30 - sustain duration (t.release.start-t.sustain.start) 
% 31 - release duration (t.offset-t.release.start)
% 32 - attack rate (t.attack.end-t.onset)/note.duration
% 33 - sustain rate (t.release.start-t.sustain.start)/note.duration
% 34 - release rate (t.offset-t.sustain.end)/note.duration
%
% Pedals:
% 35 - mean pedal 109 depression during note
% 36 - mean pedal 111 depression during note
% 37 - pedal 109 value at note onset
% 38 - pedal 111 value at note onset
% 39 - pedal 109 value at t.mhv
% 40 - pedal 111 value at t.mhv
% 41 - pedal 109 value at note offset
% 42 - pedal 111 value at note offset
% 43 - pedal 109 duration of depression during note
% 44 - pedal 111 duration of depression during note
%
% Late additions:
% 45 - key attack mean depression - aka attack Amoy (sum of As till Amax / attack duration)
% 46 - key attack mean depression rate (/Amax)
%
%%

printout=0; nameout=[];
ivar=1;
while ivar<=length(varargin),
    if strcmp(varargin(ivar),'nameout') && ivar<length(varargin), nameout=varargin{ivar+1}; ivar=ivar+1; end
    if strcmp(varargin(ivar),'printout'), printout=1; end
    if strcmp(varargin(ivar),'noprintout'), printout=0; break; end
    ivar = ivar+1;
end

if ischar(notetime), notet = dlmread(notetime);
else notet = notetime;
end
[nt1,nt2] = size(notet);


% ------- PARAMETRIC VARS -------

npar = 46; % Number of parameters gathered for each note and mean per key thereof
threshold=80; % Threshold of min key depression for considering a note
threshup=140; % Threshold for key depression falling back to before we can envision a new note
threshdown=125; % Threshold for minimum key depression to account for a note even without hammer launch
seuil=40; % Threshold for local min detection
floor=11; % Minimum level not to account for missing timestamps
attackfloor=30; % Minimum level after which to account for attack
maxdthresh=188; % Threshold for considering the key fully depressed
depth=11; % number of samples to account for in finding min/max increase over
megatop=1e9; % Use for highest init.

% -------

% --- Passing vector [mkey per key, mall in all] for means and SDs ---
% 0 = no mean/SD calculated for this parameter; 1 = non-zero mean/SD; 2 = full mean/SD incl. zeros; 3 = non-(9.tMHV=0) mean/SD

%      [1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46]  sheer visual indication of rank index
mkey = [0  0  0  1  1  1  0  1  0  1  1  1  1  1  1  1  1  0  0  2  2  2  2  2  2  0  0  0  1  2  1  1  2  1  2  2  2  2  3  3  2  2  2  2  1  1];




datamat = zeros(2,3,npar); % yet unknown size

alpha = 0; % Counter for removing empty lines


%% ------- NOTE DETECTION ------------------------------------------

for i=1:nt1 % Notetime's lines
    if (notet(i,1)<128) % This is a key or pedal
        
        % --- Counters ---
        
        datamat(i+1-alpha,1,1) = notet(i,1); % Key #
        nbnotes = 0; % Number of notes on this key
        
        
        % --- Go through the key and identify its separated notes
        j=3;
        while j<=nt2-1 % Go through all timestamps
            
            if ((notet(i,j-2)~=notet(i,j)-2 && notet(i,j-2)~=notet(i,j) && ((notet(i,j-2)~=notet(i,j)-4 && notet(i,j-2)~=notet(i,j)-6 && notet(i,j-2)~=notet(i,j)-8) || notet(i,j+1)<floor) ) || (minlocal==1 && j>jminloc) || j==3)...
                    && notet(i,j)>0 && ~isnan(notet(i,j))
                % If new note, as identified by 2 successive samples farther apart than 8 msec, or by the existence of a local min (which becomes the note's onset)
                
                nbnotes = nbnotes + 1; % One more note on this key
                datamat(i+1-alpha,nbnotes+2,3:npar) = 0; % params. init.
                
                while notet(i,j+1)<4 || (notet(i,j+1)==4 && (j<=nt2-3 && notet(i,j+3)<4)), % Eliminate the feeble noisy initial key depression
                    if j<nt2-2, j=j+2; 
                    else break; 
                    end
                end 
                datamat(i+1-alpha,nbnotes+2,2) = notet(i,j); % Note onset
                tfin=0; % tmp note offset
                j3=j; % Preemptive loop index for identifying local minima
                minlocal=0; % Tag for a local min
                amaxav=0; % Max value before a possible local min
                amaxap=0; % Max value after a possible local min
                jminloc=0; % Rank of local min
                jini=j; % Store the rank of first timestamp
                shift=1; % Rank for placing values when missing timestamp occurs
                

                % --- Detection of local min ---

                while(j3<nt2-5 && (notet(i,j3)==notet(i,j3+2)-2 || ((notet(i,j3)==notet(i,j3+2)-4 || notet(i,j3)==notet(i,j3+2)-6 || notet(i,j3)==notet(i,j3+2)-8) && notet(i,j3+1)>=floor) )) % Continuity
                    % ! Local mins not detected in the first 4 msecs of the very first note in the file and in the last 6 msecs of the very last note in the file (which does not matter one bit).
                    if j3>6 % Not at the beginning of notetime
                        amaxav=max(amaxav,notet(i,j3+1)); % Set the left-side max
                        
                        if (notet(i,j3+1)<=amaxav-seuil) && (notet(i,j3)==notet(i,j3+4)-2 || notet(i,j3)==notet(i,j3+4)-4 || notet(i,j3)==notet(i,j3+4)-6 || notet(i,j3)==notet(i,j3+4)-8)...
                                && (notet(i,j3+1)<=notet(i,j3-1) && notet(i,j3+1)<=notet(i,j3-3) && notet(i,j3+1)<=notet(i,j3+3) && notet(i,j3+1)<=notet(i,j3+5))...
                                && ((j3<33 && notet(i,j3+1)<=notet(i,4)) || notet(i,j3+1)<=notet(i,j3-29)) && ((j3>nt2-31 && notet(i,j3)<=notet(i,2*fix(nt2/2))) || (j3<=nt2-31 && notet(i,j3+1)<=notet(i,j3+31)))...
                                && ((notet(i,1)<108 && notet(i,j3+1)<threshup) || (notet(i,1)>=108 && notet(i,j3+1)<threshold)) && amaxav>=threshold,
                                
                            % Continuous +...
                            % smaller value than before and after, i.e. very local minimum +...
                            % smaller value than 'seuil' before and after (largerly local min) +...
                            % smaller than 'threshup' if key or than 'threshold' (lower) if pedal
                            % smaller than amaxav-'seuil' with amaxav>'threshold'.......
                            % => Local min in j3, leftward validated

                            j4=j3+2;
                            while(j4<nt2-7 && notet(i,j4)>0 && notet(i,j4)==notet(i,j4+2)-2 || ((notet(i,j4)==notet(i,j4+2)-4 || notet(i,j4)==notet(i,j4+2)-6 || notet(i,j4)==notet(i,j4+2)-8) && notet(i,j4+1)>=floor) )
                                % Forward loop to find right-side max, w/ continuity
                                amaxap=max(amaxap,notet(i,j4+1)); % Right-side max
                                if (notet(i,j4)==notet(i,j4+4)-2 || notet(i,j4)==notet(i,j4+4)-4 || notet(i,j4)==notet(i,j4+4)-6 || notet(i,j4)==notet(i,j4+4)-8)...
                                        && notet(i,j4+1)>=notet(i,j4+3) && notet(i,j4+1)>=notet(i,j4+5) && ((j4<33 && notet(i,j4+1)>=notet(i,4)) || notet(i,j4+1)>=notet(i,j4-29))...
                                        && ((j4>nt2-31 && notet(i,j4)<=notet(i,2*fix(nt2/2))) || (j4<=nt2-31 && notet(i,j4+1)>=notet(i,j4+31))) && notet(i,j4+1)>=notet(i,j3+1)+seuil && notet(i,j4+1)>=threshold,
                                    
                                    % Continuous +...
                                    % larger value than after, i.e. very local forward maximum +...
                                    % larger value than 'seuil' before and after (largerly local max) +...
                                    % over 'threshold' and A.j3 + 30.......
                                    % => Local max in j4
                                    
                                    break; % done
                                else
                                    j4=j4+2; % go on until no more continuity or next max
                                end
                            end
                            if amaxap-seuil>=notet(i,j3+1) && amaxav>=threshdown && amaxap>=threshdown % Significant local min
                                    jminloc=j3; % Index of local min
                                    minlocal=1; % Existence of local min
                                    j3=nt2-5; % Switch to the end to break out of loop
                            elseif notet(i,1)<108 % It's a key: run this conditional test
                                % detection of a second hammer after a local min
                                for h=i+1:nt1
                                    if notet(h,1)==notet(i,1)+128 % hammer corresponding to the key
                                        for jh=3:2:(nt2-2)
                                            if notet(h,jh)>notet(i,j3) && notet(h,jh)<notet(i,j4) % hammer activated after the local min and before the end of the note event
                                                jminloc=j3;
                                                minlocal=1;
                                                j3=nt2-5;
                                                break;
                                            end
                                        end
                                    end
                                end
                            end

                        end
                    end
                    j3=j3+2;
                end

                
                % --- Back to keys and pedals
                
                while((j<=(nt2-3)) && (notet(i,j)==notet(i,j+2)-2 || ((notet(i,j)==notet(i,j+2)-4 || notet(i,j)==notet(i,j+2)-6 || notet(i,j)==notet(i,j+2)-8) && notet(i,j+1)>=floor) || notet(i,j)==notet(i,j+2)) && (minlocal==0 || j<jminloc)) 
                    % Loop till end of note: continuity + test conditions
                    
                    if  notet(i,j)==notet(i,j+2), tfin=notet(i,j)+2; else tfin = notet(i,j+2); end % Test of recurring final timestamp
                    
                    % CORREcTIONS
                    if notet(i,j)==notet(i,j+2)-4 % One missing timestamp
                        if notet(i,j)~=notet(i,j-2)
                            datamat(i+1-alpha,nbnotes+2,npar+(j-jini)/2+shift+1) = (notet(i,j+1)+notet(i,j+3))/2; % Add virtual mean value
                            datamat(i+1-alpha,nbnotes+2,5) = datamat(i+1-alpha,nbnotes+2,5) + (notet(i,j+1)+notet(i,j+3))/2; % One more virtual angle
                            shift=shift+1;
                        end
                    end
                    if notet(i,j)==notet(i,j+2)-6 % Two missing values
                        if notet(i,j)~=notet(i,j-2)
                            datamat(i+1-alpha,nbnotes+2,npar+(j-jini)/2+shift+1) = (notet(i,j+1)+notet(i,j+3))/2; % Add virtual mean value
                            datamat(i+1-alpha,nbnotes+2,5) = datamat(i+1-alpha,nbnotes+2,5) + (notet(i,j+1)+notet(i,j+3))/2; % One more virtual angle
                            shift=shift+1;
                        end
                        if j>4,
                            if notet(i,j)~=notet(i,j-4)
                                datamat(i+1-alpha,nbnotes+2,npar+(j-jini)/2+shift+2) = (notet(i,j+1)+notet(i,j+3))/2; % Add virtual mean value
                                datamat(i+1-alpha,nbnotes+2,5) = datamat(i+1-alpha,nbnotes+2,5) + (notet(i,j+1)+notet(i,j+3))/2; % One more virtual angle
                                shift=shift+1;
                            end
                        end
                    end
                    if notet(i,j)==notet(i,j+2)-8 % Three missing values
                        if notet(i,j)~=notet(i,j-2)
                            datamat(i+1-alpha,nbnotes+2,npar+(j-jini)/2+shift+1) = (notet(i,j+1)+notet(i,j+3))/2; % Add virtual mean value
                            datamat(i+1-alpha,nbnotes+2,5) = datamat(i+1-alpha,nbnotes+2,5) + (notet(i,j+1)+notet(i,j+3))/2; % One more virtual angle
                            shift=shift+1;
                        end
                        if j>4,
                            if notet(i,j)~=notet(i,j-4)
                                datamat(i+1-alpha,nbnotes+2,npar+(j-jini)/2+shift+2) = (notet(i,j+1)+notet(i,j+3))/2; % Add virtual mean value
                                datamat(i+1-alpha,nbnotes+2,5) = datamat(i+1-alpha,nbnotes+2,5) + (notet(i,j+1)+notet(i,j+3))/2; % One more virtual angle
                                shift=shift+1;
                            end
                        end
                        if j>6,
                            if notet(i,j)~=notet(i,j-6)
                                datamat(i+1-alpha,nbnotes+2,npar+(j-jini)/2+shift+3) = (notet(i,j+1)+notet(i,j+3))/2; % Add virtual mean value
                                datamat(i+1-alpha,nbnotes+2,5) = datamat(i+1-alpha,nbnotes+2,5) + (notet(i,j+1)+notet(i,j+3))/2; % One more virtual angle
                            end
                        end
                    end
                    
                    % --- Fill in note parameters
                    datamat(i+1-alpha,nbnotes+2,npar+(j-jini)/2+shift) = notet(i,j+1); % Values stored
                    datamat(i+1-alpha,nbnotes+2,5) = datamat(i+1-alpha,nbnotes+2,5) + notet(i,j+1); % One more angle
                    if notet(i,j+1) > datamat(i+1-alpha,nbnotes+2,6), % new Amax
                        datamat(i+1-alpha,nbnotes+2,7) = notet(i,j);  % t.Amax
                        datamat(i+1-alpha,nbnotes+2,6) = notet(i,j+1); % Amax
                    end

                    
                    j=j+2;
                end
                
                % Q&D fix for last timestamp in note
                datamat(i+1-alpha,nbnotes+2,npar+(j-jini)/2+shift) = notet(i,j+1); % Last value stored
                datamat(i+1-alpha,nbnotes+2,5) = datamat(i+1-alpha,nbnotes+2,5) + notet(i,j+1); % One last angle
                
                
                % Attack Amoy
%                 for ja=npar+1:npar+(datamat(i+1-alpha,nbnotes+2,7)-datamat(i+1-alpha,nbnotes+2,2))/2+1, % Every stored A till t.Amax
%                     datamat(i+1-alpha,nbnotes+2,45) = datamat(i+1-alpha,nbnotes+2,45) + datamat(i+1-alpha,nbnotes+2,ja); % One more angle for attack Amoy
%                 end
                
                jammin = npar+1;
                while datamat(i+1-alpha,nbnotes+2,jammin)<=attackfloor && jammin<size(datamat,3), jammin=jammin+1; end % Delay attack account until depression over floor
                
                for ja=jammin:npar+(datamat(i+1-alpha,nbnotes+2,7)-datamat(i+1-alpha,nbnotes+2,2))/2+1, % Every stored A till t.Amax
                    datamat(i+1-alpha,nbnotes+2,45) = datamat(i+1-alpha,nbnotes+2,45) + datamat(i+1-alpha,nbnotes+2,ja); % One more angle for attack Amoy
                end
                
                % --- Test conditions: note validity ---
                
                valid=0;
                if (datamat(i+1-alpha,nbnotes+2,6)>=threshdown), valid=1; % Amax over high threshold
                elseif datamat(i+1-alpha,nbnotes+2,6)>=threshold, % Amax over low threshold
                    
                    % Hammer detection
                    for h=i+1:nt1
                        if notet(h,1)==notet(i,1)+128 % hammer corresponding to the key
                            for jh=3:2:(nt2-2)
                                if notet(h,jh)>datamat(i+1-alpha,nbnotes+2,2) && notet(h,jh)<tfin % hammer activated after note onset and before note offset
                                    valid=1;
                                    break;
                                end
                            end
                        end
                    end
                end

                                
                % --- Fill in note data ---
                
                if valid==1
                    datamat(i+1-alpha,nbnotes+2,1) = nbnotes; % Note #
                    datamat(i+1-alpha,nbnotes+2,3) = tfin; % Note offset
                    datamat(i+1-alpha,nbnotes+2,4) = datamat(i+1-alpha,nbnotes+2,3) - datamat(i+1-alpha,nbnotes+2,2); % Note duration
                    datamat(i+1-alpha,nbnotes+2,5) = datamat(i+1-alpha,nbnotes+2,5)/(datamat(i+1-alpha,nbnotes+2,4)/2 + 1); % Amoy
                    if jammin-npar-1 < (datamat(i+1-alpha,nbnotes+2,7)-datamat(i+1-alpha,nbnotes+2,2))/2 + 1,
                        datamat(i+1-alpha,nbnotes+2,45) = datamat(i+1-alpha,nbnotes+2,45)/((datamat(i+1-alpha,nbnotes+2,7)-datamat(i+1-alpha,nbnotes+2,2))/2 + 1 - (jammin-npar-1)); % Attack Amoy with attack onset correction
                        datamat(i+1-alpha,nbnotes+2,46) = datamat(i+1-alpha,nbnotes+2,45) / datamat(i+1-alpha,nbnotes+2,6); % Attack Amoy rate
                        
                        datamat(i+1-alpha,nbnotes+2,16) = 2*(jammin-npar-1); % Attack delay
                    end
                else
                    datamat(i+1-alpha,nbnotes+2,:)=0; % Re-init
                    nbnotes = nbnotes-1; % Go back to less notes
                end
            end
            if j<nt2-2, j=j+2; else break; end
        end
        
        datamat(i+1-alpha,1,2) = nbnotes; % Number of notes on this key
        
        if nbnotes==0, alpha=alpha+1; % Counter for removing empty lines
        else
            % --- Clear the key ---
            datamat(i+1-alpha,nbnotes+3:size(datamat,2),:) = 0; % Erase the last discarded note if the very last...
        end
        
        
        % --- HAMMERS ---
        
    else % Go through hammer lines in notetime in order to store hammer data in the right notes along the right key
        for i1=2:size(datamat,1)
            if (notet(i,1)-128) == datamat(i1,1,1) % Hammer corresponding to key i1
                j1=3;
                while notet(i,j1)>0, % Hammer timestamps browsing
                    for j2=3:datamat(i1,1,2)+2 % In datamat / key i1, over each note
                        if ((datamat(i1,j2,2) <= notet(i,j1)) && (datamat(i1,j2,3) > notet(i,j1))) % Current timestamp within note duration
                            if (datamat(i1,j2,8)==0)                                                                    % || ( abs(datamat(i1,j2,7)-notet(i,j1)) < abs(datamat(i1,j2,7)-datamat(i1,j2,9)) ),
                                % No registered MHV yet: always take the first MHV                                      % or new hammer hit closer in time to t.Amax
                                datamat(i1,j2,9) = notet(i,j1); % t.MHV
                                datamat(i1,j2,8) = notet(i,j1+1); % MHV
                            end
                        end
                    end
                    j1=j1+2;
                end
            end
        end
    end
end


%% ------- NOTES DATA ------------------------------------------

[dtm1,dtm2,dtm3] = size(datamat);

for i=2:dtm1
    for jn=3:datamat(i,1,2)+2 % note by note
        
        % ATTACKS
        
        if datamat(i,jn,8)>0, % MHV is registered for note n
            datamat(i,jn,10) = datamat(i,jn,9) - datamat(i,jn,2); % speed of attack dt(mhv-onset)
            if datamat(i,jn,10)~=0, datamat(i,jn,11) = datamat(i,jn,8)/datamat(i,jn,10); else datamat(i,jn,11)=0; end % slope of attack MHV/speed.of.attack
            datamat(i,jn,14) = datamat(i,jn,9) - datamat(i,jn,7); % directness / hammer-key attacks match: dt(MHV-Amax)
            if datamat(i,jn,6)>0, datamat(i,jn,15) = datamat(i,jn,8)/datamat(i,jn,6); else datamat(i,jn,15)=0; end % hammer/key correlation: MHV/Amax
            
            % sharpness of attack slope: (A@((t.onset+t.mhv)/2))/A@(t.mhv), 
            % i.e. mean of datamat-stored angles indexed @ (npar+1+) round(x/2) & fix(x/2) (even val for x) with x = (t.mhv-t.onset)/2, / mean of datamat-stored angles indexed @ (npar+1+) round(y/2) & fix(y/2) with y = t.mhv-t.onset
            if (datamat(i,jn,9)-datamat(i,jn,2)-datamat(i,jn,16))>0 && datamat(i,jn,npar+2*round((datamat(i,jn,9)-datamat(i,jn,2)-datamat(i,jn,16))/4))~=0, % dtMHV and A(tMHV) with attack correction
                datamat(i,jn,17) = ( datamat(i,jn,npar+1+round((datamat(i,jn,9)-datamat(i,jn,2)+datamat(i,jn,16))/4)) + datamat(i,jn,npar+1+fix((datamat(i,jn,9)-datamat(i,jn,2)+datamat(i,jn,16))/4)) )...
                    / ( datamat(i,jn,npar+round((datamat(i,jn,9)-datamat(i,jn,2))/2)) + datamat(i,jn,npar+fix((datamat(i,jn,9)-datamat(i,jn,2))/2)) );
            else datamat(i,jn,17)=0;
            end
            
        end
        
        datamat(i,jn,12) = datamat(i,jn,7) - datamat(i,jn,2); % speed of key attack: dt(Amax-onset)
        if datamat(i,jn,12)~=0, datamat(i,jn,13) = datamat(i,jn,6)/datamat(i,jn,12); else datamat(i,jn,13)=0; end % slope of key attack: Amax/speed.of.key.attack
        
        % sharpness of key attack slope: (A@((t.onset+t.amax)/2))/Amax, i.e. mean of datamat-stored angles indexed @ (npar+1+) round(x/2) & fix(x/2) (even val for x) with x = (t.amax-t.onset)/2:
        if datamat(i,jn,6)>0 && (datamat(i,jn,7)-datamat(i,jn,2)-datamat(i,jn,16))>0, 
            datamat(i,jn,16) = ( datamat(i,jn,npar+1+round((datamat(i,jn,7)-datamat(i,jn,2)+datamat(i,jn,16))/4)) + datamat(i,jn,npar+1+fix((datamat(i,jn,7)-datamat(i,jn,2)+datamat(i,jn,16))/4)) )/(2*datamat(i,jn,6)); 
        else datamat(i,jn,16)=0; 
        end
        
        
        % KEY DEPRESSION PROFILES
        % Full key depression
        itop1=1;
        while datamat(i,jn,npar+itop1)>0 && itop1<dtm3-npar, % loop on angle values
            if datamat(i,jn,npar+itop1)>=maxdthresh % high enough angle to assume FKD
                itop2=itop1+1;
                while itop2<=dtm3-npar && datamat(i,jn,npar+itop2)>=maxdthresh, itop2=itop2+1; end
                if itop2-itop1 > datamat(i,jn,20), % index interval larger than before, i.e. longest consecutive FKD yet
                    datamat(i,jn,18) = itop1; % FKD start index
                    datamat(i,jn,19) = itop2-1; % FKD end index
                    datamat(i,jn,20) = itop2-itop1; % FKD approx. duration in indexes
                end
            end
            itop1=itop1+1;
        end
        if datamat(i,jn,18)>0,
            datamat(i,jn,18) = datamat(i,jn,2) + 2*(datamat(i,jn,18)-1); % full key-depression start: t(A>maxdthresh for 1st time) (t.onset + index converted in time)
            datamat(i,jn,19) = datamat(i,jn,2) + 2*(datamat(i,jn,19)-1); % full key-depression end: t(A>maxdthresh for last time) (t.onset + index converted in time)
            datamat(i,jn,20) = datamat(i,jn,19) - datamat(i,jn,18); % full key-depression duration: fkd start-end
            datamat(i,jn,21) = datamat(i,jn,20)/datamat(i,jn,4); % full key-depression rate: fkd.duration/note.duration
            datamat(i,jn,22) = datamat(i,jn,18) - datamat(i,jn,2); % pre- full key-depression duration: t(fkd.start-onset)
            datamat(i,jn,23) = datamat(i,jn,22)/datamat(i,jn,4); % pre- full key-depression rate
            datamat(i,jn,24) = datamat(i,jn,3) - datamat(i,jn,19); % post- full key-depression duration: t(offset-fkd.end)
            datamat(i,jn,25) = datamat(i,jn,24)/datamat(i,jn,4); % post- full key-depression rate
        end
        
        
        % ADSR
        
        % Attack-end
        iattack=1; % counter for t.attack.end
        attackend=(datamat(i,jn,7)-datamat(i,jn,2))/2 +1; % for storing the coarse attack-end point; default: t.amax index or max size
        inflexattack=megatop; % for storing the minimized inflexion rate
        
        while iattack<=dtm3-npar && datamat(i,jn,npar+iattack)<datamat(i,jn,6)/2 && datamat(i,jn,npar+iattack)>0, iattack=iattack+1; end % start at amax/2; before: irrelevant noise
        
        while iattack<=dtm3-npar && datamat(i,jn,npar+iattack)<datamat(i,jn,6) && ( (datamat(i,jn,npar+iattack)<datamat(i,jn,npar+iattack+depth) && iattack<=dtm3-npar-depth) || (iattack>dtm3-npar-depth && datamat(i,jn,npar+iattack)<datamat(i,jn,dtm3)) )
            % before Amax and before fall-off
            if (iattack<=dtm3-npar-depth && mean(datamat(i,jn,npar+iattack:npar+iattack+depth)>=datamat(i,jn,6)/2)==1) || ( iattack>dtm3-npar-depth && mean(datamat(i,jn,npar+iattack:dtm3)>=datamat(i,jn,6)/2)==1)
                % all points from iattack to iattack+depth >= Amax/2
                % while datamat(i,jn,npar+iattack)<datamat(i,jn,6)/2 && datamat(i,jn,npar+iattack)>0, iattack=iattack+1; end % re-check against local mins
                if iattack>depth, 
                    if iattack<=dtm3-npar-depth, inflexattacktmp=sum(datamat(i,jn,npar+iattack+1:npar+iattack+depth)-datamat(i,jn,npar+iattack)) - sum(datamat(i,jn,npar+iattack)-datamat(i,jn,npar+iattack-depth:npar+iattack-1)); 
                        % inflexion / discrete derivate over [depth] steps
                    else inflexattacktmp=(sum(datamat(i,jn,npar+iattack+1:dtm3)-datamat(i,jn,npar+iattack)) - sum(datamat(i,jn,npar+iattack)-datamat(i,jn,2*(npar+iattack)-dtm3:npar+iattack-1))) * depth/(dtm3-npar-iattack);
                        % depth too big an interval up => use data up to dtm3, look down (dtm3-iattack-npar) steps, and adjust the inflexion formula by depth/(...) proportionality
                    end
                else inflexattacktmp=(sum(datamat(i,jn,npar+iattack+1:npar+2*iattack-1)-datamat(i,jn,npar+iattack)) - sum(datamat(i,jn,npar+iattack)-datamat(i,jn,npar+1:npar+iattack-1))) * depth/iattack; 
                    % adjusted inflexion formula to near the onset in case of fast attack
                end
                if inflexattacktmp<inflexattack, % if first or smallest attack-end inflexion point
                    inflexattack=inflexattacktmp; % store the smallest inflexion
                    attackend=iattack; % store rank of coarse attack-end rank
                end
                iattack=iattack+1;
            else
                if iattack<dtm3-npar-depth, iattack=iattack+depth+1; % Get one step ahead of the smaller-than-Amax/2 local min
                else iattack=dtm3-npar;
                end
                while datamat(i,jn,npar+iattack)>datamat(i,jn,6)/2, iattack=iattack-1; end
                iattack=iattack+1;
            end
        end
        if attackend<dtm3-npar-depth, attacktack=attackend+depth; else attacktack=dtm3-npar; end
        while attackend<dtm3-npar && (datamat(i,jn,npar+attackend)<datamat(i,jn,npar+attackend+1)) && attackend<=attacktack, attackend=attackend+1; end % refine the attack-end rank
        datamat(i,jn,26) = datamat(i,jn,2) + 2*(attackend-1); % t(attack.end=decay.start)
        datamat(i,jn,29) = datamat(i,jn,26) - datamat(i,jn,2); % attack duration
        
        % Decay-end
        idecay=attackend+1; % counter for t.decay.end
        decayend=attackend;
%         decayend=(datamat(i,jn,3)-datamat(i,jn,2))/2 +1; % for storing the coarse decay-end point; default: offset index
        inflexdecay=megatop; % for storing the minimized inflexion rate
        while idecay<=dtm3-npar && datamat(i,jn,npar+idecay)>=datamat(i,jn,6), idecay=idecay+1; end % Get further than any top
        while idecay<=dtm3-npar && datamat(i,jn,npar+idecay)>datamat(i,jn,6)/2 && ( (idecay<=dtm3-npar-depth && datamat(i,jn,npar+idecay)>datamat(i,jn,npar+idecay+depth)) || (idecay>dtm3-npar-depth && datamat(i,jn,npar+idecay)>datamat(i,jn,dtm3)) ), 
            % before Amax/2 (high enough) and before plateau
            if idecay<=dtm3-npar-depth,  inflexdecaytmp=sum(datamat(i,jn,npar+idecay)-datamat(i,jn,npar+idecay+1:npar+idecay+depth)); % inflexion / discrete derivate over [depth] steps
            else inflexdecaytmp = (sum(datamat(i,jn,npar+idecay)-datamat(i,jn,npar+idecay+1:dtm3))) * depth/(dtm3-npar-idecay); % adjusted inflexion formula
            end
            if inflexdecaytmp<inflexdecay, % if first or smallest decay-end inflexion point
                inflexdecay=inflexdecaytmp; % store the smallest inflexion
                decayend=idecay; % store rank of coarse attack-end rank
            end
            idecay=idecay+1;
        end
%         while(decayend<dtm3-npar && datamat(i,jn,npar+decayend)>datamat(i,jn,npar+decayend+1) && datamat(i,jn,npar+decayend+1)>0), decayend=decayend+1; end % refine the decay-end rank
        datamat(i,jn,27) = datamat(i,jn,2) + 2*(decayend-1); % t(decay.end)
        
        % Sustain-in (decay.end refinement)
        isusin=min(decayend,dtm3-npar);
        while isusin<=dtm3-npar && datamat(i,jn,npar+isusin)>datamat(i,jn,6)/2 && ( (isusin<=dtm3-npar-depth && (datamat(i,jn,npar+isusin)>datamat(i,jn,npar+isusin+depth)+3 || datamat(i,jn,npar+isusin)<datamat(i,jn,npar+isusin+depth)-3))...
                || ( isusin>dtm3-npar-depth && (datamat(i,jn,npar+isusin)>datamat(i,jn,dtm3)+3 || datamat(i,jn,npar+isusin)<datamat(i,jn,dtm3)-3) ) ),
            if isusin<dtm3-npar, isusin=isusin+1; else break; end % Loop till sustain in
        end
        if datamat(i,jn,npar+isusin)>datamat(i,jn,6)/2, % There is a point of sustain in
            isus=isusin;
            while  isus<=min(dtm3-npar-1,isusin+depth) && datamat(i,jn,npar+isus)~=datamat(i,jn,npar+isus+1), isus=isus+1; end % Refine the sustain-in point
            datamat(i,jn,27) = datamat(i,jn,2) + 2*(isus-1); % t(sustain.start as decay.end)
        end
        
        % Release-start
        irel=(datamat(i,jn,3)-datamat(i,jn,2))/2-depth; % counter for release.start: init=offset.index-1
        relin=(datamat(i,jn,3)-datamat(i,jn,2))/2 +1;
%         relin=decayend; % for storing the coarse release-start point; default: attack-end index
        inflexrel=megatop; % for storing the minimized inflexion rate
        while irel<=dtm3-npar && datamat(i,jn,npar+irel)<datamat(i,jn,6)/2 && irel>decayend+depth, irel=irel-1; end % start at amax/2; before: irrelevant noise

        while irel<=dtm3-npar && irel>=decayend && ( (datamat(i,jn,npar+irel)<datamat(i,jn,npar+irel-depth) && irel>=decayend+depth) || (irel<decayend+depth && datamat(i,jn,npar+irel)<datamat(i,jn,npar+decayend)) )
            % while falling off and before attack-end
            if (irel>=decayend+depth && mean(datamat(i,jn,npar+irel-depth:npar+irel)>=datamat(i,jn,6)/2)==1) || (irel<decayend+depth && mean(datamat(i,jn,npar+decayend:npar+irel)>=datamat(i,jn,6)/2)==1) % all points from irel-depth to irel >= Amax/2
                % while datamat(i,jn,npar+irel)<datamat(i,jn,6)/2 && irel>attackend+depth, irel=irel-1; end % re-check against local mins
                if irel>=decayend+depth, inflexreltmp=2*sum(datamat(i,jn,npar+irel-depth:npar+irel-1)-datamat(i,jn,npar+irel)) - sum(datamat(i,jn,npar+irel)-datamat(i,jn,npar+irel+1:npar+irel+depth)); % inflexion / discrete derivate over [depth] steps
                    %
                else inflexreltmp = (2*sum(datamat(i,jn,npar+decayend:npar+irel-1)-datamat(i,jn,npar+irel)) - sum(datamat(i,jn,npar+irel)-datamat(i,jn,npar+irel+1:npar+2*irel-decayend))) * depth/(irel-decayend);
                    %  
                end
                if inflexreltmp<inflexrel, % if first or smallest release-start inflexion point
                    inflexrel=inflexreltmp; % store the smallest inflexion
                    relin=irel; % store rank of coarse release-start rank
                end
                irel=irel-1;
            else
                if irel>decayend+depth+1, irel=irel-depth-1; % Get one step ahead of the smaller-than-Amax/2 local min
                else irel=decayend;
                end
                while irel<=dtm3-npar && datamat(i,jn,npar+irel)>datamat(i,jn,6)/2, irel=irel+1; end
                irel=irel-1;
            end
        end
%         finerel=megatop;
%         if relin>=decayend+depth, irel=relin-depth+4; else irel=decayend+2; end
%         relin2=relin;
%         while irel<=dtm3-npar && irel<=relin
%             if irel<=dtm3-npar-4,
%                 finereltmp=2*sum(datamat(i,jn,npar+irel-4:npar+irel-1)-datamat(i,jn,npar+irel)) - sum(datamat(i,jn,npar+irel)-datamat(i,jn,npar+irel+1:npar+irel+4));
%                 if finereltmp<=finerel,
%                     finerel=finereltmp;
%                     relin2=irel;
%                 end
%             elseif datamat(i,jn,npar+irel) >= datamat(i,jn,npar+irel-1), 
%                 if relin2==irel-1, relin2=irel; end
%             end
%             irel=irel+1;
%         end
        % while(datamat(i,jn,npar+relin)<datamat(i,jn,npar+relin-1)) && relin>=relim, relin=relin-1; end % refine the release-start rank
        datamat(i,jn,28) = datamat(i,jn,2) + 2*(relin-1); % t(release.start)
        
        if (datamat(i,jn,27)>=datamat(i,jn,28)-4) || (datamat(i,jn,28)>=datamat(i,jn,3)-4), datamat(i,jn,28)=datamat(i,jn,27); end % Test decay ends before release starts & release starts before offset
        datamat(i,jn,30) = datamat(i,jn,28) - datamat(i,jn,27); % sustain duration
        datamat(i,jn,31) = datamat(i,jn,3) - datamat(i,jn,28); % release duration

        datamat(i,jn,32) = datamat(i,jn,29)/datamat(i,jn,4); % attack rate
        datamat(i,jn,33) = datamat(i,jn,30)/datamat(i,jn,4); % sustain rate
        datamat(i,jn,34) = datamat(i,jn,31)/datamat(i,jn,4); % release rate
        
        
        % PEDALS W/ KEYS
        
        for p=0:1
            if datamat(dtm1-p,1,1)==109 % Soft pedal
                for pn=3:datamat(dtm1-p,1,2)+2
                    if (datamat(dtm1-p,pn,2)<=datamat(i,jn,3) && datamat(dtm1-p,pn,3)>=datamat(i,jn,2)), % Pedal overlapping the note
%                     if (datamat(dtm1-p,pn,2)>=datamat(i,jn,2) && datamat(dtm1-p,pn,2)<datamat(i,jn,3)) || (datamat(dtm1-p,pn,3)<=datamat(i,jn,3) && datamat(dtm1-p,pn,3)>datamat(i,jn,2)) % pedal event starts before note offset or ends after note onset
                        pkstart = max( npar + (max(datamat(i,jn,2),datamat(dtm1-p,pn,2))-datamat(dtm1-p,pn,2))/2 +1, npar+1); % rank to start taking the pedal value: onset.diff/2 +1
                        pkend = min(npar + (min(datamat(i,jn,3),datamat(dtm1-p,pn,3))-datamat(dtm1-p,pn,2))/2 +1, dtm3); % rank of last pedal sample before the first of note offset or pedal event end
                        
                        datamat(i,jn,43) = datamat(i,jn,43) + 2*(pkend-pkstart); % Pedal depression duration during note (possibly summable)
                        datamat(i,jn,35) = ( datamat(i,jn,35)*(datamat(i,jn,43)-(2*(pkend-pkstart))) + mean(datamat(dtm1-p,pn,pkstart:pkend))*(2*(pkend-pkstart)) ) / datamat(i,jn,4); % mean pedal depression during note prorated to account for duration
                        
                        if datamat(dtm1-p,pn,2)<=datamat(i,jn,2), datamat(i,jn,37) = datamat(dtm1-p,pn,pkstart); end % Pedal value at note onset if pedal already on then
                        if datamat(i,jn,9)>0 && (datamat(dtm1-p,pn,2)<=datamat(i,jn,9) && datamat(dtm1-p,pn,3)>=datamat(i,jn,9)), % Pedal on at t.mhv
                            datamat(i,jn,39) = datamat(dtm1-p,pn, npar + max( (2*round(datamat(i,jn,9)/2)-datamat(dtm1-p,pn,2))/2 +1 , 1)); % Pedal value at note.t.mhv
                        end
                        if datamat(dtm1-p,pn,3)>=datamat(i,jn,3), datamat(i,jn,41) = datamat(dtm1-p,pn,pkend); end % Pedal value at note offset if pedal still on then
                    end
                end
            elseif datamat(dtm1-p,1,1)==111 % Sustain pedal
                for pn=3:datamat(dtm1-p,1,2)+2 % Exact same code as for pedal 109, with different assignements of course
                    if (datamat(dtm1-p,pn,2)<=datamat(i,jn,3) && datamat(dtm1-p,pn,3)>=datamat(i,jn,2)), % Pedal overlapping the note
%                     if (datamat(dtm1-p,pn,2)>=datamat(i,jn,2) && datamat(dtm1-p,pn,2)<datamat(i,jn,3)) || (datamat(dtm1-p,pn,3)<=datamat(i,jn,3) && datamat(dtm1-p,pn,3)>datamat(i,jn,2)) % pedal event starts before note offset or ends after note onset
                        pkstart = npar + (max(datamat(i,jn,2),datamat(dtm1-p,pn,2))-datamat(dtm1-p,pn,2))/2 +1; % rank to start taking the pedal value: onset.diff/2 +1
                        pkend = min(npar + (min(datamat(i,jn,3),datamat(dtm1-p,pn,3))-datamat(dtm1-p,pn,2))/2 +1, dtm3); % rank of last pedal sample before the first of note offset or pedal event end

                        datamat(i,jn,44) = datamat(i,jn,44) + 2*(pkend-pkstart); % Pedal depression duration during note (possibly summable)
                        datamat(i,jn,36) = ( datamat(i,jn,36)*(datamat(i,jn,44)-(2*(pkend-pkstart))) + mean(datamat(dtm1-p,pn,pkstart:pkend))*(2*(pkend-pkstart)) ) / datamat(i,jn,4); % mean pedal depression during note prorated to account for duration
                        
                        if datamat(dtm1-p,pn,2)<=datamat(i,jn,2), datamat(i,jn,38) = datamat(dtm1-p,pn,pkstart); end % Pedal value at note onset if pedal already on then
                        if datamat(i,jn,9)>0 && (datamat(dtm1-p,pn,2)<=datamat(i,jn,9) && datamat(dtm1-p,pn,3)>=datamat(i,jn,9)), % Pedal on at t.mhv
                            datamat(i,jn,40) = datamat(dtm1-p,pn, npar + max( (2*round(datamat(i,jn,9)/2)-datamat(dtm1-p,pn,2))/2 +1 , 1)); % Pedal value at note.t.mhv
                        end
                        if datamat(dtm1-p,pn,3)>=datamat(i,jn,3), datamat(i,jn,42) = datamat(dtm1-p,pn,pkend); end % Pedal value at note offset if pedal still on then
                    end
                end
            end
        end
    end
end



%% ------- MEANS and SDs ------------------------------------------

% --- MEANS and SDs per key ---
counts=zeros(dtm1,npar);

for i=2:dtm1, % key by key
    counts(i,1:npar)=datamat(i,1,2);
    for k=4:npar,
        kmrank=0;
        tmpvect=zeros(1,1);
        if mkey(k)==1,
            for j=3:datamat(i,1,2)+2, % each note on key i
                if datamat(i,j,k)~=0, kmrank=kmrank+1; tmpvect(kmrank)=datamat(i,j,k); end % store non-zero values in tmp vector
            end
            counts(i,k) = length(tmpvect); % Number of notes intervening in mean calculation
            datamat(i,1,k) = mean(tmpvect); % Mean of non-zeros values
            if isnan(datamat(i,1,k)), datamat(i,1,k)=0; end
            datamat(i,2,k) = std(tmpvect); % SD of non-zeros values
            if isnan(datamat(i,2,k)), datamat(i,2,k)=0; end
            
        elseif mkey(k)==2,
            datamat(i,1,k)=mean(datamat(i,3:counts(i,k)+2,k)); % straight-up mean incl. zeros
            datamat(i,2,k)=std(datamat(i,3:counts(i,k)+2,k)); % straight-up SD incl. zeros
            
        elseif mkey(k)==3,
            for j=3:datamat(i,1,2)+2, % each note on key i
                if datamat(i,j,9)~=0, kmrank=kmrank+1; tmpvect(kmrank)=datamat(i,j,k); end % store non-mhv=zero values in tmp vector
            end
            counts(i,k) = length(tmpvect); % Number of notes intervening in mean calculation
            datamat(i,1,k) = mean(tmpvect); % Mean of non-mhv=zero values
            if isnan(datamat(i,1,k)), datamat(i,1,k)=0; end
            datamat(i,2,k) = std(tmpvect); % SD of non-mhv=zero values
            if isnan(datamat(i,2,k)), datamat(i,2,k)=0; end
            
        elseif mkey(k)==4,
            for j=3:datamat(i,1,2)+2, % each note on key i
                if datamat(i,j,20)~=0, kmrank=kmrank+1; tmpvect(kmrank)=datamat(i,j,k); end % store non-(fkd.duration=zero) values in tmp vector
            end
            counts(i,k) = length(tmpvect); % Number of notes intervening in mean calculation
            datamat(i,1,k) = mean(tmpvect); % Mean of non-(fkd.duration=zero) values
            if isnan(datamat(i,1,k)), datamat(i,1,k)=0; end
            datamat(i,2,k) = std(tmpvect); % SD of non-(fkd.duration=zero) values
            if isnan(datamat(i,2,k)), datamat(i,2,k)=0; end
        end
    end
end


% --- Total means over keys and notes and total means (over keys) of SDs (between notes on same key) ---
while datamat(dtm1,1,1)>108, dtm1=dtm1-1; end % Eliminate pedals from global means calculation

datamat(1,1,2) = sum(datamat(2:dtm1,1,2)); % total number of notes
datamat(1,1,3) = mean(datamat(2:dtm1,1,2)); % mean number of notes per key
datamat(1,2,2) = std(datamat(2:dtm1,1,2)); % SD of number of notes between keys

for k=4:npar
    counts(1,k)=sum(counts(:,k));
    if mkey(k)==1,
        for i=2:dtm1,
            if counts(i,k)>0, datamat(1,1:2,k) = datamat(1,1:2,k) + datamat(i,1:2,k)*counts(i,k); end % sum over keys * nb of non-zero notes, for mean of means and mean of SDs, prorated by number of notes per key
        end
        if counts(1,k)>0, datamat(1,1:2,k) = datamat(1,1:2,k)/counts(1,k); else datamat(1,1:2,k)=0; end % total mean
        
    elseif mkey(k)==2,
        for i=2:dtm1, datamat(1,1:2,k) = datamat(1,1:2,k) + datamat(i,1:2,k)*counts(i,k); end
        if counts(1,k)>0, datamat(1,1:2,k) = datamat(1,1:2,k)/counts(1,k); else datamat(1,1:2,k)=0; end
            
    elseif mkey(k)==3,
        for i=2:dtm1,
            if counts(i,8)>0, datamat(1,1:2,k) = datamat(1,1:2,k) + datamat(i,1:2,k)*counts(i,k); end % sum over keys * nb of non-zero notes, for mean of means and mean of SDs, prorated by number of notes per key
        end
        if counts(1,k)>0, datamat(1,1:2,k) = datamat(1,1:2,k)/counts(1,k); else datamat(1,1:2,k)=0; end % total mean
        
    elseif mkey(k)==4,
        for i=2:dtm1,
            if counts(i,20)>0, datamat(1,1:2,k) = datamat(1,1:2,k) + datamat(i,1:2,k)*counts(i,k); end % sum over keys * nb of non-zero notes, for mean of means and mean of SDs, prorated by number of notes per key
        end
        if counts(1,k)>0, datamat(1,1:2,k) = datamat(1,1:2,k)/counts(1,k); else datamat(1,1:2,k)=0; end % total mean
    end
end


% --- SDs between all notes ---
for k=4:npar,
    kmrank=0;
    tmpvect=zeros(1,1);
    if mkey(k)==1,
        for i=2:dtm1,
            for j=3:datamat(i,1,2)+2,
                if datamat(i,j,k)~=0, kmrank=kmrank+1; tmpvect(kmrank)=datamat(i,j,k); end % store non-zero values in tmp vector
            end
        end
        
    elseif mkey(k)==2,
        for i=2:dtm1,
            for j=3:datamat(i,1,2)+2,
                kmrank=kmrank+1; tmpvect(kmrank)=datamat(i,j,k); % store all values in tmp vector
            end
        end
        
    elseif mkey(k)==3,
        for i=2:dtm1,
            for j=3:datamat(i,1,2)+2,
                if datamat(i,j,9)~=0, kmrank=kmrank+1; tmpvect(kmrank)=datamat(i,j,k); end % store non-mhv=zero values in tmp vector
            end
        end
        
    elseif mkey(k)==4,
        for i=2:dtm1,
            for j=3:datamat(i,1,2)+2,
                if datamat(i,j,20)~=0, kmrank=kmrank+1; tmpvect(kmrank)=datamat(i,j,k); end % store non-(fkd.duration=zero) values in tmp vector
            end
        end
    end
    datamat(1,3,k) = std(tmpvect); % SD of non-zero values over each note and key
    if isnan(datamat(1,3,k)), datamat(1,3,k)=0; end
end



%% ------- PRINTOUT ------------------------------------------

% Output as text file, yet formatted to fit within an Excel table; therefore, .xls extension
% Lines: Global means, mean SDs and SDs, 
%        blank,
%        Each key's mean, SD then each of its notes
%        blank.
%
% Columns: every npar stat


if printout,
    
    % --- Name the output file
    if isempty(nameout),
        if ~ischar(notetime), notetime = inputname(1); end
    else notetime = nameout;
    end
    
    if strcmp(notetime(1:2),'nt'), outname=['notesmat' notetime(3:length(notetime)) '.xls'];
    else outname=['notesmat ' notetime '.xls'];
    end
    
    fid = fopen(outname,'w+');
    
    
    % --- Define columns' labels
    
    titles={'Note # on key', 'Onset time', 'Offset time', 'Note duration', 'Mean key depression Amoy', 'Max key depression Amax', 'Time of Amax', 'MHV', 'Time of MHV',...
        'Speed of attack [dt(mhv-onset)]', 'Slope of attack [mhv/speed]', 'Speed of key attack [dt(amax-onset)]', 'Slope of key attack [amax/speed]', 'Directness [dt(mhv-amax)]', 'MHV/Amax correlation',...
        'Sharpness of key-attack slope [A.half.tamax/Amax]', 'Sharpness of attack slope [A.half.tmhv/A.tmhv]',...
        'Full key-depression (FKD) start', 'FKD end', 'FKD duration', 'FKD rate', 'pre-FKD duration', 'pre-FKD rate', 'post-FKD duration', 'post-FKD rate',...
        'Time of attack-end', 'Time of decay-end', 'Time of release start', 'Attack duration', 'Sustain duration', 'Release duration', 'Attack rate', 'Sustain rate', 'Release rate',...
        'Soft pedal depression', 'Sustain pedal depression', 'Soft pedal at note onset', 'Sustain pedal at note onset', 'Soft pedal at MHV', 'Sustain pedal at MHV', 'Soft pedal at note offset', 'Sustain pedal at note offset',...
        'Soft pedal depression length','Sustain pedal depression length','Key attack mean depression','Key attack mean depression rate'};
    
    for i=1:length(titles), fprintf(fid, '%s \t', titles{i}); end
    
    
    % --- Means and SDs ---
    
    til={'MEAN','Mean SD','Tot. SD'};
    
    for j=1:3 % Means, mean SDs and total SDs
        fprintf(fid,'\n %s\t', til{j}); % in note # column
        for k=2:npar, fprintf(fid,'%.4f\t', datamat(1,j,k)); end % Actual values
    end
    
    % --- All notes ---
    
    for i=2:size(datamat,1) % all keys
        fprintf(fid,'\n');
        for j=1:datamat(i,1,2)+2 % all notes + mean&SD per key
            fprintf(fid,'\n');
            for k=1:npar, fprintf(fid,'%.4f\t',datamat(i,j,k)); end            
        end
    end
    
    fclose(fid);
end



end
