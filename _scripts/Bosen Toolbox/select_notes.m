function out = select_notes(matin,mode,ine,print)

% 2 modes: 'nt' or 'list' etc.
%
% - Select notes by clicking on them in a pianoroll
%       IN: chords mat, notetime file or mat
%
% - Select the notes specified in input chain, format [Key# Time; ...] (one line per note as key/time couple)
%       IN: chords mat, notes selection mat
%
% print: on by default, removable with 'no', special output file name can be specified
%
% !(think about notesperkey as input?)!
%
% OUT: notes stats mat, + mean and SD thereof on lines 1 & 2
%
%
% ------- NOTE FEATURES ------- 
%
% BASICS:
% 1 - Key #
% 2 - Note onset
% 3 - Note offset
% 4 - Note duration
% 5 - Amoy
% 6 - Amax
% 7 - t.Amax
% 8 - MHV
% 9 - t.MHV
%  
% ATTACK:
% 10 - speed of attack: dt(MHV-onset)
% 11 - slope of attack: MHV/speed.of.attack
% 12 - speed of key attack: dt(Amax-onset)
% 13 - slope of key attack
% 14  directness: dt(MHV-Amax)
% 15 - MHV/Amax
% 16 - sharpness of key attack slope
% 17 - sharpness of attack slope
% 
% DEPRESSION PROFILE:
% FKD
% 18  fkd start
% 19  fkd end
% 20  fkd duration
% 21  fkd rate
% 22 - pre-fkd duration
% 23 - pre-fkd rate
% 24 - post-fkd duration
% 25 - post-fkd rate
% ADSR 
% 26  t.attack.end 
% 27  t.decay.end 
% 28  t.release.start 
% 29 - attack duration
% 30 - sustain duration 
% 31 - release duration
% 32 - attack rate
% 33 - sustain rate
% 34 - release rate
% 
% PEDALS:
% 35 - pedal 109 Amoy/note
% 36 - pedal 111 Amoy/note
% 37 - pedal 109 @onset
% 38 - pedal 111 @onset
% 39 - pedal 109 @t.mhv
% 40 - pedal 111 @t.mhv
% 41 - pedal 109 @offset
% 42 - pedal 111 @offset
% 43 - pedal 109 depression duration
% 44 - pedal 111 depression duration
% 
% COMPARISON WITH CHORDS OTHER NOTES:
% npar+1 - onset lag (on chord onset)
% npar+2 - onset lag rate (/chord duration)
% npar+3 - onset lag amount (sum(A.other.notes.bef.on))
% npar+4 - onset lag amount rate (/ mean(Amoy.other.ns))
% npar+5 - offset lead (on chord offset)
% npar+6 - offset lead rate (/chord duration)
% npar+7 - offset lead amount
% npar+8 - offset lead amount rate 
% npar+9 - sync rate (note.dur/chord.dur)
% npar+10 - sync amount rate (sum(A.other.notes.when.note.on)/mean(Amoy.other.notes))
%
%


iname=[];
if nargin<3, disp('Something is missing in input');
else
    if nargin<4, printout=1; 
    elseif strcmp(print,'no')|| strcmp(print,'noprint')|| strcmp(print,'noprintout'), printout=0;
    else printout=1; if ischar(print), iname=print; end
    end
    
    if strcmp(mode,'nt')||strcmp(mode,'notetime')||strcmp(mode,'proll')||strcmp(mode,'pianoroll')||strcmp(mode,'fig')||strcmp(mode,'click'),
        
        % Display pianoroll to select notes
        keys = pianoroll(ine);
        hold on;
        title('Click on the note(s) you wish to select; when done press ENTER','FontSize',12);

        [x y]=ginput();
        title([num2str(length(y)) ' notes selected'],'FontSize',12);
        
        tmp=0;
        for i=1:length(y),
            if y(i)+1<=length(keys),
                sel(i-tmp,1)=keys(fix(y(i))+1);
                sel(i-tmp,2)=round(x(i,1));
            else tmp=tmp+1;
            end
        end
        disp(sel);
        
    elseif strcmp(mode,'list')||strcmp(mode,'select')||strcmp(mode,'selection')||strcmp(mode,'val')||strcmp(mode,'ch'),
        
        % Notes selected as input
        
        if ischar(ine), sel=dlmread(ine);
        else sel=ine; ine=inputname(1);
        end
        
    end
    
    %=================================================================================>
    % Selekshun !!!!!!!!!!!!

    
    % --- PARAMETRIC VARS ---
    npar = 44; % Original number of parameters gathered for each note, by itself
    npar2 = npar + 10; % Total number of parameters gathered for each note: by itself + by comparison with other notes in the same chord
    dmeans = 4; % Number of different means and SDs calculated over the whole file: (mean + SD) of all chords (with some features only multi-notes), left hand, right hand and difference thereof
    
    
    % OUTPUT MAT
    out = zeros(2,npar2);
    
    io=2; % output line rank
    
    for i=dmeans+1:size(matin,1), % Scan all chords
        for isel=1:size(sel,1), % Scan all selections
            % Check time
            if matin(i,1,2)<=sel(isel,2) && matin(i,1,3)>=sel(isel,2), % Time match
                for j=4:matin(i,1,1)+3 % Scan all of chord's notes
                    if matin(i,j,1)==sel(isel,1) && matin(i,j,2)<=sel(isel,2) && matin(i,j,3)>=sel(isel,2),
                        % Key and time match
                        io=io+1;
                        out(io,:) = matin(i,j,1:npar2);
                        break;
                    end
                end
            end
        end
    end
    
    % MEANS & SDs
    out(1,1) = size(sel,1); % Total number of selected notes
    out(1,4:npar2) = mean(out(3:size(out,1),4:npar2),1); % Means
    out(2,:) = std(out(3:size(out,1),:)); % SDs
    
    
    % PRINTOUT
    if printout,
        if isempty(iname), iname=ine; end
        nameout = ['Select_notes ' iname '.xls'];
        
        fid = fopen(nameout,'w+');
        
        titles={'#','Key','Note onset','Note offset','Note duration','Amoy','Amax','t.Amax','MHV','t.MHV',...
            'Speed of attack','Slope of attack','Speed of key attack','Slope of key attack','Directness MHV-Amax','MHV/Amax','Sharpness of key attack slope','Sharpness of attack slope',...
            'FKD start','FKD end','FKD duration','FKD rate','Pre-FKD duration','Pre-FKD rate','Post-FKD duration','Post-FKD rate',...
            't.attack.end','t.decay.end','t.release.start','Attack duration','Sustain duration','Release duration','Attack rate','Sustain rate','Release rate',...
            'Soft pedal depression', 'Sustain pedal depression', 'Soft pedal @note.onset','Sustain pedal @note.onset', 'Soft pedal @MHV', 'Sustain pedal @MHV', 'Soft pedal @note.offset', 'Sustain pedal @note.offset',...
            'Soft pedal depression length','Sustain pedal depression length',...
            'Note onset lag','Note onset lag rate','Note onset lag amount','Note onset lag amount rate','Note offset lead','Note offset lead rate','Note offset lead amount','Note offset lead amount rate','Sync rate','Sync amount rate'};
        
        for i=1:length(titles), fprintf(fid, '%s \t', titles{i}); end
        
        fprintf(fid,'\n\n MEAN\t %d\t\t\t',out(1,1));
        for i=4:npar2, fprintf(fid,'%.4f\t',out(1,i)); end
        fprintf(fid,'\nStDev\t');
        for i=1:npar2, fprintf(fid,'%.4f\t',out(2,i)); end
        fprintf(fid,'\n');
        
        for l=3:size(out,1),
            fprintf(fid,'\n%d\t',l-2);
            for i=1:npar2, fprintf(fid,'%.4f\t',out(l,i)); end
        end
        
        fclose(fid);
    end
    
end
end




