function [chords note] = select_chords(matin,mode,ine,print,sep)


% 2 modes: 'nt' or 'list' etc.
%
% - Select chords by clicking on them in a pianoroll
%       IN: chords mat, notetime file or mat
%
% - Select the chords specified in input chain, format [Some Key#, Time; ...] (one line per note as key/time couple)
%       IN: chords mat, notes selection mat
%
% print: on by default, removable with 'no', special output file name can be specified
%
%
% OUT: 3D matrix:
%  Dim.1: 1: selected chords mean, 2: left-hand chords mean, 3: right-hand chords mean, 4: mean diff. between hands, + rest: selected chords
%  Dim.2: 1: chord features, 2&3: mean/SD of chord's notes features, + each note
%  Dim.3: descriptive features
%
% ------------------------------------------
%
% chout(1,1,1:3) = [0, total number of chords, 0];
% chout(1,1,4:chpar) = [mean of chords features];
% chout(1,2,1:3) = [0, total number of notes, mean number of notes per chord];
% chout(1,3,1:3) = [0, 0, 0];
% chout(1,2:3,4:npar2) = [mean of mean/SD of chords' notes features];
% chout(1,4,1:3) = [0, 0, 0];
% chout(1,4,4:chpar) = [SD of chords features];
% chout(1,5,1:3) = [0, 0, SD of number of notes per chord];
% chout(1,6,1:3) = [0, 0, 0];
% chout(1,5:6,4:npar2) = [SD of mean/SD of chords' notes features];
%
% chout(2:4,1:6,:) = the same but for resp. left-hand chords only, right-hand chords only and diff. between hands
%
% chout(i,1,1:chpar) = chord's descriptive features;
% chout(i,2,1:npar2) = mean of chord's notes features;
% chout(i,3,1:npar2) = SD of chord's notes features;
%
% ------------------------------------------
%
% ------- NOTE FEATURES -------
%
% BASICS:
% 1 - Key #
% 2 - Note onset
% 3 - Note offset
% 4 - Note duration
% 5 - Amoy
% 6 - Amax
% 7 - t.Amax
% 8 - MHV
% 9 - t.MHV
%
% ATTACK:
% 10 - speed of attack: dt(MHV-onset)
% 11 - slope of attack: MHV/speed.of.attack
% 12 - speed of key attack: dt(Amax-onset)
% 13 - slope of key attack
% 14  directness: dt(MHV-Amax)
% 15 - MHV/Amax
% 16 - sharpness of key attack slope
% 17 - sharpness of attack slope
%
% DEPRESSION PROFILE:
% FKD
% 18  fkd start
% 19  fkd end
% 20  fkd duration
% 21  fkd rate
% 22 - pre-fkd duration
% 23 - pre-fkd rate
% 24 - post-fkd duration
% 25 - post-fkd rate
% ADSR
% 26  t.attack.end
% 27  t.decay.end
% 28  t.release.start
% 29 - attack duration
% 30 - sustain duration
% 31 - release duration
% 32 - attack rate
% 33 - sustain rate
% 34 - release rate
%
% PEDALS:
% 35 - pedal 109 Amoy/note
% 36 - pedal 111 Amoy/note
% 37 - pedal 109 @onset
% 38 - pedal 111 @onset
% 39 - pedal 109 @t.mhv
% 40 - pedal 111 @t.mhv
% 41 - pedal 109 @offset
% 42 - pedal 111 @offset
% 43 - pedal 109 depression duration
% 44 - pedal 111 depression duration
%
% COMPARISON WITH CHORDS OTHER NOTES:
% npar+1 - onset lag (on chord onset)
% npar+2 - onset lag rate (/chord duration)
% npar+3 - onset lag amount (sum(A.other.notes.bef.on))
% npar+4 - onset lag amount rate (/ mean(Amoy.other.ns))
% npar+5 - offset lead (on chord offset)
% npar+6 - offset lead rate (/chord duration)
% npar+7 - offset lead amount
% npar+8 - offset lead amount rate
% npar+9 - sync rate (note.dur/chord.dur)
% npar+10 - sync amount rate (sum(A.other.notes.when.note.on)/mean(Amoy.other.notes))
%
% ------------------------------------------
%
% ------- CHORDS FEATURES -------
%
% BASICS:
% 1  Number of notes in the chord
% 2  Chord onset (min(note.onsets))
% 3  Chord offset (max(note.offsets))
% 4  Chord duration (3-2)
%
% RETAKES:
% 5 - max Amax
% 6 - max MHV
% 7 - min onset lag (melody lead)
% 8 - min onset lag rate
% 9 - min offset lead (last-note-trail)
% 10 - min offset lead rate
%
%
% WITHIN-CHORD ADD:
%
% PEDALS:
% 11 - Pedal 109 Amoy /chord
% 12 - Pedal 109 A.duration /chord
% 13 - Pedal 109 full-depression /chord
% 14 - Pedal 109 mid-depression /chord
% 15 - Pedal 109 mid-depression value /chord
% 16 - Pedal 111 Amoy /chord
% 17 - Pedal 111 A.duration /chord
% 18 - Pedal 111 full-depression /chord
% 19 - Pedal 111 mid-depression /chord
% 20 - Pedal 111 mid-depression value /chord
%
% 21 - Pedal 109 value @chord onset
% 22 - Pedal 109 bin. @chord onset
% 23 - Pedal 109 lead/lag /onset
% 24 - Pedal 111 value @chord onset
% 25 - Pedal 111 bin. @chord onset
% 26 - Pedal 111 lead/lag /onset
%
% 27 - Pedal 109 value @chord offset
% 28 - Pedal 109 bin. @chord offset
% 29 - Pedal 109 trail/lead /offset
% 30 - Pedal 111 value @chord offset
% 31 - Pedal 111 bin. @chord offset
% 32 - Pedal 111 trail/lead /offset
%
%
%
% BETWEEN-CHORDS, ONE CHORD / THE OTHERS
%
% INTERVALS:
% 33 - IOI: onset from next chord
% 34 - OffOnI: IOI-chord.duration
% 35 - OffOnI direction: <0 => -1, >0 => +1
%
% 36 - IOI within hand
% 37 - OffOnI within hand
% 38  OffOnI direction within hand
%
% 39 - IOI with other hand
% 40 - OffOnI with other hand
% 41  OffOnI direction with other hand
%
%
% OVERLAPS:
% 42 - overlap from other chords
% 43 - overlap rate (/chord duration)
% 44 - overlap amount from other chords: => sum of As (during target chord) for all overlapping chords
% 45 - overlap amount rate (/ chord Amoy)
% 46 - number of overlaps
%
% 47 - overlap from same hand
% 48 - overlap rate from same hand
% 49 - overlap amount from same hand
% 50 - overlap amount rate from same hand
% 51 - number of overlaps from same hand
%
% 52 - overlap from the other hand
% 53 - overlap rate from the other hand
% 54 - overlap amount from the other hand
% 55 - overlap amount rate from the other hand
% 56 - number of overlaps from the other hand
%
%% ------------------------------------------



iname=[];
if nargin<3, disp('Something is missing in input');
else
    if nargin<4, printout=1;
    elseif strcmp(print,'no')|| strcmp(print,'noprint')|| strcmp(print,'noprintout'), printout=0;
    else printout=1; if ischar(print), iname=print; end
    end
    if nargin<5, sep=0;
    elseif strcmp(sep,'sep')||strcmp(sep,'separate'), sep=1;
    else sep=0;
    end
        
    if strcmp(mode,'nt')||strcmp(mode,'notetime')||strcmp(mode,'proll')||strcmp(mode,'pianoroll')||strcmp(mode,'fig')||strcmp(mode,'click'),
        
        % Display pianoroll to select notes
        keys = pianoroll(ine,'chords',matin);
        hold on;
        title('Click within the chord(s) you wish to select; when done press ENTER','FontSize',12);
        
        [x y]=ginput();
        title([num2str(length(y)) ' chords selected'],'FontSize',12);
        
        tmp=0;
        for i=1:length(y),
            if y(i)+1<=length(keys),
                sel(i-tmp,1)=keys(fix(y(i))+1);
                sel(i-tmp,2)=round(x(i,1));
            else tmp=tmp+1;
            end
        end
        disp(sel);
        
    elseif strcmp(mode,'list')||strcmp(mode,'select')||strcmp(mode,'selection')||strcmp(mode,'val')||strcmp(mode,'ch'),
        
        % Notes selected as input
        
        if ischar(ine), sel=dlmread(ine);
        else sel=ine; ine=inputname(1);
        end
        
    end
    
    %% =================================================================================>
    % Selekshun !!!!!!!!!!!!
    
    
    % --- PARAMETRIC VARS ---
    npar = 44; % Original number of parameters gathered for each note, by itself
    npar2 = npar + 10; % Total number of parameters gathered for each note: by itself + by comparison with other notes in the same chord
    chpar = 56; % Total number of parameters gathered for each chord
    dmeans = 1; % Number of different means and SDs calculated over the whole file: (mean + SD) of all chords (no hand-sep stuff)
    megatop=1e9; % Use for highest init.
    
    % --- Passing vector [mkey per note (means/SDs per chord and total), mchord per chord (for total mean/SD)] ---
    % 0 = no mean/SD calculated for this parameter; 1 = non-zero mean/SD; 2 = full mean/SD incl. zeros; 3 = non-(9.tMHV=0) mean/SD; 4 = multi-note chords only; 5 = but for last chord (IOIs)
    
    %      [1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 +1 +2 +3 +4 +5 +6 +7 +8 +9 +10]  sheer visual indication of rank index
    mkey = [2  2  2  2  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  2  2  2  2  2  2  1  1  1  1  2  1  1  2  1  2  2  2  2  3  3  2  2  2  2  1  1  1  1  1  1  1  1  1  1];
    
    %        [1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56]
    mchord = [0  0  0  2  1  1  4  4  4  4  2  2  2  2  1  2  2  2  2  1  2  2  1  2  2  1  2  2  1  2  2  1  5  5  5  1  1  1  1  1  1  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2];
    
    
    % OUTPUT MAT
    chout = zeros(dmeans+1,6,max(npar2,chpar));
    
    for isel=1:size(sel,1), % Scan all selections
        optime=megatop; % To measure the best fitting chord to the click (in the time-range)
        
        for i=dmeans+1:size(matin,1), % Scan all chords
            if matin(i,1,2)<=sel(isel,2) && matin(i,1,3)>=sel(isel,2) && min(matin(i,4:matin(i,1,1)+3,1))<=sel(isel,1) && max(matin(i,4:matin(i,1,1)+3,1))>=sel(isel,1),
                % Time match & key-range match
                if matin(i,1,4)<optime,
                    optime=matin(i,1,4);
                    chout(isel+dmeans,:,:) = matin(i,1:max(size(chout,2),matin(i,1,1)+3),:);
                end
            end
        end
    end
    
    %     % Old way
    %     for i=dmeans+1:size(matin,1), % Scan all chords
    %         for isel=1:size(sel,1), % Scan all selections
    %             % Check time
    %             if matin(i,1,2)<=sel(isel,2) && matin(i,1,3)>=sel(isel,2), % Time match
    %                 if min(matin(i,4:matin(i,1,1)+3,1))<=sel(isel,1) && max(matin(i,4:matin(i,1,1)+3,1))>=sel(isel,1),
    %                     io=io+1;
    %                     chout(io,1:matin(i,1,1)+3,:) = matin(i,1:matin(i,1,1)+3,:);
    %                 end
    %             end
    %         end
    %     end
    
    
    %% ------- GLOBAL STATS ------------------------------------------
    
    % --- MEANS/SDs of CHORD STATS (1:4,[1 4],:) ---
    
    chout(1,1,2) = size(chout,1) - dmeans; % Total number of chords in the file
    
    for k=4:chpar
        kmrank=0;
        tmpvect=zeros(1,1);
        
        if mchord(k)==1 % non-zero
            for i=dmeans+1:size(chout,1)
                if chout(i,1,k)~=0,
                    kmrank=kmrank+1; tmpvect(kmrank)=chout(i,1,k); % Store non-zero values in tmp vector
                end
            end
            chout(1,1,k) = mean(tmpvect); % Mean of non-zero values
            chout(1,4,k) = std(tmpvect); % SD of non-zero values
            
        elseif mchord(k)==2 % all
            chout(1,1,k) = mean(chout(dmeans+1:size(chout,1),1,k)); % straight-up mean incl. zeros
            chout(1,4,k) = std(chout(dmeans+1:size(chout,1),1,k)); % straight-up SD incl. zeros
            
        elseif mchord(k)==4 % multi-note chords
            for i=dmeans+1:size(chout,1)
                if chout(i,1,1)>1,
                    kmrank=kmrank+1; tmpvect(kmrank)=chout(i,1,k); % Store multi-note chords values in tmp vector
                end
            end
            chout(1,1,k) = mean(tmpvect); % Mean of multi-note chords values
            chout(1,4,k) = std(tmpvect); % SD of multi-note chords values
            
        elseif mchord(k)==5 % but for last chord
            chout(1,1,k) = mean(chout(dmeans+1:size(chout,1)-1,1,k)); % straight-up mean without last chord
            chout(1,4,k) = std(chout(dmeans+1:size(chout,1)-1,1,k)); % straight-up SD without last chord
            
        end
    end
    
    
    
    % --- MEANS/SDs of CHORDS' NOTESTATS (1:4,[2 3 5 6],:) ---
    
    chout(1,2,2) = sum(chout(dmeans+1:size(chout,1),1,1)); % Total number of notes in the file
    chout(1,2,3) = mean(chout(dmeans+1:size(chout,1),1,1)); % Mean number of notes per chord
    chout(1,5,3) = std(chout(dmeans+1:size(chout,1),1,1)); % SD of number of notes per chord
    
    for k=4:npar2
        kmrank=0;
        tmpvect=zeros(2,1);
        
        if mkey(k)==1, % non-zero
            for i=dmeans+1:size(chout,1)
                if chout(i,2,k)~=0,
                    kmrank=kmrank+1; tmpvect(1,kmrank)=chout(i,2,k); tmpvect(2,kmrank)=chout(i,3,k); % Store non-zero values in tmp vector
                end
            end
            chout(1,2,k) = mean(tmpvect(1,:)); % Mean of non-zero values for notes' mean
            chout(1,3,k) = mean(tmpvect(2,:)); % Mean of non-zero values for notes' SD
            chout(1,5,k) = std(tmpvect(1,:)); % SD of non-zero values for notes' means
            chout(1,6,k) = std(tmpvect(2,:)); % SD of non-zero values for notes' SD
            
        elseif mkey(k)==2, % all
            chout(1,2,k) = mean(chout(dmeans+1:size(chout,1),2,k)); % straight-up mean of mean incl. zeros
            chout(1,3,k) = mean(chout(dmeans+1:size(chout,1),3,k)); % straight-up mean of SD incl. zeros
            chout(1,5,k) = std(chout(dmeans+1:size(chout,1),2,k)); % straight-up SD of mean incl. zeros
            chout(1,6,k) = std(chout(dmeans+1:size(chout,1),3,k)); % straight-up SD of SD incl. zeros
            
        elseif mkey(k)==3, % non-mhv=zero
            for i=dmeans+1:size(chout,1)
                if chout(i,2,9)~=0,
                    kmrank=kmrank+1; tmpvect(1,kmrank)=chout(i,2,k); tmpvect(2,kmrank)=chout(i,3,k); % Store non-mhv=zero values in tmp vector
                end
            end
            chout(1,2,k) = mean(tmpvect(1,:)); % Mean of non-mhv=zero values for notes' mean
            chout(1,3,k) = mean(tmpvect(2,:)); % Mean of non-mhv=zero values for notes' SD
            chout(1,5,k) = std(tmpvect(1,:)); % SD of non-mhv=zero values for notes' means
            chout(1,6,k) = std(tmpvect(2,:)); % SD of non-mhv=zero values for notes' SD
        end
        
    end
    
    %% PRINTOUT
    
    if sep,
        
        % Build chords and notes 2D matrices
        
        chords = zeros(size(chout,1)+dmeans,chpar+2*npar2); % (dmeans*2+chords) * features(:chord's + its notes' mean+SD); dmeans*2 => Mean & SD
        
        for i=1:dmeans
            chords(2*i-1,1:chpar) = chout(i,1,1:chpar); % Means of chords' features
            chords(2*i-1,chpar+1:chpar+npar2) = chout(i,2,1:npar2); % Means of chords' notes' mean
            chords(2*i-1,chpar+npar2+1:chpar+2*npar2) = chout(i,3,1:npar2); % Means of chords' notes' SD
            
            chords(2*i,1:chpar) = chout(i,4,1:chpar); % SDs of chords' features
            chords(2*i,chpar+1:chpar+npar2) = chout(i,5,1:npar2); % SDs of chords' notes' mean
            chords(2*i,chpar+npar2+1:chpar+2*npar2) = chout(i,5,1:npar2); % SDs of chords' notes' SD
        end
        
        for i=dmeans+1:size(chout,1),
            chords(i+dmeans,1:chpar) = chout(i,1,1:chpar);  % Chords' features
            chords(i+dmeans,chpar+1:chpar+npar2) = chout(i,2,1:npar2); % Chord's mean notes features
            chords(i+dmeans,chpar+npar2+1:chpar+2*npar2) = chout(i,3,1:npar2); % SD of chord's notes features
        end
        
        
        note = zeros(chout(1,2,2)+2*dmeans,npar2+1); % (dmeans*2+each note) * ('Chord #' + note features*2[mean+SD])
        
        for i=1:dmeans
            note(2*i-1,2:npar2+1) = chout(i,2,1:npar2); % Mean of notes' mean-per-chord
            note(2*i-1,npar2+2:2*npar2+1) = chout(i,5,1:npar2); % SD of notes' mean-per-chord
            note(2*i,2:npar2+1) = chout(i,3,1:npar2); % Mean of notes' SD-per-chord
            note(2*i,npar2+2:2*npar2+1) = chout(i,6,1:npar2); % SD of notes' SD-per-chord
        end
        
        nrank=2*dmeans; % rank to store note within 'notes'
        for i=dmeans+1:size(chout,1), % Scan chords
            for j=4:chout(i,1,1)+3 % Scan each note within the chord
                nrank=nrank+1;
                note(nrank,1) = i-dmeans; % Chord #
                note(nrank,2:npar2+1) = chout(i,j,1:npar2); % Note's features
            end
        end
        
        if printout,
            
            % --- Name the output files
            if isempty(iname), iname=ine; end
            outchords = ['Select_chords.only ' iname '.xls'];
            outnotes = ['Select_chords.notes ' iname '.xls'];
                        
            
            % --- CHORDS-ONLY OUTPUT ---
            
            fid1 = fopen(outchords,'w+');
            
            % --- Define chords-only columns' labels: chord features then notes-per-chord means then SD
            
            titles={'#','Number of notes','Chord onset','Chord offset','Chord duration','max Amax', 'max MHV','Melody lead','Melody lead rate','Last-note trail','Last-note trail rate',...
                'Soft pedal depression/chord','Soft pedal duration/chord','Soft pedal full-depression length/chord','Soft pedal mid-depression length/chord','Soft pedal mid-depression level/chord',...
                'Sustain pedal depression/chord','Sustain pedal duration/chord','Sustain pedal full-depression length/chord','Sustain pedal mid-depression length/chord','Sustain pedal mid-depression level/chord',...
                'Soft pedal @onset','Soft pedal bin. @onset','Soft pedal lead @onset','Sustain pedal @onset','Sustain pedal bin. @onset','Sustain pedal lead @onset',...
                'Soft pedal @offset','Soft pedal bin. @offset','Soft pedal trail @offset','Sustain pedal @offset','Sustain pedal bin. @offset','Sustain pedal trail @offset',...
                'IOI','OffOnI','OffOnI direction','IOI same-hand','OffOnI same-hand','OffOnI direction same-hand','IOI other-hand','OffOnI other-hand','OffOnI direction other-hand',...
                'Overlap length','Overlap length rate','Overlap amount','Overlap amount rate','Number of overlaps',...
                'Overlap length same-hand','Overlap length rate same-hand','Overlap amount same-hand','Overlap amount rate same-hand','Number of overlaps same-hand',...
                'Overlap length other-hand','Overlap length rate other-hand','Overlap amount other-hand','Overlap amount rate other-hand','Number of overlaps other-hand',...
                'Key','Note onset','Note offset','Note duration','Amoy','Amax','t.Amax','MHV','t.MHV',...
                'Speed of attack','Slope of attack','Speed of key attack','Slope of key attack','Directness MHV-Amax','MHV/Amax','Sharpness of key attack slope','Sharpness of attack slope',...
                'FKD start','FKD end','FKD duration','FKD rate','Pre-FKD duration','Pre-FKD rate','Post-FKD duration','Post-FKD rate',...
                't.attack.end','t.decay.end','t.release.start','Attack duration','Sustain duration','Release duration','Attack rate','Sustain rate','Release rate',...
                'Soft pedal depression', 'Sustain pedal depression', 'Soft pedal @note.onset','Sustain pedal @note.onset', 'Soft pedal @MHV', 'Sustain pedal @MHV', 'Soft pedal @note.offset', 'Sustain pedal @note.offset',...
                'Soft pedal depression length','Sustain pedal depression length',...
                'Note onset lag','Note onset lag rate','Note onset lag amount','Note onset lag amount rate','Note offset lead','Note offset lead rate','Note offset lead amount','Note offset lead amount rate','Sync rate','Sync amount rate'};
            
            for i=1:length(titles), fprintf(fid1, '%s \t', titles{i}); end
            
            % --- Means and SDs ---
            
            til={'All notes',''};
            
            for i=1:dmeans, % 1 diff. means
                fprintf(fid1,'\n %s\t\t',til{i,1}); % Title in empty/useless first column
                for k=2:chpar, fprintf(fid1,'%.4f\t',chout(i,1,k)); end % Chords mean
                fprintf(fid1,'Mean of notes mean\t');
                for k=2:npar2, fprintf(fid1,'%.4f\t',chout(i,2,k)); end
                fprintf(fid1,'\n\t\t');
                for k=2:chpar, fprintf(fid1,'\t'); end
                fprintf(fid1,'Mean of notes SD\t');
                for k=2:npar2, fprintf(fid1,'%.4f\t',chout(i,3,k)); end
                
                fprintf(fid1,'\n %s\t\t',til{i,2});
                for k=2:chpar, fprintf(fid1,'%.4f\t',chout(i,4,k)); end % Chords SD
                fprintf(fid1,'SD of notes mean\t');
                for k=2:npar2, fprintf(fid1,'%.4f\t',chout(i,5,k)); end
                fprintf(fid1,'\n\t\t');
                for k=2:chpar, fprintf(fid1,'\t'); end
                fprintf(fid1,'SD of notes SD\t');
                for k=2:npar2, fprintf(fid1,'%.4f\t',chout(i,6,k)); end
                fprintf(fid1,'\n');
            end
            
            % --- All Chords ---
            
            for i=dmeans+1:size(chout,1)
                fprintf(fid1,'\n\n%d\t',i-dmeans); % Chord #
                for k=1:chpar, fprintf(fid1,'%.4f\t',chout(i,1,k)); end % Chord's features
                for k=1:npar2, fprintf(fid1,'%.4f\t',chout(i,2,k)); end % Chord's notes features mean
                fprintf(fid1,'\n\t');
                for k=1:npar2, fprintf(fid1,'%.4f\t',chout(i,3,k)); end % Chord's notes features SD
                for k=1:chpar, fprintf(fid1,'\t'); end
            end
            
            fclose(fid1);
            
            
            % --- NOTES-FROM-CHORDS OUTPUT ---
            
            fid2 = fopen(outnotes,'w+');
            
            % --- Define notes columns' labels: chord features then notes-per-chord means then SD
            
            titles={'#','Key','Note onset','Note offset','Note duration','Amoy','Amax','t.Amax','MHV','t.MHV',...
                'Speed of attack','Slope of attack','Speed of key attack','Slope of key attack','Directness MHV-Amax','MHV/Amax','Sharpness of key attack slope','Sharpness of attack slope',...
                'FKD start','FKD end','FKD duration','FKD rate','Pre-FKD duration','Pre-FKD rate','Post-FKD duration','Post-FKD rate',...
                't.attack.end','t.decay.end','t.release.start','Attack duration','Sustain duration','Release duration','Attack rate','Sustain rate','Release rate',...
                'Soft pedal depression', 'Sustain pedal depression', 'Soft pedal @note.onset','Sustain pedal @note.onset', 'Soft pedal @MHV', 'Sustain pedal @MHV', 'Soft pedal @note.offset', 'Sustain pedal @note.offset',...
                'Soft pedal depression length','Sustain pedal depression length',...
                'Note onset lag','Note onset lag rate','Note onset lag amount','Note onset lag amount rate','Note offset lead','Note offset lead rate','Note offset lead amount','Note offset lead amount rate','Sync rate','Sync amount rate'};
            
            for i=1:length(titles), fprintf(fid1, '%s \t', titles{i}); end
            
            % --- Means and SDs ---
            
            til={'All notes',''};
            
            for i=1:dmeans, % 1 diff. means
                fprintf(fid2,'\n %s\tMean of mean-per-chord\t',til{i,1});
                for k=3:npar2+1, fprintf(fid2,'%.4f\t',note(2*i-1,k)); end
                fprintf(fid2,'\n\tSD of mean-per-chord\t');
                for k=npar2+3:2*npar2+1, fprintf(fid2,'%.4f\t',note(2*i-1,k)); end
                
                fprintf(fid2,'\n %s\tMean of SD-per-chord\t',til{i,2});
                for k=3:npar2+1, fprintf(fid2,'%.4f\t',note(2*i,k)); end
                fprintf(fid2,'\n\tSD of SD-per-chord\t');
                for k=npar2+3:2*npar2+1, fprintf(fid2,'%.4f\t',note(2*i,k)); end
                fprintf(fid2,'\n');
            end
            
            % --- All notes ---
            for i=2*dmeans+1:size(note,1)
                fprintf(fid2,'\n');
                for k=1:npar2+1, fprintf(fid2,'%.4f\t',note(i,k)); end
            end
            
            fclose(fid2);
            
        end
        
    else chords=chout;
        
        if printout,
            
            % --- Name the output file
            if isempty(iname), iname=ine; end
            nameout = ['Select_chords ' iname '.xls'];
            
            fid = fopen(nameout,'w+');
            
            % --- Define columns' labels: chord features then notes-per-chord means then SD
            
            titles={'#','Number of notes','Chord onset','Chord offset','Chord duration','max Amax', 'max MHV','Melody lead','Melody lead rate','Last-note trail','Last-note trail rate',...
                'Soft pedal depression/chord','Soft pedal duration/chord','Soft pedal full-depression length/chord','Soft pedal mid-depression length/chord','Soft pedal mid-depression level/chord',...
                'Sustain pedal depression/chord','Sustain pedal duration/chord','Sustain pedal full-depression length/chord','Sustain pedal mid-depression length/chord','Sustain pedal mid-depression level/chord',...
                'Soft pedal @onset','Soft pedal bin. @onset','Soft pedal lead @onset','Sustain pedal @onset','Sustain pedal bin. @onset','Sustain pedal lead @onset',...
                'Soft pedal @offset','Soft pedal bin. @offset','Soft pedal trail @offset','Sustain pedal @offset','Sustain pedal bin. @offset','Sustain pedal trail @offset',...
                'IOI','OffOnI','OffOnI direction','IOI same-hand','OffOnI same-hand','OffOnI direction same-hand','IOI other-hand','OffOnI other-hand','OffOnI direction other-hand',...
                'Overlap length','Overlap length rate','Overlap amount','Overlap amount rate','Number of overlaps',...
                'Overlap length same-hand','Overlap length rate same-hand','Overlap amount same-hand','Overlap amount rate same-hand','Number of overlaps same-hand',...
                'Overlap length other-hand','Overlap length rate other-hand','Overlap amount other-hand','Overlap amount rate other-hand','Number of overlaps other-hand',...
                'Key','Note onset','Note offset','Note duration','Amoy','Amax','t.Amax','MHV','t.MHV',...
                'Speed of attack','Slope of attack','Speed of key attack','Slope of key attack','Directness MHV-Amax','MHV/Amax','Sharpness of key attack slope','Sharpness of attack slope',...
                'FKD start','FKD end','FKD duration','FKD rate','Pre-FKD duration','Pre-FKD rate','Post-FKD duration','Post-FKD rate',...
                't.attack.end','t.decay.end','t.release.start','Attack duration','Sustain duration','Release duration','Attack rate','Sustain rate','Release rate',...
                'Soft pedal depression', 'Sustain pedal depression', 'Soft pedal @note.onset','Sustain pedal @note.onset', 'Soft pedal @MHV', 'Sustain pedal @MHV', 'Soft pedal @note.offset', 'Sustain pedal @note.offset',...
                'Soft pedal depression length','Sustain pedal depression length',...
                'Note onset lag','Note onset lag rate','Note onset lag amount','Note onset lag amount rate','Note offset lead','Note offset lead rate','Note offset lead amount','Note offset lead amount rate','Sync rate','Sync amount rate'};
            
            for i=1:length(titles), fprintf(fid, '%s \t', titles{i}); end
            
            % --- Means and SDs ---
            
            til={'Chords mean','Chords SD'};
            
            for i=1:dmeans, % 4 diff. means
                fprintf(fid,'\n %s\t\t',til{i,1}); % Title in empty/useless first column
                for k=2:chpar, fprintf(fid,'%.4f\t',chout(i,1,k)); end % Chords mean
                fprintf(fid,'Mean of notes mean\t');
                for k=2:npar2, fprintf(fid,'%.4f\t',chout(i,2,k)); end
                fprintf(fid,'\n\t\t');
                for k=2:chpar, fprintf(fid,'\t'); end
                fprintf(fid,'Mean of notes SD\t');
                for k=2:npar2, fprintf(fid,'%.4f\t',chout(i,3,k)); end
                
                fprintf(fid,'\n %s\t\t',til{i,2});
                for k=2:chpar, fprintf(fid,'%.4f\t',chout(i,4,k)); end % Chords SD
                fprintf(fid,'SD of notes mean\t');
                for k=2:npar2, fprintf(fid,'%.4f\t',chout(i,5,k)); end
                fprintf(fid,'\n\t\t');
                for k=2:chpar, fprintf(fid,'\t'); end
                fprintf(fid,'SD of notes SD\t');
                for k=2:npar2, fprintf(fid,'%.4f\t',chout(i,6,k)); end
                fprintf(fid,'\n');
            end
            
            % --- All Chords ---
            
            for i=dmeans+1:size(chout,1)
                fprintf(fid,'\n\n%d\t',i-dmeans); % Chord #
                for k=1:chpar, fprintf(fid,'%.4f\t',chout(i,1,k)); end % Chord's features
                for k=1:npar2, fprintf(fid,'%.4f\t',chout(i,2,k)); end % Chord's notes features mean
                for j=3:chout(i,1,1)+3 % Notes features SD + each note's features
                    fprintf(fid,'\n\t');
                    for k=1:chpar, fprintf(fid,'\t'); end
                    for k=1:npar2, fprintf(fid,'%.4f\t',chout(i,j,k)); end % Chord's note features
                end
            end
            
            fclose(fid);
        end
    end
    
end
end




