function csvmat = boe2midi(namein,chorded,bpm,piecenum)

%csvmat = boe2midi(namein,chorded,bpm,piecenum)
% Converts boe or notetime files (and chord matrice) into MIDI
% INPUT:
% - namein: name of the notetime or boe file
% - chords: chord matrix, or 0, or 'boe' for indicating boe input
% - bpm
%
% OUTPUT: CSV File
% l.1: header
% l.2: start track
% l.3: tempo
% l.X: MIDI event: 1-track, 2-time (in ms), 3-type (Note_on, Note_off, CC), 4-channel, 5-key, 6-velocity
% l.end-1: end track
% l.end: EOF
%


if nargin<2, chorded = 'off'; bpm = 60; piecenum = 0;
elseif nargin<3, bpm = 60; piecenum = 0;
elseif nargin<4, piecenum = 0;
end

if strcmp(chorded,'boe'), intype = 0; % Dealing with boe file input
else intype=1; % Notetime input
end


if intype==0, % boe,
    nt = notetime(namein);
    
    if ~piecenum,
%         if ~isnan(str2double(namein(24))), % Standard nomenclature, 24th char is piece number
%             piecenum = str2double(namein(24));
%         else disp('Piece ID cannot be retrieved, hand separation is off.');
%         end
    end
    
    chmat = chords(nt,piecenum);
    
else % notetime
    nt = dlmread(namein);
    
    if ischar(chorded), % No chord matrix input
        if ~piecenum,
            if ~isnan(str2double(namein(16))), % Standard nomenclature, 16th char is piece number
                piecenum = str2double(namein(16));
            else disp('Piece ID cannot be retrieved, hand separation is off.');
            end
        end
        chmat = chords(nt,piecenum);
        
    else chmat = chorded;
    end
end

% ------ PEDAL PARAMETERS ------
poff=50; % Threshold below which a pedal is deemed non-depressed
ptop=200; % Threshold over which a pedal is deemed fully depressed
pstep=20; % Pedal depression step before sending MIDI CC



%% --- NOTES - KEYS ---

csvmat = zeros(1,6);
% Track, time in ms, type, channel, key, velocity
% Type: 1 for note_on, 0 for note_off

count=1;
for i=1:size(chmat,1)-4, % loop on chords
    for j=1:chmat(i+4,1,1), % loop on notes
        
        % NOTE ON
        % Track
        csvmat(count,1) = 1; % Track = 1, Ch = 0, always

        % Timing
        if chmat(i+4,j+3,9)>0, % t.MHV defined => Note_on defined as t.MHV
            csvmat(count,2) = round(chmat(i+4,j+3,9) *bpm/60);
        else
            % Fall back on Amax timing
            csvmat(count,2) = round(chmat(i+4,j+3,7) *bpm/60);
        end
        
        % Type: Note_on
        csvmat(count,3) = 2;
        
        % Key number
        csvmat(count,5) = chmat(i+4,j+3,1);
        
        % Velocity
        if chmat(i+4,j+3,8)>0, % MHV defined
            csvmat(count,6) = round(chmat(i+4,j+3,8) * 127/200);
        else
            % Interpolate from Amax
            % MHV = .73 * Amax - 26
            csvmat(count,6) = round((.73*chmat(i+4,j+3,6) - 26) * 127/200);
        end
        
        % NOTE OFF
        % Track
        csvmat(count+1,1) = 1; % Track = 1, Ch = 0, always
        
        % Timing
        csvmat(count+1,2) = round(chmat(i+4,j+3,3) *bpm/60);
        
        % Type: Note_off
        csvmat(count+1,3) = 1;
        
        % Key number
        csvmat(count+1,5) = chmat(i+4,j+3,1);
        
        % Velocity: same as Note_on
        csvmat(count+1,6) = csvmat(count,6);
        
        count=count+2;
    end
end



%% --- PEDALS - Control change ---
% Soft pedal: 109 => CC67
% Sustain pedal: 111 => CC64

soft = nt(nt(:,1)==109,:);
sust = nt(nt(:,1)==111,:);



% % Init at zero
% softmat = [1 0 3 0 67 0];
% sustmat = [1 0 3 0 64 0];


if ~isempty(soft),
    i=4;
    softmat=0;
    softmat = midipedals(softmat,soft,1,i,poff,ptop,pstep,67);
    csvmat = [csvmat; softmat];
end

if ~isempty(sust),
    i=4;
    sustmat=0;
    sustmat = midipedals(sustmat,sust,1,i,poff,ptop,pstep,64);
    csvmat = [csvmat; sustmat];
end



%% FINALIZE

i=1;
while i<=size(csvmat,1),
    if csvmat(i,1)==0, csvmat(i,:)=[]; 
    else i=i+1;
    end
end

% Order by timing
csvmat = sortrows(csvmat,2);

% % End of pedal CC
% csvmat(size(csvmat,1)+1,1:6) = [1 csvmat(size(csvmat,1),2)+2, 2, 0, 67, 0];
% csvmat(size(csvmat,1)+1,1:6) = [1 csvmat(size(csvmat,1),2), 2, 0, 64, 0];


% PRINTOUT CSV WITH HEADERS AND TYPES AND EOF

% Name
if intype==0, % boe input
    csvname = [namein(1:7), namein(19:length(namein)-4), '.csv'];
        
else csvname = [namein(4:length(namein)) '.csv'];
end

for i=1:length(csvname), 
    if strcmp(csvname(i), ' '), csvname(i) = '_';
    end
end

fid = fopen(csvname,'w+');

% Header
fprintf(fid,'0, 0, Header, 1, 1, 1000\n1, 0, Start_track\n1, 0, Time_signature, 4, 2, 32, 8\n1, 0, Tempo, 1000000\n');

% Data
typelabel = {'Note_off_c','Note_on_c','Control_c'};


for i=1:size(csvmat,1),
    fprintf(fid,'%d, ',csvmat(i,1));
    fprintf(fid,'%d, ',csvmat(i,2));
    fprintf(fid,'%s, ',typelabel{csvmat(i,3)});
    fprintf(fid,'%d, ',csvmat(i,4));
    fprintf(fid,'%d, ',csvmat(i,5));
    fprintf(fid,'%d\n',csvmat(i,6));
end

% EOF
%fprintf(fid,'1, %d, End_track\n',csvmat(size(csvmat,1),2));
%fprintf(fid,'0, 0, End_of_file\n');

fclose(fid);



% MIDI OUT

midiname = [csvname(1:length(csvname)-4), '.mid'];

setenv('PATH',[getenv('PATH') ':/usr/local/bin']);

%system(['csvmidi ' csvname ' ' midiname]);




end

%%

function mat = midipedals(mat,pedal,count,i,poff,ptop,pstep,pcc)
% Recursion for gathering MIDI CC pedal events

while (i<length(pedal)-1 && pedal(i)<=poff),
    i=i+2;
end

% Store first pedal event higher than low threshold
mat(count,1) = 1; % Track = 1, Ch = 0, always
mat(count,2) = pedal(i-1); % Timing
mat(count,3) = 3; % Type: CC
mat(count,5) = pcc; % Soft pedal: 67, Sustain pedal: 64
mat(count,6) = max(0, min( round((pedal(i)-poff)*127/(ptop-poff)) ,127));
% boe value converted to MIDI range with minimum and maximum thresholds

tmp = pedal(i);
count=count+1;
i=i+2;

while (i<length(pedal)-1 && pedal(i)>poff),
    % Loop until back down below low threshold, or until end of line
    
    while (i<length(pedal)-1 && pedal(i)<ptop && pedal(i)>poff),
        % Loop until upper threshold reached
        
        while (i<length(pedal)-1 && pedal(i)<tmp+pstep && pedal(i)>tmp-pstep && pedal(i)<ptop && pedal(i)>poff),
            % Loop while within pstep of change in pedal depression
            i=i+2;           
        end
        
        if (i<length(pedal)-1 && pedal(i)<ptop && pedal(i)>poff),
            % Change bigger than pstep => Store pedal event
            mat(count,1) = 1; % Track = 1, Ch = 0, always
            mat(count,2) = pedal(i-1); % Timing
            mat(count,3) = 3; % Type: CC
            mat(count,5) = pcc; % Soft pedal: 67, Sustain pedal: 64
            mat(count,6) = max(0, min( round((pedal(i)-poff)*127/(ptop-poff)) ,127));
            % boe value converted to MIDI range with minimum and maximum thresholds
            
            tmp = pedal(i);
            count=count+1;
        end
        
        i=i+2;
        
    end
    
    if (i<length(pedal)-1 && pedal(i)>poff),
        % Store pedal event when reaching top threshold
        mat(count,1) = 1; % Track = 1, Ch = 0, always
        mat(count,2) = pedal(i-1); % Timing
        mat(count,3) = 3; % Type: CC
        mat(count,5) = pcc; % Soft pedal: 67, Sustain pedal: 64
        mat(count,6) = 127;
        tmp=127;
        count=count+1;
        
        while(i<length(pedal)-1 && pedal(i)>=ptop),
            i=i+2;
        end
        
    else i=i+2;
    end
end

if i<=length(pedal)-1,
    % Store pedal event when reaching low threshold
    mat(count,1) = 1; % Track = 1, Ch = 0, always
    mat(count,2) = pedal(i-1); % Timing
    mat(count,3) = 3; % Type: CC
    mat(count,5) = pcc; % Soft pedal: 67, Sustain pedal: 64
    mat(count,6) = 0;
    
    count=count+1;
    
    mat = midipedals(mat,pedal,count,i,poff,ptop,pstep,pcc);
end

end








