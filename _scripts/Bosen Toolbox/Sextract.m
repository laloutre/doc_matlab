function ext = Sextract(file)

% boe parser
% INPUT : boe data file name
% OUTPUT : Matrix of events at each timestamp
%

THRESHOLD=2; % parameter : time increment

fidt = fopen(file,'r');
test = fscanf(fidt,'%2x',1);
fclose(fidt);

if ~isempty(test), % Hex file format
    ext = extract_hex(file,THRESHOLD);
else % bin file format
    ext = extract_bin(file,THRESHOLD);
end

% dlmwrite('events.txt', ext)
end


function ext = extract_hex(file,THRESHOLD)
%open file
fid = fopen(file,'r');

%Initialization
eventnum=0;
bitnum=0;
key = 0;
hex2bit=0;

%Event Matrix
events=[];

%read the first 2 characters
hex2bit = fscanf(fid,'%2x',1);

%loop
while ~feof(fid)
    %If to make sure you read the whole file
    if hex2bit ~= 255
        hex2bit = fscanf(fid,'%2x',1);
    end

    %Conditions that determines if we go in the while loop
    if hex2bit == 255
        %Enter the loop
        eventLoop = 1;

        %Write  first 2 bits to the events matrix
        eventnum = eventnum + 1;
        bitnum = bitnum + 1;
        events(eventnum,bitnum) = hex2bit;

        %Write timestamp to the events matrix
        time = fscanf(fid,'%6x',1);
        bitnum = bitnum + 1;
        events(eventnum,bitnum) = time;


    else
        %Don't enter the loop
        eventLoop=0;
    end

    validevent = 0;

    while eventLoop==1
        hex2bit = fscanf(fid,'%2x',1);
        if hex2bit==255
            %Break the loop
            bitnum=0;
            eventLoop=0;

            %Write subsequent bits to the event matrix
        else
            %Write values from even columns (# key pressed) to a tmp variable
            modulo = mod(bitnum,2);
            if modulo==0
                key = hex2bit;
                bitnum = bitnum + 1;

                %Determines if values of odd columns (value of the key) are > 2
                %If yes, both key # and key value get written to the matrix
            elseif (modulo==1) && (hex2bit>THRESHOLD)
                bitnum = bitnum + 1;
                events(eventnum,(bitnum-1)) = key;
                events(eventnum,bitnum) = hex2bit;
                validevent = validevent+1;
                key = 0;

                %If not in the first three columns or as a value > 2, then
                %clear tmp and redo the loop
            elseif (modulo==1)
                key = 0;
                bitnum = bitnum - 1;
            end

            %Make sure we stay in the loop
            eventLoop=1;

            if feof(fid)

                break;
            end

        end
    end
    if validevent==0
        eventnum = eventnum - 1;
    end
end
fclose(fid);

ext = events;

end


function out = extract_bin(file,THRESHOLD)

fid = fopen(file,'r'); % open file

dat = fread(fid); % read & store bin data

% Init
events=[]; % Event Matrix
line = 1;
col = 1;
for i=1:length(dat) % loop
    
    if(dat(i)==255)
    line = line+1;
    col = 1;
    end
    
    events(line,col) = dat(i);
    col = col+1;
end

fclose(fid);
%events(:,1) = [];
out = events;

end