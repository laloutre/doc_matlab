function g_factor(matin,varargin)

%%
% GUI Launcher
% Comparison in time of chords features between several performances grouped by timbre
% One subplot per feature
% One line per subplot per timbre
% Additional means comparison
% INPUT: chord matrices OR .mat file of many chord matrices
%        Options:
%               Printout
%               Batch (for bypassing the GUI)
%               No plot
%               Timbre tag reference (numeric or timbre labels)
%               Timbre label specifications (bypass standard labels)
%               Features tag reference (numeric or feature names)
%
% OUTPUT: Figure!
%         % Matrix, 4D : [timeline + features] * [time-wise mean + time chunks] * timbres * [perf-wise mean + SD]
%
% [Change winsize line 585 to optimize segmentation]
%%

% PARAMETERS
npar = 46; % Original number of parameters gathered for each note, by itself
npar2 = npar + 10; % Total number of parameters gathered for each note: by itself + by comparison with other notes in the same chord
chpar = 56; % Total number of parameters gathered for each chord
dmeans = 4; % Number of different means and SDs calculated over the whole file: (mean + SD) of all chords (with some features only multi-notes), left hand, right hand and difference thereof
% threshand = [58 64 64 55]; % Note threshold to separate hands for each of the four pieces

timnames = {'Bright','Round','Dry','Dark','Velvety'};

[chvars nvars] = varnames;

chmats = zeros(dmeans+1,6,max(npar2,chpar),1); % 4D matrix containing all input 3D chord matrices
chnames = cell(1,1);

go = 1; % Run it tag
printout=0; % Print out .xls file tag
batch=0; % Run batch tag (GUI off)
plotit=1; % Plot tag
timtag=0; % Timbres-referring argument tag
features=0; % Features-referring argument tag
nameout='matout';

if isnumeric(matin), % First input is a matrix var.
    chmats(1:size(matin,1),1:size(matin,2),1:size(matin,3),1) = matin;
    chnames{1} = inputname(1);
    
    ivar=1;
    while ivar<=length(varargin),
        if strcmp(varargin(ivar),'printout'), printout=1;
            
        elseif strcmp(varargin(ivar),'batch'), batch=1;
            
        elseif strcmp(varargin(ivar),'noplot'), plotit=0;
            
        elseif strcmp(varargin(ivar),'plotit'), plotit=1;
            
        elseif strcmp(varargin(ivar),'outname'),
            if ivar<length(varargin) && ischar(varargin{ivar+1}),
                nameout = varargin{ivar+1};
                ivar=ivar+1;
            else disp('Missing output name.');
            end
            
        elseif strcmp(varargin(ivar),'timbres') || strcmp(varargin(ivar),'timbre'), 
            if ivar<length(varargin),
                timtag = varargin{ivar+1};
                ivar=ivar+1;
            else disp('Missing timbre tag reference after option is called.'); go=0;
            end
            
        elseif strcmp(varargin(ivar),'timnames'),
            if ivar<length(varargin),
                timnames = varargin{ivar+1};
                ivar=ivar+1;
            else disp('Missing timbre labels reference after option is called.'); go=0;
            end
            
        elseif strcmp(varargin(ivar),'features') || strcmp(varargin(ivar),'features'),
            if ivar<length(varargin),
                features = varargin{ivar+1};
                ivar=ivar+1;
            else disp('Missing features reference after option is called.'); go=0;
            end
        
        elseif isnumeric(varargin{ivar}),
            chmats(1:size(varargin{ivar},1),1:size(varargin{ivar},2),1:size(varargin{ivar},3),ivar+1) = varargin{ivar}; % Store input matrices in one, chmats
            chnames{ivar+1} = inputname(ivar+1);

        else
            go=0;
            disp('Wrong input type. Process interrupted.');
        end
        ivar=ivar+1;
    end
    
elseif ischar(matin), % First input is a .mat
    w=whos('-file',matin);
    load(matin);
    for i=1:length(w),
        chnames{i} = w(i).name;
        eval(['chmats(1:size(' w(i).name ',1),1:size(' w(i).name ',2),1:size(' w(i).name ',3),i) = ' w(i).name ';']); % Store all matrices in .mat in one, chmats
    end
    
    ivar=1;
    while ivar<=length(varargin),
        if strcmp(varargin(ivar),'printout'), printout=1;
            
        elseif strcmp(varargin(ivar),'batch'), batch=1;
            
        elseif strcmp(varargin(ivar),'noplot'), plotit=0;
            
        elseif strcmp(varargin(ivar),'plotit'), plotit=1;
            
        elseif strcmp(varargin(ivar),'outname'),
            if ivar<length(varargin) && ischar(varargin{ivar+1}),
                nameout = varargin{ivar+1};
                ivar=ivar+1;
            else disp('Missing output name.');
            end
            
        elseif strcmp(varargin(ivar),'timbres') || strcmp(varargin(ivar),'timbre'),
            if ivar<length(varargin),
                timtag = varargin{ivar+1};
                ivar=ivar+1;
            else disp('Missing timbre tag reference after option is called.'); go=0;
            end
            
        elseif strcmp(varargin(ivar),'timnames'),
            if ivar<length(varargin),
                timnames = varargin{ivar+1};
                ivar=ivar+1;
            else disp('Missing timbre labels reference after option is called.'); go=0;
            end
            
        elseif strcmp(varargin(ivar),'features') || strcmp(varargin(ivar),'features'),
            if ivar<length(varargin),
                features = varargin{ivar+1};
                ivar=ivar+1;
            else disp('Missing features reference after option is called.'); go=0;
            end
            
        else
            go=0;
            disp('Wrong input type. Process interrupted.');
        end
        ivar=ivar+1;
    end

    
else go=0;
    disp('Wrong input type; chord matrices required. Process interrupted.');
    
end


% --- Test of timbres tag reference validity ---

if isnumeric(timtag) && ~sum(timtag), % timtag=0, no specification
else
    if isnumeric(timtag) && length(timtag)==size(chmats,4) && max(timtag)<=length(timnames), % References as numbers
        % Pass timtag along
        
    elseif iscell(timtag) && length(timtag)==size(chmats,4), % References as cells
        timtagtmp = zeros(length(timtag),1);
        for i=1:length(timtag),
            for k=1:length(timnames),
                if strcmp(timtag{i},timnames{k}), timtagtmp(i) = k; end
            end
        end
        if sum(timtagtmp), % At least one matching timbre label
            clear timtag;
            timtag=timtagtmp;
        else disp('Unvalid timbres reference specifications; check its length and content.'); go=0;
        end
        
    else disp('Unvalid timbres reference specifications; check its length and content.'); go=0;
    end
end



% --- Test of features reference validity ---

if isnumeric(features) && ~sum(features) % features=0, no specfication
else
   if isnumeric(features) && max(features)<=chpar+npar2, % References as numbers
       % Pas features along
   elseif iscell(features),
       featmp = zeros(length(features),1);
       for i=1:length(features),
           for j=2:length(chvars),
               if strcmp(features{i},chvars{j}), featmp(i) = j-1; end
           end
           for j=2:length(nvars),
               if strcmp(features{i},nvars{j}), featmp(i) = chpar+j-1; end
           end
       end
       if sum(featmp), % At least one matching feature label
           clear features;
           features = featmp;
           
       else disp('No valid features actually selected.'); go=0;
       end
       
   end
end


% --- Run! ---
if go, 
    if ~batch, selector(chmats,chnames,dmeans,chpar,npar2,chvars,nvars,timnames,printout,timtag,nameout,features,plotit); % Launch the interface: Selection window
        
    else % Bypass the GUI, prepare accordingly
        
        chval = zeros(chpar,1);
        nval = zeros(npar2,1);
        
        for i=1:length(features),
            if features(i)<=chpar, chval(features(i))=1;
            else nval(features(i)-chpar)=1;
            end
        end
        
        % features = sort(features);
        % chval = features(1:find(chval<=chpar,1,'last'));
        % nval = features(find(chval>chpar,1,'first'):length(features)) - chpar;
        
        printit(1:2) = printout;
        
        Go_Callback(0,0,chmats,chnames,dmeans,chpar,npar2,chvars,nvars,chval,nval,0,0,0,0,timnames,printit,timtag,nameout,features,plotit);
        
        % Go_Callback(src,eventdata,   chmats,chnames,dmeans,chpar,npar2,chvars,nvars,   chval,nval,   h0,warp,mline,nsd,   timnames,printit,timtag,nameout,plotit)
    end
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% FEATURE SELECTION WINDOW

function selector(chmats,chnames,dmeans,chpar,npar2,chvars,nvars,timnames,printout,timtag,nameout,features,plotit)

% Features selection GUI

% ScreenSize is a four-element vector: [left, bottom, width, height]:
scrsz = get(0,'ScreenSize');

% Configure display for 4 columns
colw = min(250,fix((scrsz(3)-40)/4)); % Column width
bestwidth = 20 + 4*colw; % Width for 4 columns

nperc = max(fix(chpar/2),fix(npar2/2))+1; % Number of checkboxes per column, i.e. half of chords' or notes' feats
caseheight = min(20,(scrsz(4)-160)/nperc); % Height for one checkbox (20 or less if lack of space)
bestheight = 150 + caseheight*nperc; % Column height

% Figure
h0 = figure('Color',[1,1,1],'Name','Selection','NumberTitle','off','Position',[20 50 bestwidth bestheight]);


% --- FILL ---

% --- Titles ---
uicontrol('Style','text','String','Please select the features to display','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',16,'Position',[1 bestheight-25 bestwidth 24]);
uicontrol('Style','text','String',' <--- Chord features --->','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',12,'Position',[80 bestheight-120 colw-30 20]);
uicontrol('Style','text','String','||','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',14,'Position',[2*colw-10 bestheight-120 20 20]);
uicontrol('Style','text','String',' <--- Notes features --->','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',12,'Position',[2*colw+80 bestheight-120 colw-30 20]);


% --- Checkboxes for each feature ---
% Chord features
for i=1:chpar
    chcol = fix(i/nperc); % # of columns already filled
    
    val=0; % Check box value, i.e. checked or not; preset to checked if specified in input argument
    if sum(features),
        for k=1:length(features), if features(k)==i, val=1; end; end
    end
    
    % One checkbox per feature
    chval(i) = uicontrol('Style','checkbox','String',chvars{i+1},'FontSize',round(caseheight/2),'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[10+colw*chcol bestheight-150-caseheight*(i-1-chcol*nperc+(chcol>=1)) colw-10 caseheight],'Value',val,'Callback',{@Click_Callback});
end

% Note features (mean per chord)
for i=1:npar2
    ncol = fix(i/nperc); % # of columns alrady filled
    
    val=0; % Check box value, i.e. checked or not; preset to checked if specified in input argument
    if sum(features),
        for k=1:length(features), if features(k)==i+chpar, val=1; end; end
    end
    
    % One checkbox per feature
    nval(i) = uicontrol('Style','checkbox','String',nvars{i+1},'FontSize',round(caseheight/2),'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[10+colw*(ncol+2) bestheight-150-caseheight*(i-1-ncol*nperc+(ncol>=1)) colw-10 caseheight],'Value',val,'Callback',{@Click_Callback});
end


% --- Options ---

warp = uicontrol('Style','checkbox','String','Synchonize performances','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[round(bestwidth/2-50)+100 bestheight-50 250 20], 'Value',1,'Callback',{@Click_Callback});
nsd = uicontrol('Style','checkbox','String','Plot note features SD-per-chord','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[round(bestwidth/2-50)+100 bestheight-75 300 20],'Callback',{@Click_Callback});
% uicontrol('Style','text','String','in addition to mean','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',12,'Position',[3*colw+30 bestheight-92 170 20]);

printit(1) = uicontrol('Style','checkbox','String','Printout','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[3*colw+30 bestheight-25 200 20],'Value',printout,'Callback',{@Click_Callback});
printit(2) = uicontrol('Style','checkbox','String','with SD','FontSize',10,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[3*colw+130 bestheight-25 200 20],'Callback',{@Click_Callback});

mline = uicontrol('Style','checkbox','String','Display means line','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[3*colw+30 bestheight-50 200 20],'Callback',{@Click_Callback});
sdbar = uicontrol('Style','checkbox','String','Display Errorbars','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[3*colw+50 bestheight-75 200 20],'Value',1,'Callback',{@Click_Callback});

% % Hand selection
% piesplit = uicontrol('Style','checkbox','String','Split hands','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[40 bestheight-40 120 20],'Callback',{@Split_Callback});
% pienum = uicontrol('Style','popupmenu','String',{'Piece ?','Piece #1','Piece #2','Piece #3','Piece #4'},'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',10,'Position',[160 bestheight-40 100 20],'Callback',{@Pnum_Callback});
% leftonly = uicontrol('Style','checkbox','String','Left only','FontSize',10,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[60 bestheight-65 120 20],'Callback',{@LeftO_Callback});
% rightonly = uicontrol('Style','checkbox','String','Right only','FontSize',10,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[60 bestheight-85 120 20],'Callback',{@RightO_Callback});

% Super tricky timbre selector
uicontrol('Style','pushbutton','String','Select the timbres','FontSize',14,'Position',[40 bestheight-60 280 40],'Callback',{@Ftimbres_Callback,chnames,bestheight,h0,timtag});


% --- Go ---
uicontrol('Style','pushbutton','String','Go','FontSize',16,'Position',[round(bestwidth/2-50) bestheight-80 80 40],'FontWeight','bold','Callback',{@Go_Callback,chmats,chnames,dmeans,chpar,npar2,chvars,nvars,chval,nval,h0,warp,mline,nsd,sdbar,timnames,printit,timtag,nameout,features,plotit});

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% CALLBACKS

% --- For features' checkboxes ---

function Click_Callback(hObject,eventdata)
end


% --- For tricky timbre selector in popup window ---

function Ftimbres_Callback(hObject,eventdata,chnames,bestheight,h0,timtag)

% Number of mats
fnum = length(chnames);

if ~timtag, timtag(1:fnum)=0; end % No preset


% Popup figure
hp = figure('Position',[60 bestheight-300 140*fnum 260],'Color',[1,1,1],'Name','Pieces','NumberTitle','off','Menubar','none');

% Menu: Timbre selection per perf.
for i=1:fnum
    uicontrol('Style','text','String',['File #' num2str(i)],'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',12,'Position',[140*(i-1)+10 220 100 20]);
    uicontrol('Style','text','String',chnames{i},'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',12,'Position',[140*(i-1)+10 190 100 20]);
    timbred(i) = uicontrol('Style','popupmenu','String',{'Timbre?','Bright','Round','Dry','Dark','Velvety'},'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',10,'Position',[140*(i-1)+10 160 100 20],'Value',timtag(i)+1,'Callback',{@Tnum_Callback});
end

uicontrol('Style','pushbutton','String','Save','FontSize',14,'FontWeight','bold','Position',[140*fnum/2-40 50 80 40],'Callback',{@Save_Callback,h0,timbred});

end

function Tnum_Callback(hObject,eventdata)
flag_pnum = get(hObject,'Value');
switch flag_pnum;
    case 'Bright'
    case 'Dark'
    case 'Dry'
    case 'Round'
    case 'Velvety'
end

end

function Save_Callback(hObject,eventdata,h0,timbred)

% Save GUI timbre selection
data = guidata(h0);
for i=1:length(timbred), data.timbres(i) = get(timbred(i),'Value') -1; end % Minus one because case 1 <=> No choice
guidata(h0,data);

end



%% --- For launching the plotting function ---

function Go_Callback(src,eventdata,chmats,chnames,dmeans,chpar,npar2,chvars,nvars,chval,nval,h0,warp,mline,nsd,sdbar,timnames,printit,timtag,nameout,features,plotit)

% Eval features selection
if h0, % i.e. launch through GUI
    
    for i=1:length(chval), chsel(i) = get(chval(i),'Value'); end % 1 if feature i is selected (checked box), else 0
    for i=1:length(nval), nsel(i) = get(nval(i),'Value'); end
    
    % Eval options selection
    warped = get(warp,'Value');
    sdbarred = get(sdbar,'Value');
    printed(1) = get(printit(1),'Value');
    printed(2) = get(printit(2),'Value');
    mlined = get(mline,'Value');
    sdn = get(nsd,'Value');
    
else % Batch processed, GUI bypassed
    chsel = chval;
    nsel = nval;
    printed=printit;
    warped = 1;
    sdbarred = 1;
    mlined = 0;
    sdn = 0;
    
end


% Eval pieces selection
% splitit = get(piesplit,'Value');
% piecemain = get(pienum,'Value') -1; % Minus 1 so as p01 <=> 1, etc.
% lefton = get(leftonly,'Value');
% righton = get(rightonly,'Value');
%
% if piecemain==0 % No global piece selection, so look at the tricky popup selector
% else % If a global piece is selected, overlook the tricky popup selector
%
%     for i=1:length(chnames), piece(i) = piecemain; end % Define all mats' piece as the global one
%
% end


% --- Get performances' timbres ---

tim = zeros(length(chnames),1);

if isnumeric(timtag) && ~sum(timtag), % timtag=0, no specification => get from the timbre selector GUI
    
    if h0, data = guidata(h0); % Call up stored values for pieces selection
    else data = [];
    end
        
        
    if ~(isempty(data)), % Fine selection was accomplished
        for i=1:length(data.timbres), tim(i) = data.timbres(i); end % Affect to piece var.
        
    else tim(1:length(chnames)) = 0; % Zero-sum vector to be used later
        disp('You forgot to specify any timbre!');
    end
    
else
    if h0, data = guidata(h0); % Check the GUI timbre input to supercede the original input specs
    else data = [];
    end
    
    for i=1:length(timtag),
        if ~(isempty(data)) && i<=length(data.timbres) && isnumeric(data.timbres(i)) && isfinite(data.timbres(i)), % there is something GUI-specified for perf.(i)
            tim(i) = data.timbres(i);
        else 
            tim(i) = timtag(i);
        end
    end
    
    if ~sum(tim), disp('You forgot to specify any timbre!'); end
    
end


%%%%%%
% If to close previously open plots
% fion = get(0,'Children');
% if length(fion)>1, close(find(fion~=1)); end
%%%%%%


if (sum(chsel)+sum(nsel)), % Are any features selected?
    plotem(chmats,chnames,dmeans,chpar,npar2,chvars,nvars,chsel,nsel,h0,warped,mlined,sdn,sdbarred,tim,timnames,printed,timtag,nameout,features,plotit);
else
    disp('Please select at least one feature to plot');
end
end


% --- For going back to selection ---

function Select_Callback(hObject,eventdata,h0,chmats,chnames,dmeans,chpar,npar2,chvars,nvars,timnames,printed,timtag,nameout,features,plotit)
if ishandle(h0), figure(h0);
else selector(chmats,chnames,dmeans,chpar,npar2,chvars,nvars,timnames,printed(1),timtag,nameout,features,plotit);
end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% PLOTTING

function plotem(chmats,chnames,dmeans,chpar,npar2,chvars,nvars,chsel,nsel,h0,warped,mlined,sdn,sdbarred,tim,timnames,printed,timtag,nameout,features,plotit)

% --- New figure ---
if plotit,
    scrsz = get(0,'ScreenSize');
    h = figure('Color',[1,1,1],'Name','Display','NumberTitle','off','Position',[10 50 scrsz(3)-20 scrsz(4)-150]);
    if h0,
        uicontrol('Style','pushbutton','String','Selection','FontSize',12,'Position',[10 10 120 30],'Callback',{@Select_Callback,h0,chmats,chnames,dmeans,chpar,npar2,chvars,nvars,timnames,printed,timtag,nameout,features,plotit});
    end
end

% --- Design ---

chfeats = sum(chsel); % Number of chord features to display, i.e. number of checked boxes
nfeats = sum(nsel); % Number of note features to display, i.e. number of checked boxes

chnum = size(chmats,4); % Number of performances to compare
ntim = length(timnames); % Default: 5

% Colors
color = [0 0 1;1 0 0;0 1 0;0 1 1;1 0 1]; % 5 basic
if ntim>=length(color), % Add colors to fit all performances
    for i=length(color)+1:ntim+1, color(i,:) = color(i-5,:)./2; end
end

% Get the timbre labels subset actually used
timrank=1;
timon=0;
timlabs={};
for t=1:ntim,
    if sum(tim==t),
        timlabs{timrank} = timnames{t};
        timon(timrank)=t;
        timrank=timrank+1;
    end
end


% Find features on
chon = find(chsel==1);
noton = find(nsel==1);


% --- First onset sync + Warp ---

onsetini = min(chmats(dmeans+1,1,2,tim>0)); % Earliest onset among performances with a timbre assigned

delay = zeros(1,chnum);
if warped, durashun = zeros(1,chnum); end

for i=1:chnum,
    if tim(i)>0,
        delay(i) = chmats(dmeans+1,1,2,i) - onsetini; % Delay between perf's 1st onset and earliest overall onset
        if warped, durashun(i) = max(chmats(dmeans+1:size(chmats,1),1,2,i)) - chmats(dmeans+1,1,2,i); end % Performance i duration from first to last onset
    end
end


if warped, % Define the coefficient for having performances match in duration
    warpcoef = zeros(2,chnum);
    % Perf.i: x.warped(i) = a(i)*x + b(i), w/ a(i) = warpcoef(1,i), and b(i) = warpcoef(2,i)
    
    for i=1:chnum,
        if tim(i)>0,
            warpcoef(1,i) = mean(durashun)/durashun(i);
            warpcoef(2,i) = chmats(dmeans+1,1,2,i) * (1 - warpcoef(1,i))  - delay(i); % b(i) = x0(i) * (1-a(i)) - delay(i), w/ x0 = t(first onset for perf.i) and delay(i) = delta(x0(i), onsetini) to synchronize all first onsets
        end
    end
    
else
    warpcoef(1,1:chnum) = 1;
    for i=1:chnum, warpcoef(2,i) = -delay(i); end
end


% --- Maximum on/offset, to trace the mean's line ---
for i=1:chnum,
    if tim(i)>0,
        maxoffset = round(warpcoef(1,i).*max(chmats(dmeans+1:size(chmats,1),1,3,i)) + warpcoef(2,i)); % Maximum (warped) offset, to trace the mean's line
        maxonset = round(warpcoef(1,i).*max(chmats(dmeans+1:size(chmats,1),1,2,i)) + warpcoef(2,i)); % Maximum (warped) onset
    end
end
maxoff = max(maxoffset);
maxon = max(maxonset);



% --- Define a time range to window it out ---
winsize = 1000; % 500 ms window as default
timeline = (onsetini+round(winsize/2):winsize:maxon-round(winsize/2));


% --- Output design ---

outer = zeros(1+chfeats+(sdn+1)*nfeats,1+length(timeline),length(timon),2); % Output matrix, 4D : [timeline + features (chord's + notes + notes SD if selected)] * [time-wise mean + time chunks] * timbres * [perf-wise mean + SD]

% Store timeline
for t=1:length(timon),
    outer(1,2:length(timeline)+1,1) = timeline;
    outer(1,2:length(timeline)+1,2) = timeline;
end

warning off;

% --- Actual plot ---

for i=1:chfeats, % Loop on selected features
    
    if plotit, hold on; hi(i) = subplot(chfeats+(sdn+1)*nfeats,8,8*(i-1)+1:8*(i-1)+7); end % One subplot per feature, with additional one for SDn, for each selected note feature, if option selected
    
    timpos=0; % For placing timbre mean in eastern mean plot
    
    for t=1:ntim, % Timbres
        
        if sum(tim==t), % There is at least one perf. with timbre (t)
            % Define a tmp matrix to store the (i)th feature's values of perfs with timbre (t) along the timeline (through window-sweeping)
            tmp = zeros(sum(tim==t),length(timeline));
            
            ton = find(tim==t);
            
            for j=1:length(timeline), % Final dots to plot
                
                for f=1:length(ton) % perfs with target timbre
                    timematch = find ( ((warpcoef(1,ton(f)) .* chmats(dmeans+1:size(chmats,1),1,2,ton(f)) + warpcoef(2,ton(f))) >= timeline(j)-round(winsize/2)) & ...
                        ((warpcoef(1,ton(f)) .* chmats(dmeans+1:size(chmats,1),1,2,ton(f)) + warpcoef(2,ton(f))) < timeline(j)+round(winsize/2)) );
                    % Returns all chmats' lines corresponding to a chord whose onset falls within the window at position j; yes, this is magic.
                    
                    tmp(f,j) = mean( chmats(timematch,1,chon(i),ton(f)) ); % Collecting values
                    if ~isfinite(tmp(f,j)), tmp(f,j)=0; end
                end
                
                % Store in output matrix
                outer(i+1,j+1,timon==t,1) = mean(tmp(tmp(:,j)~=0,j),1); % Store the mean for timbre t, for time-chunk j, for feature i
                if ~isfinite(outer(i+1,j+1,timon==t,1)), outer(i+1,j+1,timon==t,1)=0; end
                outer(i+1,j+1,timon==t,2) = std(tmp(tmp(:,j)~=0,j),0,1); % Store the SD for timbre t, for time-chunk j, for feature i
                if ~isfinite(outer(i+1,j+1,timon==t,2)), outer(i+1,j+1,timon==t,2)=0; end
            end
            
            % Store perf-wise mean/SD
            tmperf = zeros(length(ton),1);
            for f=1:length(ton),
                tmperf(f) = mean(tmp(f,tmp(f,:)~=0),2); % Mean per perf.
                if ~isfinite(tmperf(f)), tmperf(f)=0; end
            end
            
            outer(i+1,1,timon==t,1) = mean(tmperf(tmperf~=0)); % Mean of mean-per-perf.
            outer(i+1,1,timon==t,2) = std(tmperf(tmperf~=0)); % SD of mean-per-perf.
            

            
            % And now, we plot!
            
            if plotit,
                errors = 2/sqrt(length(ton)); % Coef. to apply to errorbars (default: 2SE)
                
                tmplot = outer(i+1,2:length(timeline)+1,timon==t,1);
                for iii=2:length(tmplot)-1, if tmplot(iii)==0, tmplot(iii) = (tmplot(iii-1)+tmplot(iii+1))/2; end; end
                
                tmplotsd = outer(i+1,2:length(timeline)+1,timon==t,2);
                                
                if tmplot(length(tmplot))==0, 
                    tmplot(length(tmplot)) = [];
                    tmplotsd(length(tmplotsd))=[];
                end
                
                hold on;
                if sdbarred, errorbar(hi(i),timeline(1:length(tmplot)),tmplot,errors*tmplotsd,'-','Color',color(t,:));                    
                else plot(hi(i),timeline(1:length(tmplot)),tmplot,'-o','Color',color(t,:));
                end
                
                % Plot feature mean as line
                if mlined, hold on; plot(hi(i),[0 maxoff],[outer(i+1,1,timon==t,1) outer(i+1,1,timon==t,1)],':','Color',color(t,:)); end
                
                % Plot mean as bar in adjacent subplot
                hm(i) = subplot(chfeats+(sdn+1)*nfeats,8,8*i);
                hold on;
                timpos = timpos+1;
                
                fill([timpos-.4,timpos+.4,timpos+.4,timpos-.4],[0,0,outer(i+1,1,timon==t,1),outer(i+1,1,timon==t,1)],color(t,:)); % For the mean
                
                % Home-made errorbars
                plot([timpos,timpos],[outer(i+1,1,timon==t,1)-errors*outer(i+1,1,timon==t,2),outer(i+1,1,timon==t,1)+errors*outer(i+1,1,timon==t,2)],'-','Color',color(t+1,:));
                plot([timpos-.4,timpos+.4],[outer(i+1,1,timon==t,1)-errors*outer(i+1,1,timon==t,2),outer(i+1,1,timon==t,1)-errors*outer(i+1,1,timon==t,2)],'-','Color',color(t+1,:));
                plot([timpos-.4,timpos+.4],[outer(i+1,1,timon==t,1)+errors*outer(i+1,1,timon==t,2),outer(i+1,1,timon==t,1)+errors*outer(i+1,1,timon==t,2)],'-','Color',color(t+1,:));
            end
            
        end
    end
    
    
    % Legend
    if plotit,
        if timon,
            for j=1:length(timon), hold on; hplot(j)=plot(hi(i),0,outer(i+1,1,j,1),'-','LineWidth',1.5,'Color',color(timon(j),:)); end
            lel = legend(hi(i),hplot(:),timlabs{:},'Location','SouthEast');
            hplot=[];
        end
        
        ydims = get(hi(i),'YLim');
        set(hm(i),'YLim',ydims);
%         haxys = findobj([hi(i) hm(i)],'type','axes');
%         linkaxes(haxys,'y');
        ylabel(hi(i),chvars{chon(i)+1},'FontSize',16);
    end
    
end


% Note features
for i=1:nfeats, % Loop on selected features
    
    if plotit, hi(i+chfeats) = subplot(chfeats+(sdn+1)*nfeats,8,8*(i+chfeats-1)+1:8*(i+chfeats-1)+7); end % One subplot per feature, with additional one for SDn, for each selected note feature, if option selected
    
    timpos=0; % For placing timbre mean in eastern mean plot
    
    for t=1:ntim, % Timbres
        
        if sum(tim==t), % There is at least one perf. with timbre (t)
            % Define a tmp matrix to store the (i)th feature's values of perfs with timbre (t) along the timeline (through window-sweeping)
            tmp = zeros(sum(tim==t),length(timeline));
            
            ton = find(tim==t);
                        
            for j=1:length(timeline), % Final dots to plot
                
                for f=1:length(ton) % perfs with target timbre
                    timematch = find ( ((warpcoef(1,ton(f)) .* chmats(dmeans+1:size(chmats,1),1,2,ton(f)) + warpcoef(2,ton(f))) > timeline(j)-round(winsize/2)) & ...
                        ((warpcoef(1,ton(f)) .* chmats(dmeans+1:size(chmats,1),1,2,ton(f)) + warpcoef(2,ton(f))) < timeline(j)+round(winsize/2)) );
                    % Returns all chmats' lines corresponding to a chord whose onset falls within the window at position j; yes, this is magic.
                    
                    tmp(f,j) = mean( chmats(timematch,2,noton(i),ton(f)) ); % Collecting values
                    if ~isfinite(tmp(f,j)), tmp(f,j)=0; end
                end
                
                % Store in output matrix
                outer(chfeats+i+1,j+1,timon==t,1) = mean(tmp(tmp(:,j)~=0,j),1); % Store the mean for timbre t, for time-chunk j, for feature i
                if ~isfinite(outer(chfeats+i+1,j+1,timon==t,1)), outer(chfeats+i+1,j+1,timon==t,1)=0; end
                outer(chfeats+i+1,j+1,timon==t,2) = std(tmp(tmp(:,j)~=0,j),0,1); % Store the SD for timbre t, for time-chunk j, for feature i
                if ~isfinite(outer(chfeats+i+1,j+1,timon==t,2)), outer(chfeats+i+1,j+1,timon==t,2)=0; end
            end
            
            % Store perf-wise mean/SD
            tmperf = zeros(length(ton),1);
            for f=1:length(ton),
                tmperf(f) = mean(tmp(f,tmp(f,:)~=0),2); % Mean per perf.
                if ~isfinite(tmperf(f)), tmperf(f)=0; end
            end
            
            outer(chfeats+i+1,1,timon==t,1) = mean(tmperf(tmperf~=0)); % Mean of mean-per-perf.
            outer(chfeats+i+1,1,timon==t,2) = std(tmperf(tmperf~=0)); % SD of mean-per-perf.
            
            
            % And now, we plot!
            
            if plotit,
                errors = 2/sqrt(length(ton)); % Coef. to apply to errorbars (default: 2SE)
                
                tmplot = outer(chfeats+i+1,2:length(timeline)+1,timon==t,1);
                for iii=2:length(tmplot)-1, if tmplot(iii)==0, tmplot(iii) = (tmplot(iii-1)+tmplot(iii+1))/2; end; end
                
                tmplotsd = outer(chfeats+i+1,2:length(timeline)+1,timon==t,2);
                                
                if tmplot(length(tmplot))==0,
                    tmplot(length(tmplot)) = [];
                    tmplotsd(length(tmplotsd))=[];
                end
                
                hold on;
                if sdbarred, errorbar(hi(i+chfeats),timeline(1:length(tmplot)),tmplot,errors*tmplotsd,'-','Color',color(t,:));                    
                else plot(hi(i+chfeats),timeline(1:length(tmplot)),tmplot,'-o','Color',color(t,:));
                end
                
                % Plot feature mean as line
                if mlined, hold on; plot(hi(i+chfeats),[0 maxoff],[outer(chfeats+i+1,1,timon==t,1) outer(chfeats+i+1,1,timon==t,1)],':','Color',color(t,:)); end
                
                % Plot mean as bar in adjacent subplot
                hm(i+chfeats) = subplot(chfeats+(sdn+1)*nfeats,8,8*(i+chfeats));
                hold on;
                timpos = timpos+1;
                
                fill([timpos-.4,timpos+.4,timpos+.4,timpos-.4],[0,0,outer(chfeats+i+1,1,timon==t,1),outer(chfeats+i+1,1,timon==t,1)],color(t,:)); % For the mean
                
                % Home-made errorbars
                plot([timpos,timpos],[outer(chfeats+i+1,1,timon==t,1)-errors*outer(chfeats+i+1,1,timon==t,2),outer(chfeats+i+1,1,timon==t,1)+errors*outer(chfeats+i+1,1,timon==t,2)],'-','Color',color(t+1,:));
                plot([timpos-.4,timpos+.4],[outer(chfeats+i+1,1,timon==t,1)-errors*outer(chfeats+i+1,1,timon==t,2),outer(chfeats+i+1,1,timon==t,1)-errors*outer(chfeats+i+1,1,timon==t,2)],'-','Color',color(t+1,:));
                plot([timpos-.4,timpos+.4],[outer(chfeats+i+1,1,timon==t,1)+errors*outer(chfeats+i+1,1,timon==t,2),outer(chfeats+i+1,1,timon==t,1)+errors*outer(chfeats+i+1,1,timon==t,2)],'-','Color',color(t+1,:));
            end
            
        end
    end
    
    
    % Legend
    if plotit,
        if timon,
            for j=1:length(timon), hold on; hplot(j)=plot(hi(i+chfeats),0,outer(chfeats+i+1,1,j,1),'-','LineWidth',1.5,'Color',color(timon(j),:)); end
            lel = legend(hi(i+chfeats),hplot(:),timlabs{:},'Location','SouthEast');
            hplot=[];
        end
        
        ydims = get(hi(i+chfeats),'YLim');
        set(hm(i+chfeats),'YLim',ydims);
%         haxys = findobj([hi(i+chfeats) hm(i+chfeats)],'type','axes');
%         linkaxes(haxys,'y');
        ylabel(hi(i+chfeats),nvars{noton(i)+1},'FontSize',16);
    end
    
end


% Note features SD per chord, if selected
if sdn,
    for i=1:nfeats, % Loop on selected features
        
        if plotit, hi(i+nfeats+chfeats) = subplot(chfeats+(sdn+1)*nfeats,8,8*(i+chfeats+nfeats-1)+1:8*(i+chfeats+nfeats-1)+7); end % One subplot per feature, with additional one for SDn, for each selected note feature, if option selected
        
        timpos=0; % For placing timbre mean in eastern mean plot
        
        for t=1:ntim, % Timbres
            
            if sum(tim==t), % There is at least one perf. with timbre (t)
                % Define a tmp matrix to store the (i)th feature's values of perfs with timbre (t) along the timeline (through window-sweeping)
                tmp = zeros(sum(tim==t),length(timeline));
                
                ton = find(tim==t); % Timbre match
                
                for j=1:length(timeline) % Final dots to plot
                    
                    for f=1:length(ton), % perfs with target timbre
                        timematch = find ( ((warpcoef(1,ton(f)) .* chmats(dmeans+1:size(chmats,1),1,2,ton(f)) + warpcoef(2,ton(f))) > timeline(j)-round(winsize/2)) & ...
                            ((warpcoef(1,ton(f)) .* chmats(dmeans+1:size(chmats,1),1,2,ton(f)) + warpcoef(2,ton(f))) < timeline(j)+round(winsize/2)) );
                        % Returns all chmats' lines corresponding to a chord whose onset falls within the window at position j; yes, this is magic.
                        
                        tmp(f,j) = mean( chmats(timematch,3,noton(i),ton(f)) ); % Collecting values
                        if ~isfinite(tmp(f,j)), tmp(f,j)=0; end
                    end
                    
                    % Store in output matrix
                    outer(chfeats+nfeats+i+1,j+1,timon==t,1) = mean(tmp(tmp(:,j)~=0,j),1); % Store the mean for timbre t, for time-chunk j, for feature i
                    if ~isfinite(outer(chfeats+nfeats+i+1,j+1,timon==t,1)), outer(chfeats+nfeats+i+1,j+1,timon==t,1)=0; end
                    outer(chfeats+nfeats+i+1,j+1,timon==t,2) = std(tmp(tmp(:,j)~=0,j),0,1); % Store the SD for timbre t, for time-chunk j, for feature i
                    if ~isfinite(outer(chfeats+nfeats+i+1,j+1,timon==t,2)), outer(chfeats+nfeats+i+1,j+1,timon==t,2)=0; end
                end
                
                % Store perf-wise mean/SD
                tmperf = zeros(length(ton),1);
                for f=1:length(ton),
                    tmperf(f) = mean(tmp(f,tmp(f,:)~=0),2); % Mean per perf.
                    if ~isfinite(tmperf(f)), tmperf(f)=0; end
                end
                
                outer(chfeats+nfeats+i+1,1,timon==t,1) = mean(tmperf(tmperf~=0)); % Mean of mean-per-perf.
                outer(chfeats+nfeats+i+1,1,timon==t,2) = std(tmperf(tmperf~=0)); % SD of mean-per-perf.
                
                
                % And now, we plot!
                
                if plotit,
                    errors = 2/sqrt(length(ton)); % Coef. to apply to errorbars (default: 2SE)
                    
                    tmplot = outer(chfeats+nfeats+i+1,2:length(timeline)+1,timon==t,1);
                    for iii=2:length(tmplot)-1, if tmplot(iii)==0, tmplot(iii) = (tmplot(iii-1)+tmplot(iii+1))/2; end; end
                    
                    
                    tmplotsd = outer(chfeats+nfeats+i+1,2:length(timeline)+1,timon==t,2);
                                        
                    if tmplot(length(tmplot))==0,
                        tmplot(length(tmplot)) = [];
                        tmplotsd(length(tmplotsd))=[];
                    end                    
                    
                    hold on;
                    if sdbarred,
                        
                        errorbar(hi(i+chfeats+nfeats),timeline(1:length(tmplot)),tmplot,errors*tmplotsd,'-','Color',color(t,:));
                        
                    else plot(hi(i+chfeats+nfeats),timeline(1:length(tmplot)),tmplot,'-o','Color',color(t,:));
                    end
                    
                    % Plot feature mean as line
                    if mlined, hold on; plot(hi(i+chfeats+nfeats),[0 maxoff],[outer(chfeats+nfeats+i+1,1,timon==t,1) outer(chfeats+nfeats+i+1,1,timon==t,1)],':','Color',color(t,:)); end
                    
                    % Plot mean as bar in adjacent subplot
                    hm(i+chfeats+nfeats) = subplot(chfeats+(sdn+1)*nfeats,8,8*(i+chfeats+nfeats));
                    hold on;
                    timpos = timpos+1;
                    
                    fill([timpos-.4,timpos+.4,timpos+.4,timpos-.4],[0,0,outer(chfeats+nfeats+i+1,1,timon==t,1),outer(chfeats+nfeats+i+1,1,timon==t,1)],color(t,:)); % For the mean
                    
                    % Home-made errorbars
                    plot([timpos,timpos],[outer(chfeats+nfeats+i+1,1,timon==t,1)-errors*outer(chfeats+nfeats+i+1,1,timon==t,2),outer(chfeats+nfeats+i+1,1,timon==t,1)+errors*outer(chfeats+nfeats+i+1,1,timon==t,2)],'-','Color',color(t+1,:));
                    plot([timpos-.4,timpos+.4],[outer(chfeats+nfeats+i+1,1,timon==t,1)-errors*outer(chfeats+nfeats+i+1,1,timon==t,2),outer(chfeats+nfeats+i+1,1,timon==t,1)-errors*outer(chfeats+nfeats+i+1,1,timon==t,2)],'-','Color',color(t+1,:));
                    plot([timpos-.4,timpos+.4],[outer(chfeats+nfeats+i+1,1,timon==t,1)+errors*outer(chfeats+nfeats+i+1,1,timon==t,2),outer(chfeats+nfeats+i+1,1,timon==t,1)+errors*outer(chfeats+nfeats+i+1,1,timon==t,2)],'-','Color',color(t+1,:));
                end
                
            end
        end
        
        
        % Legend
        if plotit,
            if timon,
                for j=1:length(timon), hold on; hplot(j)=plot(hi(i+chfeats+nfeats),0,outer(chfeats+nfeats+i+1,1,j,1),'-','LineWidth',1.5,'Color',color(timon(j),:)); end
                lel = legend(hi(i+chfeats+nfeats),hplot(:),timlabs{:},'Location','SouthEast');
                hplot=[];
            end
            
            ydims = get(hi(i+chfeats+nfeats),'YLim');
            set(hm(i+chfeats+nfeats),'YLim',ydims);
%             haxys = findobj([hi(i+chfeats+nfeats) hm(i+chfeats+nfeats)],'type','axes');
%             linkaxes(haxys,'y');
            ylabel(hi(i+chfeats+nfeats),['SD ' nvars{noton(i)+1}],'FontSize',16);
        end
        
    end
end




% X-Axis
if plotit,
    xmin = onsetini; % Min onset time for the first chord of each perf.
    xmax = maxoff; % Max offset time for any chord in any perf. (with delay compensation for onset sync.)
    set(hi(:),'XLim',[xmin-200 xmax]);
    haxes = findobj(hi(:),'type','axes');
    linkaxes(haxes,'x');
end

warning on;

% --- OUTPUT ---

assignin('base',nameout,outer);
fprintf('Results output in the workspace variable %s \n',nameout);

% Matrix, 4D : [timeline + features] * [time-wise mean + time chunks] * timbres * [perf-wise mean + SD]

if printed(1),
    fid = fopen([nameout '.xls'],'w+');
    
    fprintf(fid,'Timeline\t \t \t Mean \t');
    for k=2:size(outer,2), fprintf(fid,'%d \t',outer(1,k,1,1)); end % Timeline
    
    for i=1:chfeats
        fprintf(fid,'\n \n %s',chvars{chon(i)+1});
        for t=1:size(outer,3), % Timbres
            fprintf(fid,'\n \t %s \t mean \t',timlabs{t});
            for k=1:size(outer,2),fprintf(fid,'%.3f \t',outer(i+1,k,t,1)); end % Print feature i, time k, timbre t, mean
            
            if printed(2),
                fprintf(fid,'\n \t \t sd \t');
                for k=1:size(outer,2),fprintf(fid,'%.3f \t',outer(i+1,k,t,2)); end % Print feature i, time k, timbre t, SD
            end
        end
    end
    
    for i=1:nfeats
        fprintf(fid,'\n \n %s',nvars{noton(i)+1});
        for t=1:size(outer,3), % Timbres
            fprintf(fid,'\n \t %s \t mean \t',timlabs{t});
            for k=1:size(outer,2),fprintf(fid,'%.3f \t',outer(chfeats+i+1,k,t,1)); end % Print feature chfeats+i, time k, timbre t, mean
            
            if printed(2),
                fprintf(fid,'\n \t \t sd \t');
                for k=1:size(outer,2),fprintf(fid,'%.3f \t',outer(chfeats+i+1,k,t,2)); end % Print feature chfeats+i, time k, timbre t, SD
            end
        end
    end
    
    if sdn,
        for i=1:nfeats
            fprintf(fid,'\n \n %s',['SD ' nvars{noton(i)+1}]);
            for t=1:size(outer,3), % Timbres
                fprintf(fid,'\n \t %s \t mean \t',timlabs{t});
                for k=1:size(outer,2),fprintf(fid,'%.3f \t',outer(chfeats+nfeats+i+1,k,t,1)); end % Print feature chfeats+nfeats+i, time k, timbre t, mean
                
                if printed(2),
                    fprintf(fid,'\n \t \t sd \t');
                    for k=1:size(outer,2),fprintf(fid,'%.3f \t',outer(chfeats+nfeats+i+1,k,t,2)); end % Print feature chfeats+nfeats+i, time k, timbre t, SD
                end
            end
        end
    end
    
    
    fclose(fid);
end

end



function [chv nv] = varnames()

% --- Define columns' labels: chord features then notes-per-chord means then SD

chv={'#','Number of notes','Chord onset','Chord offset','Chord duration','max Amax', 'max MHV','Melody lead','Melody lead rate','Last-note trail','Last-note trail rate',...
    'Soft pedal depression/chord','Soft pedal duration/chord','Soft pedal full-depression length/chord','Soft pedal mid-depression length/chord','Soft pedal mid-depression level/chord',...
    'Sustain pedal depression/chord','Sustain pedal duration/chord','Sustain pedal full-depression length/chord','Sustain pedal mid-depression length/chord','Sustain pedal mid-depression level/chord',...
    'Soft pedal @onset','Soft pedal bin. @onset','Soft pedal lead @onset','Sustain pedal @onset','Sustain pedal bin. @onset','Sustain pedal lead @onset',...
    'Soft pedal @offset','Soft pedal bin. @offset','Soft pedal trail @offset','Sustain pedal @offset','Sustain pedal bin. @offset','Sustain pedal trail @offset',...
    'IOI','OffOnI','OffOnI direction','IOI same-hand','OffOnI same-hand','OffOnI direction same-hand','IOI other-hand','OffOnI other-hand','OffOnI direction other-hand',...
    'Overlap length','Overlap length rate','Overlap amount','Overlap amount rate','Number of overlaps',...
    'Overlap length same-hand','Overlap length rate same-hand','Overlap amount same-hand','Overlap amount rate same-hand','Number of overlaps same-hand',...
    'Overlap length other-hand','Overlap length rate other-hand','Overlap amount other-hand','Overlap amount rate other-hand','Number of overlaps other-hand'};

% --- Define notes columns' labels: notes features

nv={'#','Key','Note onset','Note offset','Note duration','Amoy','Amax','t.Amax','MHV','t.MHV',...
    'Speed of attack','Slope of attack','Speed of key attack','Slope of key attack','Directness MHV-Amax','MHV/Amax','Sharpness of key attack slope','Sharpness of attack slope',...
    'FKD start','FKD end','FKD duration','FKD rate','Pre-FKD duration','Pre-FKD rate','Post-FKD duration','Post-FKD rate',...
    't.attack.end','t.decay.end','t.release.start','Attack duration','Sustain duration','Release duration','Attack rate','Sustain rate','Release rate',...
    'Soft pedal depression', 'Sustain pedal depression', 'Soft pedal @note.onset','Sustain pedal @note.onset', 'Soft pedal @MHV', 'Sustain pedal @MHV', 'Soft pedal @note.offset', 'Sustain pedal @note.offset',...
    'Soft pedal depression length','Sustain pedal depression length','Key attack mean depression','Key attack mean depression rate',...
    'Note onset lag','Note onset lag rate','Note onset lag amount','Note onset lag amount rate','Note offset lead','Note offset lead rate','Note offset lead amount','Note offset lead amount rate','Sync rate','Sync amount rate'};


end