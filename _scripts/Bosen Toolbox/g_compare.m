function g_compare(varargin)

% GUI Launcher
% Comparison in time of chords features between several performances
% One subplot per feature
% One line per subplot per performance
% Additional means comparison
% INPUT: chord matrices
%

% PARAMETERS
npar = 46; % Original number of parameters gathered for each note, by itself
npar2 = npar + 10; % Total number of parameters gathered for each note: by itself + by comparison with other notes in the same chord
chpar = 56; % Total number of parameters gathered for each chord
dmeans = 4; % Number of different means and SDs calculated over the whole file: (mean + SD) of all chords (with some features only multi-notes), left hand, right hand and difference thereof
threshand = [58 64 64 55]; % Note threshold to separate hands for each of the four pieces

[chvars nvars] = varnames;

chmats = zeros(dmeans+1,6,max(npar2,chpar),length(varargin)); % 4D matrix containing all input 3D chord matrices
chnames = cell(1,length(varargin));

go=1; % Computability check


% Store mats' names
for i=1:length(varargin),
    if isnumeric(varargin{i}),
        chmats(1:size(varargin{i},1),1:size(varargin{i},2),1:size(varargin{i},3),i) = varargin{i};
        chnames{i} = inputname(i);
    else go=0;
        disp('Wrong input type; chord matrices required. Process interrupted.');
    end
end

if go, selector(chmats,chnames,dmeans,chpar,npar2,chvars,nvars,threshand); end % Launch the interface: Selection window

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% FEATURE SELECTION WINDOW

function selector(chmats,chnames,dmeans,chpar,npar2,chvars,nvars,threshand)

% Features selection GUI

% ScreenSize is a four-element vector: [left, bottom, width, height]:
scrsz = get(0,'ScreenSize');

% Configure display for 4 columns

colw = min(250,fix((scrsz(3)-40)/4)); % Column width
bestwidth = 20 + 4*colw; % Width for 4 columns

nperc = max(fix(chpar/2),fix(npar2/2))+1; % Number of checkboxes per column, i.e. half of chords' or notes' feats
caseheight = min(20,(scrsz(4)-160)/nperc); % Height for one checkbox (20 or less if lack of space)
bestheight = 150 + caseheight*nperc; % Column height

% Figure
h0 = figure('Color',[1,1,1],'Name','Selection','NumberTitle','off','Position',[20 50 bestwidth bestheight]);

% Fill
% Titles
uicontrol('Style','text','String','Please select the features to display','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',16,'Position',[1 bestheight-25 bestwidth 24]);
uicontrol('Style','text','String',' <--- Chord features --->','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',12,'Position',[80 bestheight-120 colw-30 20]);
uicontrol('Style','text','String','||','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',14,'Position',[2*colw-10 bestheight-120 20 20]);
uicontrol('Style','text','String',' <--- Notes features --->','ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',12,'Position',[2*colw+80 bestheight-120 colw-30 20]);

% Checkboxes for each feature
for i=1:chpar
    chcol = fix(i/nperc); % # of columns already filled
    chval(i) = uicontrol('Style','checkbox','String',chvars{i+1},'FontSize',round(caseheight/2),'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[10+colw*chcol bestheight-150-caseheight*(i-1-chcol*nperc+(chcol>=1)) colw-10 caseheight],'Callback',{@Click_Callback});
end

for i=1:npar2
    ncol = fix(i/nperc); % # of columns alrady filled
    nval(i) = uicontrol('Style','checkbox','String',nvars{i+1},'FontSize',round(caseheight/2),'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[10+colw*(ncol+2) bestheight-150-caseheight*(i-1-ncol*nperc+(ncol>=1)) colw-10 caseheight],'Callback',{@Click_Callback});
end

% Options
% Warp
warp = uicontrol('Style','checkbox','String','Synchonize performances','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[round(bestwidth/2-50)+100 bestheight-70 250 20], 'Value',1,'Callback',{@Click_Callback});


% SDs and errorbars
ebar = uicontrol('Style','checkbox','String','Display errorbars','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[3*colw+40 bestheight-25 250 20],'Callback',{@Click_Callback});
sdbar = uicontrol('Style','checkbox','String','Display SDc around means','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[3*colw+40 bestheight-50 300 20],'Callback',{@Click_Callback});
mline = uicontrol('Style','checkbox','String','Display means line','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[3*colw+40 bestheight-75 300 20],'Callback',{@Click_Callback});

% Hand selection
piesplit = uicontrol('Style','checkbox','String','Split hands','FontSize',12,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[40 bestheight-40 120 20],'Callback',{@Click_Callback});
pienum = uicontrol('Style','popupmenu','String',{'Piece ?','Piece #1','Piece #2','Piece #3','Piece #4'},'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',10,'Position',[160 bestheight-40 100 20],'Callback',{@Pnum_Callback});
leftonly = uicontrol('Style','checkbox','String','Left only','FontSize',10,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[60 bestheight-65 120 20],'Callback',{@Click_Callback});
rightonly = uicontrol('Style','checkbox','String','Right only','FontSize',10,'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'Position',[60 bestheight-85 120 20],'Callback',{@Click_Callback});

% Super tricky pieces selector
uicontrol('Style','pushbutton','String','Files from different pieces?','FontSize',10,'FontAngle','italic','Position',[160 bestheight-75 180 20],'Callback',{@Fpieces_Callback,chnames,bestheight,h0});

% Go
uicontrol('Style','pushbutton','String','Go','FontSize',16,'Position',[round(bestwidth/2-50) bestheight-80 80 40],'FontWeight','bold','Callback',{@Go_Callback,chmats,chnames,dmeans,chpar,npar2,chvars,nvars,chval,nval,h0,warp,ebar,sdbar,mline,piesplit,pienum,threshand,leftonly,rightonly});

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% CALLBACKS

% --- For features and options checkboxes ---

function Click_Callback(hObject,eventdata)
end

function Pnum_Callback(hObject,eventdata)
flag_pnum = get(hObject,'Value');
switch flag_pnum;
    case 'Piece #1'
    case 'Piece #2'
    case 'Piece #3'
    case 'Piece #4'
end

end

% --- For tricky pieces selector in popup window ---

function Fpieces_Callback(hObject,eventdata,chnames,bestheight,h0)

% Number of mats
fnum = length(chnames);

% Popup figure
hp = figure('Position',[180 bestheight-450 140*fnum 400],'Color',[1,1,1],'Name','Pieces','NumberTitle','off','Menubar','none');

% Menu: Piece selection per mat
for i=1:fnum
    uicontrol('Style','text','String',['File #' num2str(i)],'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',12,'Position',[140*(i-1)+10 370 100 20]);
    uicontrol('Style','text','String',chnames{i},'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',12,'Position',[140*(i-1)+10 350 130 20]);
    piecesnum(i) = uicontrol('Style','popupmenu','String',{'Piece ?','Piece #1','Piece #2','Piece #3','Piece #4'},'ForegroundColor',[0,0,0],'BackgroundColor',[1,1,1],'FontSize',10,'Position',[140*(i-1) 320 100 20],'Callback',{@Pnum_Callback});
end

uicontrol('Style','pushbutton','String','Save','FontSize',14,'FontWeight','bold','Position',[140*fnum/2-40 50 80 40],'Callback',{@Save_Callback,h0,piecesnum});

end


function Save_Callback(hObject,eventdata,h0,piecesnum)

data = guidata(h0);
for i=1:length(piecesnum), data.pieces(i) = get(piecesnum(i),'Value') -1; end
guidata(h0,data);

end



% --- For launching the plotting function ---

function Go_Callback(src,eventdata,chmats,chnames,dmeans,chpar,npar2,chvars,nvars,chval,nval,h0,warp,ebar,sdbar,mline,piesplit,pienum,threshand,leftonly,rightonly)

% Eval features selection
for i=1:length(chval), chsel(i) = get(chval(i),'Value'); end % 1 if feature i is selected (checked box), else 0
for i=1:length(nval), nsel(i) = get(nval(i),'Value'); end

% Eval options selection
warped = get(warp,'Value');
ebarred = get(ebar,'Value');
sdized = get(sdbar,'Value');
mlined = get(mline,'Value');

% Eval pieces selection
splitit = get(piesplit,'Value');
piecemain = get(pienum,'Value') -1; % Minus 1 so as p01 <=> 1, etc.
lefton = get(leftonly,'Value');
righton = get(rightonly,'Value');

if piecemain==0 % No global piece selection, so look at the tricky popup selector
    
    data = guidata(h0); % Call up stored values for pieces selection
    
    if ~(isempty(data)), % Fine selection was accomplished
        for i=1:length(data.pieces), piece(i) = data.pieces(i); end % Affect to piece var.
        
    else piece(1:length(chnames)) = 0; % Zero-sum vector to be used later
    end
    
else % If a global piece is selected, overlook the tricky popup selector
    
    for i=1:length(chnames), piece(i) = piecemain; end % Define all mats' piece as the global one
    
end


%%%%%%
% If to close previously open plots
% fion = get(0,'Children');
% if length(fion)>1, close(find(fion~=1)); end
%%%%%%

if (sum(chsel)+sum(nsel)), % Are any features selected?
    plotem(chmats,chnames,dmeans,chpar,npar2,chvars,nvars,chsel,nsel,h0,warped,ebarred,sdized,mlined,splitit,piece,threshand,lefton,righton);
else
    disp('Please select at least one feature to plot');
end
end


% --- For going back to selection ---

function Select_Callback(hObject,eventdata,h0,chmats,chnames,dmeans,chpar,npar2,chvars,nvars,threshand)
if ishandle(h0), figure(h0);
else selector(chmats,chnames,dmeans,chpar,npar2,chvars,nvars,threshand);
end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% PLOTTING

function plotem(chmats,chnames,dmeans,chpar,npar2,chvars,nvars,chsel,nsel,h0,warped,ebarred,sdized,mlined,splitit,piece,threshand,lefton,righton)

% --- New figure ---

scrsz = get(0,'ScreenSize');
h = figure('Color',[1,1,1],'Name','Display','NumberTitle','off','Position',[10 50 scrsz(3)-20 scrsz(4)-150]);
uicontrol('Style','pushbutton','String','Selection','FontSize',12,'Position',[10 10 120 30],'Callback',{@Select_Callback,h0,chmats,chnames,dmeans,chpar,npar2,chvars,nvars,threshand});

% --- Design ---

chfeats = sum(chsel); % Number of chord features to display, i.e. number of checked boxes
nfeats = sum(nsel); % Number of note features to display, i.e. number of checked boxes

chnum = size(chmats,4); % Number of performances to compare

% Colors
color = [0 0 1;1 0 0;0 1 0;0 1 1;1 0 1]; % 5 basic
if chnum>=length(color), % Add colors to fit all performances
    for i=length(color)+1:chnum+1, color(i,:) = color(i-5,:)./2; end
end


% --- Find features on ---

chon = find(chsel==1);
noton = find(nsel==1);

% chrank=1;
% for i=1:chpar,
%     if chsel(i), chon(chrank)=i; chrank=chrank+1; end
% end
% nrank=1;
% for i=1:npar2,
%     if nsel(i), noton(nrank)=i; nrank=nrank+1; end
% end


% --- First onset sync + Warp ---

onsetini = min(chmats(dmeans+1,1,2,:));

delay = zeros(1,chnum);
if warped, durashun = zeros(1,chnum); end

for i=1:chnum,
    delay(i) = chmats(dmeans+1,1,2,i) - onsetini; % Delay between perf's 1st onset and earliest overall onset
    
    if warped, durashun(i) = max(chmats(dmeans+1:size(chmats,1),1,2,i)) - chmats(dmeans+1,1,2,i); end % Performance i duration from first to last onset
    
end


if warped, % Define the coefficient for having performances match in duration
    warpcoef = zeros(2,chnum); 
    % Perf.i: x.warped(i) = a(i)*x + b(i), w/ a(i) = warpcoef(1,i), and b(i) = warpcoef(2,i)
    
    for i=1:chnum,
        warpcoef(1,i) = mean(durashun)/durashun(i);
        warpcoef(2,i) = chmats(dmeans+1,1,2,i) * (1 - warpcoef(1,i))  - delay(i); % b(i) = x0(i) * (1-a(i)) - delay(i), w/ x0 = t(first onset for perf.i) and delay(i) = delta(x0(i), onsetini) to synchronize all first onsets
    end        
        
else
    warpcoef(1,1:chnum) = 1;
    for i=1:chnum, warpcoef(2,i) = -delay(i); end
end


% --- Maximum offset, to trace the mean's line ---
for i=1:chnum,
    maxo = round(warpcoef(1,i).*max(chmats(dmeans+1:size(chmats,1),1,3,i)) + warpcoef(2,i)); % Maximum (warped) offset, to trace the mean's line
end
maxoff = max(maxo);



hold on;

% --- Actual plot ---

if (~splitit || sum(piece==0)) % All chords on same plot regardless of the hand (GUI default)
    
    if splitit && sum(piece==0), % In case at least one piece specifier was forgotten despite hand-splitting intentions
        disp('In order to separate chords between hands, the piece # is required for all input perfs. Things will go on without hand discrimination.');
    end
    
    for i=1:chfeats, % Loop on selected features
        hi(i) = subplot(chfeats+nfeats,8,8*(i-1)+1:8*(i-1)+7);
        
        for j=1:chnum, % Loop on files
            hold on;
            % Plot each chord's feature value
            plot(hi(i),round(warpcoef(1,j).*chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,1,2,j) + warpcoef(2,j)),chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,1,chon(i),j),'-o','Color',color(j,:));
            
            % Plot feature mean as line
            if mlined, hold on; plot(hi(i),[0 maxoff],[chmats(1,1,chon(i),j) chmats(1,1,chon(i),j)],':','Color',color(j,:)); end
            
            % Plot mean as bar in adjacent subplot
            hm(i) = subplot(chfeats+nfeats,8,8*i);
            hold on;
            fill([j-.4,j+.4,j+.4,j-.4],[0,0,chmats(1,1,chon(i),j),chmats(1,1,chon(i),j)],color(j,:)); % For the mean
            if sdized,
                % Home-made errorbars
                plot([j,j],[chmats(1,1,chon(i),j)-chmats(1,4,chon(i),j),chmats(1,1,chon(i),j)+chmats(1,4,chon(i),j)],'-','Color',color(j+1,:));
                plot([j-.4,j+.4],[chmats(1,1,chon(i),j)-chmats(1,4,chon(i),j),chmats(1,1,chon(i),j)-chmats(1,4,chon(i),j)],'-','Color',color(j+1,:));
                plot([j-.4,j+.4],[chmats(1,1,chon(i),j)+chmats(1,4,chon(i),j),chmats(1,1,chon(i),j)+chmats(1,4,chon(i),j)],'-','Color',color(j+1,:));
                
                % fill([j-.4,j+.4,j+.4,j-.4],[chmats(1,1,chon(i),j)-chmats(1,4,chon(i),j),chmats(1,1,chon(i),j)-chmats(1,4,chon(i),j),chmats(1,1,chon(i),j)+chmats(1,4,chon(i),j),chmats(1,1,chon(i),j)+chmats(1,4,chon(i),j)],color(j,:),'EdgeColor','none','FaceAlpha',.2); % For the SDc
            end
        end
        
        ydims = get(hi(i),'YLim');
        set(hm(i),'YLim',ydims);
        
        ylabel(hi(i),chvars{chon(i)+1},'FontSize',16);
        for j=1:chnum, hold on; hplot(j)=plot(hi(i),0,chmats(1,1,chon(i),j),'-','LineWidth',1.5,'Color',color(j,:)); end
        lel = legend(hi(i),hplot(:),chnames{:},'Location','SouthEast');
        hplot=[];
        
    end
    
    
    for i=1:nfeats, % Loop on selected features
        hi(i+chfeats) = subplot(chfeats+nfeats,8,8*(i+chfeats-1)+1:8*(i+chfeats-1)+7);
        
        for j=1:chnum, % Loop on files
            hold on;
            if ebarred, % Plot values + whiskers for each chord
                subplot(hi(i+chfeats)); % Focus on good subplot
                errorbar(hi(i+chfeats),round(warpcoef(1,j).*chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,1,2,j) + warpcoef(2,j)),chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,2,noton(i),j),chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,3,noton(i),j),'-o','Color',color(j,:));
                
            else % Plot each chord's feature value
                plot(hi(i+chfeats),round(warpcoef(1,j).*chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,1,2,j) + warpcoef(2,j)),chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,2,noton(i),j),'-o','Color',color(j,:));
            end
            
            % Plot feature mean as line
            if mlined, hold on; plot(hi(i+chfeats),[0 maxoff],[chmats(1,2,noton(i),j) chmats(1,2,noton(i),j)],':','Color',color(j,:)); end
            
            % Plot mean as bar in adjacent subplot
            hm(i+chfeats) = subplot(chfeats+nfeats,8,8*(i+chfeats));
            hold on;
            fill([j-.4,j+.4,j+.4,j-.4],[0,0,chmats(1,2,noton(i),j),chmats(1,2,noton(i),j)],color(j,:)); % For the mean
            if sdized,
                % Home-made errorbars
                plot([j,j],[chmats(1,2,noton(i),j)-chmats(1,5,noton(i),j),chmats(1,2,noton(i),j)+chmats(1,5,noton(i),j)],'-','Color',color(j+1,:));
                plot([j-.4,j+.4],[chmats(1,2,noton(i),j)-chmats(1,5,noton(i),j),chmats(1,2,noton(i),j)-chmats(1,5,noton(i),j)],'-','Color',color(j+1,:));
                plot([j-.4,j+.4],[chmats(1,2,noton(i),j)+chmats(1,5,noton(i),j),chmats(1,2,noton(i),j)+chmats(1,5,noton(i),j)],'-','Color',color(j+1,:));
                
                %             fill([j-.4,j+.4,j+.4,j-.4],[chmats(1,2,noton(i),j)-chmats(1,5,noton(i),j),chmats(1,2,noton(i),j)-chmats(1,5,noton(i),j),chmats(1,2,noton(i),j)+chmats(1,5,noton(i),j),chmats(1,2,noton(i),j)+chmats(1,5,noton(i),j)],color(j,:),'EdgeColor','none','FaceAlpha',.2); % For the SDc
            end
            
            hold on;
            if ebarred,
                fill([j-.15 j+.15 j+.15 j-.15],[chmats(1,2,noton(i),j)-chmats(1,3,noton(i),j),chmats(1,2,noton(i),j)-chmats(1,3,noton(i),j),chmats(1,2,noton(i),j)+chmats(1,3,noton(i),j),chmats(1,2,noton(i),j)+chmats(1,3,noton(i),j)],color(j,:));
                
                %             errorbar(j,chmats(1,2,noton(i),j),chmats(1,3,noton(i),j),'Color',color(j,:),'LineWidth',4); % For the mean of SDn
            end
            
        end
        
        ydims = get(hi(i+chfeats),'YLim');
        set(hm(i+chfeats),'YLim',ydims);
        
        ylabel(hi(i+chfeats),nvars{noton(i)+1},'FontSize',16);
        for j=1:chnum, hold on; hplot(j)=plot(hi(i+chfeats),0,chmats(1,2,noton(i),j),'-','LineWidth',1.5,'Color',color(j,:)); end
        legend(hi(chfeats+i),hplot(:),chnames{:},'Location','SouthEast');
        hplot=[];
    end
    
    
else % With chords split by hand
    
    for i=1:length(piece), pthresh(i) = threshand(piece(i)); end % Cutting point between hands for each mat
    
    
    for i=1:chfeats,
        hi(i) = subplot(chfeats+nfeats,8,8*(i-1)+1:8*(i-1)+7);
        
        for j=1:chnum,
            hold on;
            
            % Use tmp vector to store values for feat. chon(i) perf. j, then split by hand, with chon(i) = i.th feat. selected
            
            tmp(:,1) = round(warpcoef(1,j).*chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,1,2,j) + warpcoef(2,j)); % Quite a violent way to just set the chord onset timestamps (minus original onset sync between perfs)
            tmp(:,2) = chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,1,chon(i),j); % Feat. chon(i) perf.j values
            tmp(:,3) = chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,2,1,j); % Mean note-key numbers for each chord => assign to rightful hand
            
            if ~righton, % Left hand
                plot(hi(i),tmp(tmp(:,3)<=pthresh(j),1),tmp(tmp(:,3)<=pthresh(j),2),'-.v','Color',color(j,:));
                if mlined, hold on; plot(hi(i),[0 maxoff],[chmats(2,1,chon(i),j) chmats(2,1,chon(i),j)],'-.','Color',color(j,:)); end % LH Mean line
            end
            if ~lefton, % Right hand
                plot(hi(i),tmp(tmp(:,3)>pthresh(j),1),tmp(tmp(:,3)>pthresh(j),2),':^','Color',color(j,:));
                if mlined, hold on; plot(hi(i),[0 maxoff],[chmats(3,1,chon(i),j) chmats(3,1,chon(i),j)],':','Color',color(j,:)); end % RH Mean line
            end
            
            tmp=[]; % Clear for next perf.
            
            
            % Perf's mean for feat chon(i), in its own eastern subplot
            
            hm(i) = subplot(chfeats+nfeats,8,8*i);
            hold on;
            
            if ~lefton && ~righton, % Both hands to plot
                fill([j-.4,j,j,j-.4],[0,0,chmats(2,1,chon(i),j),chmats(2,1,chon(i),j)],color(j,:)); % Left-hand mean
                fill([j,j+.4,j+.4,j],[0,0,chmats(3,1,chon(i),j),chmats(3,1,chon(i),j)],color(j,:)); % Right-hand mean
                XTag = 'L R';
                
                if sdized,
                    % Home-made errorbars Left
                    plot([j-.2,j-.2],[chmats(2,1,chon(i),j)-chmats(2,4,chon(i),j),chmats(2,1,chon(i),j)+chmats(2,4,chon(i),j)],'-','Color',color(j+1,:));
                    plot([j-.4,j],[chmats(2,1,chon(i),j)-chmats(2,4,chon(i),j),chmats(2,1,chon(i),j)-chmats(2,4,chon(i),j)],'-','Color',color(j+1,:));
                    plot([j-.4,j],[chmats(2,1,chon(i),j)+chmats(2,4,chon(i),j),chmats(2,1,chon(i),j)+chmats(2,4,chon(i),j)],'-','Color',color(j+1,:));
                    
                    % Home-made errorbars Right
                    plot([j+.2,j+.2],[chmats(3,1,chon(i),j)-chmats(3,4,chon(i),j),chmats(3,1,chon(i),j)+chmats(3,4,chon(i),j)],'-','Color',color(j+1,:));
                    plot([j,j+.4],[chmats(3,1,chon(i),j)-chmats(3,4,chon(i),j),chmats(3,1,chon(i),j)-chmats(3,4,chon(i),j)],'-','Color',color(j+1,:));
                    plot([j,j+.4],[chmats(3,1,chon(i),j)+chmats(3,4,chon(i),j),chmats(3,1,chon(i),j)+chmats(3,4,chon(i),j)],'-','Color',color(j+1,:));
                end
                
            else
                if ~righton % Left hand only
                    
                    fill([j-.4,j+.4,j+.4,j-.4],[0,0,chmats(2,1,chon(i),j),chmats(2,1,chon(i),j)],color(j,:)); % Left-hand mean
                    XTag = 'L';
                    
                    if sdized,
                        % Home-made errorbars Left
                        plot([j,j],[chmats(2,1,chon(i),j)-chmats(2,4,chon(i),j),chmats(2,1,chon(i),j)+chmats(2,4,chon(i),j)],'-','Color',color(j+1,:));
                        plot([j-.4,j+.4],[chmats(2,1,chon(i),j)-chmats(2,4,chon(i),j),chmats(2,1,chon(i),j)-chmats(2,4,chon(i),j)],'-','Color',color(j+1,:));
                        plot([j-.4,j+.4],[chmats(2,1,chon(i),j)+chmats(2,4,chon(i),j),chmats(2,1,chon(i),j)+chmats(2,4,chon(i),j)],'-','Color',color(j+1,:));
                    end
                    
                end
                
                
                
                if ~lefton % Right hand only
                    
                    fill([j-.4,j+.4,j+.4,j-.4],[0,0,chmats(3,1,chon(i),j),chmats(3,1,chon(i),j)],color(j,:)); % Right-hand mean
                    XTag = 'R';
                    
                    if sdized,
                        % Home-made errorbars Right
                        plot([j,j],[chmats(3,1,chon(i),j)-chmats(3,4,chon(i),j),chmats(3,1,chon(i),j)+chmats(3,4,chon(i),j)],'-','Color',color(j+1,:));
                        plot([j-.4,j+.4],[chmats(3,1,chon(i),j)-chmats(3,4,chon(i),j),chmats(3,1,chon(i),j)-chmats(3,4,chon(i),j)],'-','Color',color(j+1,:));
                        plot([j-.4,j+.4],[chmats(3,1,chon(i),j)+chmats(3,4,chon(i),j),chmats(3,1,chon(i),j)+chmats(3,4,chon(i),j)],'-','Color',color(j+1,:));
                    end
                end
            end
            
        end
        
        % Fit Y scale of means graphs according to timescape
            
        ydims = get(hi(i),'YLim');
        set(hm(i),'YLim',ydims);
        
        ylabel(hi(i),chvars{chon(i)+1},'FontSize',16);
        for j=1:chnum, hold on; hplot(j)=plot(hi(i),0,chmats(1,1,chon(i),j),'-','LineWidth',1.5,'Color',color(j,:)); end
        legend(hi(i),hplot(:),chnames{:},'Location','SouthEast');
        hplot=[];
        
        set(hm(i),'XTickLabel',XTag); % Indicate L or R mean, or both 
    end
    
    
    for i=1:nfeats,
        hi(i+chfeats) = subplot(chfeats+nfeats,8,8*(i+chfeats-1)+1:8*(i+chfeats-1)+7);
        
        for j=1:chnum,
            hold on;
            
            % Use tmp vector to store values for feat. noton(i) perf. j, then split by hand, with noton(i) = i.th feat. selected
            tmp(:,1) = round(warpcoef(1,j).*chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,1,2,j) + warpcoef(2,j)); % Quite a violent way to just set the chord onset timestamps (minus original onset sync between perfs)
            tmp(:,2) = chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,2,noton(i),j); % Feat. noton(i) perf.j values
            tmp(:,3) = chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,2,1,j); % Mean note-key numbers for each chord => assign to rightful hand
            tmp(:,4) = chmats(dmeans+1:find(chmats(dmeans+1:size(chmats,1),1,2,j)>0,1,'last')+4,3,noton(i),j); % SD
            
            if ~righton, % Left hand
                hold on;
                if ebarred, 
                    subplot(hi(i+chfeats)); 
                    errorbar(tmp(tmp(:,3)<=pthresh(j),1),tmp(tmp(:,3)<=pthresh(j),2),tmp(tmp(:,3)<=pthresh(j),4),'-.v','Color',color(j,:)); 
                    
                else plot(hi(i+chfeats),tmp(tmp(:,3)<=pthresh(j),1),tmp(tmp(:,3)<=pthresh(j),2),'-.v','Color',color(j,:));
                end
                
                if mlined, hold on; plot(hi(i+chfeats),[0 maxoff],[chmats(2,2,noton(i),j) chmats(2,2,noton(i),j)],'-.','Color',color(j,:)); end % LH Mean line
            end
            
            if ~lefton, % Right hand
                hold on;
                if ebarred,
                    subplot(hi(i+chfeats));
                    errorbar(tmp(tmp(:,3)>pthresh(j),1),tmp(tmp(:,3)>pthresh(j),2),tmp(tmp(:,3)>pthresh(j),4),':^','Color',color(j,:));
                    
                else plot(hi(i+chfeats),tmp(tmp(:,3)>pthresh(j),1),tmp(tmp(:,3)>pthresh(j),2),':^','Color',color(j,:));
                end
                
                if mlined, hold on; plot(hi(i+chfeats),[0 maxoff],[chmats(3,2,noton(i),j) chmats(3,2,noton(i),j)],':','Color',color(j,:)); end % RH Mean line
            end
            
            tmp=[]; % Clear for next perf.
            
            
            
            % Perf's mean for feat noton(i), in its own eastern subplot
            
            hm(i+chfeats) = subplot(chfeats+nfeats,8,8*(i+chfeats));
            hold on;
            
            if ~lefton && ~righton, % Both hands to plot
                fill([j-.4,j,j,j-.4],[0,0,chmats(2,2,noton(i),j),chmats(2,2,noton(i),j)],color(j,:)); % Left-hand mean
                fill([j,j+.4,j+.4,j],[0,0,chmats(3,2,noton(i),j),chmats(3,2,noton(i),j)],color(j,:)); % Right-hand mean
                XTag = 'L R';
                
                if sdized,
                    hold on;
                    % Home-made errorbars Left
                    plot([j-.2,j-.2],[chmats(2,2,noton(i),j)-chmats(2,5,noton(i),j),chmats(2,2,noton(i),j)+chmats(2,5,noton(i),j)],'-','Color',color(j+1,:));
                    plot([j-.4,j],[chmats(2,2,noton(i),j)-chmats(2,5,noton(i),j),chmats(2,2,noton(i),j)-chmats(2,5,noton(i),j)],'-','Color',color(j+1,:));
                    plot([j-.4,j],[chmats(2,2,noton(i),j)+chmats(2,5,noton(i),j),chmats(2,2,noton(i),j)+chmats(2,5,noton(i),j)],'-','Color',color(j+1,:));
                    
                    % Home-made errorbars Right
                    plot([j+.2,j+.2],[chmats(3,2,noton(i),j)-chmats(3,5,noton(i),j),chmats(3,2,noton(i),j)+chmats(3,5,noton(i),j)],'-','Color',color(j+1,:));
                    plot([j,j+.4],[chmats(3,2,noton(i),j)-chmats(3,5,noton(i),j),chmats(3,2,noton(i),j)-chmats(3,5,noton(i),j)],'-','Color',color(j+1,:));
                    plot([j,j+.4],[chmats(3,2,noton(i),j)+chmats(3,5,noton(i),j),chmats(3,2,noton(i),j)+chmats(3,5,noton(i),j)],'-','Color',color(j+1,:));
                end
                
                hold on;
                
                if ebarred, % For the mean of SDn
                    hold on;
                    fill([j-.3 j-.1 j-.1 j-.3],[chmats(2,2,noton(i),j)-chmats(2,3,noton(i),j),chmats(2,2,noton(i),j)-chmats(2,3,noton(i),j),chmats(2,2,noton(i),j)+chmats(2,3,noton(i),j),chmats(2,2,noton(i),j)+chmats(2,3,noton(i),j)],color(j,:));
                    fill([j+.1 j+.3 j+.3 j+.1],[chmats(3,2,noton(i),j)-chmats(3,3,noton(i),j),chmats(3,2,noton(i),j)-chmats(3,3,noton(i),j),chmats(3,2,noton(i),j)+chmats(3,3,noton(i),j),chmats(3,2,noton(i),j)+chmats(3,3,noton(i),j)],color(j,:));
                end
                
            else
                if ~righton, % Left hand only
                    
                    fill([j-.4,j+.4,j+.4,j-.4],[0,0,chmats(2,2,noton(i),j),chmats(2,2,noton(i),j)],color(j,:)); % Left-hand mean
                    XTag = 'L';
                    
                    if sdized,
                        hold on;
                        % Home-made errorbars Left
                        plot([j,j],[chmats(2,2,noton(i),j)-chmats(2,5,noton(i),j),chmats(2,2,noton(i),j)+chmats(2,5,noton(i),j)],'-','Color',color(j+1,:));
                        plot([j-.4,j+.4],[chmats(2,2,noton(i),j)-chmats(2,5,noton(i),j),chmats(2,2,noton(i),j)-chmats(2,5,noton(i),j)],'-','Color',color(j+1,:));
                        plot([j-.4,j+.4],[chmats(2,2,noton(i),j)+chmats(2,5,noton(i),j),chmats(2,2,noton(i),j)+chmats(2,5,noton(i),j)],'-','Color',color(j+1,:));
                    end
                    
                    hold on;
                    
                    if ebarred, % For the mean of SDn
                        fill([j-.15 j+.15 j+.15 j-.15],[chmats(2,2,noton(i),j)-chmats(2,3,noton(i),j),chmats(2,2,noton(i),j)-chmats(2,3,noton(i),j),chmats(2,2,noton(i),j)+chmats(2,3,noton(i),j),chmats(2,2,noton(i),j)+chmats(2,3,noton(i),j)],color(j,:));
                    end
                    
                end
                
                if ~lefton, % Right-hand only
                    
                    fill([j-.4,j+.4,j+.4,j-.4],[0,0,chmats(3,2,noton(i),j),chmats(3,2,noton(i),j)],color(j,:)); % Right-hand mean
                    XTag = 'R';
                    
                    if sdized,
                        % Home-made errorbars Right
                        plot([j,j],[chmats(3,2,noton(i),j)-chmats(3,5,noton(i),j),chmats(3,2,noton(i),j)+chmats(3,5,noton(i),j)],'-','Color',color(j+1,:));
                        plot([j-.4,j+.4],[chmats(3,2,noton(i),j)-chmats(3,5,noton(i),j),chmats(3,2,noton(i),j)-chmats(3,5,noton(i),j)],'-','Color',color(j+1,:));
                        plot([j-.4,j+.4],[chmats(3,2,noton(i),j)+chmats(3,5,noton(i),j),chmats(3,2,noton(i),j)+chmats(3,5,noton(i),j)],'-','Color',color(j+1,:));
                    end
                    
                    hold on;
                    
                    if ebarred, % For the mean of SDn
                        fill([j-.15 j+.15 j+.15 j-.15],[chmats(3,2,noton(i),j)-chmats(3,3,noton(i),j),chmats(3,2,noton(i),j)-chmats(3,3,noton(i),j),chmats(3,2,noton(i),j)+chmats(3,3,noton(i),j),chmats(3,2,noton(i),j)+chmats(3,3,noton(i),j)],color(j,:));
                    end
                    
                end
                
            end
            
            
        end
        
        % Fit Y scale of means graphs according to timescape
        
        ydims = get(hi(i+chfeats),'YLim');
        set(hm(i+chfeats),'YLim',ydims);
        
        ylabel(hi(i+chfeats),nvars{noton(i)+1},'FontSize',16);
        for j=1:chnum, hold on; hplot(j)=plot(hi(i+chfeats),0,chmats(1,2,noton(i),j),'-','LineWidth',1.5,'Color',color(j,:)); end
        legend(hi(chfeats+i),hplot(:),chnames{:},'Location','SouthEast');
        hplot=[];
        
        set(hm(i+chfeats),'XTickLabel',XTag); % Indicate L or R mean, or both 
    end
    
    
    
end





% X-Axis
xmin = onsetini; % Min onset time for the first chord of each perf.
xmax = maxoff; % Max offset time for any chord in any perf. (with delay compensation for onset sync.)
set(hi(:),'XLim',[xmin-200 xmax]);
haxes = findobj(hi(:),'type','axes');
linkaxes(haxes,'x');




end






function [chv nv] = varnames()

% --- Define columns' labels: chord features then notes-per-chord means then SD

chv={'#','Number of notes','Chord onset','Chord offset','Chord duration','max Amax', 'max MHV','Melody lead','Melody lead rate','Last-note trail','Last-note trail rate',...
    'Soft pedal depression/chord','Soft pedal duration/chord','Soft pedal full-depression length/chord','Soft pedal mid-depression length/chord','Soft pedal mid-depression level/chord',...
    'Sustain pedal depression/chord','Sustain pedal duration/chord','Sustain pedal full-depression length/chord','Sustain pedal mid-depression length/chord','Sustain pedal mid-depression level/chord',...
    'Soft pedal @onset','Soft pedal bin. @onset','Soft pedal lead @onset','Sustain pedal @onset','Sustain pedal bin. @onset','Sustain pedal lead @onset',...
    'Soft pedal @offset','Soft pedal bin. @offset','Soft pedal trail @offset','Sustain pedal @offset','Sustain pedal bin. @offset','Sustain pedal trail @offset',...
    'IOI','OffOnI','OffOnI direction','IOI same-hand','OffOnI same-hand','OffOnI direction same-hand','IOI other-hand','OffOnI other-hand','OffOnI direction other-hand',...
    'Overlap length','Overlap length rate','Overlap amount','Overlap amount rate','Number of overlaps',...
    'Overlap length same-hand','Overlap length rate same-hand','Overlap amount same-hand','Overlap amount rate same-hand','Number of overlaps same-hand',...
    'Overlap length other-hand','Overlap length rate other-hand','Overlap amount other-hand','Overlap amount rate other-hand','Number of overlaps other-hand'};

% --- Define notes columns' labels: notes features

nv={'#','Key','Note onset','Note offset','Note duration','Amoy','Amax','t.Amax','MHV','t.MHV',...
    'Speed of attack','Slope of attack','Speed of key attack','Slope of key attack','Directness MHV-Amax','MHV/Amax','Sharpness of key attack slope','Sharpness of attack slope',...
    'FKD start','FKD end','FKD duration','FKD rate','Pre-FKD duration','Pre-FKD rate','Post-FKD duration','Post-FKD rate',...
    't.attack.end','t.decay.end','t.release.start','Attack duration','Sustain duration','Release duration','Attack rate','Sustain rate','Release rate',...
    'Soft pedal depression', 'Sustain pedal depression', 'Soft pedal @note.onset','Sustain pedal @note.onset', 'Soft pedal @MHV', 'Sustain pedal @MHV', 'Soft pedal @note.offset', 'Sustain pedal @note.offset',...
    'Soft pedal depression length','Sustain pedal depression length','Key attack mean depression','Key attack mean depression rate',...
    'Note onset lag','Note onset lag rate','Note onset lag amount','Note onset lag amount rate','Note offset lead','Note offset lead rate','Note offset lead amount','Note offset lead amount rate','Sync rate','Sync amount rate'};


end