function keys = pianoroll_plus(numfiles,varargin)

% Plots multiple pianorolls into the same plot
%
% Varargin options:
%   '# # # ...' = select the keys to display, e.g. '42 45 78'
%
%   Lines:
%   'onset' = Vertical lines marking each onset
%   'offset' = Vertical lines marking each offset
%   'noline' = Lines off
%   'hammer' = Display hammer's maximum velocity as vertical red arrow (default)
%   'nohammer' = Hide hammers
%   'pedal' = Shadow over whole Y axis when pedal(s) active
%   'nopedal' = No shadow
%   'chords' = frame the chords; follow by chord matrix name
%

if ~isnumeric(numfiles), 
    if strcmp(numfiles,'cell'), 
        files = varargin{1}; 
        numfiles = size(files,1);
        ivar = 2;
        
    elseif strcmp(numfiles,'text')
        fid = fopen(varargin{1}); 
        c = textscan(fid,'%s','delimiter','\n'); 
        fclose(fid);
        files = c{1};
        numfiles = size(files,1);
        ivar=2;
        
    else disp('Specify first the number of performances that are to be displayed');
    end
    
else
    for n=1:numfiles, files{n} = varargin{n}; end
    ivar=numfiles+1;
end

onsetline=0; offsetline=0; hammer=1; pedals=0; selectkeys=0; chords=0; chmat=cell(numfiles,1);

while ivar<=length(varargin),
    ivarold=ivar; % in case of 'chords', ivar is incremented; refer to ivarold to assess param validity

    if strcmp(varargin(ivar),'onset'), onsetline=1;
    elseif strcmp(varargin(ivar),'offset'), offsetline=1;
    elseif strcmp(varargin(ivar),'noline'), onsetline=0; offsetline=0;

    elseif strcmp(varargin(ivar),'pedal'), pedals=1;
    elseif strcmp(varargin(ivar),'nopedal'), pedals=0;

    elseif strcmp(varargin(ivar),'nohammer'), hammer=0;
    elseif strcmp(varargin(ivar),'hammer'), hammer=1;

        
    elseif strcmp(varargin(ivar),'chords'),
        chords=1; % tag chords for framing
        if strcmp(varargin(ivar+1),'cell')
            chmat = varargin{ivar+2};
            ivar=ivar+3;
        elseif strcmp(varargin(ivar+1),'text')
            fid = fopen(varargin{ivar+2});
            ch = textscan(fid,'%s','delimiter','\n');
            fclose(fid);
            chmat = ch{1};
            ivar=ivar+3;
        else
            for t=ivar+1:ivar+numfiles
                chmat{t}=varargin(ivar); % read mat
            end
            ivar=ivar+numfiles+1; % read next slot for chord matrix
        end
        
    elseif strcmp(varargin(ivar),'nochords'), chords=0;

        
    elseif ~isnumeric(varargin{ivar}) && ~isempty(str2double(varargin{ivar})) % Select keys
        if isnumeric(strread(varargin{ivar}))
            selectkeys=strread(varargin{ivar});
        end

    end

    if isnumeric(varargin{ivarold}) || ~isempty(str2double(varargin{ivarold}))
    else
        switch char(varargin(ivarold))
            case {'prolls','pianorolls','noline','onset','offset','hammer','nohammer','pedal','nopedal','chords','nochords'}
            otherwise
                disp([char(varargin(ivarold)) ' is not a valid parameter.'])
        end
    end
    ivar=ivar+1;
end

% SELECT KEYS

if (isempty(selectkeys) || ~selectkeys(1,1)), % no specified select keys (yet)
    keyval=zeros(111,1);
    
    for n=1:numfiles
        % Files in
        if ischar(files{n}), nmat = dlmread(files{n});
        else nmat=files{n};
        end
        
        for i=1:size(nmat,1)
            if nmat(i,1)<112, keyval(nmat(i,1))=1; end % tag active key/pedal
        end
    end

    srank=1;
    for i=1:length(keyval),
        if keyval(i),
            selectkeys(srank) = i;
            srank=srank+1;
        end
    end
end

% COLORS
colors = {[0 0 1],[1 0 0],[0 1 0],[1 0 1],[0 1 1]};

if numfiles>length(colors), % Add colors to fit all performances
    for i=length(colors)+1:numfiles, colors{i} = colors{i-length(color)}./2; end
end


for i=1:numfiles,
    hold on;
    keys = pianorolling(files{i},onsetline,offsetline, hammer, pedals,selectkeys,chords,chmat{i},colors{i});
    hold on;
end

end



function keys = pianorolling(textname,onsetline,offsetline, hammer, pedals,selectkeys,chords,chmat,color)

% Plots pianoroll display of notetime file/matrix
% OUTPUT: figure (+ vector of all keys displayed)

% --- Initialize Varargins ---

% --- PARAMETERS ---
dmeans=4; % Number of lines of means in chords mat

% --- DATA IN ---
if ischar(textname), nmat = dlmread(textname);
else nmat=textname;
end
[dim1,dim2] = size(nmat);

% --- Gather number of keys in ---

if (isempty(selectkeys) || ~selectkeys(1,1)) % All keys to be displayed
    top = find(nmat(:,1)<128, 1,'last'); % Highest index identifying a key or pedal
else
    i2=1;
    while i2<=length(selectkeys), % Scan the selected keys
        ski=0; % tag for selected key actually played (if match between selectkey(i2) & one nmat key)
        for i=1:dim1, % Scan the keys played
            if nmat(i,1)==selectkeys(i2), ski=1; break; end
        end
        if ~ski, selectkeys(i2)=[]; i2=i2-1; end % TRIM SELECTED KEYS
        i2=i2+1;
    end
    top=length(selectkeys);
end


% --- Earliest key onset ---
delay = min(nmat(1:find(nmat(:,1)<109,1,'last'),3)) -100; % Find the earliest onset (+100ms space)


% --- Total duration / Final time ---

maxdur=0; % counter
for i=1:top,
    jnt = 3;
    while(nmat(i,jnt)>0 && jnt<dim2-1), jnt=jnt+2; end % Up to the highest timestamp on key i
    maxdur = max( maxdur, max(nmat(i,jnt)-delay,nmat(i,jnt-2)-delay) ); % In theory, compare maxdur & nmat(i,jnt-2); nmat(i,jnt)=0 but if the last index (dim2-1)
end



% ------- PIANOROLL PLOTTING ------------------------------------------

for i=1:dim1
    tmpmat=zeros(1,maxdur+2); % Temp vector for key position data

    % --- KEYS & PEDALS ---
    if nmat(i,1)<128

        % Fill in temp vector at each msec
        for j=3:2:dim2-3
            if nmat(i,j+1)~=0 && ~isnan(nmat(i,j+1)) && nmat(i,j)-delay<maxdur+1 && nmat(i,j)-delay>0,
                tmpmat(1,nmat(i,j)-delay)=(nmat(i,j+1))/250; % Key angle value in tmpdata with right timestamp as index
                tmpmat(1,nmat(i,j)+1-delay)=((nmat(i,j+1)+nmat(i,j+3))/2)/250; % Smooth odd msecs
            end

            % Correction of missing timestamp
            if nmat(i,j)==nmat(i,j+2)-4 && nmat(i,j)+2-delay>0,
                tmpmat(1,nmat(i,j)+2-delay)=(nmat(i,j+1))/250;
                tmpmat(1,nmat(i,j)+3-delay)=((nmat(i,j+1)+nmat(i,j+3))/2)/250;
            end

            % End of note
            if nmat(i,j)~=nmat(i,j+2)-2 && nmat(i,j)~=nmat(i,j+2)-4 && nmat(i,j)+1-delay>0 
                tmpmat(1,nmat(i,j)+1-delay)=((nmat(i,j+1))/2)/250; % Smooth odd msecs
            end
        end
        
        % Last timestamp correction
        if nmat(i,dim2)~=0 && ~isnan(nmat(i,dim2)) && nmat(i,dim2-1)<maxdur+1 && nmat(i,dim2-1)-delay>0, % Correction relevant
            if round(dim2/2)==dim2/2 % dim2 is an even number, therefore a key angle
                tmpmat(1,nmat(i,dim2-1)-delay)=(nmat(i,dim2))/250;
            else % dim2 is an odd number, therefore a timestamp (whose key angle is missing); thus, check angle at dim2-1
                tmpmat(1,nmat(i,dim2-2)-delay)=(nmat(i,dim2-1))/250;
            end
        end
        
        
        % --- PLOT ---
        if (isempty(selectkeys) || ~selectkeys(1,1)), plot(i+tmpmat(1,:),'-','Color',color);
        else
            for i2=1:length(selectkeys), if selectkeys(i2)==nmat(i,1), plot(i2+tmpmat(1,:),'-','Color',color); end; end
        end
        hold on


        % --- OPTIONS ---
        if nmat(i,1)<109, % Key => Vertical lines @ onsets/offsets

            if onsetline,
                for j=2:maxdur,
                    if tmpmat(1,j)~=0 && tmpmat(1,j-1)==0, plot([j-1 j-1],[0 top+1],':','LineWidth',0.01,'Color',color); end
                end
            end

            if offsetline,
                for j=2:maxdur, if tmpmat(1,j-1)~=0 && tmpmat(1,j)==0, plot([j j],[0 top+1],':','LineWidth',0.01,'Color',color); end
                end
            end

        elseif pedals, % Pedal => Pedal shades
            for j=2:maxdur,
                if tmpmat(1,j)~=0 && tmpmat(1,j-1)==0, % onset
                    j2=j; while j2<=maxdur && tmpmat(1,j2)~=0, j2=j2+1; end % move till the end of pedal depression
                    fill([j-1,j2,j2,j-1],[0,0,top+1,top+1],'y','EdgeColor','none','FaceAlpha',0.15); % plot yellow shade
                end
            end

        end


        % --- HAMMERS ---
    elseif hammer, % Hammer display not deactivated
        ikey=1;
        while ikey<=dim1 && nmat(ikey,1)~=nmat(i,1)-128, ikey=ikey+1; end
        if ikey<=dim1, % Key corresponding to hammer detected
            jh=3;
            while nmat(i,jh+1)>0 && nmat(i,jh)-delay<maxdur+1 && jh<dim2
                if (isempty(selectkeys) || ~selectkeys(1,1)),
                    plot([nmat(i,jh)-delay,nmat(i,jh)-delay],[ikey,ikey+(nmat(i,jh+1))/250],'-','LineWidth',1,'Color',color); % Plot vertical red line at t.mhv
                    plot([nmat(i,jh)-delay-10,nmat(i,jh)-delay],[ikey+(nmat(i,jh+1))/250-0.05,ikey+(nmat(i,jh+1))/250],'-','Color',color); % Plot arrow wing
                    plot([nmat(i,jh)-delay+10,nmat(i,jh)-delay],[ikey+(nmat(i,jh+1))/250-0.05,ikey+(nmat(i,jh+1))/250],'-','Color',color); % Plot arrow wing

                else
                    for i2=1:length(selectkeys)
                        if selectkeys(i2)==nmat(i,1)-128
                            plot([nmat(i,jh)-delay,nmat(i,jh)-delay],[i2,i2+(nmat(i,jh+1))/250],'-','LineWidth',1,'Color',color);
                            plot([nmat(i,jh)-delay-10,nmat(i,jh)-delay],[i2+(nmat(i,jh+1))/250-0.05,i2+(nmat(i,jh+1))/250],'-','Color',color);
                            plot([nmat(i,jh)-delay+10,nmat(i,jh)-delay],[i2+(nmat(i,jh+1))/250-0.05,i2+(nmat(i,jh+1))/250],'-','Color',color);
                        end
                    end

                end
                jh=jh+2;
            end
            hold on

        end
    end
end


% --- CHORDS FRAMING ---
if chords,
    for i=dmeans+1:size(chmat,1) % each chord

        % Y-axis (keys) chord limits
        chnote=zeros(chmat(i,1,1),1); % vector of chords' notes' indexes in nmat
        chnote(:,1) = chmat(i,4:chmat(i,1,1)+3,1); % Store key for all chord's notes

        if (isempty(selectkeys) || ~selectkeys(1,1)), % All keys
            for i2=1:dim1 % Scan each key from notetime
                for j=1:size(chnote,1) % each note on the chord
                    if chnote(j,1)==nmat(i2,1), chnote(j,2)=i2; end % Store chord 'i' note 'j' index from nmat
                end
            end
        else % Selected set of keys
            for i2=1:length(selectkeys),
                for j=1:size(chnote,1) % each note on the chord
                    if chnote(j,1)==selectkeys(i2), chnote(j,2)=i2; end % Store chord 'i' note 'j' index from selectkeys
                end
            end
        end

        indmin=chnote(chnote(:,1)==min(chnote(:,1)),2);
        indmax=chnote(chnote(:,1)==max(chnote(:,1)),2);

        % Plot
        if indmin(1,1)>0 && indmax(1,1)>0, % All chord's notes are displayed
            plot([chmat(i,1,2)-delay,chmat(i,1,2)-delay],[indmin(1,1)-0.02,indmax(1,1)+0.96],'-', 'Color', color);
            plot([chmat(i,1,3)-delay,chmat(i,1,3)-delay],[indmin(1,1)-0.02,indmax(1,1)+0.96],'-', 'Color', color);
            plot([chmat(i,1,2)-delay,chmat(i,1,3)-delay],[indmin(1,1)-0.02,indmin(1,1)-0.02],'-', 'Color', color);
            plot([chmat(i,1,2)-delay,chmat(i,1,3)-delay],[indmax(1,1)+0.96,indmax(1,1)+0.96],'-', 'Color', color);

        elseif indmin(1,1)>0, % No max note
            % Find the second max/max key indexed
            for n=1:size(chnote,1), if chnote(n,2)>0 && chnote(n,1)>indmax(1,1), indmax(1,1)=chnote(n,1); end; end % Find the max amongst indexed notes
            indmax(1,1)=chnote(chnote(:,1)==indmax(1,1),2); % Take its index

            plot([chmat(i,1,2)-delay,chmat(i,1,2)-delay],[indmin(1,1)-0.02,indmax(1,1)+0.96],'-', 'Color', color);
            plot([chmat(i,1,3)-delay,chmat(i,1,3)-delay],[indmin(1,1)-0.02,indmax(1,1)+0.96],'-', 'Color', color);
            plot([chmat(i,1,2)-delay,chmat(i,1,3)-delay],[indmin(1,1)-0.02,indmin(1,1)-0.02],'-', 'Color', color);
            % Plot up to second max and do not close the frame

            fprintf('Warning: Notes from Chord #%d are missing in the pianoroll display',i);

        elseif indmax(1,1)>0, % No min note
            % Find the second min/min key indexed
            indmin(1,1)=megatop;
            for n=1:size(chnote,1), if chnote(n,2)>0 && chnote(n,1)<indmin(1,1), indmin(1,1)=chnote(n,1); end; end % Find the min amongst indexed notes
            indmin(1,1)=chnote(chnote(:,1)==indmin(1,1),2); % Take its index

            plot([chmat(i,1,2)-delay,chmat(i,1,2)-delay],[indmin(1,1)-0.02,indmax(1,1)+0.96],'-', 'Color', color);
            plot([chmat(i,1,3)-delay,chmat(i,1,3)-delay],[indmin(1,1)-0.02,indmax(1,1)+0.96],'-', 'Color', color);
            plot([chmat(i,1,2)-delay,chmat(i,1,3)-delay],[indmax(1,1)+0.96,indmax(1,1)+0.96],'-', 'Color', color);
            % Plot down to second min and do not close the frame

            fprintf('Warning: Notes from Chord #%d are missing in the pianoroll display',i);

        else % No min no max
            for n=1:size(chnote,1),
                if chnote(n,2)>0,
                    plot([chmat(i,1,2)-delay,chmat(i,1,2)-delay],[chnote(n,2)-0.02,chnote(n,2)+0.96],'-', 'Color', color);
                    plot([chmat(i,1,3)-delay,chmat(i,1,3)-delay],[chnote(n,2)-0.02,chnote(n,2)+0.96],'-', 'Color', color);
                    % Plot chord onset & offset only at present keys' level
                end
            end

            fprintf('Warning: Notes from Chord #%d are missing in the pianoroll display',i);

        end

    end
end



% ------- AXIS ------------------------------------------

% --- Axis labels ---

xlabel('Time in milliseconds','FontSize',18);
ylabel('Key','FontSize',18);
set(gca,'Ygrid','on');


% --- Keys # as Y-axis tick labels ---

keys = 1:top+1;
keys(1) = 0;
if (isempty(selectkeys) || ~selectkeys(1,1)), keys(2:top+1)= nmat(1:top,1); % Vector containg each played key's # (with a zero to boot)
else keys(2:top+1)=selectkeys(:);
end

keychars = num2str(keys(:));
keynumbers = char(keychars);
set(gca,'YTick',0:top);
set(gca,'YTickLabel',keynumbers);

% ------------------------------------------

hold on;

end


