function note = notetime(nameofile, varargin)

% Creates key#-Vs-time data matrix from parsed (by events.m) raw data
% IN: raw file name
% varargins: 
% - 'clean' (default) or 'noclean': to apply (or not) data mistakes correction
% - 'shortname' (default) or 'longname': to shorten the output file's name when the input name follows the
% requisite standardized format (RECXXX MM-DD HHMM AB pYY timbre.zzz)
%
% events.m parser returns events: lines=timestamps, columns= 255, timestamp, couples note/value
%
% OUT: text file and return of notetime matrix

events = extract(nameofile);

status=1;
namecut=1;
for i=1:length(varargin)
    if strcmp(varargin(i),'clean'), status=1;
    elseif strcmp(varargin(i),'noclean'), status=0; 
    end
    if strcmp(varargin(i),'longname'), namecut=0; 
    elseif strcmp(varargin(i),'shortname'), namecut=1; 
    end
end

% --- Parameters ---
threshold=40; 
% minimum value for Amax (maximum key depression) to keep a note in (i.e. clear notetime of the notes 
% that cannot result in a sound event)

% --- Dimensions ---
[dim1,dim2]=size(events); %dim1 = nb of timestamps, dim2 = 2*nb of occurrences per timestamp

% --- Size notetime ---
maxtime=events(dim1,2);
note=zeros(254,maxtime); % matrix (note #, nb of occurrences, timestamp  of occurrence, value)

% --- Creation of notetime ---
for i = 1:254,
    note(i,1) = i;
    note(i,2) = 0;
end

for i = 1:dim1
    for j = 3:2:dim2-1 % note # with 2-step
        if (events(i,j) ~= 0) && ~isnan(events(i,j)),
            numnote = events(i,j);
            note(numnote,2) = note(numnote,2) + 1; %increment nb of occurences
            currentocc = note(numnote,2);
            note(numnote,(1+2*currentocc)) = events(i,2); %timestamp
            note(numnote,(2+2*currentocc)) = events(i,j+1); %value
        end
    end
end


[nt1,nt2] = size(note);

% --- Correction of notetime ---

if status==1 % controls whether correction called for
    for i=1:nt1
        if note(i,1)<123, % not a hammer
            if (note(i,2)~=0) && ~isnan(note(i,2)) % otherwise, line will be deleted later
                
                % ------- Case 1 : Same timestamp twice in a row -------
                
                for j=5:2:nt2-4
                    if note(i,j)==note(i,j+2),
                        if note(i,j+2)+2==note(i,j+4) ||  note(i,j+2)-4==note(i,j-2),
                            note(i,j)=note(i,j-2)+2;
                        elseif note(i,j)+4==note(i,j+4) ||  note(i,j)-2==note(i,j-2),
                            note(i,j+2)=note(i,j)+2;
                        end
                    end
                end
                if note(i,3)==note(i,5),
                    if note(i,5)==note(i,7)+2, note(i,3)=note(i,5);
                    elseif note(i,3)==note(i,7)+4, note(i,5)=note(i,3)+2;
                    end
                end

                if note(i,nt2-3)==note(i,nt2-1),
                    if note(i,nt2-3)==note(i,nt2-5)+2, note(i,nt2-1)=note(i,nt2-3)+2; end
                end

                % ------- Case 2 : singleton 0 -------
                
                for j=5:2:nt2-3
                    if note(i,j+1)==0 && (note(i,j-1)~=0 && note(i,j-2)==note(i,j)-2) && (note(i,j+3)~=0 && note(i,j+2)==note(i,j)+2),
                        note(i,j+1)=note(i,j-1);
                    end
                end

                % ------- Case 3 : doubleton 0 -------

                for j=7:2:nt2-3
                    if note(i,j+1)==0 && (note(i,j-1)==0 && note(i,j)==note(i,j-2)+2)...
                            && (note(i,j-3)~=0 && note(i,j-4)==note(i,j-2)-2) && (note(i,j+3)~=0 && note(i,j+2)==note(i,j)+2),
                        note(i,j-1)=note(i,j-3);
                        note(i,j+1)=note(i,j-1);
                    end
                end

                % ------- Case 4 : missing timestamp -------
                
                for j=3:2:nt2-2
                    if note(i,j)==note(i,j+2)-4,
                        for j2=nt2:-1:j+2, note(i,j2+2)=note(i,j2); end
                        note(i,j+2)=note(i,j)+2;
                        note(i,j+3)=(note(i,j+1)+note(i,j+5))/2;
                    end
                end

                % 
                
                % ------- Case 5 : Keys without any sounding event (only silent key depressions)
                
                amax=0;
                for j=4:2:nt2, amax=max(amax,note(i,j)); end
                if amax<threshold, note(i,2)=0; end
            end
        end
    end
end


% --- TRIM ---
ic=1;
while ic<=size(note,1),
    if (isnan(note(ic,2)) || note(ic,2)==0), % Empty key line to clear
        note(ic,:)=[];
        ic=ic-1; % notetime size reduced by one => change the loop index
    end
    ic=ic+1;
end


% --- WRITE ---

if namecut==0, nameof=nameofile;
else nameof=[nameofile(1:7),nameofile(19:length(nameofile)-4)]; % Shortened output name
end

if status==0, name = ['notetime ',nameof];
else name = ['nt ',nameof];
end

dlmwrite(name, note);

end







