function  reponse = cellstructfind(cell, param , valeur)

reponse = [];

for i = 1 : size(cell,2)
   if(structfind(cell{i},param,valeur))
       reponse = [reponse,i];
   end
end

