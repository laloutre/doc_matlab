function [wcoeff,score,latent,temp,explained] = plotpcamat(data)
% [wcoeff,score,latent,temp,explained] = plotpcamat(data)
%
%prends un dataset en entr�e 
%et trace le r�sultat de la pca.

ratings = vachercher(data,'double');
z = zscore(ratings);
[wcoeff,score,latent,temp,explained] = pca(z,'VariableWeights','variance');
hold on;

nb_pieces       = length(unique(data.piece));
nb_pianistes    = length(unique(data.pianiste));
nb_nuances      = length(unique(data.nuance));

couleurs    = hsv(nb_nuances);

patched     =   zeros(length(data),1);



for i =1 :length(score)% avant j'avais mis ca : for i =1 :length(ratings) ??? WHY?
    x = [];
    y = [];
    z = [];
    X = score(i,1);
    Y = score(i,2);
    %Z = score(i,3);
    
    %C'est ici qu'on affiche les points
    S = 10;
    scatter(X,Y,S,couleurs(double(data.nuance(i)),:),'fill');
    %et ici leur l�gendes (numero de pianiste, de piece et couleur pour le
    %timbre.
    text(X+0.01, Y+0.01, strcat(num2str(data.rec(i)),char(data.pianiste(i)),char(data.piece(i))),'BackgroundColor',...
       couleurs(double(data.nuance(i)),:) ,'FontSize',10  );
    %ici on relie les points entre eux si ils ne le sont pas d�j�...
%     if(~patched(i))
%     indices = data(data.pianiste == data.pianiste(i)  & data.nuance == data.nuance(i),:).Properties.ObsNames;
%         for j = 1:length(indices)
%         x = [x,score(str2double(indices(j)),1)];
%         y = [y,score(str2double(indices(j)),2)];
%         z = [z,score(str2double(indices(j)),3)];
%         patched(str2double(indices(j)))=1;
%         end
    %...et l� on modifie la couleur de ces traits en fonction du pianiste
    %plot(x,y,'LineWidth',1,'Color',couleurs(double(data.pianiste(i))+nb_nuances,:));
    %(on rajoute nb_nuance car le vecteur de couleurs est en fait d'une
    %longueur nb_nuances+nb_pianistes+1)
   % end
end
legend('Brillant','sombre','rond','sec','velout�')
%legend('Sec','Rond','Brillant','Sombre','Velout�')
disp('ok');