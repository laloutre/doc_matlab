function out = Snmat2midi(in)

% filePattern = fullfile(pwd, '*.boe');
% boefiles = dir(filePattern);
% for ii=1:length(boefiles)
% boefiles(ii).name

out = [];
ind =1;
%  in = boe2nmat(boefiles(ii).name);
% in = boe2nmat(in);
 in = double(in);

for i=1:length(in)

out(i,1) = 1;
out(i,2) = 1;
out(i,3) = in(i,2)-145;%pitch
out(i,4) = in(i,3);%velo
out(i,5) = in(i,1)/1000; %onset
out(i,6) = (in(i,1)+500)/1000; %offset

% temp = in(i:end,:);
% 
%  temp2 = find(temp(:,5) == out(ind,3));
%  out(ind,6) = (in(temp2(2)+(i-1),2)-out(ind,5))/1000;
    
end

    if(~isempty(out))
    out = matrix2midi(out);
    writemidi2(out,strrep(' test2.boe','.boe','.mid'));
    end
% end
end