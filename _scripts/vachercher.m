
function fichiers = vachercher(repertoire,type)
    %
    %A = vachercher( REPERTOIRE , TYPE ) 
    %cherche récursivement les fichiers du type TYPE dans les repertoires et sous
    %repertoires contenus dans REPERTOIRE et les ramène dans un tableau
    %de structures de la forme :
    %A.nom
    %A.chemin
    %
    %A = vachercher( DATASET , TYPE ) 
    %cherche récursivement les données du type TYPE dans un dataset.

    filesep = '/';
    if(isa(repertoire,'dataset'))
    fichiers = recherche_dataset(repertoire,type);

    elseif(isdir(repertoire))
    fichiers = '';
    fichiers = recherche_recursive(repertoire,fichiers,type);
    fichiers = fichiers;
    end
    
function fichiers =recherche_recursive(repertoire,fichiers,type)
filesep = '/';
mute=1;    
contenu = dir(strcat(repertoire));
%pour tout ce qui est dans le repertoire en argument
for i=1:length(contenu)
   %si c'est un repertoire on relance la fonction sur lui mm
    if(contenu(i).isdir &&...
            strcmp(contenu(i).name,'.')~=1 &&...
            strcmp(contenu(i).name,'..')~=1)
        if(mute==0)disp(strcat(repertoire,filesep,contenu(i).name));end
        fichiers = recherche_recursive(strcat(repertoire,filesep,contenu(i).name)...
            ,fichiers,type);
    

    end
    
end
    %sinon on affiche le contenu
        waves = dir(strcat(repertoire,filesep,'*.',type));
        for j=1:length(waves)
        if(mute ==0)disp(waves(j).name);end
        indice = length(fichiers)+1;
        fichiers(indice).nom = waves(j).name;
        fichiers(indice).chemin = strcat(repertoire,filesep,waves(j).name);
        end
        
function fichiers =recherche_dataset(dataset,type)

filesep = '/';
temp = get(dataset,'VarNames');
fichiers = [];
for i = 1 : size(dataset,2)
    if( isa( dataset{1,i} , type ) )
        tempcast =cast(dataset(:,i),type);
        fichiers = [fichiers,tempcast]; 
    end 
end

