function fig = Sfft(input,TEMPS_S)

Fs = length(input)/TEMPS_S;

NFFT = length(input);
Y = fft(input,NFFT);
f = linspace(0,Fs/2,NFFT/2);
Y_plot = abs(Y(1:floor(NFFT/2)));

figure;  
set(gcf,'color','w');
plot(f, 2*Y_plot,'LineWidth',4);

xlabel('Fr�quence [Hz]','FontSize',40);
ylabel('Amplitude','FontSize',40);

set(gca,'FontSize',14);

xlim([100 500]);
ylim([100 max(2*Y_plot)+100000000]);


grid;