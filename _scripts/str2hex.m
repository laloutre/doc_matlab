function hex = str2hex(chaine)

temp = '';
hex = dec2hex(chaine);

for i=1 : size(hex)
   temp = [temp,hex(i,1:2),' ']; 
end
hex = temp;
end