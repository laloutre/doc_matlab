function [dec,out]= hexget(file,t_chaine,offset,nb_octets,pos)

        dec = file(pos+t_chaine +offset :...
            pos+t_chaine +offset +nb_octets-1);

    dec = dec2hex(dec);
    dec = reshape(dec',1,size(dec,2)*size(dec,1));
    dec = hex2num(dec);
    dec = swapbytes(dec);
end