function [coefs,scores,variances,t2] = pcaXD(matin,varargin)

%% PCA
% Plots all relevant (accounting for more than 5% of total variance) principal components' dimensions in each 2D plot combination
%
% If standard input (standar=1, default), results, pianist, timbres, pieces and REC#s are automatically retrieved
% Else: individual specification required:
%    Matin would be the input results
%    + In varargins: special 'data' (4 columns: REC#, piece#, timbre, pianist ID)
%
% Optional parameters in varargin:
%       - Input format: standard, with factor variables contained in matin, or not
%       - Factors:
%               - 1: 1st color-identified factor (default: timbre, col.3)
%               - 2: 2nd point-type-identified factor (default: piece, col.2)
%               - 3: Cross-type-identified factor (default: pianist, col.4)
%               - 4: labels-identified factor (default: none; else REC#, col.1)
%       - Name or outname: of figures output
%       - Ellipse: represent each timbre mean by a cross enellipsed over its SE (between same-timbre performances)
%       - link: lines to join same-factors performances
%       - Timbre labels: specify non-default series of timbre descriptors
%       - Color: define a non-standard set of colors (in cell)
%       - 3D: so that three significant dimensions will be plotted in 3D insted of 3 2D projections
%
%
%%%%%%%%%%%%%%%%%%

%% PARAMETERS AND INITIALIZATION

% Default optional parameters value
%
outname=''; % No output save name specified
ell=1; % Ellipse for field of each timbre
link=0; % For linking same-factors samples
%labelsin = 0; % Display 3rd-factor labels next to points
posline = 1; % Where to center the 1D vertical line
standar = 1; % Standard input format
D3 = 1; % Plot 3 sig. dims in one 3D plot

[desc factor ntim reorder variables timnames color catfields catlabels fname grname factwo sigthreshold sigassign facthree] = stariables();
factor1 = facthree(1); % Default: timbre
factor2 = facthree(2); % Default: piece
factor3 = facthree(3); % Default: pianist
factor4 = 0; % Default: none

sigthresh = 0.1; % Percentage of variance accounted for by one dimension in order to deem it relevant

% Plot parameters
msize=12; % MarkerSize
figpos = [0 0 1280 800]; % Figure position and size

% Varargins
%
arg=1;
while arg<=length(varargin),
    if strcmp(varargin(arg), 'format'),
        if arg<length(varargin),
            if isnumeric(varargin{arg+1}),
                standar = varargin{arg+1};
            elseif ischar(varargin{arg+1}),
                if strcmp(varargin{arg+1},'standard'), standar=1;
                elseif strcmp(varargin{arg+1},'nostandard') || strcmp(varargin{arg+1},'nonstandard'), standar=0;
                end
            end
        end
        arg=arg+1;
        
    elseif strcmp(varargin(arg),'factors'),
        if arg<length(varargin),
            if isnumeric(varargin{arg+1}),
                factor1=varargin{arg+1}(1);
                if length(varargin{arg+1})>1, factor2=varargin{arg+1}(2); else factor2=0; factor3=0; factor4=0; end
                if length(varargin{arg+1})>2, factor3=varargin{arg+1}(3); else factor3=0; factor4=0; end
                if length(varargin{arg+1})>3, factor4=varargin{arg+1}(4); else factor4=0; end
            elseif ischar(varargin{arg+1}),
                if strcmp(varargin{arg+1}(1),'timbre'), factor1=3;
                elseif strcmp(varargin{arg+1}(1),'pianist'), factor1=4;
                elseif strcmp(varargin{arg+1}(1),'piece'), factor1=2;
                end
                if length(varargin{arg+1})>1,
                    if strcmp(varargin{arg+1}(2),'timbre'), factor2=3;
                    elseif strcmp(varargin{arg+1}(2),'pianist'), factor2=4;
                    elseif strcmp(varargin{arg+1}(2),'piece'), factor2=2;
                    end
                else factor2=0; factor3=0; factor4=0;
                end
                if length(varargin{arg+1})>2,
                    if strcmp(varargin{arg+1}(3),'timbre'), factor3=3;
                    elseif strcmp(varargin{arg+1}(3),'pianist'), factor3=4;
                    elseif strcmp(varargin{arg+1}(3),'piece'), factor3=2;
                    elseif strcmp(varargin{arg+1}(3),'rec'), factor3=1;
                    end
                else factor3=0; factor4=0;
                end
                if length(varargin{arg+1})>3,
                    if strcmp(varargin{arg+1}(4),'timbre'), factor4=3;
                    elseif strcmp(varargin{arg+1}(4),'pianist'), factor4=4;
                    elseif strcmp(varargin{arg+1}(4),'piece'), factor4=2;
                    elseif strcmp(varargin{arg+1}(4),'rec'), factor4=1;
                    end
                end
            else factor4=0;
            end
        end
        arg=arg+1;
        
    elseif strcmp(varargin(arg),'color'),
        if arg<length(varargin),
            if isnumeric(varargin{arg+1}), color = varargin{arg+1};
            else disp('Specified colors to use must be contained in a matrix');
            end
        end
        arg=arg+1;
        
    elseif strcmp(varargin(arg),'timbrelabels') || strcmp(varargin(arg),'timbre labels'),
        if arg<length(varargin),
            if iscell(varargin{arg+1}), timnames = varargin{arg+1};
            else disp('Specified timbre labels must be contained in a cell array');
            end
        end
        arg=arg+1;
        
    elseif strcmp(varargin(arg),'Name') || strcmp(varargin(arg),'name') || strcmp(varargin(arg),'outname'),
        if arg<length(varargin), outname = varargin{arg+1}; end
        arg=arg+1;
        
    elseif strcmp(varargin(arg),'Ellipse') || strcmp(varargin(arg),'ellipse'),
        if arg<length(varargin),
            if isnumeric(varargin{arg+1}), ell=(varargin{arg+1}>0);
            elseif ischar(varargin{arg+1}),
                if strcmp(varargin{arg+1},'yes') || strcmp(varargin{arg+1},'on'), ell=1;
                elseif strcmp(varargin{arg+1},'no') || strcmp(varargin{arg+1},'off'), ell=0;
                end
            end
        end
        arg=arg+1;
        
    elseif strcmp(varargin(arg),'Link') || strcmp(varargin(arg),'link'),
        if arg<length(varargin),
            if isnumeric(varargin{arg+1}), link=varargin{arg+1};
            end
        end
        arg=arg+1;
        
    elseif strcmp(varargin(arg),'posline'),
        if arg<length(varargin),
            posline = varargin{arg+1};
        end
        arg=arg+1;
        
    elseif strcmp(varargin(arg),'data'),
        if arg<length(varargin),
            if isnumeric(varargin{arg+1}) && size(varargin{arg+1},1)==size(resin,1) && size(varargin{arg+1},2)==4,
            else fprintf('Unvalid data specifier (must be a %d by 4 matrix)',size(resin,1));
            end
        end
        arg=arg+1;
        
    elseif strcmp(varargin(arg),'3D'),
        if arg<length(varargin),
            if isnumeric(varargin{arg+1}), D3 = varargin{arg+1};
            elseif ischar(varargin{arg+1})
                if strcmp(varargin{arg+1},'yes') || strcmp(varargin{arg+1},'y'), D3=1;
                elseif strcmp(varargin{arg+1},'no') || strcmp(varargin{arg+1},'n'), D3=0;
                end
            end
        end
        
    end
    arg=arg+1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% INPUT


% Input standardism
if standar,
    resin = matin(:,desc+1:size(matin,2));
    data = matin(:,1:4); % REC#, Piece#, timbre, pianist ID#
else
    resin = matin;
    data=[];
end

% Clear NaNs
%
for i=1:size(resin,1),
    for j=1:size(resin,2),
        if ~isfinite(resin(i,j)), resin(i,j)=mean(resin(isfinite(resin(:,j)),j)); end
    end
end

hold off;



% FACTORS
%
% Warnings
%
if isempty(data), disp('Factors data is not identified; you fail.');
else
    if factor1, tim = data(:,factor1); % First, color-id'ed factor, default = timbre
    else tim=[];
    end
    if factor2, pieces = data(:,factor2); % Second, marker-id'ed factor, default = piece
    else pieces=[];
    end
    if factor3, pianists = data(:,factor3); % Third, cross-id'ed factor, default = pianist
    else pianists=[];
    end
    if factor4, rec = data(:,factor4); % Fourth, label-id'ed factor, default = none
        reclabels=cell(length(rec),1);
        if factor4==1, for v=1:length(rec), reclabels{v,1}=num2str(rec(v)); end % Store REC#
        else for v=1:length(rec), reclabels{v,1}=grname{factor4-1,rec(v)}; end % Store factor identifier (pianist initials, timbre label, piece name)
        end
    else rec=[];
    end
    
    
    if factor1 && (isempty(tim) || sum(tim==0)==length(tim)), disp('Warning: first factor will not be identified'); factor1=0; end
    if factor1 && (isempty(tim) || sum(tim==0)==length(tim)) && ell, disp('Warning: display of mean first-factor fields cannot be displayed'); factor1=0; ell=0; end
    if factor2 && (isempty(pieces) || sum(pieces==0)==length(pieces)), disp('Warning: second factor will not be identified'); factor2=0; end
    if factor3 && (isempty(pianists) || sum(pianists==0)==length(pianists)), disp('Warning: third factor will not be identified'); factor3=0; end
    if factor4 && (isempty(rec) || sum(rec==0)==length(rec)), disp('Warning: fourth factor will not be identified'); end
    
    
    
    % Affect
    %
    
    if factor1,
        if max(tim)>length(color), % Add colors to fit all timbres
            for i=length(color)+1:max(tim), color(i,:) = color(i-5,:)./2; end
        end
        for i=1:length(tim)
            colt(i,:)=color(tim(i),:); 
            %colt(i)=color(tim(i)); 
        end
        
        timrank=1;
        for i=min(tim):max(tim),
            if sum(tim(:)==i),
                timon(timrank)=i;
                timrank=timrank+1;
            end
        end
        numtim = length(timon);
        for i=1:numtim, names{i} = [fname{factor1-1} ': ' grname{factor1-1,timon(i)}]; end
        
    else numtim=0;
    end
    
    
    if factor2,
        pio={'o';'s';'d';'v';'p';'h';'>';'<';'^';'*'};
        if max(pieces)>length(pio),
            for i=length(pio)+1:max(pieces), pio{i} = pio{i-length(pio)}; end
        end
        for i=1:length(pieces), pi(i)=pio(pieces(i)); end
        
        pirank=1;
        for i=min(pieces):max(pieces),
            if sum(pieces(:)==i),
                pieon(pirank)=i;
                pirank=pirank+1;
            end
        end
        numpie = length(pieon);
        for i=1:numpie, names{numtim+i} = [fname{factor2-1} ': ' grname{factor2-1,pieon(i)}]; end
        
    else numpie=0;
    end
    
    
    if factor3,
        piao={'x','+','*','^','h','>','<','p','v','d','s','o'}; % Cross type
        piasize = [msize, msize-2, (msize-4)*ones(1,10)];
        if max(pianists)>length(piao),
            for i=length(piao)+1:max(pianists), 
                piao{i} = piao{i-length(piao)}; 
                piasize(i) = piasize(i-length(piao));
            end
        end
        for i=1:length(pianists), piani(i)=piao(pianists(i)); end
        
        piarank=1;
        for i=min(pianists):max(pianists),
            if sum(pianists(:)==i),
                pianon(piarank)=i;
                piarank=piarank+1;
            end
        end
        numpian = length(pianon);
        for i=1:numpian, names{length(timon)+length(pieon)+i} = [fname{factor3-1} ': ' grname{factor3-1,pianon(i)}]; end
        
        % Crosses colour
        for i=1:length(color),
            if sum(color(i,:)==0)==2, % Primary colour
                piacol(i,:) = mod(color(i,:)+1,2); % Complementary colour
            else piacol(i,:) = [0 0 0];
            end
        end
        
    else numpian=0;
    end
    
    
    if factor4,
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% PCA
    %
    [coefs,scores,variances,t2] = princomp(resin,'econ');
    %[coefs,scores,variances,t2] = pca(resin);
    
    
    % Dimensions to plot
    %
    Xdim = find(variances(:)./sum(variances)>sigthresh,1,'last');
    
    
    % Ellipse means
    %
    if ell && factor1,
        
        scoremeans=zeros(numtim,Xdim); % Means per timbre for all relevant dims
        scorerrs=zeros(numtim,Xdim); % SEs per timbre for all relevant dims
        
        for t=1:numtim,
            sctmp=zeros(1,Xdim);
            rank=1;
            for i=1:size(resin,1),
                if tim(i)==timon(t),
                    sctmp(rank,1:Xdim)=scores(i,1:Xdim); % relevant PCA dims for one timbre-colored performances
                    rank=rank+1;
                end
            end
            for e=1:Xdim,
                scoremeans(t,e)=mean(sctmp(:,e));
                scorerrs(t,e)=std(sctmp(:,e))./sqrt(size(sctmp,1));
            end
        end
    end
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% --- PLOT ---
    %
    % Each performance
    % First-factor-specific colors
    % Second-factor-specific markers
    % Third-factor-specific crosses
    % fourth-factor-specific labels
    
    
    
    % Case of only 1 dim. sig.
    if Xdim ==1,
        
        % Factors 1 and 2: markers
        if factor2 && factor1,
            for i=1:size(resin,1), 
                plot(posline,scores(i,1),pi{i},'MarkerFaceColor',colt(i,:),'MarkerEdgeColor',colt(i,:),'MarkerSize',msize); 
                hold on; 
            end
            
        elseif factor1, 
            for i=1:size(resin,1), 
                plot(posline,scores(i,1),'o','MarkerFaceColor',colt(i,:),'MarkerEdgeColor',colt(i,:),'MarkerSize',msize); 
                hold on; 
            end
            
        elseif factor2,
            for i=1:size(resin,1), plot(posline,scores(i,1),pi{i},'MarkerSize',msize); 
                hold on; 
            end
            
%         else
%             for i=1:size(resin,1), 
%                 plot(posline,scores(i,1),'o','MarkerSize',msize); 
%                 hold on; 
%             end
        end
        
        % Factor 3: crosses
        if factor3,
            if factor1,
                for i=1:size(resin,1),
                    plot(scores(i,x),scores(i,y),piani{i},'MarkerEdgeColor',piacol(tim(i),:),'MarkerSize',piasize(pianists(i)));
                    hold on;
                end
            else
                for i=1:size(resin,1),
                    plot(scores(i,x),scores(i,y),piani{i},'MarkerEdgeColor','k','MarkerSize',piasize(pianists(i)));
                    hold on;
                end
            end
        end
        
        
        % Factor 1: mean ellipse
        if ell && factor1,
            for i=1:numtim,
                plot(posline,scoremeans(i,1),'x','MarkerSize',msize+6,'Color',color(timon(i),:));
                hold on;
                ellipse(.2,scorerrs(i,1),0,posline,scoremeans(i,1),color(timon(i),:));
                hold on;
            end
        end
        
        
        % Factor 4: labels
        if factor4,
            for i=1:size(resin,1), side(i,1) = posline-.01+.05*(-1)^i; end
            orderer = scores(:,1);
            orderer(:,2) = 1:size(orderer,1);
            orderer=sortrows(orderer,1);
            side(:,2) = orderer(:,2);
            side=sortrows(side,2);
            side(:,2) = [];
            text(side,scores(:,1),reclabels,'FontSize',msize-2);
        end
        
        
        % Legend
        for i=1:numtim, 
            hold on; 
            hplot(i)=plot(0,0,'Visible','off','Color',color(timon(i),:),'LineWidth',2); 
        end
        
        for i=1:numpie, 
            hplot(numtim+i)=plot(0,0,pio{pieon(i)},'Visible','off','Color','k','MarkerFaceColor','k'); 
        end
        
        for i=1:numpian,
            hplot(numtim+numpie+i)=plot(0,0,piao{pianon(i)},'Visible','off','Color','k');
        end
        
        if ~isempty(names), 
            hleg = legend(hplot(:),names,'Location','NorthWest'); 
            set(hleg,'FontSize',msize+10);
        end
        
        % Add points to keep all markers inside the figure
        plot(0,min(scores(:,1))-.1*max(abs(scores(:,1))),'.w','MarkerSize',2);
        plot(0,max(scores(:,1))+.1*max(abs(scores(:,1))),'.w','MarkerSize',2);
        
        ylabel(['Dimension 1 explained variance = ' num2str(variances(1)/sum(variances))],'FontSize',msize+12);
        set(gca,'XLim',[0 2]);
        set(gcf,'Position',figpos);
        
        hgsave(['PCA_' outname '_1D.fig']);
        hold off;
        
        
        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % 3D plot
    elseif Xdim==3 && D3,
        
        zmin = min(scores(:,3))-.1*max(abs(scores(:,3))); % Starting point for vertical indicator lines
        
        % Same-factors performances vectors
        if sum(link),
            if length(link)>2,
                if length(link)>3, disp('Warning: only the first three factors are used for linking same-factors samples.'); end
                for i1=1:max(data(:,link(1))),
                    for i2=1:max(data(:,link(2))),
                        for i3=1:max(data(:,link(3))),
                            testsame = (data(:,link(1))==i1 & data(:,link(2))==i2 & data(:,link(3))==i3);
                            if sum(testsame)>1, % More than one sample with matching factors
                                if factor1,
                                    plot3([scores(testsame,1);scores(find(testsame==1,1,'first'),1)],[scores(testsame,2);scores(find(testsame==1,1,'first'),2)],[scores(testsame,3);scores(find(testsame==1,1,'first'),3)],'-','Color',colt(find(testsame==1,1,'first'),:));
                                    hold on;
                                else
                                    plot3([scores(testsame,1);scores(find(testsame==1,1,'first'),1)],[scores(testsame,2);scores(find(testsame==1,1,'first'),2)],[scores(testsame,3);scores(find(testsame==1,1,'first'),3)],'-');
                                    hold on;
                                end
                            end
                        end
                    end
                end
                
            elseif length(link)==2,
                for i1=1:max(data(:,link(1))),
                    for i2=1:max(data(:,link(2))),
                        testsame = (data(:,link(1))==i1 & data(:,link(2))==i2);
                        if sum(testsame)>1, % More than one sample with matching factors
                            if factor1,
                                plot3([scores(testsame,1);scores(find(testsame==1,1,'first'),1)],[scores(testsame,2);scores(find(testsame==1,1,'first'),2)],[scores(testsame,3);scores(find(testsame==1,1,'first'),3)],'-','Color',colt(find(testsame==1,1,'first'),:));
                                hold on;
                            else
                                plot3([scores(testsame,1);scores(find(testsame==1,1,'first'),1)],[scores(testsame,2);scores(find(testsame==1,1,'first'),2)],[scores(testsame,3);scores(find(testsame==1,1,'first'),3)],'-');
                                hold on;
                            end
                        end
                    end
                end
                
            else disp('Warning: incorrect input for linking same-factors samples; linking command discarded.');
            end
        end
        
        % Factor1: mean ellipse
        if ell && factor1,
            for i=1:numtim,
                plot3(scoremeans(i,1),scoremeans(i,2),scoremeans(i,3),'x','MarkerSize',msize+6,'Color',color(timon(i),:));
                hold on;
                plot3([scoremeans(i,1) scoremeans(i,1)],[scoremeans(i,2) scoremeans(i,2)],[zmin scoremeans(i,3)],'--','Color',color(timon(i),:));
                hold on;
                ellipse3(scorerrs(i,1),scorerrs(i,2),0,scoremeans(i,1),scoremeans(i,2),scoremeans(i,3),3,color(timon(i),:));
                hold on;
                ellipse3(scorerrs(i,1),scorerrs(i,3),0,scoremeans(i,1),scoremeans(i,3),scoremeans(i,2),2,color(timon(i),:));
                hold on;
                ellipse3(scorerrs(i,2),scorerrs(i,3),0,scoremeans(i,2),scoremeans(i,3),scoremeans(i,1),1,color(timon(i),:));
                hold on;
            end
        end
        
        
        % Factors 1 and 2: markers
        if factor2 && factor1,
            for i=1:size(resin,1), 
                plot3(scores(i,1),scores(i,2),scores(i,3),pi{i},'MarkerFaceColor',colt(i,:),'MarkerEdgeColor',colt(i,:),'MarkerSize',msize-2); hold on; 
                plot3([scores(i,1) scores(i,1)],[scores(i,2) scores(i,2)],[zmin scores(i,3)],':','Color',colt(i,:)); hold on;
            end
            
        elseif factor1,
            for i=1:size(resin,1), 
                plot3(scores(i,1),scores(i,2),scores(i,3),'o','MarkerFaceColor',colt(i,:),'MarkerEdgeColor',colt(i,:),'MarkerSize',msize-2); hold on; 
                plot3([scores(i,1) scores(i,1)],[scores(i,2) scores(i,2)],[zmin scores(i,3)],':','Color',colt(i,:)); hold on;
            end
        elseif factor2,
            for i=1:size(resin,1), 
                plot3(scores(i,1),scores(i,2),scores(i,3),pi{i},'MarkerSize',msize-2); hold on;
                plot3([scores(i,1) scores(i,1)],[scores(i,2) scores(i,2)],[zmin scores(i,3)],':','Color',colt(i,:)); hold on;
            end
            
%         else for i=1:size(resin,1), 
%                 plot3(scores(i,1),scores(i,2),scores(i,3),'o','MarkerSize',msize-2); hold on; 
%                 plot3([scores(i,1) scores(i,1)],[scores(i,2) scores(i,2)],[zmin scores(i,3)],':','Color',colt(i,:)); hold on;
%             end
        end
        
        
        % Factor 3: crosses
        if factor3,
            if factor1,
                for i=1:size(resin,1),
                    plot3(scores(i,1),scores(i,2),scores(i,3),piani{i},'MarkerEdgeColor',piacol(tim(i),:),'MarkerSize',piasize(pianists(i))-2);
                    hold on;
                end
            else
                for i=1:size(resin,1),
                    plot3(scores(i,1),scores(i,2),scores(i,3),piani{i},'MarkerEdgeColor','k','MarkerSize',piasize(pianists(i))-2);
                    hold on;
                end
            end
        end
        
        
        % Factor 4: labels
        if factor4,
            text(scores(:,1)+.01*max(abs(scores(:,1))),scores(:,2)+.01*max(abs(scores(:,2))),scores(:,3),reclabels,'FontSize',msize-2);
            hold on;
        end
        
        
        % Legend
        for i=1:numtim, 
            hold on; 
            hplot(i)=plot3(0,0,0,'Visible','off','Color',color(timon(i),:),'LineWidth',2); 
        end
        
        for i=1:numpie, 
            hplot(numtim+i)=plot3(0,0,0,pio{pieon(i)},'Visible','off','Color','k','MarkerFaceColor','k'); 
        end
        
        for i=1:numpian, 
            hplot(numtim+numpie+i)=plot3(0,0,0,piao{pianon(i)},'Visible','off','Color','k'); 
        end
        
        
        if ~isempty(names), 
            hleg = legend(hplot(:),names,'Location','NorthWest'); 
            set(hleg,'FontSize',msize+10);
        end
        
        % Add points to keep all markers inside the figure
        plot3(min(scores(:,1))-.1*max(abs(scores(:,1))),min(scores(:,2))-.1*max(abs(scores(:,2))),0,'.w','MarkerSize',2);
        plot3(max(scores(:,1))+.1*max(abs(scores(:,1))),max(scores(:,2))+.1*max(abs(scores(:,2))),0,'.w','MarkerSize',2);
        plot3(0,0,max(scores(:,3))+.1*max(abs(scores(:,3))),'.w','MarkerSize',2);
        
        
        xlabel(['Dimension ' num2str(1) ' explained variance = ' num2str(variances(1)/sum(variances))],'FontSize',msize+12);
        ylabel(['Dimension ' num2str(2) ' explained variance = ' num2str(variances(2)/sum(variances))],'FontSize',msize+12);
        zlabel(['Dimension ' num2str(3) ' explained variance = ' num2str(variances(3)/sum(variances))],'FontSize',msize+12);
        
        grid on;
        axis equal;
        set(gcf,'Position',figpos);
        
        hgsave(['PCA_' outname '_3D.fig']);
        hold off;
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        
        
        
        
        
        % General case
    else
        
        for x=1:Xdim-1
            for y=x+1:Xdim
                
                % Same-factors performances vectors
                if sum(link),
                    if length(link)>2,
                        if length(link)>3, disp('Warning: only the first three factors are used for linking same-factors samples.'); end
                        for i1=1:max(data(:,link(1))),
                            for i2=1:max(data(:,link(2))),
                                for i3=1:max(data(:,link(3))),
                                    testsame = (data(:,link(1))==i1 & data(:,link(2))==i2 & data(:,link(3))==i3);
                                    if sum(testsame)>1, % More than one sample with matching factors
                                        if factor1,
                                            plot([scores(testsame,x);scores(find(testsame==1,1,'first'),x)],[scores(testsame,y);scores(find(testsame==1,1,'first'),y)],'-','Color',colt(find(testsame==1,1,'first'),:));
                                            hold on;
                                        else
                                            plot([scores(testsame,x);scores(find(testsame==1,1,'first'),x)],[scores(testsame,y);scores(find(testsame==1,1,'first'),y)],'-');
                                            hold on;
                                        end
                                    end
                                end
                            end
                        end
                        
                    elseif length(link)==2,
                        for i1=1:max(data(:,link(1))),
                            for i2=1:max(data(:,link(2))),
                                testsame = (data(:,link(1))==i1 & data(:,link(2))==i2);
                                if sum(testsame)>1, % More than one sample with matching factors
                                    if factor1,
                                        plot([scores(testsame,x);scores(find(testsame==1,1,'first'),x)],[scores(testsame,y);scores(find(testsame==1,1,'first'),y)],'-','Color',colt(find(testsame==1,1,'first'),:));
                                        hold on;
                                    else
                                        plot([scores(testsame,x);scores(find(testsame==1,1,'first'),x)],[scores(testsame,y);scores(find(testsame==1,1,'first'),y)],'-');
                                        hold on;
                                    end
                                end
                            end
                        end
                        
                    else disp('Warning: incorrect input for linking same-factors samples; linking command discarded.');
                    end
                end
                
                
                % Factor 1 mean ellipse
                if ell && factor1,
                    for i=1:numtim,
                        plot(scoremeans(i,x),scoremeans(i,y),'x','MarkerSize',msize+6,'Color',color(timon(i),:));
                        hold on;
                        ellipse(scorerrs(i,x),scorerrs(i,y),0,scoremeans(i,x),scoremeans(i,y),color(timon(i),:));
                        hold on;
                    end
                end
                

                
                % Factors 1 and 2: markers
                if factor1 && factor2,
                    for i=1:size(resin,1), 
                        plot(scores(i,x),scores(i,y),pi{i},'MarkerFaceColor',colt(i,:),'MarkerEdgeColor',colt(i,:),'MarkerSize',msize); 
                        hold on; 
                    end
                    
                elseif factor1, 
                    for i=1:size(resin,1), plot(scores(i,x),scores(i,y),'o','MarkerFaceColor',colt(i,:),'MarkerEdgeColor',colt(i,:),'MarkerSize',msize); 
                        hold on; 
                    end
                    
                elseif factor2,
                    for i=1:size(resin,1), plot(scores(i,x),scores(i,y),pi{i},'MarkerSize',msize);
                        hold on;
                    end
                    
%                 else
%                     for i=1:size(resin,1), plot(scores(i,x),scores(i,y),'o','MarkerSize',msize); 
%                         hold on; 
%                     end
                end
                
                
                % Factor 3: crosses
                if factor3,
                    if factor1,
                        for i=1:size(resin,1),
                            plot(scores(i,x),scores(i,y),piani{i},'MarkerEdgeColor',piacol(tim(i),:),'MarkerSize',msize); 
                            hold on;
                        end
                    else
                        plot(scores(i,x),scores(i,y),piani{i},'MarkerEdgeColor','k','MarkerSize',msize);
                        hold on;
                    end
                end
                
                
                % Factor 4: labels
                if factor4,
                    text(scores(:,x)+.01*max(abs(scores(:,x))),scores(:,y),reclabels,'FontSize',msize-2);
                    hold on;
                end
                

                
                % Legend
                for i=1:numtim,
                    hold on; 
                    hplot(i)=plot(0,0,'Visible','off','Color',color(timon(i),:),'LineWidth',2); 
                end
                
                for i=1:numpie, 
                    hplot(length(timon)+i)=plot(0,0,pio{pieon(i)},'Visible','off','Color','k','MarkerFaceColor','k'); 
                end
                
                for i=1:numpian,
                    hplot(length(timon)+length(pieon)+i)=plot(0,0,piao{pianon(i)},'Visible','off','Color','k');
                end
                
                if ~isempty(names),
                    hleg = legend(hplot(:),names,'Location','NorthWest');
                    set(hleg,'FontSize',msize+10);
                end
                
                % Add points to keep all markers inside the figure
                plot(min(scores(:,1))-.1*max(abs(scores(:,1))),min(scores(:,2))-.1*max(abs(scores(:,2))),'.w','MarkerSize',2);
                plot(max(scores(:,1))+.1*max(abs(scores(:,1))),max(scores(:,2))+.1*max(abs(scores(:,2))),'.w','MarkerSize',2);
                                
                xlabel(['Dimension ' num2str(x) ' explained variance = ' num2str(variances(x)/sum(variances))],'FontSize',msize+12);
                ylabel(['Dimension ' num2str(y) ' explained variance = ' num2str(variances(y)/sum(variances))],'FontSize',msize+12);
                
                axis equal;
                set(gcf,'Position',figpos);
                
                hgsave(['PCA_' outname '_' num2str(x) 'vs' num2str(y) '.fig']);
                hold off;
                
            end
        end
    end
end

end


function h=ellipse(ra,rb,ang,x0,y0,C,Nb)
% Ellipse adds ellipses to the current plot
%
% ELLIPSE(ra,rb,ang,x0,y0) adds an ellipse with semimajor axis of ra,
% a semimajor axis of radius rb, a semimajor axis of ang, centered at
% the point x0,y0.
%
% The length of ra, rb, and ang should be the same.
% If ra is a vector of length L and x0,y0 scalars, L ellipses
% are added at point x0,y0.
% If ra is a scalar and x0,y0 vectors of length M, M ellipse are with the same
% radii are added at the points x0,y0.
% If ra, x0, y0 are vectors of the same length L=M, M ellipses are added.
% If ra is a vector of length L and x0, y0 are  vectors of length
% M~=L, L*M ellipses are added, at each point x0,y0, L ellipses of radius ra.
%
% ELLIPSE(ra,rb,ang,x0,y0,C)
% adds ellipses of color C. C may be a string ('r','b',...) or the RGB value.
% If no color is specified, it makes automatic use of the colors specified by
% the axes ColorOrder property. For several circles C may be a vector.
%
% ELLIPSE(ra,rb,ang,x0,y0,C,Nb), Nb specifies the number of points
% used to draw the ellipse. The default value is 300. Nb may be used
% for each ellipse individually.
%
% h=ELLIPSE(...) returns the handles to the ellipses.
%
% as a sample of how ellipse works, the following produces a red ellipse
% tipped up at a 45 deg axis from the x axis
% ellipse(1,2,pi/8,1,1,'r')
%
% note that if ra=rb, ELLIPSE plots a circle
%

% written by D.G. Long, Brigham Young University, based on the
% CIRCLES.m original
% written by Peter Blattner, Institute of Microtechnology, University of
% Neuchatel, Switzerland, blattner@imt.unine.ch


% Check the number of input arguments

if nargin<1,
    ra=[];
end;
if nargin<2,
    rb=[];
end;
if nargin<3,
    ang=[];
end;

%if nargin==1,
%  error('Not enough arguments');
%end;

if nargin<5,
    x0=[];
    y0=[];
end;

if nargin<6,
    C=[];
end

if nargin<7,
    Nb=[];
end

% set up the default values

if isempty(ra),ra=1;end;
if isempty(rb),rb=1;end;
if isempty(ang),ang=0;end;
if isempty(x0),x0=0;end;
if isempty(y0),y0=0;end;
if isempty(Nb),Nb=300;end;
if isempty(C),C=get(gca,'colororder');end;

% work on the variable sizes

x0=x0(:);
y0=y0(:);
ra=ra(:);
rb=rb(:);
ang=ang(:);
Nb=Nb(:);

if isstr(C),C=C(:);end;

if length(ra)~=length(rb),
    error('length(ra)~=length(rb)');
end;
if length(x0)~=length(y0),
    error('length(x0)~=length(y0)');
end;

% how many inscribed elllipses are plotted

if length(ra)~=length(x0)
    maxk=length(ra)*length(x0);
else
    maxk=length(ra);
end;

% drawing loop

for k=1:maxk
    
    if length(x0)==1
        xpos=x0;
        ypos=y0;
        radm=ra(k);
        radn=rb(k);
        if length(ang)==1
            an=ang;
        else
            an=ang(k);
        end;
    elseif length(ra)==1
        xpos=x0(k);
        ypos=y0(k);
        radm=ra;
        radn=rb;
        an=ang;
    elseif length(x0)==length(ra)
        xpos=x0(k);
        ypos=y0(k);
        radm=ra(k);
        radn=rb(k);
        an=ang(k)
    else
        rada=ra(fix((k-1)/size(x0,1))+1);
        radb=rb(fix((k-1)/size(x0,1))+1);
        an=ang(fix((k-1)/size(x0,1))+1);
        xpos=x0(rem(k-1,size(x0,1))+1);
        ypos=y0(rem(k-1,size(y0,1))+1);
    end;
    
    co=cos(an);
    si=sin(an);
    the=linspace(0,2*pi,Nb(rem(k-1,size(Nb,1))+1,:)+1);
    %  x=radm*cos(the)*co-si*radn*sin(the)+xpos;
    %  y=radm*cos(the)*si+co*radn*sin(the)+ypos;
    h(k)=line(radm*cos(the)*co-si*radn*sin(the)+xpos,radm*cos(the)*si+co*radn*sin(the)+ypos);
    set(h(k),'color',C(rem(k-1,size(C,1))+1,:));
    set(h(k),'Linewidth',2);
    
end;
end


function h=ellipse3(ra,rb,ang,x0,y0,z0,staticdim,C,Nb)
% Ellipse in 3D plot

% Check the number of input arguments

if nargin<1,
    ra=[];
end;
if nargin<2,
    rb=[];
end;
if nargin<3,
    ang=[];
end;

%if nargin==1,
%  error('Not enough arguments');
%end;

if nargin<6,
    x0=[];
    y0=[];
    z0=[];
end;

if nargin<7,
    staticdim=[];
end

if nargin<8,
    C=[];
end

if nargin<9,
    Nb=[];
end

% set up the default values

if isempty(ra),ra=1;end;
if isempty(rb),rb=1;end;
if isempty(ang),ang=0;end;
if isempty(x0),x0=0;end;
if isempty(y0),y0=0;end;
if isempty(z0),z0=0;end;
if isempty(staticdim), staticdim=3; end;
if isempty(Nb),Nb=300;end;
if isempty(C),C=get(gca,'colororder');end;

% work on the variable sizes

x0=x0(:);
y0=y0(:);
ra=ra(:);
rb=rb(:);
ang=ang(:);
Nb=Nb(:);

if isstr(C),C=C(:);end;

if length(ra)~=length(rb),
    error('length(ra)~=length(rb)');
end;
if length(x0)~=length(y0),
    error('length(x0)~=length(y0)');
end;
if length(z0)~=length(x0),
    error('length(z0)~=length(x0)');
end;

% how many inscribed elllipses are plotted

if length(ra)~=length(x0)
    maxk=length(ra)*length(x0);
else
    maxk=length(ra);
end;

% drawing loop

for k=1:maxk
    
    if length(x0)==1
        xpos=x0;
        ypos=y0;
        zpos=z0;
        radm=ra(k);
        radn=rb(k);
        if length(ang)==1
            an=ang;
        else
            an=ang(k);
        end;
    elseif length(ra)==1
        xpos=x0(k);
        ypos=y0(k);
        zpos=z0(k);
        radm=ra;
        radn=rb;
        an=ang;
    elseif length(x0)==length(ra)
        xpos=x0(k);
        ypos=y0(k);
        zpos=z0(k);
        radm=ra(k);
        radn=rb(k);
        an=ang(k)
    else
        rada=ra(fix((k-1)/size(x0,1))+1);
        radb=rb(fix((k-1)/size(x0,1))+1);
        an=ang(fix((k-1)/size(x0,1))+1);
        xpos=x0(rem(k-1,size(x0,1))+1);
        ypos=y0(rem(k-1,size(y0,1))+1);
        zpos=z0(rem(k-1,size(z0,1))+1);
    end;
    
    co=cos(an);
    si=sin(an);
    the=linspace(0,2*pi,Nb(rem(k-1,size(Nb,1))+1,:)+1);
    zlength = length(radm*cos(the)*co-si*radn*sin(the)+xpos);
    zos = ones(1,zlength) .* zpos;
    %  x=radm*cos(the)*co-si*radn*sin(the)+xpos;
    %  y=radm*cos(the)*si+co*radn*sin(the)+ypos;
    if staticdim==1,
        h(k)=line(zos,radm*cos(the)*co-si*radn*sin(the)+xpos,radm*cos(the)*si+co*radn*sin(the)+ypos);
    elseif staticdim==2,
        h(k)=line(radm*cos(the)*co-si*radn*sin(the)+xpos,zos,radm*cos(the)*si+co*radn*sin(the)+ypos);
    else
        h(k)=line(radm*cos(the)*co-si*radn*sin(the)+xpos,radm*cos(the)*si+co*radn*sin(the)+ypos,zos);
    end
    set(h(k),'color',C(rem(k-1,size(C,1))+1,:));
     set(h(k),'Linewidth',2);
    
end;
end