function [all_scores all_posthoc all_fx] = batch_create(mat,initials,pianum)
% Launches Stat creation for all matrices included in a .mat file ('Results_XX.mat')

% PARAMETERS
tims = {'Bright','Round','Dry','Dark','Velvety'};

load(mat);
w=whos('-file',mat);

if nargin<2, 
    name = mat;
    if strcmp(name(length(name)-3:length(name)),'.mat') && length(name)>5, initials = name(length(name)-5:length(name)-4);
    else initials = name(length(name)-1:length(name));
    end
end

if nargin<3, pianum=1; end

rootdir=[initials ' Analysis'];

mkdir(rootdir);
cd(rootdir);

fid = fopen(['All_Scores_' initials '.xls'],'w+');
fid2 = fopen(['All_PostHoc_' initials '.xls'],'w+');
fid3 = fopen(['All_Fx_Sizes_' initials '.xls'],'w+');

rrrank = 1;
postrank=0;

for i=1:length(w),
    if length(w(i).name)>3 && strcmp(w(i).name(1:4),'tags'),
    else
        disp(rrrank);
        
        if length(w(i).name)>8 && strcmp(w(i).name(1:8),'Results_'), outname = w(i).name(9:length(w(i).name));
        else outname = w(i).name;
        end
        if pianum==1, foldername = outname(1:length(outname)-3);
        else foldername = outname(1:length(outname)-length(initials)-1);
        end
        mkdir(foldername);
        cd(foldername);
        
        eval(['[tmp tmpost tmpfx] = create_stats(' w(i).name ',pianum);']);

        all_scores(1,rrrank)=0;
        all_fx(1,rrrank)=0;
        for t=1:length(tmp), all_scores(t,rrrank) = tmp(t); all_fx(t,rrrank) = tmpfx(t); end
        
        % Print column name
        fprintf(fid,'%s\t',outname);
        fprintf(fid3,'%s\t',outname);
        rrrank = rrrank+1;
        
        
        shoc = size(tmpost,2);
        all_posthoc(1,postrank+1:postrank+shoc)=0;
        for t=1:shoc, for u=1:size(tmpost,1), all_posthoc(u,postrank+t) = tmpost(u,t); end; end
        
        % Print column name
        fprintf(fid2,'%s\t',outname);
        for t=1:shoc, fprintf(fid2,'\t'); end
        postrank = postrank + shoc+1;

        cd('..');
    end
end

% PRINT All_Scores
for i=1:size(all_scores,1),
    fprintf(fid,'\n');
    for j=1:size(all_scores,2),
        fprintf(fid,'%.3f\t',all_scores(i,j));
    end
end

fclose(fid);


% PRINT All_Fx_Sizes
for i=1:size(all_fx,1),
    fprintf(fid3,'\n');
    for j=1:size(all_fx,2),
        fprintf(fid3,'%.3f\t',all_fx(i,j));
    end
end

fclose(fid3);


% PRINT All_posthoc
% Timbres header

fprintf(fid2,'\n');
for j=1:length(w),
    for t=1:length(tims)-1,
        for u=t+1:length(tims)
            fprintf(fid2,'%s\t',tims{t});
        end
    end
    fprintf(fid2,'\t');
end
fprintf(fid2,'\n');
for j=1:length(w),
    for t=1:length(tims)-1,
        for u=t+1:length(tims)
            fprintf(fid2,'%s\t',tims{u});
        end
    end
    fprintf(fid2,'\t');
end

for i=1:size(all_posthoc,1),
    fprintf(fid2,'\n');
    for j=1:size(all_posthoc,2),
        fprintf(fid2,'%d\t',all_posthoc(i,j));
%         if all_posthoc(i,j), fprintf(fid2,'%d\t',all_posthoc(i,j));
%         else fprintf(fid2,'\t');
%         end
    end
end

fclose(fid2);

cd('..');
end









