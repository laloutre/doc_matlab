function boxplot_sig(matin,labels)

% matin: [REC#,piece,timbre,pianist,stats] * [original rank, sorted rank, perfs]
% matin of sig res

[desc factor maxtim reorder variables timmm col catfields catelabel] = stariables();

if ischar(matin), resin=dlmread(matin); iname=matin;
else resin=matin; iname=inputname(1);
end

if nargin<2, 
    labels = cell(1,size(reorder,2));
    for i=1:length(reorder), labels{reorder(i)} = variables{i}; end
end

% Categories rank starts
cate = catfields(:,1);
cate(length(cate)+1) = catfields(size(catfields,1),2)+1;

sz = transform_results(resin(3:size(resin,1),:));

maxtim = max(sz(:,factor));

timrank=1;
for i=1:maxtim,
    if sum(sz(:,factor)==i), % There is at least one timbre ID corresponding to value i
        timon(timrank)=i;
        timrank=timrank+1;
    end
end
ntim=length(timon);

if maxtim>length(col), % Add colors to fit all timbres
    for i=length(col)+1:maxtim, col(i,:) = col(i-5,:)./2; end
end

clf;
hold off;

for i=1:length(cate)-1,
    labs={};
    jini=find(resin(2,:)>=cate(i),1,'first');
    jend=find(resin(2,:)<cate(i+1),1,'last');
    tops = max(max(abs(sz(:,jini:jend))));
    if ~isempty(jini) && ~isempty(jend),
        for j=jini:jend,
            labs(j-jini+1)=labels(resin(2,j));
            hold on;
            boxplot(sz(:,j),sz(:,factor),'colors',col(timon,:),'symbol','+','widths',.6/ntim,'positions',(j-jini+1)+.8/ntim*(1:ntim));
            hold on;
            plot([j-jini+1 j-jini+1],[-tops tops],':k');
            
        end
        
        % Legend
        set(gca,'XTick',1.5:1:jend-jini+1.5);
        set(gca,'XTickLabel',labs);
        for k=1:ntim, hold on; hplot(k)=plot(0,0,'+','Color',col(timon(k),:),'Visible','off'); end
        legend(hplot(:),timmm(timon),'Location','SouthEast');
        xlim([.9 jend-jini+2]);
        if jend>=jini, hgsave(['Boxplot ' iname '_' num2str(i) ' ' catelabel{i} '.fig']); end
        close;
    end
end

hold off;