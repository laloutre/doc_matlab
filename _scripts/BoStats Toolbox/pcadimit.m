function [varsout sigvars coefs scores variances resin varsin] = pcadimit(matin,varargin)

% Identifies the dep.vars. most prominent in each relevant PCA dimension
% IN: Z-Scores, sorted vars
% OUT: selection of most prominent variables in each PCA dim. linear combination
% Standard selection threshold: variables wears more weight than the mean over the dim.
% 'Redux' selection threshold: variables wears more than 0.1 weight
%

if ischar(matin), resin=dlmread(matin); else resin=matin; end

[desc factor ngps reorder variables] = stariables();

resin = resin(:,desc+1:size(resin,2)); % Keep only dep.vars

vars = cell(1,size(reorder,2));
for i=1:length(reorder), vars{reorder(i)} = variables{i}; end

ivar=1;
redux=0;
while(ivar<=length(varargin)),
    if strcmp(varargin(ivar),'redux'), redux=1;
    elseif strcmp(varargin(ivar),'variables'), 
        if ivar<length(varargin), vars = varargin{ivar+1}; ivar=ivar+1; 
        else disp('Something missing in the argins');
        end
    end
    ivar=ivar+1;
end

if size(resin,2)~=length(vars), disp('Different # of dep.vars between input data and vars labels');
else
    
%     % Clean
%     iv=1;
%     vrank=1;
%     varsin={};
%     while iv<=size(resin,2),
%         if (sum(abs(resin(:,iv)))==0) || (sum(isnan(resin(:,iv)))==size(resin,1)) || sum(resin(:,iv)==Inf)==size(resin,1),
%             resin(:,iv)=[];
%         else
%             if sum(isnan(resin(:,iv)))>0 || sum(resin(:,iv)==Inf)>0,
%                 for j=1:size(resin,1),
%                     if isnan(resin(iv,j)) || resin(iv,j)==Inf, resin(iv,j)=0; end
%                 end
%             end
%             iv=iv+1;
%             varsin(vrank) = vars(iv);
%             vrank=vrank+1;
%         end
%     end
    
    
    % Clean
    drank=1;
    delvar=[];
    for i=1:size(resin,2)
        if (sum(abs(resin(:,i)))==0) || (sum(isnan(resin(:,i)))==size(resin,1)) || sum(resin(:,i)==Inf)==size(resin,1),
            delvar(drank) = i;
            drank=drank+1;
        elseif sum(isnan(resin(:,i)))>0 || sum(resin(:,i)==Inf)>0,
            for j=1:size(resin,1),
                if isnan(resin(j,i)) || resin(j,i)==Inf, resin(j,i)=0; end
            end
        end
    end
    
    resin(:,delvar)=[];
    varsin=vars;
    varsin(delvar)=[];
    varsout = cell(1,1);
    
    % PCA
    [coefs scores variances] = princomp(resin,'econ');
    
    Xdim = find(variances(:)./sum(variances)>0.05,1,'last');
    sigvars=zeros(1,Xdim);
    for i=1:Xdim,
        if redux, f = find(abs(coefs(:,i))>.1);
        else f = find(abs(coefs(:,i))>mean(abs(coefs(:,i))));
        end
        for t=1:length(f), varsout(t,i) = varsin(f(t)); end
        sigvars(1:length(f),i)=f;
    end
    
end
end

