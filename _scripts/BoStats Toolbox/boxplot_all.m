function boxplot_all(matin,Z,labels)

% matin: Standard Results: [REC#,piece,timbre,pianist,stats] * [perfs]


[desc factor maxtim reorder variables timmm col catfields catelabel] = stariables();
catfields=catfields+desc; % + desc. columns

if ischar(matin), resin=dlmread(matin); iname=matin;
else resin=matin; iname=inputname(1);
end
if length(iname)>8 && strcmp(iname(1:8),'Results_'), iname = iname(9:length(iname));
elseif length(iname)>3 && strcmp(iname(1:3),'ZS_'), iname = iname(4:length(iname));
end

if nargin<3, 
    labels = cell(1,size(reorder,2));
    for i=1:length(reorder), labels{reorder(i)} = variables{i}; end
end

if nargin<2, Z=0;
elseif strcmp(Z,'Z') || strcmp(Z,'z') || strcmp(Z,'zscore') || strcmp(Z,'Zscore'), Z=1;
elseif strcmp(Z,'res') || strcmp(Z,'R') || strcmp(Z,'r') || strcmp(Z,'results') || strcmp(Z,'Results'), Z=0;
end


% % Categories rank starts
% cate = catfields(:,1);
% cate(length(cate)+1) = catfields(size(catfields,1),2)+1;

if Z, sz = resin;
else sz = transform_results(resin,1); % Sorted
end

maxtim = max(sz(:,factor));

timrank=1;
for i=1:maxtim,
    if sum(sz(:,factor)==i), % There is at least one timbre ID corresponding to value i
        timon(timrank)=i;
        timrank=timrank+1;
    end
end
ntim=length(timon);

if maxtim>length(col), % Add colors to fit all timbres
    for i=length(col)+1:maxtim, col(i,:) = col(i-5,:)./2; end
end

clf;
hold off;

for n=1:size(catfields,1),
    tops = max(max(abs(sz(:,catfields(n,1):catfields(n,2)))));
    for j=catfields(n,1):catfields(n,2),
        boxplot(sz(:,j),sz(:,factor),'colors',col(timon,:),'symbol','+','widths',.6/ntim,'positions',(j-catfields(n,1)+1)+.8/ntim*(1:ntim));
        hold on;
        plot([j-catfields(n,1)+1,j-catfields(n,1)+1],[-tops tops],':k');
    end
    
    % Legend
    set(gca,'XTick',1.5:1:catfields(n,2)-catfields(n,1)+1.5);
    set(gca,'XTickLabel',labels(catfields(n,1)-desc:catfields(n,2)-desc));
    for k=1:ntim, hold on; hplot(k)=plot(0,0,'+','Color',col(timon(k),:),'Visible','off'); end
    legend(hplot(:),timmm(timon),'Location','SouthEast');
    xlim([.9 catfields(n,2)-catfields(n,1)+2]);
    hgsave(['Boxplot ' iname '_' num2str(n) ' ' catelabel{n} '.fig']);
    close;
    
end

hold off;

end