function [sz szmean szerr ssorted] = transform_results(resin,sort,category)


% IN: Standard results matrix [Performances * Vars]
%       + Sorting options, according to one specified category-rank vector or to default vector
%       => Specify 'sort' in the 2nd field
%
% Default: 335 vars  + REC# + piece + timbre + pianist
% OUT: Z-scores, Z-scores means and stderr per timbre + Sorted input if specifed

warning off;

[desc factor ntim reorder] = stariables();

if nargin<3, category(:,2)=reorder; category(:,1) = 1:length(reorder); end
if nargin<2, sort=0; end

if strcmp(sort,'sort'), sort=0; end

[dimsamp dimvars] = size(resin);

maxtim = max(resin(:,factor)); % Number of timbres (default: 5)

timrank=1;
for i=1:maxtim,
    if sum(resin(:,factor)==i), % There is at least one timbre ID corresponding to value i
        timon(timrank)=i;
        timrank=timrank+1;
    end
end
ntim=length(timon);


validvals=cell(1,dimvars);
sz = NaN*zeros(dimsamp, dimvars);

if sort,
    ssorted = zeros(dimsamp+2, dimvars);
    ssorted(1:2,desc+1:dimvars) = category'; % Ordering ranks
    ssorted(3:dimsamp+2,:) = resin;
    
    ssorted = ssorted';
    ssorted(desc+1:dimvars,:) = sortrows(ssorted(desc+1:dimvars,:),2);
    ssorted = ssorted';

    sz(:,1:desc) = ssorted(3:dimsamp+2,1:desc); % REC# Piece Timbre
    for j=desc+1:dimvars, 
        validvals{j} = find(isfinite(ssorted(3:dimsamp+2,j)));

        sz(validvals{j},j) = zscore(ssorted(validvals{j}+2,j)); 
    end

else
    ssorted=0;
    sz(:,1:desc) = resin(:,1:desc);
    for j=desc+1:dimvars, 
        validvals{j} = find(isfinite(resin(:,j)));
        sz(validvals{j},j) = zscore(resin(validvals{j},j));
    end
end

szlite = sz(:,[factor,desc+1:dimvars]);
szlite = sortrows(szlite,1); % Sorted by timbre

szmean = zeros(ntim,dimvars-desc+1);
szerr = zeros(ntim,dimvars-desc+1);

for i=1:ntim, 
    sztmp = szlite(find(szlite(:,1)==timon(i),1,'first'):find(szlite(:,1)==timon(i),1,'last'),:);
    validtmp={};
    for j=1:size(szlite,2),
        validtmp{j} = find(isfinite(sztmp(:,j)));
        szmean(i,j) = mean(sztmp(validtmp{j},j),1);
        szerr(i,j) = std(sztmp(validtmp{j},j),0,1) ./ sqrt(length(validtmp{j}));
        szerr(i,1)=timon(i);
    end
end

warning on;

end


