function [resout resort]= repeatit_extended(matin,name,matcher,factol,varsnames)

% Batch for repeated-measures ANOVA
%
% Matin: one line per sample; C1= REC#, C2= piece#, C3= timbre, C4= pianist, C5:end= vars
%
% Resout: one line per var;
%       (WARNING: INCOMPLETE)
%
%       C1:C[ntim+1=6]: Normality of distributions p-value for each group, + sum
%       C[ntim+2=7]:C[ntim+1+reps=9]: Levene's homogeneity of variance p-values
%       C[ntim+reps+2=10]: Mauchly's sphericity p-value
%       C[ntim+reps+3=11]: Huynh-Feldt epsilon correction term (to F-value, against lack of sphericity)
%
%
%       C[ntim+reps+4=12]:C[ntim+reps+7=15]: [df F p fxsize] between Groups     :       "Groups" = dependant var. (timbre) i.e. between-subjects factor
%                                                                                        (p<=0.05 => lines (one pianist, one piece, one timbre each) grouped by timbre differ significantly from one timbre group to the others, wrt the dep. var.)
%
%       C[ntim+reps+8=16]:C[ntim+reps+13=21]: [df F p fxsize dfHF pHF] in Time  :       "Time" = repetition, i.e. within-subject factor
%                                                                                        (p<=0.05 => repetitions by one pianist on one piece of the same timbre are not consistent wrt the dep. var.)
%
%       C[ntim+reps+14=22]:C[ntim+reps+19=27]: [df F p fxsize dfHF pHF] Interaction:    "Interaction" = Time-by-group effect, i.e. between-subjects interaction effect
%
%       C[ntim+reps+20=28]:C[ntim+reps+24=32]: [df F p fxsize pHF] Subjects:            "Subjects" = Matching between rows/repetitions, i.e. similarity between repetition-subjects,
%                                                                                        i.e. repeated-measures matching effectiveness (test of improvement of RM over straight-up ANOVA),
%                                                                                        or, how each line (one pianist, one piece, one timbre) varies from the others:
%                                                                                        (p<=0.05 => lines differ, i.e. when pianist, piece and timbre differ, dep.var. changes (duh))
%


printout=1; % Set to 1 to print xlses out

if ischar(matin), resin=dlmread(matin); else resin=matin; end

[desc factor ntim reor vari groups] = stariables();

if nargin<3, matcher=resin(:,2:4); factol = factor-1; end

[dimsamp dimvar] = size(resin);

if nargin>3 && iscell(varsnames), vari = varsnames; end

% Reorder for repeat
% Output 3D matrix: Samples * [3 desc + measures] * vars
[resort sorter] = sortasrep(resin(:,desc+1:dimvar),matcher);


ngroup = 14; % Number of group variance comparison values
nrepeat = 7; % Number of repeated measures values: 3 RM * [from 4 to 6] stats each
reps = size(resort,2); % Number of repetitions
nstats = 2*(ntim+1) + (reps+4) + 2; % (ntim+1) normality of distributions + (# of repetitions + 4) Levene's + sphericity + Huynh-Feldt epsilon correction

resout = zeros(dimvar-desc,nstats+ngroup+nrepeat); % Assumptions + ANOVA + RM

% tempo(:,2) = resort(:,factor-1,1); % Factor for non-param tests

warning off;

% GO
for i=1:dimvar-desc % Loop on vars

    % ASSUMPTIONS

    % Normality of distributions
    % K-S tests of normal distribution (for each timbre)
    for t=1:ntim
        rest = resin(resin(:,factor)==t,i+desc);
        if sum(abs(rest))>0,
            if length(rest)<4,
                distrest = normcdf(rest,mean(rest),std(rest));
                if sum(~isnan(distrest)), % test of cdf validity (at least one ~NaN)
                    [ksbin ksp] = kstest(rest,[rest,distrest]);
                    resout(i,t) = ksp; % K-S p-value
                end
            else
                [libin lip] = lillietest(rest); % Lilliefors correction to KS test
                resout(i,t) = lip; % K-S lillie p-value
            end
        end
    end
    resout(i,ntim+1) = sum(resout(i,1:ntim)>.05); % Number of normally-distributed groups


    % Normality of distributions, collapsed
    % K-S tests of normal distribution (for each timbre)
    for t=1:ntim
        rest = resort(sorter(:,factol)==t,i);
        if sum(abs(rest))>0,
            if length(rest)<4,
                distrest = normcdf(rest,mean(rest),std(rest));
                if sum(~isnan(distrest)), % test of cdf validity (at least one ~NaN)
                    [ksbin ksp] = kstest(rest,[rest,distrest]);
                    resout(i,ntim+1+t) = ksp; % K-S p-value
                end
            else
                [libin lip] = lillietest(rest); % Lilliefors correction to KS test
                resout(i,ntim+1+t) = lip; % K-S lillie p-value
            end
        end
    end

    resout(i,2*(ntim+1)) = sum(resout(i,ntim+1:2*ntim)>.05); % Number of normally-distributed collapsed groups

    % Levene's homogeneity of variance
    % By repetition
    for j=1:reps,
        resout(i,2*(ntim+1)+j) = vartestn(resort(:,j,i),sorter(:,factol),'off','robust');
    end

    % Mean over reps
    resout(i,2*(ntim+1)+reps+1) = mean(resout(i,2*(ntim+1)+1:2*(ntim+1)+reps),2);

    % Collapsed
    resout(i,2*(ntim+1)+reps+2) = vartestn(mean(resort(:,:,i),2),sorter(:,factol),'off','robust');

    % Overall
    resout(i,2*(ntim+1)+reps+3) = vartestn(resin(:,i+desc),resin(:,factor),'off','robust');

    % The Good one
    resout(i,2*(ntim+1)+reps+4) = vartestener(resin(:,i+desc),resin(:,factor),matcher,factol);


    % Sphericity
    resout(i,2*(ntim+1)+reps+5) = Mauspher(resort(:,:,i));


    % Huynh-Feldt epsilon correction
    resout(i,2*(ntim+1)+reps+6) = epsHF(resort(:,:,i));




    % REPEATED MEASURES

    tmpcell = cell(1,ntim);
    for t=1:ntim,
        tmpcell{1,t} = resort(sorter(:,factol)==t,:,i);
    end

    [p table] = anova_rm(tmpcell,'off');

    % Between-subjects: groups
    resout(i,nstats+1) = table{3,3}; % df Groups
    resout(i,nstats+2) = table{3,5}; % F Groups
    resout(i,nstats+3) = table{3,6}; % p Groups
    resout(i,nstats+4) = table{3,2}/table{7,2}; % Effect size Groups


    % Robust test of equality of means (over collapsed-(in-time)-repetitions vector): Welch
    %     tempo(:,1) = mean(resort(:,:,i),2);
    [Fw df1w df2w Pw] = Wtest([mean(resort(:,:,i),2) sorter(:,factol)]);
    resout(i,nstats+5) = Fw;
    resout(i,nstats+6) = df2w;
    resout(i,nstats+7) = Pw;

    % Welch improved
    [Fwi df1wi df2wi Pwi] = Wmetatest([resin(:,i+desc) resin(:,factor)],[mean(resort(:,:,i),2) sorter(:,factol);]);
    resout(i,nstats+8) = Fwi;
    resout(i,nstats+9) = df2wi;
    resout(i,nstats+10) = Pwi;


    % Kruskal-Wallis non-parametric test
    [kwp kwtab] = kruskalwallis(mean(resort(:,:,i),2),sorter(:,factol),'off');
    if ~isempty(kwtab{2,5}), resout(i,nstats+11) = kwtab{2,5}; end % chi-square
    resout(i,nstats+12) = kwp; % p-value

    % Kruskal-Wallis non-parametric test improved
    [kwP kwF] = anovaizer(resin(:,i+desc),resin(:,factor),matcher,factol,'Kruskal');
    resout(i,nstats+13) = kwF; % chi-square
    resout(i,nstats+14) = kwP; % p-value



    % Within subjects
    % Time
    resout(i,nstats+ngroup+1) = table{2,3}; % df Time
    resout(i,nstats+ngroup+2) = table{2,5}; % F Time
    resout(i,nstats+ngroup+3) = table{2,6}; % p Time
    resout(i,nstats+ngroup+4) = table{2,2}/table{7,2}; % Effect size Time
    resout(i,nstats+ngroup+5) = table{2,3} * resout(i,ntim+1+reps+4); % HF-corrected df Time
    resout(i,nstats+ngroup+6) = 1 - fcdf(table{2,5}, resout(i,nstats+ngroup+5), table{5,3}*resout(i,nstats+ngroup+5)); % HF-corrected p-value Time
    % p2 = 1 - [cumulative distribution function](F.t,df.t2,(n.samples-n.groups)*df.t2)

    % Time-by-group
    resout(i,nstats+ngroup+7) = table{4,3}; % df Interaction
    resout(i,nstats+ngroup+8) = table{4,5}; % F Interaction
    resout(i,nstats+ngroup+9) = table{4,6}; % p Interaction
    resout(i,nstats+ngroup+10) = table{4,2}/table{7,2}; % Effect size Interaction
    resout(i,nstats+ngroup+11) = table{4,3} * resout(i,ntim+1+reps+4); % HF-corrected df Interaction
    resout(i,nstats+ngroup+12) = 1 - fcdf(table{4,5}, resout(i,nstats+ngroup+11), table{5,3}*resout(i,nstats+ngroup+5)); % HF-corrected p-value Interaction
    % p2 = 1 - [cumulative distribution function](F.I,df.I2,(n.samples-n.groups)*df.t2)

    % Error-time
    resout(i,nstats+ngroup+13) = table{5,3}; % df Subjects
    resout(i,nstats+ngroup+14) = table{5,5}; % F Subjects
    resout(i,nstats+ngroup+15) = table{5,6}; % p Subjects
    resout(i,nstats+ngroup+16) = table{5,2}/table{7,2}; % Effect size Subjects
    resout(i,nstats+ngroup+17) = 1 - fcdf(table{5,5}, table{5,3}, table{5,3}*resout(i,nstats+ngroup+5)); % HF-corrected p-value Subjects
    % p2 = 1 - [cumulative distribution function](F.s,df.s,(n.samples-n.groups)*df.t2)


end

warning on;


% PRINTOUT

if printout,

    % Name the output files
    if nargin<2 || isempty(name) || ~sum(name~=0),
        if ischar(matin), in=matin;
        else in=inputname(1);
        end
    else in=name;
    end
    
    if length(in)>8 && strcmp(in(1:8),'Results_'), in = in(9:length(in)); end
    
    nameout=['Repeater extended' in '.xls'];
    fid = fopen(nameout,'w+');

    % HEADER

    % Assumptions
    fprintf(fid,'\t');
    for t=1:ntim, fprintf(fid,['KS p ' groups{t} '\t']); end
    fprintf(fid,'Sum KS\t');
    for t=1:ntim, fprintf(fid,['KS p ' groups{t} 'collapsed\t']); end
    fprintf(fid,'Sum KS collapsed\t');

    for i=1:reps, fprintf(fid,['Levene p t' num2str(i) '\t']); end
    fprintf(fid,'Levene p mean\t Levene p collapsed\t Levene p overall\t Levene p best\t');

    fprintf(fid,'Sph. p \t HF epsilon \t');

    % ANOVA
    fprintf(fid,'df Group\t F Group\t p Group\t Fx size Gp\t Welch F Gp\t Welch df2\t Welch p Gp\t Welch F better\t Welch df2 better\t Welch p better\t KW Chi2 Gp\t KW p Gp\t KW Chi2 better\t KW p better\t');


    % RM
    lab1={'df','F','p','Fx size','df HF','p HF'};
    lab2={'Time','Interaction','Subjects'};

    for i=1:length(lab2),
        for j=1:length(lab1),
            if sum([i,j]~=[3 5]), fprintf(fid,[lab1{j} ' ' lab2{i} '\t']); end
        end
    end


    % DATA
    for i=1:size(resort,3),
        fprintf(fid,'\n %s \t',vari{i});
        for t=1:size(resout,2), fprintf(fid,'%.3f\t',resout(i,t)); end
    end
    fclose(fid);




    % PRINTOUT CONDENSED

    nameout2=['Repeater condensed extended' in '.xls'];
    fid2 = fopen(nameout2,'w+');


    % HEADER

    % Assumptions
    fprintf(fid2,'\t');
    fprintf(fid2,'KS sum\t KS collapsed sum\t Levene p mean\t Levene p collapsed\t Levene p overall\t Levene p best\t');


    % ANOVA
    fprintf(fid2,'p Group\t Fx size Gp\t Welch p Gp\t Welch p better\t KW p Gp\t KW p better\t');

    % RM
    filler = [4 6 10 12];
    condenser = [ntim+1,2*(ntim+1),2*(ntim+1)+reps+[1 2 3 4],nstats+[3 4 7 10 12 14],nstats+ngroup+filler];
    % Sum of norm.distr.(Normal+collapsed), Levene p mean+collapsed+overall+better, ANOVA [p, fxsize, Welch p (*2), KW p (*2)], RM [ [Time, Interaction]*[fx,pHF] ]

    for i=1:length(lab2),
        for j=1:length(lab1),
            if sum((6*(i-1)+j)==filler), fprintf(fid2,[lab1{j} ' ' lab2{i} '\t']); end
        end
    end


    % DATA
    for i=1:size(resort,3),
        fprintf(fid2,'\n %s \t',vari{i});
        for t=1:length(condenser), fprintf(fid2,'%.3f\t',resout(i,condenser(t))); end
    end
    fclose(fid2);

end

end




function [resout matchout] = sortasrep(resin,matchcol)

if size(resin,1)~=size(matchcol,1), disp('Different sizes of resin and matcher; fail');
else

    resout=zeros(1,1,size(resin,2));
    matchout(1,:)=matchcol(1,:);

    tmprank = ones(1,size(resin,1));

    for k=1:size(resin,1), % loop on samples
        newline=1;
        for i=1:size(resout,1), % Loop on stored repetition-samples
            if sum(matchcol(k,:)==matchout(i,:)) == size(matchcol,2),
                resout(i,tmprank(i),:) = resin(k,:);
                tmprank(i) = tmprank(i)+1;
                newline=0;
            end
        end
        if newline,
            matchout(size(matchout,1)+1,:) = matchcol(k,:);
            resout(size(resout,1)+1,1,:) = resin(k,:);
            tmprank(size(matchout,1)) = tmprank(size(matchout,1)) +1;

        end
    end
end

% function resout = sortasrep(resin,desc,matchcol)
%
% if nargin<3, matchcol=2:4; end
%
% resout=zeros(1,length(matchcol),size(resin,2)-desc);
% resout(1,1:length(matchcol),1)=resin(1,matchcol);
%
% tmprank = (length(matchcol))*ones(1,size(resin,1));
%
% for k=1:size(resin,1), % loop on samples
%     newline=1;
%     for i=1:size(resout,1), % Loop on stored repetition-samples
%         if sum(resin(k,matchcol)==resout(i,1:length(matchcol))) == length(matchcol),
%             resout(i,tmprank(i)+1,:) = resin(k,max(matchcol)+1:size(resin,2));
%             tmprank(i) = tmprank(i)+1;
%             newline=0;
%         end
%     end
%     if newline,
%         resout(size(resout,1)+1,1:length(matchcol),1) = resin(k,matchcol);
%         resout(size(resout,1),length(matchcol)+1,:) = resin(k,max(matchcol)+1:size(resin,2));
%         tmprank(size(resout,1)) = tmprank(size(resout,1))+1;
%     end
% end
%
% end

end



function [p, table] = anova_rm(X, displayopt)
%   [p, table] = anova_rm(X, displayopt)
%   Single factor, tepeated measures ANOVA.
%
%   [p, table] = anova_rm(X, displayopt) performs a repeated measures ANOVA
%   for comparing the means of two or more columns (time) in one or more
%   samples(groups). Unbalanced samples (i.e. different number of subjects
%   per group) is supported though the number of columns (followups)should
%   be the same.
%
%   DISPLAYOPT can be 'on' (the default) to display the ANOVA table, or
%   'off' to skip the display. For a design with only one group and two or
%   more follow-ups, X should be a matrix with one row for each subject.
%   In a design with multiple groups, X should be a cell array of matrices.
%
%   Copyright 2008, Arash Salarian
%

if nargin < 2
    displayopt = 'on';
end

if ~iscell(X)
    X = {X};
end

%number of groups
s = size(X,2);

%subjects per group
n_h = zeros(s, 1);
for h=1:s
    n_h(h) = size(X{h}, 1);
end
n = sum(n_h);

%number of follow-ups
t = size(X{1},2);

% overall mean
y = 0;
for h=1:s
    y = y + sum(sum(X{h}));
end
y = y / (n * t);

% allocate means
y_h = zeros(s,1);
y_j = zeros(t,1);
y_hj = zeros(s,t);
y_hi = cell(s,1);
for h=1:s
    y_hi{h} = zeros(n_h(h),1);
end

% group means
for h=1:s
    y_h(h) = sum(sum(X{h})) / (n_h(h) * t);
end

% follow-up means
for j=1:t
    y_j(j) = 0;
    for h=1:s
        y_j(j) = y_j(j) + sum(X{h}(:,j));
    end
    y_j(j) = y_j(j) / n;
end

% group h and time j mean
for h=1:s
    for j=1:t
        y_hj(h,j) = sum(X{h}(:,j) / n_h(h));
    end
end

% subject i'th of group h mean
for h=1:s
    for i=1:n_h(h)
        y_hi{h}(i) = sum(X{h}(i,:)) / t;
    end
end

% calculate the sum of squares
ssG = 0;
ssSG = 0;
ssT = 0;
ssGT = 0;
ssR = 0;

for h=1:s
    for i=1:n_h(h)
        for j=1:t
            ssG  = ssG  + (y_h(h) - y)^2;
            ssSG = ssSG + (y_hi{h}(i) - y_h(h))^2;
            ssT  = ssT  + (y_j(j) - y)^2;
            ssGT = ssGT + (y_hj(h,j) - y_h(h) - y_j(j) + y)^2;
            ssR  = ssR  + (X{h}(i,j) - y_hj(h,j) - y_hi{h}(i) + y_h(h))^2;
        end
    end
end

% calculate means
if s > 1
    msG  = ssG  / (s-1);
    msGT = ssGT / ((s-1)*(t-1));
end
msSG = ssSG / (n-s);
msT  = ssT  / (t-1);
msR  = ssR  / ((n-s)*(t-1));


% calculate the F-statistics
if s > 1
    FG  = msG  / msSG;
    FGT = msGT / msR;
end
FT  = msT  / msR;
FSG = msSG / msR;


% single or multiple sample designs?
if s > 1
    % case for multiple samples
    pG  = 1 - fcdf(FG, s-1, n-s);
    pT  = 1 - fcdf(FT, t-1, (n-s)*(t-1));
    pGT = 1 - fcdf(FGT, (s-1)*(t-1), (n-s)*(t-1));
    pSG = 1 - fcdf(FSG, n-s, (n-s)*(t-1));

    p = [pT, pG, pSG, pGT];

    table = { 'Source' 'SS' 'df' 'MS' 'F' 'Prob>F'
        'Time'  ssT t-1 msT FT pT
        'Group' ssG s-1 msG FG pG
        'Interaction' ssGT (s-1)*(t-1) msGT FGT pGT
        'Subjects (matching)' ssSG n-s msSG FSG pSG
        'Error' ssR (n-s)*(t-1) msR  [] []
        'Total' [] [] [] [] []
        };
    table{end, 2} = sum([table{2:end-1,2}]);
    table{end, 3} = sum([table{2:end-1,3}]);

    if (isequal(displayopt, 'on'))
        digits = [-1 -1 0 -1 2 4];
        statdisptable(table, 'multi-sample repeated measures ANOVA', 'ANOVA Table', '', digits);
    end
else
    % case for only one sample
    pT  = 1 - fcdf(FT, t-1, (n-s)*(t-1));
    pSG = 1 - fcdf(FSG, n-s, (n-s)*(t-1));

    p = [pT, pSG];

    table = { 'Source' 'SS' 'df' 'MS' 'F' 'Prob>F'
        'Time'  ssT t-1 msT FT pT
        'Subjects (matching)' ssSG n-s msSG FSG pSG
        'Error' ssR (n-s)*(t-1) msR  [] []
        'Total' [] [] [] [] []
        };
    table{end, 2} = sum([table{2:end-1,2}]);
    table{end, 3} = sum([table{2:end-1,3}]);

    if (isequal(displayopt, 'on'))
        digits = [-1 -1 0 -1 2 4];
        statdisptable(table, 'repeated measures ANOVA', 'ANOVA Table', '', digits);
    end
end

end




function [p F] = vartestener(x,group,matcher,factol)

[x2 sor2] = sortasrep(x,matcher);

[RSS df1] = vartestn_mod(x,group);
[no1 no2 SSE df2] = vartestn_mod(mean(x2(:,:),2),sor2(:,factol));

if (df2 > 0), mse = SSE/df2; else mse = NaN; end

if (SSE~=0)
    F = (RSS/df1) / mse;
    p = 1 - fcdf(F,df1,df2);     % Probability of F given equal means.
elseif (RSS==0)                 % Constant Matrix case.
    F = NaN;
    p = NaN;
else                            % Perfect fit case.
    F = Inf;
    p = 0;
end

end



function [RSS df1 SSE df2] = vartestn_mod(x,group)
%VARTESTN Test for equal variances across multiple groups.
%   VARTESTN(X,GROUP) requires a vector X, and a GROUP argument that is a
%   categorical variable, vector, string array, or cell array of strings
%   with one row for each element of X.  X values corresponding to the same
%   value of GROUP are placed in the same group.  The function tests for
%   equal variances across groups.
%
%   VARTESTN treats NaNs as missing values, and ignores them.
%
%   P = VARTESTN(...) returns the p-value, i.e., the probability of
%   observing the given result, or one more extreme, by chance if the null
%   hypothesis of equal variances is true.  Small values of P cast doubt
%   on the validity of the null hypothesis.
%
%   [...] = VARTESTN(X,GROUP,DISPLAYOPT) with DISPLAYOPT='on' (the default)
%   displays the box plot and table.  DISPLAYOPT='off' omits these displays.
%
%   [...] = VARTESTN(X,GROUP,DISPLAYOPT,TESTTYPE) with TESTTYPE='robust'
%   performs Levene's test.  This is a robust
%   alternative useful when the sample distributions are not normal, and
%   especially when they are prone to outliers.  For this test the STATS
%   output structure has a field named 'fstat' containing the test
%   statistic, and 'df1' and 'df2' containing its numerator and denominator
%   degrees of freedom.
%   Copyright 2005-2006 The MathWorks, Inc.
%   $Revision: 1.1.6.4 $  $Date: 2006/10/02 16:35:18 $
%   $Revision: moi $  $Date: 2012/06/07 18:35:18 $

error(nargchk(1,4,nargin,'struct'));

if (nargin < 2), group = []; end
dorobust=1;

% Error if no data
if isempty(x)
    error('stats:vartestn:NoData','X must not be empty');
end

% Convert group to cell array from character array, make it a column
if (ischar(group) && ~isempty(group)), group = cellstr(group); end
if (size(group, 1) == 1), group = group'; end

% If x is a matrix, convert to vector form.
[n,m] = size(x);
if isempty(group)
    x = x(:);
    group = reshape(repmat((1:m), n, 1), n*m, 1);
elseif isvector(x)
    if numel(group) ~= n
        error('stats:vartestn:InputSizeMismatch',...
            'X and GROUP must have the same length.');
    end
else
    error('stats:vartestn:BadGroup',...
        'Cannot specify a GROUP argument unless X is a vector.');
end

% Get rid of NaN
t = isnan(x);
if any(t)
    x(t) = [];
    group(t,:) = [];
end

% Compute group summary statistics
[igroup,gname] = grp2idx(group);
[gmean,gsem,gcount] = grpstats(x,igroup);
df = gcount-1;                  % group degrees of freedom
gvar = gcount .* gsem.^2;       % group variances
sumdf = sum(df);
if sumdf>0
    vp = sum(df.*gvar) / sumdf;  % pooled variance
else
    vp = NaN;
end
k = length(df);

if dorobust
    % Robust Levene's test

    % Remove single-point groups and center each group
    spgroups = find(gcount<2);
    t = ~ismember(igroup,spgroups);
    xc = x(t) - gmean(igroup(t));
    ngroups = length(gcount) - length(spgroups);

    % Now do the anova and extract results from the anova table
    [RSS df1 SSE df2] = anova1_mod(x,group,'off');

else
    % N/A
end

end



function [p,F,anovatab] = anovaizer(x,group,matcher,factol,classical)

if nargin<5, classical=1; end
if ischar(classical),
    if strcmp(classical,'classical'), classical=1;
    elseif strcmp(classical,'Kruskalwallis') || strcmp(classical,'kruskalwallis') || strcmp(classical,'KW') || strcmp(classical,'Kruskal'),
        classical=0;
    end
end


[xort xorter] = sortasrep(x,matcher);

if classical,
    [RSS df1] = anova1_mod(x,group,'off');
    [off1 off2 SSE df2] = anova1_mod(mean(xort(:,:),2),xorter(:,factol),'off');
else
    [RSS df1] = anova1_mod('kruskalwallis',x,group,'off');
    [off1 off2 SSE df2 lx] = anova1_mod('kruskalwallis',mean(xort(:,:),2),xorter(:,factol),'off');
end


if (df2 > 0)
    mse = SSE/df2;
else
    mse = NaN;
end

if (classical)
    if (SSE~=0)
        F = (RSS/df1) / mse;
        p = 1 - fcdf(F,df1,df2);     % Probability of F given equal means.
    elseif (RSS==0)                 % Constant Matrix case.
        F = NaN;
        p = NaN;
    else                            % Perfect fit case.
        F = Inf;
        p = 0;
    end
else
    F = (12 * RSS) / (lx * (lx+1));
    [xr,tieadj] = tiedrank(x);
    if (tieadj > 0)
        F = F / (1 - 2 * tieadj/(lx^3-lx));
    end
    p = 1 - chi2cdf(F, df1);
end


Table=zeros(2,5);               %Formatting for ANOVA Table printout
Table(:,1)=[ RSS SSE ]';
Table(:,2)=[ df1 df2 ]';
Table(:,3)=[ RSS/df1 mse ]';
Table(:,4)=[ F Inf ]';
Table(:,5)=[ p Inf ]';

colheads = ['Source       ';'         SS  ';'          df ';...
    '       MS    ';'          F  ';'     Prob>F  '];
if (~classical)
    colheads(5,:) = '     Chi-sq  ';
    colheads(6,:) = '  Prob>Chi-sq';
end
rowheads = ['Groups     ';'Error      '];

% Create cell array version of table
atab = num2cell(Table);
for i=1:size(atab,1)
    for j=1:size(atab,2)
        if (isinf(atab{i,j}))
            atab{i,j} = [];
        end
    end
end
atab = [cellstr(strjust(rowheads, 'left')), atab];
atab = [cellstr(strjust(colheads, 'left'))'; atab];
if (nargout > 1)
    anovatab = atab;
end

end



function [RSS df1 SSE df2 lx] = anova1_mod(x,group,displayopt,extra)
%ANOVA1 One-way analysis of variance (ANOVA).
% Returns SSR and SSE depending on whatever
%   Reference: Robert V. Hogg, and Johannes Ledolter, Engineering Statistics
%   Macmillan 1987 pp. 205-206.

classical = 1;
nargs = nargin;
if (nargin>0 && strcmp(x,'kruskalwallis'))
    % Called via kruskalwallis function, adjust inputs
    classical = 0;
    if (nargin >= 2), x = group; group = []; end
    if (nargin >= 3), group = displayopt; displayopt = []; end
    if (nargin >= 4), displayopt = extra; end
    nargs = nargs-1;
end

error(nargchk(1,3,nargs,'struct'));

if (nargs < 2), group = []; end
if (nargs < 3), displayopt = 'on'; end
% Note: for backwards compatibility, accept 'nodisplay' for 'off'
willdisplay = ~(strcmp(displayopt,'nodisplay') | strcmp(displayopt,'n') ...
    | strcmp(displayopt,'off'));

% Convert group to cell array from character array, make it a column
if (ischar(group) && ~isempty(group)), group = cellstr(group); end
if (size(group, 1) == 1), group = group'; end

% If X is a matrix with NaNs, convert to vector form.
if (length(x) < numel(x))
    if (any(isnan(x(:))))
        [n,m] = size(x);
        x = x(:);
        gi = reshape(repmat((1:m), n, 1), n*m, 1);
        if isempty(group)     % no group names
            group = gi;
        elseif (size(group,1) == m)
            group = group(gi,:);
        else
            error('stats:anova1:InputSizeMismatch',...
                'X and GROUP must have the same length.');
        end
    end
end

% If X is a matrix and GROUP is strings, use GROUPs as names
if (iscell(group) && (length(x) < numel(x)) ...
        && (size(x,2) == size(group,1)))
    named = 1;
    gnames = group;
    grouped = 0;
else
    named = 0;
    gnames = [];
    grouped = (length(group) > 0);
end

if (grouped)
    % Single data vector and a separate grouping variable
    x = x(:);
    lx = length(x);
    if (lx ~= numel(x))
        error('stats:anova1:VectorRequired','First argument has to be a vector.')
    end
    nonan = ~isnan(x);
    x = x(nonan);

    % Convert group to indices 1,...,g and separate names
    group = group(nonan,:);
    [groupnum, gnames] = grp2idx(group);
    named = 1;

    % Remove NaN values
    nonan = ~isnan(groupnum);
    if (~all(nonan))
        groupnum = groupnum(nonan);
        x = x(nonan);
    end

    lx = length(x);
    xorig = x;                    % use uncentered version to make M
    groupnum = groupnum(:);
    maxi = size(gnames, 1);
    if isa(x,'single')
        xm = zeros(1,maxi,'single');
    else
        xm = zeros(1,maxi);
    end
    countx = xm;
    if (classical)
        mu = mean(x);
        x = x - mu;                % center to improve accuracy
        xr = x;
    else
        [xr,tieadj] = tiedrank(x);
    end

    if (willdisplay)
        % Fill M with NaN in case the group sizes vary
        Mrows = 0;
        for j=1:maxi
            Mrows = max(Mrows,sum(groupnum==j));
        end
        M = NaN(Mrows,maxi);
    end
    for j = 1:maxi
        % Get group sizes and means
        k = find(groupnum == j);
        lk = length(k);
        countx(j) = lk;
        xm(j) = mean(xr(k));       % column means

        if (willdisplay)           % fill matrix for boxplot
            M(1:lk,j) = xorig(k);
        end
    end

    gm = mean(xr);                      % grand mean
    df1 = sum(countx>0) - 1;            % Column degrees of freedom
    df2 = lx - df1 - 1;                 % Error degrees of freedom
    xc = xm - gm;                       % centered
    xc(countx==0) = 0;
    RSS = dot(countx, xc.^2);           % Regression Sum of Squares
else
    % Data in matrix form, no separate grouping variable
    [r,c] = size(x);
    lx = r * c;
    if (classical)
        xr = x;
        mu = mean(xr(:));
        xr = xr - mu;           % center to improve accuracy
    else
        [xr,tieadj] = tiedrank(x(:));
        xr = reshape(xr, size(x));
    end
    countx = repmat(r, 1, c);
    xorig = x;                 % save uncentered version for boxplot
    xm = mean(xr);             % column means
    gm = mean(xm);             % grand mean
    df1 = c-1;                 % Column degrees of freedom
    df2 = c*(r-1);             % Error degrees of freedom
    RSS = r*(xm - gm)*(xm-gm)';        % Regression Sum of Squares
end

TSS = (xr(:) - gm)'*(xr(:) - gm);  % Total Sum of Squares
SSE = TSS - RSS;                   % Error Sum of Squares

end



function [P L] = Mauspher(X)
%Mauchly's sphericity test.
%[Basically, here we are testing the null hypothesis that for all p single components the
%variances are equal and all covariances are zero. It would means that all the p-axis have
%the same variability or eigenvalues (l);recall that det(R)=prod(diag(L))=Wilk's lambda].
%
%     Syntax: function [Mauspher] = Mauspher(X,alpha)
%
%     Inputs:
%          X - multivariate data matrix.
%     Output:
%          n - sample-size.
%          p - variables.
%          L - Mauchly's statistic used to test any deviation from
%              an expected sphericity.
%          P - probability that null Ho: is true.
%

%  Created by A. Trujillo-Ortiz and R. Hernandez-Walls
%             Facultad de Ciencias Marinas
%             Universidad Autonoma de Baja California
%             Apdo. Postal 453
%             Ensenada, Baja California
%             Mexico.
%             atrujo@uabc.mx
%
%  July 6, 2003.
%
%  To cite this file, this would be an appropriate format:
%  Trujillo-Ortiz, A. and R. Hernandez-Walls. (2003). Mauspher: Mauchly's sphericity tests.
%    A MATLAB file. [WWW document]. URL http://www.mathworks.com/matlabcentral/fileexchange/
%    loadFile.do?objectId=3694&objectType=FILE
%
%  References:
%
%  Davies, A. W. (1971), Percentile approximations for a class of likelihood ratio
%              criteria. Biometrika, 58:349-356.
%  Johnson, R. A. and Wichern, D. W. (1992), Applied Multivariate Statistical Analysis.
%              3rd. ed. New-Jersey:Prentice Hall. pp. 158-160.
%  Mauchly, J. W. (1940), Significance test for sphericity of a normal n-variate
%              distribution. The Annals of Mathematical Statistics, 11:204-209.
%

[n p] = size(X);
S = cov(X);  %covariances' matrix
W = (n-1)*S;
L = det(W)/((1/p)*trace(W))^p;  %Mauchly's statistic
M = (n-1)-(2*p*p+p+2)/6/p;
LL = (-1)*M*log(L);  %Chi-square approximation
A = (p+1)*(p-1)*(p+2)*(2*p*p*p+6*p*p+3*p+2)/288/p/p;
F = p*(p+1)/2-1;  %degrees of freedom
A1 = 1-chi2cdf(LL,F);
A3 = 1-chi2cdf(LL,F+4);
P = A1+(A/M/M)*(A3-A1);  %Probability that null Ho: is true.

end



function [x] = epsHF(X)
%EPSHF Huynh-Feldt epsilon.

%
% Created by A. Trujillo-Ortiz, R. Hernandez-Walls, A. Castro-Perez
%            and K. Barba-Rojo
%            Facultad de Ciencias Marinas
%            Universidad Autonoma de Baja California
%            Apdo. Postal 453
%            Ensenada, Baja California
%            Mexico.
%            atrujo@uabc.mx
%
% Copyright. November 01, 2006.
%
% --Special thanks are given to S�ren Andersen, Universit�t Leipzig, Institut
%   Psychologie I, Professur Allgemeine Psychologie & Methodenlehre, Seeburgstr
%   14-20, D-04103 Leipzig, Deutchland, for encouraging us to create this m-file--
%
% To cite this file, this would be an appropriate format:
% Trujillo-Ortiz, A., R. Hernandez-Walls, A. Castro-Perez and K. Barba-Rojo. (2006).
%   epsHF:Huynh-Feldt epsilon. A MATLAB file. [WWW document]. URL http://
%   www.mathworks.com/matlabcentral/fileexchange/loadFile.do?objectId=12853
%
% References:
% Geisser, S, and Greenhouse, S.W. (1958), An extension of Box�s results on
%     the use of the F distribution in multivariate analysis. Annals of
%     Mathematical Statistics, 29:885�891.
% Greenhouse, S.W. and Geisser, S. (1959), On methods in the analysis of
%     profile data. Psychometrika, 24:95-112.
% Huynh, M. and Feldt, L.S. (1970), Conditions under which mean square rate
%     in repeated measures designs have exact-F distributions. Journal of the
%     American Statistical Association, 65:1982-1989
% Keselman, J.C, Lix, L.M. and Keselman, H.J. (1996), The analysis of repeated
%     measurements: a quantitative research synthesis. British Journal of
%     Mathematical and Statistical Psychology, 49:275�298.
% Maxwell, S.E. and Delaney, H.D. (1990), Designing Experiments and Analyzing
%     Data: A model comparison perspective. Pacific Grove, CA: Brooks/Cole.
%

error(nargchk(1,1,nargin));

[n k] = size(X);
eGG = epsGG(X);  %call to the zipped Greenhouse-Geisser epsilon function
epsHF = (n*(k-1)*eGG-2)/((k-1)*((n-1)-(k-1)*eGG));  %Huynh-Feldt epsilon estimation
x = epsHF;

return,
end



function [x] = epsGG(X)
%EPSGG Greenhouse-Geisser epsilon.
%
% Created by A. Trujillo-Ortiz, R. Hernandez-Walls, A. Castro-Perez
%            and K. Barba-Rojo
%            Facultad de Ciencias Marinas
%            Universidad Autonoma de Baja California
%            Apdo. Postal 453
%            Ensenada, Baja California
%            Mexico.
%            atrujo@uabc.mx
%
% Copyright. October 31, 2006.
%
% --Special thanks are given to S�ren Andersen, Universit�t Leipzig, Institut
%   Psychologie I, Professur Allgemeine Psychologie & Methodenlehre, Seeburgstr
%   14-20, D-04103 Leipzig, Deutchland, for encouraging us to create this m-file--
%
% To cite this file, this would be an appropriate format:
% Trujillo-Ortiz, A., R. Hernandez-Walls, A. Castro-Perez and K. Barba-Rojo. (2006).
%   epsGG:Greenhouse-Geisser epsilon. A MATLAB file. [WWW document]. URL http://
%   www.mathworks.com/matlabcentral/fileexchange/loadFile.do?objectId=12839
%
% Reference:
% Greenhouse, S.W. and Geisser, S. (1959), On methods in the analysis
%     of profile data. Psychometrika, 24:95-112.
% Maxwell, S.E. and Delaney, H.D. (1990), Designing Experiments and
%     Analyzing Data: A model comparison perspective. Pacific Grove,
%     CA: Brooks/Cole.
%

error(nargchk(1,1,nargin));

k = size(X,2);  %number of treatments
S = cov(X);  %variance-covariance matrix
mds = mean(diag(S));  %mean of the entries on the main diagonal of S
ms = mean(mean(S));  %the mean of all entries of S
msr = mean(S,2);  %mean of all entries in row i of S
N = k^2*(mds-ms)^2;
D = (k-1)*(sum(sum(S.^2))-2*k*sum(msr.^2)+k^2*ms^2);
epsGG = N/D;  %Greenhouse-Geisser epsilon estimation
x = epsGG;

return,
end



function [F df1 df2 P] = Wtest(X)
%Welch's test for analysis of variance
%     Inputs:
%          X - data matrix (Size of matrix must be n-by-2; data=column 1, sample=column 2).
%

k=max(X(:,2));

%Statistics.
n=[];s2=[];m=[];
indice=X(:,2);
for i=1:k
    Xe=find(indice==i);
    eval(['X' num2str(i) '=X(Xe,1);']);
    eval(['n' num2str(i) '=length(X' num2str(i) ') ;']);
    eval(['s2' num2str(i) '=(std(X' num2str(i) ').^2) ;']);
    eval(['m' num2str(i) '=mean(X' num2str(i) ');']);
    eval(['xn= n' num2str(i) ';']);
    eval(['xs2= s2' num2str(i) ';']);
    eval(['xm= m' num2str(i) ';'])
    n=[n;xn];s2=[s2;xs2];m=[m;xm];
end

%Welch's Procedure.
ws=[];
for i=1:k
    eval(['w' num2str(i) '=n' num2str(i) '/s2' num2str(i) ';']);
    eval(['x= w' num2str(i) ';']);
    ws=[ws;x];
end

wps=[];
for i=1:k
    eval(['wp' num2str(i) '=w' num2str(i) '*m' num2str(i) ';']);
    eval(['x= wp' num2str(i) ';']);
    wps=[wps;x];
end

A=sum(ws);
D=sum(wps);

H=D/A;

b=[];
for i=1:k
    eval(['b' num2str(i) '=((1-(w' num2str(i) '/A)).^2)/(n' num2str(i) '-1);']);
    eval(['x=b' num2str(i) ';']);
    b=[b;x];
end

pw=[];
for i=1:k
    eval(['pws' num2str(i) '=(w' num2str(i) '*((m' num2str(i) '-H).^2)/(k-1));']);
    eval(['x=pws' num2str(i) ';']);
    pw=[pw;x];
end

E=sum(pw);
O=sum(b);

G=(1+2*(k-2)*O/(k^2-1));

F=E/G;  %Welch's F-statistic.
v1=(k-1);  %numerator degrees of freedom.
v2=(k^2-1)/(3*O);  %denominator degrees of freedom.
df1=v1;df2=v2;

% Because the denominator degrees of freedom are corrected and could results
% a fraction, the probability function associated to the F statistic is resolved
% by the Simpson's 1/3 numerical integration method.
x=linspace(.000001,F,100001);
DF=x(2)-x(1);
y=((v1/v2)^(.5*v1)/(beta((.5*v1),(.5*v2))));
y=y*(x.^((.5*v1)-1)).*(((x.*(v1/v2))+1).^(-.5*(v1+v2)));
N=length(x);
P=1-(DF.*(y(1)+y(N) + 4*sum(y(2:2:N-1))+2*sum(y(3:2:N-2)))/3.0);

end


function [F v1 v2 P] = Wmetatest(X,X2)

% Testing Welch for RM (incl. several vertical groups)
% Method here: separate sum of squares equivalent and error equivalent
% SSgroups from Full results
% SSerror from collapsed RM (repetitions' means)
%
% INPUT: X = [resin(:,dep.var.) resin(:,factor)]
%        X2 = [mean(resort(:,n.reps,dep.var.),2) resort(:,factor-1,1)]

[G v2] = Wtest2(X);

[E v1] = Wtest1(X2);

F = E/G;

x=linspace(.000001,F,100001);
DF=x(2)-x(1);
y=((v1/v2)^(.5*v1)/(beta((.5*v1),(.5*v2))));
y=y*(x.^((.5*v1)-1)).*(((x.*(v1/v2))+1).^(-.5*(v1+v2)));
N=length(x);
P=1-(DF.*(y(1)+y(N) + 4*sum(y(2:2:N-1))+2*sum(y(3:2:N-2)))/3.0);

end


function [E df1] = Wtest1(X)
% For Sum of squares equivalent

k=max(X(:,2));

%Statistics.
n=[];s2=[];m=[];
indice=X(:,2);
for i=1:k
    Xe=find(indice==i);
    eval(['X' num2str(i) '=X(Xe,1);']);
    eval(['n' num2str(i) '=length(X' num2str(i) ') ;']);
    eval(['s2' num2str(i) '=(std(X' num2str(i) ').^2) ;']);
    eval(['m' num2str(i) '=mean(X' num2str(i) ');']);
    eval(['xn= n' num2str(i) ';']);
    eval(['xs2= s2' num2str(i) ';']);
    eval(['xm= m' num2str(i) ';'])
    n=[n;xn];s2=[s2;xs2];m=[m;xm];
end

%Welch's Procedure.
ws=[];
for i=1:k
    eval(['w' num2str(i) '=n' num2str(i) '/s2' num2str(i) ';']);
    eval(['x= w' num2str(i) ';']);
    ws=[ws;x];
end

wps=[];
for i=1:k
    eval(['wp' num2str(i) '=w' num2str(i) '*m' num2str(i) ';']);
    eval(['x= wp' num2str(i) ';']);
    wps=[wps;x];
end

A=sum(ws);
D=sum(wps);

H=D/A;

pw=[];
for i=1:k
    eval(['pws' num2str(i) '=(w' num2str(i) '*((m' num2str(i) '-H).^2)/(k-1));']);
    eval(['x=pws' num2str(i) ';']);
    pw=[pw;x];
end

E=sum(pw);

v1=(k-1);  %numerator degrees of freedom.
df1=v1;


end


function [G df2] = Wtest2(X)
% For Sum of squares equivalent

k=max(X(:,2));

%Statistics.
n=[];s2=[];m=[];
indice=X(:,2);
for i=1:k
    Xe=find(indice==i);
    eval(['X' num2str(i) '=X(Xe,1);']);
    eval(['n' num2str(i) '=length(X' num2str(i) ') ;']);
    eval(['s2' num2str(i) '=(std(X' num2str(i) ').^2) ;']);
    eval(['m' num2str(i) '=mean(X' num2str(i) ');']);
    eval(['xn= n' num2str(i) ';']);
    eval(['xs2= s2' num2str(i) ';']);
    eval(['xm= m' num2str(i) ';'])
    n=[n;xn];s2=[s2;xs2];m=[m;xm];
end

%Welch's Procedure.
ws=[];
for i=1:k
    eval(['w' num2str(i) '=n' num2str(i) '/s2' num2str(i) ';']);
    eval(['x= w' num2str(i) ';']);
    ws=[ws;x];
end

A=sum(ws);


b=[];
for i=1:k
    eval(['b' num2str(i) '=((1-(w' num2str(i) '/A)).^2)/(n' num2str(i) '-1);']);
    eval(['x=b' num2str(i) ';']);
    b=[b;x];
end


O=sum(b);

G=(1+2*(k-2)*O/(k^2-1));

v2=(k^2-1)/(3*O);  %denominator degrees of freedom.
df2=v2;


end


