function fusion_means(ini1,ini2)
% Mixes 2 pianists' mean chord results
% as found in Mean_XX.mat : chall, chdiff, chleft, chright


% First mat
load(['Means_' ini1],'ch*');
w=who('-file',['Means_' ini1],'ch*');

% Change var names to tmp
for i=1:length(w), 
    eval([w{i} 'tmp  = ' w{i} ';']);
    eval(['clear ' w{i} ';']);
end


% Second mat
load(['Means_' ini2],'ch*');
w=who('-file',['Means_' ini2],'ch*');

for i=1:length(w),
    
    % affect orig. sizes
    eval(['s = size(' w{i} 'tmp,1);']); 
    
    % combine
    eval(['for u = 1:size(' w{i} ',1), ' w{i} 'tmp(s+u,:) = ' w{i} '(u,:); end;']);
    
    % rename
    eval([w{i} ' = ' w{i} 'tmp;']);
        
    % clear
    eval(['clear ' w{i}  'tmp;']);
end


save(['Means_' ini1 ini2],'ch*');

fprintf('\n You need to add tags manually \n');

% Then add double tags manually