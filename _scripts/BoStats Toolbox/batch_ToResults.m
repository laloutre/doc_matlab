function batch_ToResults(mat,tags,outliers,initials,timbres,pian)
% Launches ToResults convertion for all matrices included in a .mat file ('Means_XX.mat'),
% which ought to be chall, chdiff, chleft and chright
% Specify tags (and outliers)

runit=1;
if nargin<4, 
    name = mat;
    nrak=1;
    initials=[];
    while nrak<length(name),
        if strcmp(name(nrak),'_'), 
            initials = name(nrak+1:length(name));
            break;
        end
        nrak=nrak+1;
    end
    if isempty(initials),
        if strcmp(name(length(name)-3:length(name)),'.mat') && length(name)>5, initials = name(length(name)-5:length(name)-4);
        else initials = name(length(name)-1:length(name));
        end
    end
end

if nargin<5,
    if size(tags,2)~=3, disp('Something is lacking'); runit=0; end
elseif nargin<6,
    if size(tags,2)+size(timbres,2)==3,
        if size(tags,2)>1, tags(:,3)=tags(:,2); tags(:,2)=timbres(:,1);
        elseif size(timbres,2)>1, tags(:,2:3)=timbres(:,1:2);
        end
    elseif size(tags,2)>=2 && size(timbres,2)>=2 && ~sum(tags(:,2)~=timbres(:,2)),
        tags(:,3)=tags(:,2); tags(:,2)=timbres(:,1);        
    else disp('Something is lacking'); runit=0;
    end
else tags(:,2)=timbres; tags(:,3)=pian;
end
if nargin<3 || (~isempty(outliers) && outliers==0), outliers=[]; end


% Go
if runit,
    load(mat);
    w=who('-file',mat,'ch*');
    
    res=1;
    for i=1:length(w)
        resname = ['Results_' w{i}(3:length(w{i})) '_' initials];
        eval(['res = ToResults(' w{i} ',tags,outliers);']);
        
        if ~isempty(res),
            eval([resname '=res;']);
            
            % By piece
            for j=1:max(tags(:,1))
                respi = res(find(res(:,2)==j),:);
                if ~isempty(respi),
                    respiname = [resname(1:length(resname)-length(initials)) 'p0' num2str(j) '_' initials];
                    eval([respiname ' = respi;']);
                end
            end
        end
    end
end

tagged = ['tags_' initials];
eval([tagged '=tags;']);

savename = ['Results_' initials];

save(savename,'Results*',tagged);

