function resout = statit(matin,name,varsnames)

% Matin: one line per sample; C1= REC#, C2= piece#, C3= timbre, C4= pianist, C5:end= vars
% Resout: one line per var;
%       C1:2:C9= K-S stat for each timbre,
%       C2:2:C10= K-S p for each timbre,
%       C11:C12= Levene's homogeneity of variance stat + p,
%       C13:C14= ANOVA F + p,
%       C15:C17= Welch stat + df2 + p,
%       C18:20= Brown-Forsythe stat + df2 + p,
%       C21:C22= Kruskal-Wallis chi-square + p
%
%       C23= Sum of normality of distributions
%       C24= significance score
%       C25= F-stat of best test if p<.05
%       C26= p(best test) if <=.05
%       C27= effect size of best test if p<=0.05
%
%       C28:37= post-hoc sig. (.05) between each pair of timbres
%       C38:C42= sum of post-hoc sigs. for each timbre
%       C43= total post-hoc sig. (sum(C26:C35)/2)
%



% --- VARS and INPUT ---

printout=1; % Set to 1 to print xlses out

scoretype=2; % Defines the calculation of aggregate validation score; type 1 = summation, type 2 = selection

if ischar(matin), resin=dlmread(matin); else resin=matin; end

[dimsamp dimvar] = size(resin); % number of samples by number of vars

% Parameters
[desc factor ntim reor vari groups colors catfields catlabels fname grname factwo sigthreshold sigassign] = stariables();

if nargin>2 && iscell(varsnames), vari = varsnames; end


% ntim = max(resin(:,factor)); % # of factor groups
nstat = ntim*2 + 12; % # of stats out
nplus = 5;
ngauss = ntim*(ntim-1)/2;
npost = ngauss + ntim + 1; % # of post-hoc stats


% --- For printout ---
st1 = cell(1,3+nstat+nplus+npost);
st2 = cell(1,3+nstat+nplus+npost);

st1(1:3) = {'Category','Orig.',''}; % First columns
st2(1:3) = {'#','#',''};

for i=1:ntim,
    st1(3+2*(i-1)+1:3+2*(i-1)+2) = {['K-S ' groups{i}],''}; % Normality of distributions for each timbre
    st2(3+2*(i-1)+1:3+2*(i-1)+2) = {'D','p'};
end

st1(3+2*ntim+1:3+nstat+nplus) = {'Levene hom.var.','','ANOVA','','Welch','','','Brown-Forsythe','','','Kruskal-Wallis','','Normal','Sig.','Best test','Best test','Effect'}; % Tests
st2(3+2*ntim+1:3+nstat+nplus) = {'stat','p','F','p','stat','df2','p','stat','df2','p','chi-sq','p','Distributions','Score','F-stat','p','Size'};

for i=1:ntim-1,
    indext1 = 3+nstat+nplus+sum(ntim-(1:i-1))-i;
    for j=i+1:ntim,
        st1(indext1+j) = {[groups{i} ' Vs ']}; % Post-hoc pair comparisons
        st2(indext1+j) = {groups{j}};
    end
end

for i=1:ntim,
    st1(3+nstat+nplus+ngauss + i) = {groups{i}}; % Each timbre's post-hoc differings
    st2(3+nstat+nplus+ngauss + i) = {'pairwise diffs.'};
end

st1(3+nstat+nplus+npost) = {'Post-hoc'};
st2(3+nstat+nplus+npost) = {'Total diffs.'};


% ---

% --- INI ---

resout=zeros(dimvar-desc,nstat+nplus+npost); % stats + normality of distributions + score + post-hocs

% Welch common line
welch(:,2)=resin(:,factor);

% Brown-Forsythe common line
befe(:,2)=resin(:,factor);

warning off;



% --- GO ---

for i=desc+1:dimvar % Run through all vars
    
    % K-S tests of normal distribution (for each timbre)
    for j=1:ntim
        rest = resin(resin(:,factor)==j,i);
        if sum(abs(rest))>0,
            if length(rest)<4,
                distrest = normcdf(rest,mean(rest),std(rest));
                if sum(~isnan(distrest)), % test of cdf validity (at least one ~NaN)
                    [ksbin ksp ksval] = kstest(rest,[rest,distrest]);
                    resout(i-desc,2*j-1) = ksval; % K-S stat
                end
            else
                [libin lip lival] = lillietest(rest); % Lilliefors correction to KS test
                resout(i-desc,2*j-1) = lival; % K-S lillie stat
                resout(i-desc,2*j) = lip; % K-S lillie p-value
            end
        end
    end
    
    % Levene's homogeneity of variance
    [levp levstat] = vartestn(resin(:,i),resin(:,factor),'off','robust');
    if ~isempty(levstat.fstat), resout(i-desc,2*ntim+1) = levstat.fstat; end
    resout(i-desc,2*ntim+2) = levp;
    
    % ANOVA
    [anop anotab anostats] = anova1(resin(:,i),resin(:,factor),'off');
    if ~isempty(anotab{2,5}), resout(i-desc,2*ntim+3) = anotab{2,5}; end % F
    resout(i-desc,2*ntim+4) = anop; % p-value
    
    % Robust tests of equality of means
    % Welch
    welch(:,1)=resin(:,i);
    [Fw df1w df2w Pw] = Wtest(welch);
    resout(i-desc,2*ntim+5) = Fw;
    resout(i-desc,2*ntim+6) = df2w;
    resout(i-desc,2*ntim+7) = Pw;
    
    % Brown-Forsythe
    befe(:,1)=resin(:,i);
    [Fbf df1bf df2bf Pbf] = BFtest(befe);
    resout(i-desc,2*ntim+8) = Fbf;
    resout(i-desc,2*ntim+9) = df2bf;
    resout(i-desc,2*ntim+10) = Pbf;
    
    % Kruskal-Wallis non-parametric test
    [kwp kwtab kwstats] = kruskalwallis(resin(:,i),resin(:,factor),'off');
    if ~isempty(kwtab{2,5}), resout(i-desc,2*ntim+11) = kwtab{2,5}; end % chi-square
    resout(i-desc,2*ntim+12) = kwp; % p-value
    
    % Test for normality of distributions
    resout(i-desc,nstat+1) = sum(resout(i-desc,2:2:2*ntim)>0.05);
    
    
    % SCORE Calculation
    
    if resout(i-desc,2*ntim+2)>0, % Levene p>0 (or else unvalid)
        
        if scoretype==1, % summation-type aggregate score
            resout(i-desc,nstat+2) = ((resout(i-desc,nstat+1) + .5*sum(resout(i-desc,2:2:2*ntim)==0))/5 + 0.1) * ( 1.1*((resout(i-desc,2*ntim+2)>0.05) && (resout(i-desc,2*ntim+4)<=0.05)) + (resout(i-desc,2*ntim+7)<=0.05) + (resout(i-desc,2*ntim+10)<=0.05) ) + (resout(i-desc,2*ntim+12)<=0.05);
            % ((Normality of distributions + .5*uncomputable normality tests)/5 + 0.1) * ( 1.1*(Levene>0.05 [hom.var.] && ANOVA sig.) + Welch sig. + BF sig. ) + KW sig.
            
        elseif scoretype==2, % selection-type aggregate score
            resout(i-desc,nstat+2) = (resout(i-desc,nstat+1) + .5*sum(resout(i-desc,2:2:2*ntim)==0))*.2 ... (Normality of distributions + .5*uncomputable normality tests)/5
                * (max( ((resout(i-desc,2*ntim+2)>0.05) && (resout(i-desc,2*ntim+4)<=0.05)) , max((resout(i-desc,2*ntim+7)<=0.05),(resout(i-desc,2*ntim+10)<=0.05)) ) ... * [   max( (Levene>0.05 [hom.var.] && ANOVA sig.) , max(Welch sig.,BF sig.)
                + .1*((resout(i-desc,2*ntim+2)>0.05) && (resout(i-desc,2*ntim+4)<=0.05)) +.05*max((resout(i-desc,2*ntim+7)<=0.05),(resout(i-desc,2*ntim+10)<=0.05)) ... + .1*(Levene>0.05 [hom.var.] && ANOVA sig.) + .05*max(Welch sig.,BF sig.)
                - .5*(resout(i-desc,2*ntim+12)<=0.05) ) + (resout(i-desc,2*ntim+12)<=0.05); % -0.5*(KW sig.) ] + KW sig.
        end
        
    end
    
    % sig. p-value recap
    if resout(i-desc,nstat+2)>=sigthreshold,
        if sum( round(100*resout(i-desc,nstat+2)) == round(100*sigassign(1,:)) ), % The score indicates the most relevant, sig. test is ANOVA
            resout(i-desc,nstat+3) = resout(i-desc,2*ntim+3); % ANOVA F-test
            resout(i-desc,nstat+4) = resout(i-desc,2*ntim+4); % ANOVA p-value
            resout(i-desc,nstat+5) = anotab{2,2}/anotab{4,2}; % Effect size
            
        elseif sum( round(100*resout(i-desc,nstat+2)) == round(100*sigassign(3,:)) ), % The score indicates the most relevant, sig. test is KW
            resout(i-desc,nstat+3) = resout(i-desc,2*ntim+11); % KW chi-sq.
            resout(i-desc,nstat+4) = resout(i-desc,2*ntim+12); % KW p-value
            resout(i-desc,nstat+5) = kwtab{2,2}/kwtab{4,2}; % Effect size
            
        elseif sum( round(100*resout(i-desc,nstat+2)) == round(100*sigassign(2,:)) ), % The score indicates the most relevant, sig. test is Welch => need to refer to either ANOVA or KW stats
            if resout(i-desc,2*ntim+7)<=resout(i-desc,2*ntim+10), % Welch best
                resout(i-desc,nstat+3) = resout(i-desc,2*ntim+5);
                resout(i-desc,nstat+4) = min(resout(i-desc,2*ntim+7),resout(i-desc,2*ntim+10));
            else % BF best
                resout(i-desc,nstat+3) = resout(i-desc,2*ntim+8);
                resout(i-desc,nstat+4) = resout(i-desc,2*ntim+10);
            end
            resout(i-desc,nstat+5) = anotab{2,2}/anotab{4,2}; % Effect size via ANOVA
        end
    end
    
    
    
    % POST-HOC
    
    if resout(i-desc,nstat+2)>=sigthreshold,
        
        % Pairwise tests
        
        if sum( round(100*resout(i-desc,nstat+2)) == round(100*sigassign(1,:)) ) ||... % The score indicates the most relevant test is ANOVA
                sum( round(100*resout(i-desc,nstat+2)) == round(100*sigassign(2,1:2)) ) || ... % The score indicates the most relevant test is Welch, with ANOVA as stats backup
                ( sum( round(100*resout(i-desc,nstat+2)) == round(100*sigassign(2,3:4)) ) && resout(i-desc,2*ntim+4)<=.05 ),
            
            c = multcompare(anostats,'Display','off'); % => take ANOVA stats
            for j=1:ngauss, resout(i-desc,nstat+nplus+j) = (c(j,3)*c(j,5))>0; end % If Boundaries on 95% C.I. for (timbre x ~= timbre y) are of same sign, zero is out thus their difference is significant
            
        elseif sum( round(100*resout(i-desc,nstat+2)) == round(100*sigassign(3,:)) ) || ... % The score indicates the most relevant, sig. test is KW
                ( sum(round(100*resout(i-desc,nstat+2)) == round(100*sigassign(2,3:4))) && resout(i-desc,2*ntim+4)>.05 ), % Welch is the most relevant test, ANOVA is non-sig., take KW
            
            c = multcompare(kwstats,'Display','off'); % => take KW stats
            for j=1:ngauss, resout(i-desc,nstat+nplus+j) = (c(j,3)*c(j,5))>0; end % If Boundaries on 95% C.I. for (timbre x ~= timbre y) are of same sign, zero is out thus their difference is significant
        end
        
        
        % Each timbre's total diffs.
        
        for j=1:ntim, % Target timbre to assign total to
            for t=1:ntim-1, % First looping timbre
                for u=t+1:ntim, % Second looping timbre
                    if j==t || j==u, % Timbres match
                        resout(i-desc,nstat+nplus+ngauss+j) = resout(i-desc,nstat+nplus+ngauss+j) + resout(i-desc,nstat+5+sum(ntim-(1:t-1))-t+u); % Actual index of the relevant pairwise comparison!
                    end
                end
            end
        end
        
        
        % Total sig. pairwise tests
        
        resout(i-desc,nstat+nplus+npost) = sum(resout(i-desc,nstat+nplus+1:nstat+nplus+ngauss));
        
    end
    
end

warning on;




% ------- PRINTOUT -------

if printout,
    
    % --- Name the output files ---
    if nargin<2 || isempty(name) || ~sum(name~=0),
        if ischar(matin), in=matin;
        else in=inputname(1);
        end
    else in=name;
    end
    
    if length(in)>8 && strcmp(in(1:8),'Results_'), in = in(9:length(in)); end
    
    % --- STATS ---
    
    nameout=['Stat ' in '.xls'];
    fid = fopen(nameout,'w+');
    
    % Stats header lines
    for i=1:length(st1), fprintf(fid, '%s \t', st1{i}); end
    fprintf(fid,'\n');
    for i=1:length(st2), fprintf(fid, '%s \t', st2{i}); end
    % fprintf(fid,'\n');
    
    for i=1:dimvar-desc
        fprintf(fid,'\n %d\t %d\t',reor(i),i);
        fprintf(fid,'%s \t',vari{i}); % variable accounted for on this line
        
        for j=1:nstat+nplus+npost
            fprintf(fid,'%.3f\t',resout(i,j)); % Each stat for this var
        end
        
    end
    
    fclose(fid);
    
    % --- REDUX ---
    
    nameout2=['Sig ' in '.xls'];
    fid2 = fopen(nameout2,'w+');
    
    % Stats header lines
    for i=3:length(st1), fprintf(fid2, '%s \t', st1{i}); end
    fprintf(fid2,'\n');
    for i=3:length(st2), fprintf(fid2, '%s \t', st2{i}); end
    
    for i=1:dimvar-desc
        if resout(i,nstat+2)>=sigthreshold,
            
            fprintf(fid2,'\n%s \t',vari{i}); % variable accounted for on this line
            
            for j=1:nstat+nplus+npost
                fprintf(fid2,'%.3f\t',resout(i,j)); % Each stat for this var
            end
        end
    end
    
    fclose(fid2);
    
    
    % --- CONDENSED ---
    
    nameout3=['Condensed ' in '.xls'];
    fid3 = fopen(nameout3,'w+');
    
    keepers = [nstat+1:nstat+nplus,nstat+nplus+1:nstat+nplus+ngauss+1];
    
    % Stats header lines
    for i=1:3, fprintf(fid3, '%s \t', st1{i}); end
    for i=1:length(keepers), fprintf(fid3, '%s \t', st1{keepers(i)+3}); end
    
    fprintf(fid3,'\n');
    
    for i=1:3, fprintf(fid3, '%s \t', st2{i}); end
    for i=1:length(keepers), fprintf(fid3, '%s \t', st2{keepers(i)+3}); end
    
    
    for i=1:dimvar-desc
        fprintf(fid3,'\n %d\t %d\t',reor(i),i);
        fprintf(fid3,'%s \t',vari{i}); % variable accounted for on this line
        
        for j=1:length(keepers)
            fprintf(fid3,'%.3f\t',resout(i,keepers(j))); % Each valuable stat for this var
        end
        
    end
    
    fclose(fid3);
    
end

end





function [F df1 df2 P] = Wtest(X,alpha)
%Welch's test for analysis of variance
%     Inputs:
%          X - data matrix (Size of matrix must be n-by-2; data=column 1, sample=column 2).
%       alpha - significance level (default = 0.05).
%

if nargin < 2,
    alpha = 0.05;
end

k=max(X(:,2));

%Statistics.
n=[];s2=[];m=[];
indice=X(:,2);
for i=1:k
    Xe=find(indice==i);
    eval(['X' num2str(i) '=X(Xe,1);']);
    eval(['n' num2str(i) '=length(X' num2str(i) ') ;']);
    eval(['s2' num2str(i) '=(std(X' num2str(i) ').^2) ;']);
    eval(['m' num2str(i) '=mean(X' num2str(i) ');']);
    eval(['xn= n' num2str(i) ';']);
    eval(['xs2= s2' num2str(i) ';']);
    eval(['xm= m' num2str(i) ';'])
    n=[n;xn];s2=[s2;xs2];m=[m;xm];
end

%Welch's Procedure.
ws=[];
for i=1:k
    eval(['w' num2str(i) '=n' num2str(i) '/s2' num2str(i) ';']);
    eval(['x= w' num2str(i) ';']);
    ws=[ws;x];
end

wps=[];
for i=1:k
    eval(['wp' num2str(i) '=w' num2str(i) '*m' num2str(i) ';']);
    eval(['x= wp' num2str(i) ';']);
    wps=[wps;x];
end

A=sum(ws);
D=sum(wps);

H=D/A;

b=[];
for i=1:k
    eval(['b' num2str(i) '=((1-(w' num2str(i) '/A)).^2)/(n' num2str(i) '-1);']);
    eval(['x=b' num2str(i) ';']);
    b=[b;x];
end

pw=[];
for i=1:k
    eval(['pws' num2str(i) '=(w' num2str(i) '*((m' num2str(i) '-H).^2)/(k-1));']);
    eval(['x=pws' num2str(i) ';']);
    pw=[pw;x];
end

E=sum(pw);
O=sum(b);

G=(1+2*(k-2)*O/(k^2-1));

F=E/G;  %Welch's F-statistic.
v1=(k-1);  %numerator degrees of freedom.
v2=(k^2-1)/(3*O);  %denominator degrees of freedom.
df1=v1;df2=v2;

% Because the denominator degrees of freedom are corrected and could results
% a fraction, the probability function associated to the F statistic is resolved
% by the Simpson's 1/3 numerical integration method.
x=linspace(.000001,F,100001);
DF=x(2)-x(1);
y=((v1/v2)^(.5*v1)/(beta((.5*v1),(.5*v2))));
y=y*(x.^((.5*v1)-1)).*(((x.*(v1/v2))+1).^(-.5*(v1+v2)));
N=length(x);
P=1-(DF.*(y(1)+y(N) + 4*sum(y(2:2:N-1))+2*sum(y(3:2:N-2)))/3.0);

end





function [F df1 df2 P] = BFtest(X,alpha)
%Brown-Forsythe's Test for analysis of variance
%[In the Brown-Forsythe's test the data are transforming to yij = abs[xij - median(xj)]
%and uses the F distribution performing an one-way ANOVA using y as the
%dependent variable. The Brown-Frosythe statistic is corrected for artificial zeros
%occurring in odd sized samples.
%
%     Inputs:
%          X - data matrix (Size of matrix must be n-by-2; data=column 1, sample=column 2).
%       alpha - significance level (default = 0.05).
%

if nargin < 2,
    alpha = 0.05;
end

Y=X;
k=max(Y(:,2));

%Brown-Forsythe's Procedure.
n=[];s2=[];Z=[];
indice=Y(:,2);
for i=1:k
    Ye=find(indice==i);
    eval(['Y' num2str(i) '=Y(Ye,1);']);
    eval(['mdY' num2str(i) '=median(Y(Ye,1));']);
    eval(['n' num2str(i) '=length(Y' num2str(i) ') ;']);
    eval(['s2' num2str(i) '=(std(Y' num2str(i) ').^2) ;']);
    eval(['Z' num2str(i) '= abs((Y' num2str(i) ') - mdY' num2str(i) ');']);
    eval(['xn= n' num2str(i) ';']);
    eval(['xs2= s2' num2str(i) ';']);
    eval(['x= Z' num2str(i) ';']);
    n=[n;xn];s2=[s2;xs2];Z=[Z;x];
end


Y=[Z Y(:,2)];

%Correction for artificial zeros occurring in odd sized samples.
for i=1:k
    Ye=find(Y(:,2)==i);
    ncero=find((Y(:,1)==0)&(Y(:,2)==i));
    Y(ncero,1)=999;
    Y(ncero,1)=min(Y(Ye,1));
end

%Analysis of variance procedure.
C=(sum(Y(:,1)))^2/length(Y(:,1)); %correction term.
SST=sum(Y(:,1).^2)-C; %total sum of squares.
dfT=length(Y(:,1))-1; %total degrees of freedom.

indice=Y(:,2);
for i=1:k
    Ye=find(indice==i);
    eval(['A' num2str(i) '=Y(Ye,1);']);
end

A=[];
for i=1:k
    eval(['x =((sum(A' num2str(i) ').^2)/length(A' num2str(i) '));']);
    A=[A,x];
end

SSA=sum(A)-C; %sample sum of squares.
dfA=k-1; %sample degrees of freedom.
SSE=SST-SSA; %error sum of squares.
dfE=dfT-dfA; %error degrees of freedom.
MSA=SSA/dfA; %sample mean squares.
MSE=SSE/dfE; %error mean squares.
F=MSA/MSE; %Brown-Forsythe's F-statistic.
v1=dfA;df1=v1;
v2=dfE;df2=v2;

P = 1 - fcdf(F,v1,v2);  %probability associated to the F-statistic.

end




