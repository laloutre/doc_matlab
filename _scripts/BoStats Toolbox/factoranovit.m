function res = factoranovit(matin,f,varargin)

% Factor ANOVA for dual effect of piece and timbre
% IN: Results
%     Factors to account for (default: 2 & 3, i.e. piece & timbre)
% OUT: for each var.: normality of distribution for each piece and timbre,
%                     homogeneity of variance wrt piece and timbre,
%                     anovan's F and p for piece, timbre and piece*timbre
%

if ischar(matin), resin=dlmread(matin); iname=matin;
else resin=matin; iname = inputname(1);
end

if length(iname)>8 && strcmp(iname(1:8),'Results_'), iname=iname(9:length(iname)); end

[desc factor ngps reorder vars groups colors catfields catlabels fname grname factwo] = stariables();

if nargin<2, f=factwo; end % Default factors columns (piece & timbre)

facnum = length(f);

interac=0; % Choose to compute or not interaction effects

ivar=1;
while ivar<length(varargin),
    if strcmp(varargin(ivar),'name'), iname = varargin{ivar+1}; ivar=ivar+1;
    elseif strcmp(varargin(ivar),'labels'), fname = varargin{ivar+1}; ivar=ivar+1;
    elseif strcmp(varargin(ivar),'groups'), grname = varargin{ivar+1}; ivar=ivar+1;
    elseif strcmp(varargin(ivar),'variables'), vars = varargin{ivar+1}; ivar=ivar+1;
    elseif strcmp(varargin(ivar),'interaction') || strcmp(varargin(ivar),'interactions'), 
        if isnumeric(varargin{ivar+1}), interac = varargin{ivar+1};
        elseif ischar(varargin{ivar+1}),
            if strcmp(varargin{ivar+1},'on'), interac=1;
            elseif strcmp(varargin{ivar+1},'off'), interac=0;
            end
        end
        ivar=ivar+1;
    end
    ivar=ivar+1;
end


[dim1 dim2] = size(resin);

for i=1:facnum, fnum(i)=max(resin(:,f(i))); end

if interac, res = zeros(dim2-desc, sum(fnum) + length(f) + length(f)+.5*length(f)*(length(f)-1)); 
else res = zeros(dim2-desc, sum(fnum) + length(f) + length(f));
end
% Columns: dis.norm for each group; hom.var. for each factor; ANOVAn p for each factor (and pair thereof if interactions)


for i=1:length(f),
    resort(i,:,:) = sortrows(resin,f(i));    
end



% ANOVAN IT
warning off;
for i=desc+1:dim2, % dep.vars loop
    for j=1:length(f), % factors loop
        
        % Normality of distribution
        for k=1:fnum(j), % groups loop
            rest = resort(j,find(resort(j,:,f(j))==k,1,'first'):find(resort(j,:,f(j))==k,1,'last'),i);
            
            if sum(abs(rest))>0,
                if length(rest)<4,
%                     distrest = normcdf(rest,mean(rest),std(rest));
%                     if sum(~isnan(distrest)), % test of cdf validity (at least one ~NaN)
%                         [ksbin ksp] = kstest(rest,[rest,distrest]);
%                         res(i-desc,sum(fnum(1:j-1)) + k) = ksp; % K-S p-value
%                     end
                else
                    [libin lip] = lillietest(rest); % Lilliefors correction to KS test
                    res(i-desc,sum(fnum(1:j-1)) + k) = lip; % K-S lillie p-value
                end
            end
        end
        
        
        % Levene's homogeneity of variance
        res(i-desc, sum(fnum) + j) = vartestn(resin(:,i),resin(:,f(j)),'off','robust');
                        
    end
    
    % Factor ANOVA
    if interac,
        p = anovan(resin(:,i),resin(:,f),'model','interaction','display','off');
    else p = anovan(resin(:,i),resin(:,f),'display','off');
    end
    res(i-desc,sum(fnum)+length(f)+1:sum(fnum)+length(f)+length(p)) = p;
end




% PRINTOUT
if interac, nameout = ['FANOVA' num2str(facnum) ' ' iname ' +interac.xls'];
else nameout = ['FANOVA' num2str(facnum) ' ' iname '.xls'];
end
fid = fopen(nameout,'w+');

% Line 1: Stats titles
fprintf(fid,'\t Normality of distribution: K-S / Lilliefors test: p-values');
for i=1:facnum
    for j=1:fnum(i), fprintf(fid,'\t'); end
end

fprintf(fid,'Levenes homogeneity of variance p-values');
for i=1:facnum, fprintf(fid,'\t'); end

fprintf(fid,'Factorial ANOVA p-values \n');

% Line 2: Factors
fprintf(fid,'\t');
for i=1:facnum
    fprintf(fid,'%s',fname{i});
    for j=1:fnum(i), fprintf(fid,'\t'); end
end

for i=1:facnum, fprintf(fid,'%s\t',fname{i}); end

for i=1:facnum, fprintf(fid,'%s\t',fname{i}); end
if interac,
    for i=1:facnum-1,
        for j=i+1:facnum,
            fprintf(fid,'%s/%s\t',fname{i},fname{j});
        end
    end
end

% Line 3: Groups
fprintf(fid,'\nVariables\t');
for i=1:facnum
    for j=1:fnum(i),
        fprintf(fid,'%s \t',grname{i,j});
    end
end



% Line x: Results
for i=1:size(res,1),
    fprintf(fid,'\n%s\t',vars{i});
    for j=1:size(res,2),
        fprintf(fid,'%.4f\t',res(i,j));
    end
end

fclose(fid);

end






















