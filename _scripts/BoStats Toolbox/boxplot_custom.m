function boxplot_custom(matin,labels,Z,sorter,autosave)

% matin: Standard Results: [REC#,piece,timbre,pianist,stats] * [whichever perfs]


[desc factor maxtim reorder variables timmm col] = stariables();

if ischar(matin), resin=dlmread(matin); iname=matin;
else resin=matin; iname=inputname(1);
end
if length(iname)>8 && strcmp(iname(1:8),'Results_'), iname = iname(9:length(iname));
elseif length(iname)>3 && strcmp(iname(1:3),'ZS_'), iname = iname(4:length(iname));
end

if nargin<2, labels = variables; end

if nargin<3, Z=0;
elseif strcmp(Z,'Z') || strcmp(Z,'z') || strcmp(Z,'zscore') || strcmp(Z,'Zscore'), Z=1;
elseif strcmp(Z,'res') || strcmp(Z,'R') || strcmp(Z,'r') || strcmp(Z,'results') || strcmp(Z,'Results'), Z=0;
end


if nargin<4, sorter=0;
elseif  sorter==1 || strcmp(sorter,'sort'),
    labels = cell(1,size(reorder,2));
    for i=1:length(reorder), labels{reorder(i)} = variables{i}; end
    sorter=1;
end

if nargin<5, autosave = 1;
elseif strcmp(autosave,'nosave'), autosave=0;
end

if Z, sz = resin;
else sz = transform_results(resin,sorter);
end

maxtim = max(sz(:,factor));

timrank=1;
for i=1:maxtim,
    if sum(sz(:,factor)==i), % There is at least one timbre ID corresponding to value i
        timon(timrank)=i;
        timrank=timrank+1;
    end
end
ntim=length(timon);

if maxtim>length(col), % Add colors to fit all timbres
    for i=length(col)+1:maxtim, col(i,:) = col(i-5,:)./2; end
end

clf;
hold off;

tops = max(max(abs(sz(:,desc+1:size(sz,2)))));
for j=desc+1:size(sz,2),
    boxplot(sz(:,j),sz(:,factor),'colors',col(timon,:),'symbol','+','widths',.6/ntim,'positions',j-desc+.8/ntim*(1:ntim));
    hold on;
    plot([j-desc,j-desc],[-tops tops],':k');
end

% Legend
set(gca,'XTick',1.5:1:size(sz,2)-desc+.5);
set(gca,'XTickLabel',labels);
for k=1:ntim, hold on; hplot(k)=plot(0,0,'+','Color',col(timon(k),:),'Visible','off'); end
legend(hplot(:),timmm(timon),'Location','SouthEast');
xlim([.9 size(sz,2)-desc+2]);
if autosave, hgsave(['Boxplot ' iname '.fig']); end

hold off;

end