function plot_all(moy,err)

% IN: Z-means and Z-err matrices for category-sorted results
% OUT: Figures for each category, with significantly set-apart timbres highlighted
% Default features: 335 vars, 11 categories, 5 timbres

% Name
iname = inputname(1);
if length(iname)>7 && strcmp(iname(1:7),'ZSmean_'), iname = iname(8:length(iname)); end



[desc factor ntim reorder variables timmm color catfields catlabels] = stariables();
catfields=catfields+1; % + desc. column

ntim=size(moy,1); % One line per factor
maxtim=max(moy(:,1));

if maxtim>length(color), % Add colors to fit all factor groups
    for i=length(color)+1:maxtim, color(i,:) = color(i-5,:)./2; end
end

Sorted_Vars = cell(1,size(reorder,2));
for i=1:length(reorder), Sorted_Vars{reorder(i)} = variables{i}; end

hold off;

for n=1:size(catfields,1),
    for i=1:ntim,
        plot(moy(i,catfields(n,1):catfields(n,2)),'o-','Color',color(moy(i,1),:),'LineWidth',1.5);
        hold on;
        errorbar(moy(i,catfields(n,1):catfields(n,2)),err(i,catfields(n,1):catfields(n,2)),'Color',color(moy(i,1),:));
        hold on;
    end

    signipoint(catfields(n,1):catfields(n,2),ntim,moy,err,color);
    
    set(gca,'XTick',1:catfields(n,2)-catfields(n,1)+1);
    set(gca,'XTickLabel',Sorted_Vars(catfields(n,1)-1:catfields(n,2)-1));
    for i=1:ntim, hold on; hplot(i)=plot(0,0,'-','LineWidth',1.5,'Color',color(moy(i,1),:)); end
    legend(hplot(:),timmm(moy(:,1)),'Location','East');

    hgsave(['ResFig ' iname ' ' num2str(n) ' ' catlabels{n} '.fig']);
    hold off;

end
end


function signipoint(v,ntim,moy,err,color)

for j=1:length(v),
    for i=1:ntim
        signi=0; % tag
        for i2=1:ntim
            if i2~=i && ((moy(i2,v(j))+err(i2,v(j))<moy(i,v(j))) || (moy(i2,v(j))-err(i2,v(j))>moy(i,v(j)))), signi=signi+1; end
        end
        if signi==4, plot(j,moy(i,v(j)),'ok','MarkerSize',10,'MarkerFaceColor',color(moy(i,1),:)); hold on; end
    end
end
end


