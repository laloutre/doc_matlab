function plot_custom(resin,varnames, nameout)

% function plot_select(moy,err,select,sep,selsort)

if nargin<3, nameout = 0; end

[desc factor ntim reorder variables timmm color] = stariables();

[sz moy err] = transform_results(resin,0);


hold off;


% Plot
for i=1:ntim,
    plot(moy(i,2:size(moy,2)),'o-','Color',color(moy(i,1),:),'LineWidth',1.5);
    hold on;
    errorbar(moy(i,2:size(moy,2)),err(i,2:size(moy,2)),'Color',color(moy(i,1),:));
    hold on;
end
signipoint(2:size(moy,2),ntim,moy,err,color);
set(gca,'XTick',1:size(moy,2)-1);

set(gca,'XTickLabel',varnames);
for i=1:ntim, hold on; hplot(i)=plot(0,0,'-','LineWidth',1.5,'Color',color(moy(i,1),:)); end
legend(hplot(:),timmm(moy(:,1)),'Location','SouthEast');

hold off;

if nameout, hgsave([nameout '.fig']); end


end

function signipoint(v,ntim,moy,err,color)

for j=1:length(v),
    for i=1:ntim
        signi=0; % tag
        for i2=1:ntim
            if i2~=i && ((moy(i2,v(j))+err(i2,v(j))<moy(i,v(j))) || (moy(i2,v(j))-err(i2,v(j))>moy(i,v(j)))), signi=signi+1; end
        end
        if signi==4, plot(j,moy(i,v(j)),'ok','MarkerSize',10,'MarkerFaceColor',color(moy(i,1),:)); hold on; end
    end
end
end


