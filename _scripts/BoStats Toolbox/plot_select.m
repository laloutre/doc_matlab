function plot_select(moy,err,select,sep,selsort)

% IN: Z-means and Z-err matrices for category-sorted results
%     + vector of vars to select (rank in sorted order)
% OUT: Figure with significantly set-apart timbres highlighted
% Option sep: figures by category
% Default features: 335 vars reduced to selection, 11 categories, 5 timbres

if nargin<4, sep=0;
elseif strcmp(sep,'sep')||strcmp(sep,'separate'), sep=1;
end

if nargin<5, selsort=1;
elseif strcmp(selsort,'sort'), selsort=1;
elseif strcmp(selsort,'nosort'), selsort=0;
end

% Name
iname = inputname(1);
if length(iname)>7 && strcmp(iname(1:7),'ZSmean_'), iname = iname(8:length(iname)); end

[desc factor ntim reorder variables timmm color catfields catlabels] = stariables();
catfields=catfields+1; % + desc. column

ntim=size(moy,1); % One line per factor
maxtim=max(moy(:,1));

if maxtim>length(color), % Add colors to fit all factor groups
    for i=length(color)+1:maxtim, color(i,:) = color(i-5,:)./2; end
end

Sorted_Vars = cell(1,size(reorder,2));
for i=1:length(reorder), Sorted_Vars{reorder(i)} = variables{i}; end


% Build matrices of selected vars only
moytmp = zeros(size(moy,1),length(select)+1);
errtmp = zeros(size(err,1),length(select)+1);
% rank=1;
moytmp(:,1)=moy(:,1);
errtmp(:,1)=err(:,1);

if selsort,
    if size(select,1)==1, sel=sortrows(select');
    else sel=sortrows(select);
    end
else
    if size(select,1)==1, sel=select';
    else sel=select;
    end
end

% Delete out of bound selections
maxind=size(sel,1);
while(maxind>=1 && sel(maxind)>size(moy,2)-1), sel(maxind)=[]; maxind=maxind-1; end

for j=1:size(sel,1),
    moytmp(:,j+1) = moy(:,sel(j)+1);
    errtmp(:,j+1) = err(:,sel(j)+1);
end

hold off;

if ~sep,
    % Plot
    for i=1:ntim,
        plot(moytmp(i,2:size(moytmp,2)),'o-','Color',color(moy(i,1),:),'LineWidth',1.5);
        hold on;
        errorbar(moytmp(i,2:size(moytmp,2)),errtmp(i,2:size(moytmp,2)),'Color',color(moy(i,1),:));
        hold on;
    end
    signipoint(2:size(moytmp,2),ntim,moytmp,errtmp,color);
    set(gca,'XTick',1:size(moytmp,2)-1);

    set(gca,'XTickLabel',Sorted_Vars(sel));
    for i=1:ntim, hold on; hplot(i)=plot(0,0,'-','LineWidth',1.5,'Color',color(moy(i,1),:)); end
    legend(hplot(:),timmm(moy(:,1)),'Location','SouthEast');

    hgsave(['SelFig ' iname ' 0 all.fig']);
    hold off;

else
    for n=1:size(catfields,1),
        seltmp=find(sel>=catfields(n,1),1,'first'):find(sel<=catfields(n,2),1,'last');
        if ~isempty(seltmp),
            for i=1:ntim,
                plot(moy(i,sel(seltmp)+1),'o-','Color',color(moy(i,1),:),'LineWidth',1.5);
                hold on;
                errorbar(moy(i,sel(seltmp)+1),err(i,sel(seltmp)+1),'Color',color(moy(i,1),:));
                hold on;
            end

            signipoint(sel(seltmp)+1,ntim,moy,err,color);

            set(gca,'XTick',1:length(seltmp)+1);
            set(gca,'XTickLabel',Sorted_Vars(sel(seltmp)));
            for i=1:ntim, hold on; hplot(i)=plot(0,0,'-','LineWidth',1.5,'Color',color(moy(i,1),:)); end
            legend(hplot(:),timmm(moy(:,1)),'Location','East');

            hgsave(['SelFig ' iname ' ' num2str(n) ' ' catlabels{n} '.fig']);
            hold off;
        end
    end
end
end

function signipoint(v,ntim,moy,err,color)

for j=1:length(v),
    for i=1:ntim
        signi=0; % tag
        for i2=1:ntim
            if i2~=i && ((moy(i2,v(j))+err(i2,v(j))<moy(i,v(j))) || (moy(i2,v(j))-err(i2,v(j))>moy(i,v(j)))), signi=signi+1; end
        end
        if signi==4, plot(j,moy(i,v(j)),'ok','MarkerSize',10,'MarkerFaceColor',color(moy(i,1),:)); hold on; end
    end
end
end


