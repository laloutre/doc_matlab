function [restmp sz szmean szerr] = kiviatit(resin,sdize,select,signs,varnames)

% Kiviat plot, aka radar/spiderweb/PES plot
% IN: Full Results matrix
%     Selection of features to plot (column number minus descriptors, i.e. from 1 to 335)
%             Features to be written in line, with as many rows as separate plots to output
%     Direction of each feature plotting (+/-1)
%     Standard error (per timbre) display options: 0=no; numeric=shades (X-SE-large); 'error bars'

alph=.1;
whiskers=.05;
reverse_sign_marker = ' - '; % Indicates a reversed display before a feature label

[desc factor ngps reorder variables groups colors] = stariables;

if nargin<2, sdize=0; end

if nargin<3,
    disp('No selection set: a default one will be used.');
    select=[119 175 180 127 46 59 155 154]; 
     signs=[1 -1 -1 -1 -1 1 -1 -1];
    
    % Default 8 selection: 
    % MHV, 
    % MHV SDc, 
    % Slope of key attack SDc, 
    % Sharpness of attack slope, 
    % Overlap length same-hand, 
    % Chord duration SDc, 
    % Sustain pedal depression length, 
    % Soft pedal depression length
    
elseif nargin<4, signs = ones(size(select,1),size(select,2));  % positive direction by default if there is a selection
end

if strcmp(select,'all'), 
    select = 1:size(resin,2)-desc;
%     signs = ones(1,length(select));
end

if ~sum(sum(select)), 
    disp('No selection set: a default one will be used.');
    select=[119 175 180 127 46 59 155 154]; 
end
if ~sum(sum(signs)), signs=[1 -1 -1 -1 -1 1 -1 -1]; end

% Test signs for failure
if size(signs,1)<size(select,1),
    disp('Selection and sign input matrices do not match in size. Auto-completion with ones.');
    signs(size(signs,1)+1:size(select,1),:)=1;
end

if size(signs,2)<size(select,2),
    disp('Selection and sign input matrices do not match in size. Auto-completion with ones.');
    signs(:,size(signs,2)+1:size(select,2))=1;
end

for i=1:size(select,1),
    for j=1:size(select,2),
        if select(i,j)~=0 && signs(i,j)==0, 
            fprintf('No sign indicated for row %d column %d. Auto-completion with 1.\n',i,j);
            signs(i,j)=1;
        end
    end
end

% if size(select,2)==1, select = select'; end

% Select

numsel = size(select,1)*size(select,2) - sum(sum(select==0)); % #Rows * #Columns - # of zeroes

restmp = zeros(size(resin,1),desc+numsel);

restmp(:,1:desc) = resin(:,1:desc);

column = 0; % count the affectation

for i=1:size(select,1) % Each row/subplot
    j=1;
    while j<=size(select,2),
        if select(i,j),
            column = column+1;
            restmp(:,desc+column) = signs(i,j) .* resin(:,desc+select(i,j));
            j=j+1;
        else j=j+1;
        end
    end
end        


% Zscore
[sz szmean szerr] = transform_results(restmp);


% Plot szmean and szerr

scrsz = get(0,'ScreenSize'); % [left, bottom, width, height]
h=zeros(1,size(select,1));

feat=0; % Index for previously plotted features

for i=1:size(select,1) % Number of separate plots
    h(i) = figure('Color',[1,1,1],'Name','Kiviat plot','NumberTitle','off','Position',[10 50 scrsz(3)-20 scrsz(4)-150]);
    
    nbfeats = sum(select(i,:)~=0);
    seltmp = select(i,select(i,:)~=0);
    signtmp = signs(i,signs(i,:)~=0);
    
    for f=1:nbfeats,
        
        % Line plot
        angle = 2*pi*(f-1)/nbfeats;
        hold on;
        plot([0 3*cos(angle)],[0 3*sin(angle)],'-k');
        
        % Feature label
        if f==0, % East
            horaz='left'; vertal='middle'; % Default
        elseif (f-1)/nbfeats<.25, % NE quadrant
            horaz='left'; vertal='middle'; % Default
        elseif (f-1)/nbfeats==.25 % North
            horaz='center'; vertal='bottom';            
        elseif (f-1)/nbfeats<.5 % NW quadrant
            horaz='right'; vertal='middle';
        elseif (f-1)/nbfeats==.5 % West
            horaz='right'; vertal='middle';
        elseif (f-1)/nbfeats<.75 % SW quadrant
            horaz='right'; vertal='middle';
        elseif (f-1)/nbfeats==.75 % South
            horaz='center'; vertal='top';
        else % SE quadrant
            horaz='left'; vertal='middle';
        end
        
        % Feature sign
        if signtmp(f)<0, sigo=reverse_sign_marker; else sigo = ''; end % (minus)
        if nargin<5 || (~iscell(varnames) && ~sum(varnames~=0)), varnames(f) = variables(seltmp(f)); end
        
        
        text(3.01*cos(angle),3.01*sin(angle),[sigo varnames{f}],'FontSize',16,'HorizontalAlignment',horaz,'VerticalAlignment',vertal);
    end
    
    
    % Plot values on line
    for t=1:size(szmean,1), % Each timbre
        hold on;
        plot((1.5+szmean(t,[2+feat:1+feat+nbfeats,2+feat])).*cos(2*pi*(0:nbfeats)/nbfeats),(1.5+szmean(t,[2+feat:1+feat+nbfeats,2+feat])).*sin(2*pi*(0:nbfeats)/nbfeats),'-o','Color',colors(t,:),'LineWidth',2,'MarkerFaceColor',colors(t,:));
        
        hold on;
        hplot(t)=plot(0,0,'-','LineWidth',2,'Color',colors(t,:));
    end
    
    
    % Plot standard errors per timbre
    if isnumeric(sdize) && sdize>0, % Shading
        for t=1:size(szmean,1), % Each timbre
            for f=1:nbfeats,
                if f<nbfeats,
                    hold on;
                    x1= (1.5+szmean(t,1+feat+f)-sdize*szerr(t,1+feat+f))*cos(2*pi*(f-1)/nbfeats);
                    x2= (1.5+szmean(t,1+feat+f)+sdize*szerr(t,1+feat+f))*cos(2*pi*(f-1)/nbfeats);
                    x3= (1.5+szmean(t,1+feat+f+1)+sdize*szerr(t,1+feat+f+1))*cos(2*pi*f/nbfeats);
                    x4= (1.5+szmean(t,1+feat+f+1)-sdize*szerr(t,1+feat+f+1))*cos(2*pi*f/nbfeats);
                    
                    y1= (1.5+szmean(t,1+feat+f)-sdize*szerr(t,1+feat+f))*sin(2*pi*(f-1)/nbfeats);
                    y2= (1.5+szmean(t,1+feat+f)+sdize*szerr(t,1+feat+f))*sin(2*pi*(f-1)/nbfeats);
                    y3= (1.5+szmean(t,1+feat+f+1)+sdize*szerr(t,1+feat+f+1))*sin(2*pi*f/nbfeats);
                    y4= (1.5+szmean(t,1+feat+f+1)-sdize*szerr(t,1+feat+f+1))*sin(2*pi*f/nbfeats);
                    
                    fill([x1 x2 x3 x4],[y1 y2 y3 y4],colors(t,:),'EdgeColor','none','FaceAlpha',alph);
                    
                else
                    x1= (1.5+szmean(t,1+feat+f)-sdize*szerr(t,1+feat+f))*cos(2*pi*(f-1)/nbfeats);
                    x2= (1.5+szmean(t,1+feat+f)+sdize*szerr(t,1+feat+f))*cos(2*pi*(f-1)/nbfeats);
                    x3= (1.5+szmean(t,1+feat+1)+sdize*szerr(t,1+feat+1));
                    x4= (1.5+szmean(t,1+feat+1)-sdize*szerr(t,1+feat+1));
                    
                    y1= (1.5+szmean(t,1+feat+f)-sdize*szerr(t,1+feat+f))*sin(2*pi*(f-1)/nbfeats);
                    y2= (1.5+szmean(t,1+feat+f)+sdize*szerr(t,1+feat+f))*sin(2*pi*(f-1)/nbfeats);
                    y3= 0;
                    y4= 0;
                    
                    fill([x1 x2 x3 x4],[y1 y2 y3 y4],colors(t,:),'EdgeColor','none','FaceAlpha',alph);
                end
            end
        end
        
    elseif strcmp(sdize,'errorbars') || strcmp(sdize,'error bars'),
        
        for t=1:size(szmean,1), % Each timbre
            for f=1:nbfeats,
                hold on;
                x1= (1.5+szmean(t,1+feat+f)-szerr(t,1+feat+f))*cos(2*pi*(f-1)/nbfeats);
                x2= (1.5+szmean(t,1+feat+f)+szerr(t,1+feat+f))*cos(2*pi*(f-1)/nbfeats);
                xadd = whiskers*sin(2*pi*(f-1)/nbfeats);
                
                y1= (1.5+szmean(t,1+feat+f)-szerr(t,1+feat+f))*sin(2*pi*(f-1)/nbfeats);
                y2= (1.5+szmean(t,1+feat+f)+szerr(t,1+feat+f))*sin(2*pi*(f-1)/nbfeats);
                yadd = whiskers*cos(2*pi*(f-1)/nbfeats);
                
                plot([x1 x2],[y1 y2],'Color',colors(t,:),'LineWidth',1.5);
                plot([x1-xadd x1+xadd],[y1+yadd y1-yadd],'Color',colors(t,:),'LineWidth',1.5);
                plot([x2-xadd x2+xadd],[y2+yadd y2-yadd],'Color',colors(t,:),'LineWidth',1.5);
            end
        end
    end
    
    % Legend
    legend(hplot(:),groups,'Location','SouthEastOutside');
    axis square;
    axis off;
    
    feat = feat+nbfeats;
    
end



end