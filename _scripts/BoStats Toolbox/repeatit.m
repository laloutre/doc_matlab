function [resout resort sorter] = repeatit(matin,name,matcher,factol,varsnames)

% Batch for repeated-measures ANOVA
%
% Matin: one line per sample; C1= REC#, C2= piece#, C3= timbre, C4= pianist, C5:end= vars
%
% Resout: one line per var;
%
%       C1:C[ntim+1=6]: Normality of distributions p-value for each group (collapsed), + sum
%       C[ntim+2=7]: Levene's homogeneity of variance p-values
%       C[ntim+3=8]: Mauchly's sphericity p-value
%       C[ntim+4=9]: Huynh-Feldt epsilon correction term (to F-value, against lack of sphericity)
%
%
%       C[nstats+1=10]:C[nstats+4=13]: [df F p fxsize] between Groups:                    "Groups" = dependant var. (timbre) i.e. between-subjects factor
%                                                                                          (p<=0.05 => lines (one pianist, one piece, one timbre each) grouped by timbre differ significantly from one timbre group to the others, wrt the dep. var.)
%
%       C[nstats+5=14]:C[nstats+7=16]: [F df p] Welch robust test of equality of means (RM-improved)
%
%       C[nstats+8=17]:C[nstats+10=19]: [Chi� p fxsize] Kruskal-Wallis non-parametric test (collapsed)
%
%       C[nstats+11=20]:C[nstats+12=21] Relevant p-value and fx size (depending on assumptions)
%
%       C[nstats+ngroup+1=22]:C[nstats+ngroup+6=27]: [df F p fxsize dfHF pHF] in Time:    "Time" = repetition, i.e. within-subject factor
%                                                                                          (p<=0.05 => repetitions by one pianist on one piece of the same timbre are not consistent wrt the dep. var.)
%
%       C[nstats+ngroup+7=28]:C[ntim+ngroup+12=33]: [df F p fxsize dfHF pHF] Interaction: "Interaction" = Time-by-group effect, i.e. between-subjects interaction effect
%
%       C[nstats+ngroup+13=34]:C[nstats+ngroup+17=38]: [df F p fxsize pHF] Subjects:      "Subjects" = Matching between rows/repetitions, i.e. similarity between repetition-subjects,
%                                                                                          i.e. repeated-measures matching effectiveness (test of improvement of RM over straight-up ANOVA),
%                                                                                          or, how each line (one pianist, one piece, one timbre) varies from the others:
%                                                                                          (p<=0.05 => lines differ, i.e. when pianist, piece and timbre differ, dep.var. changes (duh))
%
%       C[nstats+ngroup+nrepeat+1=39]:C[nstats+ngroup+nrepeat+ngauss+1=49]: Post-Hoc pairwise comparisons (+ sum)
%


printout=1; % Set to 1 to print xlses out

if ischar(matin), resin=dlmread(matin); else resin=matin; end

[desc factor ntim reor vari groups] = stariables();

if nargin<3, matcher=resin(:,2:4); factol = factor-1; end

[dimsamp dimvar] = size(resin);

if nargin>3 && iscell(varsnames), vari = varsnames; end

% Reorder for repeat
% Output 3D matrix: Samples * repeated measures * vars, Samples * matching factors
[resort sorter] = sortasrep(resin(:,desc+1:dimvar),matcher);


nstats = ntim+4; % (ntim+1) normality of distributions + Levene's p + sphericity + Huynh-Feldt epsilon correction
ngroup = 12; % Number of group variance comparison values
nrepeat = 17; % Number of repeated measures values: 3 RM * [from 4 to 6] stats each
reps = size(resort,2); % Number of repetitions
ngauss = ntim*(ntim-1)/2;
nposthoc = ngauss + 1; % # of post-hoc stats

resout = zeros(dimvar-desc,nstats+ngroup+nrepeat+nposthoc); % Assumptions + ANOVA + RM

warning off;

% GO
for i=1:dimvar-desc % Loop on vars

    % ASSUMPTIONS

    % Normality of distributions, collapsed
    % K-S tests of normal distribution (for each timbre)
    for t=1:ntim
        rest = resort(sorter(:,factol)==t,i);
        if sum(abs(rest))>0,
            if length(rest)<4,
                distrest = normcdf(rest,mean(rest),std(rest));
                if sum(~isnan(distrest)), % test of cdf validity (at least one ~NaN)
                    [ksbin ksp] = kstest(rest,[rest,distrest]);
                    resout(i,t) = ksp; % K-S p-value
                end
            else
                [libin lip] = lillietest(rest); % Lilliefors correction to KS test
                resout(i,t) = lip; % K-S lillie p-value
            end
        end
    end
    resout(i,ntim+1) = sum(resout(i,1:ntim)>.05); % Number of normally-distributed collapsed groups
    
    % Levene's homogeneity of variance, collapsed
    resout(i,ntim+2) = vartestn(mean(resort(:,:,i),2),sorter(:,factol),'off','robust');
    if isnan(resout(i,ntim+2)) && ~strcmp(vari{i},''), resout(i,ntim+2) = 0; end

    % Sphericity
    resout(i,ntim+3) = Mauspher(resort(:,:,i));

    % Huynh-Feldt epsilon correction
    resout(i,ntim+4) = epsHF(resort(:,:,i));



    % REPEATED MEASURES

    tmpcell = cell(1,ntim);
    for t=1:ntim,
        tmpcell{1,t} = resort(sorter(:,factol)==t,:,i);
    end

    [p table anostats] = anova_rm(tmpcell);

    % Between-subjects: groups
    resout(i,nstats+1) = table{3,3}; % df Groups
    resout(i,nstats+2) = table{3,5}; % F Groups
    resout(i,nstats+3) = table{3,6}; % p Groups
    resout(i,nstats+4) = table{3,2}/table{7,2}; % Effect size Groups
    
    % Welch improved
    [Fwi df1wi df2wi Pwi] = Wmetatest([resin(:,i+desc) resin(:,factor)],[mean(resort(:,:,i),2) sorter(:,factol);]);
    resout(i,nstats+5) = Fwi;
    resout(i,nstats+6) = df2wi;
    resout(i,nstats+7) = Pwi;
    
    if isnan(resout(i,nstats+3)) && ~strcmp(vari{i},''), resout(i,nstats+[2:7]) = [0 1 0 0 0 1]; end
    
    % Kruskal-Wallis non-parametric test
    [kwp kwtab kwstats] = kruskalwallis(mean(resort(:,:,i),2),sorter(:,factol),'off');
    if ~isempty(kwtab{2,5}), resout(i,nstats+8) = kwtab{2,5}; end % chi-square
    resout(i,nstats+9) = kwp; % p-value
    resout(i,nstats+10) = kwtab{2,2}/kwtab{4,2}; % Effect size
    
    % Selection of the relevant p-value (depending on assumptions)
    if resout(i,ntim+1)>2, % Enough normal distributions
        if resout(i,ntim+2)>0.05, % Homogeneous variances => ANOVA
            resout(i,nstats+11) = resout(i,nstats+3);
        else % Welch
            resout(i,nstats+11) = resout(i,nstats+7);
        end
        resout(i,nstats+12) = resout(i,nstats+4); % Effect size (ANOVA)
        
    else % Nonparametric KW
        resout(i,nstats+11) = resout(i,nstats+9);
        resout(i,nstats+12) = resout(i,nstats+10); % Effect size (KW)
    end
    
    
    
    % Within subjects
    % Time
    resout(i,nstats+ngroup+1) = table{2,3}; % df Time
    resout(i,nstats+ngroup+2) = table{2,5}; % F Time
    resout(i,nstats+ngroup+3) = table{2,6}; % p Time
    resout(i,nstats+ngroup+4) = table{2,2}/table{7,2}; % Effect size Time
    resout(i,nstats+ngroup+5) = table{2,3} * resout(i,ntim+4); % HF-corrected df Time
    resout(i,nstats+ngroup+6) = 1 - fcdf(table{2,5}, resout(i,nstats+ngroup+5), table{5,3}*resout(i,nstats+ngroup+5)); % HF-corrected p-value Time
    % p2 = 1 - [cumulative distribution function](F.t,dft.v2,(n.samples-n.groups)*dft.v2)

    % Time-by-group
    resout(i,nstats+ngroup+7) = table{4,3}; % df Interaction
    resout(i,nstats+ngroup+8) = table{4,5}; % F Interaction
    resout(i,nstats+ngroup+9) = table{4,6}; % p Interaction
    resout(i,nstats+ngroup+10) = table{4,2}/table{7,2}; % Effect size Interaction
    resout(i,nstats+ngroup+11) = table{4,3} * resout(i,ntim+4); % HF-corrected df Interaction
    resout(i,nstats+ngroup+12) = 1 - fcdf(table{4,5}, resout(i,nstats+ngroup+11), table{5,3}*resout(i,nstats+ngroup+5)); % HF-corrected p-value Interaction
    % p2 = 1 - [cumulative distribution function](F.I,dfI.v2,(n.samples-n.groups)*dft.v2)

    % Error-time
    resout(i,nstats+ngroup+13) = table{5,3}; % df Subjects
    resout(i,nstats+ngroup+14) = table{5,5}; % F Subjects
    resout(i,nstats+ngroup+15) = table{5,6}; % p Subjects
    resout(i,nstats+ngroup+16) = table{5,2}/table{7,2}; % Effect size Subjects
    resout(i,nstats+ngroup+17) = 1 - fcdf(table{5,5}, table{5,3}, table{5,3}*resout(i,nstats+ngroup+5)); % HF-corrected p-value Subjects
    % p2 = 1 - [cumulative distribution function](F.s,df.s,(n.samples-n.groups)*dft.v2)
    
    if isnan(resout(i,ntim+3)) && ~strcmp(vari{i},''), resout(i,[ntim+[3 4], nstats+ngroup+[2:6,8:12,14:17] ]) = [0 0 0 1 0 0 1 0 1 0 0 1 0 1 0 1]; end
    
    
    
    % POST HOC
    if ~isnan(resout(i,nstats+10)),
        
        % Choose input stats: ANOVA or KW
        if resout(i,ntim+1)>2, pstats = anostats; else pstats = kwstats; end
        
        % Pairwise differences
        if length(pstats.n)>1,
            c = multcompare(pstats,'Display','off');

            if size(c,1)==ngauss,
                for j=1:ngauss, resout(i,nstats+ngroup+nrepeat+j) = (c(j,3)*c(j,5))>0;
                    % If Boundaries on 95% C.I. for (timbre x ~= timbre y) are of same sign, zero is out thus their difference is significant
                end
            else
                for j=1:size(c,1),
                    place=0;
                    numtim = length(pstats.gnames);
                    if iscell(pstats.gnames),
                        for q=1:numtim, timon(q)= str2double(pstats.gnames{q}); end
                    else
                        for q=1:numtim, timon(q)= str2double(pstats.gnames(q)); end
                    end

                    for t1=1:timon(c(j,1))-1,
                        for t2=t1+1:ntim,
                            place=place+1;
                        end
                    end
                    for t2=timon(c(j,1))+1:timon(c(j,2)), place=place+1; end

                    resout(i,nstats+ngroup+nrepeat+place) = (c(j,3)*c(j,5))>0;
                end
            end

            % Total number of sig. pairwise tests
            resout(i,nstats+ngroup+nrepeat+ngauss+1) = sum(resout(i,nstats+ngroup+nrepeat+1:nstats+ngroup+nrepeat+ngauss));
            
        else resout(i,nstats+ngroup+nrepeat+(1:nposthoc)) = NaN;
        end

    else resout(i,nstats+ngroup+nrepeat+(1:nposthoc)) = NaN;
    end
end

warning on;


% PRINTOUT

if printout,

    % Name the output files
    if nargin<2 || isempty(name) || ~sum(name~=0),
        if ischar(matin), in=matin;
        else in=inputname(1);
        end
    else in=name;
    end
    
    if length(in)>8 && strcmp(in(1:8),'Results_'), in = in(9:length(in)); end

    % HEADER    
    % Storage vector
    header = cell(1,nstats+ngroup+nrepeat+nposthoc);
    
    % Assumptions
    for t=1:ntim, header(t) = {['KS p ' groups{t} ' collapsed']}; end
    header(ntim+1:ntim+4) = {'Sum KS collapsed','Levene p collapsed','Sph. p','HF epsilon'};

    % ANOVA
    header(nstats+1:nstats+ngroup) = {'df Group','F Group','p Group','Fx size Gp','Welch F Gp','Welch df2 Gp','Welch p Gp','KW Chi2 collapsed','KW p collapsed','KW fx size collapsed','BEST P-VALUE','BEST FX SIZE'};

    % RM
    lab1={'df','F','p','Fx size','df HF','p HF'};
    lab2={'Time','Interaction','Subjects'};
    
    countrm=1;
    for i=1:length(lab2),
        for j=1:length(lab1),
            if sum([i,j]~=[3 5]), 
                header(nstats+ngroup+countrm) = {[lab1{j} ' ' lab2{i}]};
                countrm = countrm+1;
            end
        end
    end
    
    % Post Hoc
    countph=1;
    for i=1:ntim-1,
        for j=i+1:ntim,
            header(nstats+ngroup+nrepeat+countph) = {[groups{i} ' vs. ' groups{j}]};
            countph = countph+1;
        end
    end
    header(nstats+ngroup+nrepeat+nposthoc) = {'Pairwise Sig. Total'};
    
    % PRINT DATA
    nameout=['Repeater ' in '.xls'];
    fid = fopen(nameout,'w+');
    
    fprintf(fid,'\t');
    for i=1:length(header), fprintf(fid,'%s \t',header{i}); end
    
    for i=1:size(resort,3),
        fprintf(fid,'\n %s \t',vari{i});
        for t=1:size(resout,2), fprintf(fid,'%.3f\t',resout(i,t)); end
    end
    fclose(fid);



    % PRINTOUT CONDENSED

    nameout2=['Repeater ' in ' condensed.xls'];
    fid2 = fopen(nameout2,'w+');

    % Selected stats to display
    
    condenser = [ntim+[1 2],nstats+[3 4 7 9 10 11 12],nstats+ngroup+[4 6],nstats+ngroup+nrepeat+(1:nposthoc)]; 
    % Sum of norm.distr., Levene p collapsed, ANOVA [p, fxsize, Welch p, KW p, KW fxsize], Best p-value, Best fxsize, RM Time*[fx, pHF], Post-Hoc
    
    fprintf(fid2,'\t');
    for i=1:length(condenser), fprintf(fid2,'%s \t',header{condenser(i)}); end

    % DATA
    for i=1:size(resort,3),
        fprintf(fid2,'\n %s \t',vari{i});
        for t=1:length(condenser), fprintf(fid2,'%.3f\t',resout(i,condenser(t))); end
    end
    fclose(fid2);
    
    
    
    % PRINTOUT SIG VARS ONLY
    
    nameout3=['Repeater ' in ' Sig.xls'];
    fid3 = fopen(nameout3,'w+');
    
    fprintf(fid3,'\t');
    for i=1:length(header), fprintf(fid3,'%s \t',header{i}); end
    
    for i=1:size(resort,3),
        if resout(i,nstats+11)<=0.05, % Sig.!
            fprintf(fid3,'\n %s \t',vari{i});
            for t=1:size(resout,2), fprintf(fid3,'%.3f\t',resout(i,t)); end
        end
    end
    
    
    fclose(fid3);
end

end




function [resout matchout] = sortasrep(resin,matchcol)

if size(resin,1)~=size(matchcol,1), disp('Different sizes of resin and matcher; fail');
else

    resout=zeros(1,1,size(resin,2));
    matchout(1,:)=matchcol(1,:);

    tmprank = ones(1,size(resin,1));

    for k=1:size(resin,1), % loop on samples
        newline=1;
        for i=1:size(resout,1), % Loop on stored repetition-samples
            if sum(matchcol(k,:)==matchout(i,:)) == size(matchcol,2),
                resout(i,tmprank(i),:) = resin(k,:);
                tmprank(i) = tmprank(i)+1;
                newline=0;
            end
        end
        if newline,
            matchout(size(matchout,1)+1,:) = matchcol(k,:);
            resout(size(resout,1)+1,1,:) = resin(k,:);
            tmprank(size(matchout,1)) = tmprank(size(matchout,1)) +1;

        end
    end
end

end



function [p, table, stats] = anova_rm(X)
%   [p, table] = anova_rm(X)
%   Single factor, repeated measures ANOVA.
%
%   [p, table] = anova_rm(X) performs a repeated measures ANOVA for 
%   comparing the means of two or more columns (time) in one or more
%   samples(groups). Unbalanced samples (i.e. different number of subjects
%   per group) is supported though the number of columns (followups)should
%   be the same. For a design with only one group and two or more 
%   follow-ups, X should be a matrix with one row for each subject.
%   In a design with multiple groups, X should be a cell array of matrices.
%
%   Copyright 2008, Arash Salarian
%

if ~iscell(X)
    X = {X};
end

%number of groups
s = size(X,2);

%subjects per group
n_h = zeros(s, 1);
for h=1:s
    n_h(h) = size(X{h}, 1);
end
n = sum(n_h);

%number of follow-ups
t = size(X{1},2);

% overall mean
y = 0;
for h=1:s
    y = y + sum(sum(X{h}));
end
y = y / (n * t);

% allocate means
y_h = zeros(s,1);
y_j = zeros(t,1);
y_hj = zeros(s,t);
y_hi = cell(s,1);
for h=1:s
    y_hi{h} = zeros(n_h(h),1);
end

% group means
for h=1:s
    y_h(h) = sum(sum(X{h})) / (n_h(h) * t);
end

% follow-up means
for j=1:t
    y_j(j) = 0;
    for h=1:s
        y_j(j) = y_j(j) + sum(X{h}(:,j));
    end
    y_j(j) = y_j(j) / n;
end

% group h and time j mean
for h=1:s
    for j=1:t
        y_hj(h,j) = sum(X{h}(:,j) / n_h(h));
    end
end

% subject i'th of group h mean
for h=1:s
    for i=1:n_h(h)
        y_hi{h}(i) = sum(X{h}(i,:)) / t;
    end
end

% calculate the sum of squares
ssG = 0;
ssSG = 0;
ssT = 0;
ssGT = 0;
ssR = 0;

for h=1:s
    for i=1:n_h(h)
        for j=1:t
            ssG  = ssG  + (y_h(h) - y)^2;
            ssSG = ssSG + (y_hi{h}(i) - y_h(h))^2;
            ssT  = ssT  + (y_j(j) - y)^2;
            ssGT = ssGT + (y_hj(h,j) - y_h(h) - y_j(j) + y)^2;
            ssR  = ssR  + (X{h}(i,j) - y_hj(h,j) - y_hi{h}(i) + y_h(h))^2;
        end
    end
end

% calculate means
if s > 1
    msG  = ssG  / (s-1);
    msGT = ssGT / ((s-1)*(t-1));
end
msSG = ssSG / (n-s);
msT  = ssT  / (t-1);
msR  = ssR  / ((n-s)*(t-1));


% calculate the F-statistics
if s > 1
    FG  = msG  / msSG;
    FGT = msGT / msR;
end
FT  = msT  / msR;
FSG = msSG / msR;


% single or multiple sample designs?
if s > 1
    % case for multiple samples
    pG  = 1 - fcdf(FG, s-1, n-s);
    pT  = 1 - fcdf(FT, t-1, (n-s)*(t-1));
    pGT = 1 - fcdf(FGT, (s-1)*(t-1), (n-s)*(t-1));
    pSG = 1 - fcdf(FSG, n-s, (n-s)*(t-1));

    p = [pT, pG, pSG, pGT];

    table = { 'Source' 'SS' 'df' 'MS' 'F' 'Prob>F'
        'Time'  ssT t-1 msT FT pT
        'Group' ssG s-1 msG FG pG
        'Interaction' ssGT (s-1)*(t-1) msGT FGT pGT
        'Subjects (matching)' ssSG n-s msSG FSG pSG
        'Error' ssR (n-s)*(t-1) msR  [] []
        'Total' [] [] [] [] []
        };
    table{end, 2} = sum([table{2:end-1,2}]);
    table{end, 3} = sum([table{2:end-1,3}]);

else
    % case for only one sample
    pT  = 1 - fcdf(FT, t-1, (n-s)*(t-1));
    pSG = 1 - fcdf(FSG, n-s, (n-s)*(t-1));

    p = [pT, pSG];

    table = { 'Source' 'SS' 'df' 'MS' 'F' 'Prob>F'
        'Time'  ssT t-1 msT FT pT
        'Subjects (matching)' ssSG n-s msSG FSG pSG
        'Error' ssR (n-s)*(t-1) msR  [] []
        'Total' [] [] [] [] []
        };
    table{end, 2} = sum([table{2:end-1,2}]);
    table{end, 3} = sum([table{2:end-1,3}]);

end

% Stats structure (for multcompare)
if nargout>2,
    stats.gnames = strjust(num2str((1:s)'),'left');
    stats.n = n_h';
    stats.source = 'anova1';
    stats.means = y_h';
    stats.df = n-s;
    stats.s = sqrt(msSG);
end

end



function [P L] = Mauspher(X)
%Mauchly's sphericity test.
%[Basically, here we are testing the null hypothesis that for all p single components the
%variances are equal and all covariances are zero. It would means that all the p-axis have
%the same variability or eigenvalues (l);recall that det(R)=prod(diag(L))=Wilk's lambda].
%
%     Input: X - multivariate data matrix.
%     Output:
%          L - Mauchly's statistic used to test any deviation from an expected sphericity.
%          P - probability that null Ho: is true.
%
%  Created by A. Trujillo-Ortiz and R. Hernandez-Walls
%             Facultad de Ciencias Marinas
%             Universidad Autonoma de Baja California
%             Apdo. Postal 453
%             Ensenada, Baja California
%             Mexico.
%             atrujo@uabc.mx
%
%  July 6, 2003.
%
%  To cite this file, this would be an appropriate format:
%  Trujillo-Ortiz, A. and R. Hernandez-Walls. (2003). Mauspher: Mauchly's sphericity tests.
%    A MATLAB file. [WWW document]. URL http://www.mathworks.com/matlabcentral/fileexchange/
%    loadFile.do?objectId=3694&objectType=FILE
%

[n p] = size(X);
S = cov(X);  %covariances' matrix
W = (n-1)*S;
L = det(W)/((1/p)*trace(W))^p;  %Mauchly's statistic
M = (n-1)-(2*p*p+p+2)/6/p;
LL = (-1)*M*log(L);  %Chi-square approximation
A = (p+1)*(p-1)*(p+2)*(2*p*p*p+6*p*p+3*p+2)/288/p/p;
F = p*(p+1)/2-1;  %degrees of freedom
A1 = 1-chi2cdf(LL,F);
A3 = 1-chi2cdf(LL,F+4);
P = A1+(A/M/M)*(A3-A1);  %Probability that null Ho: is true.

end


function [x] = epsHF(X)
%EPSHF Huynh-Feldt epsilon.
%
% Created by A. Trujillo-Ortiz, R. Hernandez-Walls, A. Castro-Perez
%            and K. Barba-Rojo
%            Facultad de Ciencias Marinas
%            Universidad Autonoma de Baja California
%            Apdo. Postal 453
%            Ensenada, Baja California
%            Mexico.
%            atrujo@uabc.mx
%
% Copyright. November 01, 2006.
%
% To cite this file, this would be an appropriate format:
% Trujillo-Ortiz, A., R. Hernandez-Walls, A. Castro-Perez and K. Barba-Rojo. (2006).
%   epsHF:Huynh-Feldt epsilon. A MATLAB file. [WWW document]. URL http://
%   www.mathworks.com/matlabcentral/fileexchange/loadFile.do?objectId=12853
%

error(nargchk(1,1,nargin));

[n k] = size(X);
eGG = epsGG(X);  %call to the zipped Greenhouse-Geisser epsilon function
epsHF = (n*(k-1)*eGG-2)/((k-1)*((n-1)-(k-1)*eGG));  %Huynh-Feldt epsilon estimation
x = epsHF;

return,
end

function [x] = epsGG(X)
%EPSGG Greenhouse-Geisser epsilon.
%
% Created by A. Trujillo-Ortiz, R. Hernandez-Walls, A. Castro-Perez
%            and K. Barba-Rojo
%            Facultad de Ciencias Marinas
%            Universidad Autonoma de Baja California
%            Apdo. Postal 453
%            Ensenada, Baja California
%            Mexico.
%            atrujo@uabc.mx
%
% Copyright. October 31, 2006.
%
% To cite this file, this would be an appropriate format:
% Trujillo-Ortiz, A., R. Hernandez-Walls, A. Castro-Perez and K. Barba-Rojo. (2006).
%   epsGG:Greenhouse-Geisser epsilon. A MATLAB file. [WWW document]. URL http://
%   www.mathworks.com/matlabcentral/fileexchange/loadFile.do?objectId=12839
%

error(nargchk(1,1,nargin));

k = size(X,2);  %number of treatments
S = cov(X);  %variance-covariance matrix
mds = mean(diag(S));  %mean of the entries on the main diagonal of S
ms = mean(mean(S));  %the mean of all entries of S
msr = mean(S,2);  %mean of all entries in row i of S
N = k^2*(mds-ms)^2;
D = (k-1)*(sum(sum(S.^2))-2*k*sum(msr.^2)+k^2*ms^2);
epsGG = N/D;  %Greenhouse-Geisser epsilon estimation
x = epsGG;

return,
end



function [F df1 df2 P] = Wtest(X)
%Welch's test for analysis of variance
%     Inputs:
%          X - data matrix (Size of matrix must be n-by-2; data=column 1, sample=column 2).
%

k=max(X(:,2));

%Statistics.
n=[];s2=[];m=[];
indice=X(:,2);
for i=1:k
    Xe=find(indice==i);
    eval(['X' num2str(i) '=X(Xe,1);']);
    eval(['n' num2str(i) '=length(X' num2str(i) ') ;']);
    eval(['s2' num2str(i) '=(std(X' num2str(i) ').^2) ;']);
    eval(['m' num2str(i) '=mean(X' num2str(i) ');']);
    eval(['xn= n' num2str(i) ';']);
    eval(['xs2= s2' num2str(i) ';']);
    eval(['xm= m' num2str(i) ';'])
    n=[n;xn];s2=[s2;xs2];m=[m;xm];
end

%Welch's Procedure.
ws=[];
for i=1:k
    eval(['w' num2str(i) '=n' num2str(i) '/s2' num2str(i) ';']);
    eval(['x= w' num2str(i) ';']);
    ws=[ws;x];
end

wps=[];
for i=1:k
    eval(['wp' num2str(i) '=w' num2str(i) '*m' num2str(i) ';']);
    eval(['x= wp' num2str(i) ';']);
    wps=[wps;x];
end

A=sum(ws);
D=sum(wps);

H=D/A;

b=[];
for i=1:k
    eval(['b' num2str(i) '=((1-(w' num2str(i) '/A)).^2)/(n' num2str(i) '-1);']);
    eval(['x=b' num2str(i) ';']);
    b=[b;x];
end

pw=[];
for i=1:k
    eval(['pws' num2str(i) '=(w' num2str(i) '*((m' num2str(i) '-H).^2)/(k-1));']);
    eval(['x=pws' num2str(i) ';']);
    pw=[pw;x];
end

E=sum(pw);
O=sum(b);

G=(1+2*(k-2)*O/(k^2-1));

F=E/G;  %Welch's F-statistic.
v1=(k-1);  %numerator degrees of freedom.
v2=(k^2-1)/(3*O);  %denominator degrees of freedom.
df1=v1;df2=v2;

% Because the denominator degrees of freedom are corrected and could results
% a fraction, the probability function associated to the F statistic is resolved
% by the Simpson's 1/3 numerical integration method.
x=linspace(.000001,F,100001);
DF=x(2)-x(1);
y=((v1/v2)^(.5*v1)/(beta((.5*v1),(.5*v2))));
y=y*(x.^((.5*v1)-1)).*(((x.*(v1/v2))+1).^(-.5*(v1+v2)));
N=length(x);
P=1-(DF.*(y(1)+y(N) + 4*sum(y(2:2:N-1))+2*sum(y(3:2:N-2)))/3.0);

end

function [F v1 v2 P] = Wmetatest(X,X2)

% Testing Welch for RM (incl. several vertical groups)
% Method here: separate sum of squares equivalent and error equivalent
% SSgroups from Full results
% SSerror from collapsed RM (repetitions' means)
%
% INPUT: X = [resin(:,dep.var.) resin(:,factor)]
%        X2 = [mean(resort(:,n.reps,dep.var.),2) resort(:,factor-1,1)]

[G v2] = Wtest2(X);

[E v1] = Wtest1(X2);

F = E/G;

x=linspace(.000001,F,100001);
DF=x(2)-x(1);
y=((v1/v2)^(.5*v1)/(beta((.5*v1),(.5*v2))));
y=y*(x.^((.5*v1)-1)).*(((x.*(v1/v2))+1).^(-.5*(v1+v2)));
N=length(x);
P=1-(DF.*(y(1)+y(N) + 4*sum(y(2:2:N-1))+2*sum(y(3:2:N-2)))/3.0);

end

function [E df1] = Wtest1(X)
% For Within-Group Sum of squares equivalent

k=max(X(:,2));

%Statistics.
n=[];s2=[];m=[];
indice=X(:,2);
for i=1:k
    Xe=find(indice==i);
    eval(['X' num2str(i) '=X(Xe,1);']);
    eval(['n' num2str(i) '=length(X' num2str(i) ') ;']);
    eval(['s2' num2str(i) '=(std(X' num2str(i) ').^2) ;']);
    eval(['m' num2str(i) '=mean(X' num2str(i) ');']);
    eval(['xn= n' num2str(i) ';']);
    eval(['xs2= s2' num2str(i) ';']);
    eval(['xm= m' num2str(i) ';'])
    n=[n;xn];s2=[s2;xs2];m=[m;xm];
end

%Welch's Procedure.
ws=[];
for i=1:k
    eval(['w' num2str(i) '=n' num2str(i) '/s2' num2str(i) ';']);
    eval(['x= w' num2str(i) ';']);
    ws=[ws;x];
end

wps=[];
for i=1:k
    eval(['wp' num2str(i) '=w' num2str(i) '*m' num2str(i) ';']);
    eval(['x= wp' num2str(i) ';']);
    wps=[wps;x];
end

A=sum(ws);
D=sum(wps);

H=D/A;

pw=[];
for i=1:k
    eval(['pws' num2str(i) '=(w' num2str(i) '*((m' num2str(i) '-H).^2)/(k-1));']);
    eval(['x=pws' num2str(i) ';']);
    pw=[pw;x];
end

E=sum(pw);

v1=(k-1);  %numerator degrees of freedom.
df1=v1;


end

function [G df2] = Wtest2(X)
% For Between-Group Sum of squares equivalent

k=max(X(:,2));

%Statistics.
n=[];s2=[];m=[];
indice=X(:,2);
for i=1:k
    Xe=find(indice==i);
    eval(['X' num2str(i) '=X(Xe,1);']);
    eval(['n' num2str(i) '=length(X' num2str(i) ') ;']);
    eval(['s2' num2str(i) '=(std(X' num2str(i) ').^2) ;']);
    eval(['m' num2str(i) '=mean(X' num2str(i) ');']);
    eval(['xn= n' num2str(i) ';']);
    eval(['xs2= s2' num2str(i) ';']);
    eval(['xm= m' num2str(i) ';'])
    n=[n;xn];s2=[s2;xs2];m=[m;xm];
end

%Welch's Procedure.
ws=[];
for i=1:k
    eval(['w' num2str(i) '=n' num2str(i) '/s2' num2str(i) ';']);
    eval(['x= w' num2str(i) ';']);
    ws=[ws;x];
end

A=sum(ws);


b=[];
for i=1:k
    eval(['b' num2str(i) '=((1-(w' num2str(i) '/A)).^2)/(n' num2str(i) '-1);']);
    eval(['x=b' num2str(i) ';']);
    b=[b;x];
end


O=sum(b);

G=(1+2*(k-2)*O/(k^2-1));

v2=(k^2-1)/(3*O);  %denominator degrees of freedom.
df2=v2;


end


