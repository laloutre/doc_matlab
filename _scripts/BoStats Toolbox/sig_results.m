function [matsel varsout varsin category] = sig_results(matin,select,sort,save,category,varsin)

% Selection of relevant / correlated vars in results-matrix input
% Matin: Results_xx_chords_WW
% select: vector of sig. vars from statistical analysis
% options:
%         sort: specify if output is to be category-sorted
%         category: specify the category-rank vector if different from default
%         varsin: specify variables labels if different from default
%
% Output: First line as original var. number, second line as category-sorted line number
%         First 4 columns to keep performances identifiers (REC#, piece, timbre, pianist)
%         Following columns for each selected var.


% Take the part of input name to use it in output name
in = inputname(1);
if length(in)>8 && strcmp(in(1:8),'Results_'), in = in(9:length(in)); end
% initials = in(length(in)-1:length(in));

[desc factor ngps reorder variables] = stariables();

% Selection of relevant vars.
matsel = zeros(size(matin,1)+2,length(select)+desc);

matsel(3:size(matsel,1),1:desc)=matin(:,1:desc); % Performances identifiers

if nargin>2, % Sort option specified
    if strcmp(sort,'sort'), sort=1;
    elseif strcmp(sort,'nosort') || strcmp(sort,'no'), sort=0;
    end
else sort=0;
end

if nargin>3,
    if strcmp(save,'save'), save=1;
    elseif strcmp(save,'nosave'), save=0;
    end
else save=0;
end



if nargin>4, % category specified
    categoname = inputname(5);
    
    categowarn=0; % Tag for wrong-sized category vector announced
    if size(matin,2)~=length(category)+desc,
        disp('Wrong length of categorical vector, does not match the input matrix number of vars');
        categowarn=1;
    end
else
    category=reorder;
    categoname='category';
end

varsoff=0;
varsinoutput=0;
if nargin<6, varsin = variables;
    if length(varsin)~=size(matin,2)-desc, 
        varsoff=1;
        disp('Input variable labels that match input matrix length are required');
    end
else    
    if length(varsin)==size(matin,2)-desc,
        varsiname = inputname(6);
        varsinoutput=1;
    else varsin = variables;
        if length(varsin)==size(matin,2)-desc, disp('Wrong length of input variables labels, default labels will be used');
        else varsoff=1; disp('Wrong length of variables labels');
        end
    end
end
varsout=cell(1,length(select));

% FILL

rank=desc+1; % Column to store selected var. in

for i=1:length(select)
    for j=desc+1:size(matin,2) % Loop on input vars
        if select(i)==j-desc,
            matsel(1,rank)=j-desc;
            if nargin<4 || ~categowarn, % default or valid category 
                matsel(2,rank) = category(j-desc);
            end
            
            matsel(3:size(matsel,1),rank) = matin(:,j);
            
            if ~varsoff, varsout(rank-desc) = varsin(j-desc); end % Selected var name
            
            rank=rank+1;
            break;
        end
    end
    if rank>length(select)+desc, break; end
end

% SORT
if sort,
    % Mess for varsout
    ord(:,2) = matsel(2,desc+1:size(matsel,2))';
    ord(:,1) = 1:size(ord,1);
    ord=sortrows(ord,2);
    varstmp=cell(1,length(select));
    for i=1:size(ord,1), varstmp(i) = varsout(ord(i,1)); end
    varsout=varstmp;    
    
    matsel(:,desc+1:size(matsel,2)) = sortrows( matsel(:,desc+1:size(matsel,2))' ,2 )'; % et voil�!
end

% STORE
if save,
    if sort, outname = ['SigResSort_' in]; else outname = ['SigRes_' in]; end
    eval([outname ' = matsel;']);
    
    if sort, varsoutname = ['Sel_Sort_Vars_' in]; else varsoutname = ['Selected_Vars_' in]; end
    eval([varsoutname ' = varsout;']);
    
    if varsinoutput,
        eval([varsiname '=varsin;']);
        save(['Sig_Vars_' in],outname,categoname,varsoutname,varsiname);
    elseif ~varsoff, save(['Sig_Vars_' in],outname,categoname,varsoutname);
    else save(['Sig_Vars_' in],outname,categoname);
    end
end

% PRINTOUT

fname = ['Sig_Variables_' in '.xls'];
fid = fopen(fname,'w+');
fprintf(fid,'REC# \t Piece \t Timbre \t Pianist \t');
for i=1:length(varsout), fprintf(fid,'%s\t',varsout{i}); end

fprintf(fid,'\n Orig. rank \t \t \t \t');
for j=desc+1:size(matsel,2), fprintf(fid,'%d \t',matsel(1,j)); end

fprintf(fid,'\n Categorized rank \t \t \t \t');
for j=desc+1:size(matsel,2), fprintf(fid,'%d \t',matsel(2,j)); end

for i=3:size(matsel,1),
    fprintf(fid,'\n');
    for j=1:size(matsel,2), fprintf(fid,'%.3f\t',matsel(i,j)); end
end

fclose(fid);

end

