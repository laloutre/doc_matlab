function Piece_split(matin,splitter,savename)

% Splits the results matrix by piece (or by other splitter factor)
% IN: Results matrix, facultative splitter name or column

if ischar(matin), 
    resin=dlmread(matin);
    iname=matin;
else
    resin=matin;
    iname=inputname(1);
end

if nargin<2, splitter=2; end
if strcmp(splitter,'piece'), splitter=2;
elseif strcmp(splitter,'timbre'), splitter =3;
elseif strcmp(splitter,'pianist'), splitter=4;
end

if nargin<3,
    if length(iname)>7 && strcmp(iname(1:7),'Results'), savename = ['Results_Split' iname(8:length(iname))] ;
    else savename = ['Split_' iname];
    end
end

[desc factor ngps reorder variables groups colors catfields catlabels fname grname] = stariables();

spnames = grname(splitter-1,:);

out=cell(max(resin(:,splitter)),2);




resin = sortrows(resin,splitter);

for i=1:max(resin(:,splitter)),
    if length(iname)>7 && strcmp(iname(1:7),'Results'), 
        out{i,1} = ['Results_' spnames{i} iname(8:length(iname))];
    else out{i,1} = [spnames{i} '_' iname];
    end
    
    out{i,2} = resin(find(resin(:,2)==i,1,'first'):find(resin(:,2)==i,1,'last'),:);
end

chainout=' ';
for i=1:size(out,1),
    chainout = [chainout ' ' out{i,1}];
   eval([out{i,1} '=out{i,2};']);
end

eval(['save ' savename chainout ';']);