function resout = ToResults(resin,tags,outliers,timbres,pian)

% Takes the mean stats from Bosen Toolbox processing, adds descriptors of performer, piece and timbre for each performance
% & converts the SDs to ratios
runit=1;

desc=stariables;

% Indexes
SD=[59,111; 170,223; 227,279; 283,335]; % SDs to convert to ratios
ref=[3,55; 114,167; 115,167; 115,167]; % Means to divide SDs by

SD=SD+desc;
ref=ref+desc;

if size(SD,1)~=size(ref,1), disp('Error in SD ratios references: no match in subsets length'); runit=0;
else
    itest=1;
    while itest<=size(ref,1),
        if (ref(itest,2)-ref(itest,1))~=(SD(itest,2)-SD(itest,1)), disp('Wrong match of field lengths between targets and divisors'); runit=0; break; end
        itest=itest+1;
    end
end

if nargin<4,
    if size(tags,2)==3,
        pieces=tags(:,1);
        timbres=tags(:,2);
        pian=tags(:,3);
    else disp('Something is lacking'); runit=0;
    end
    if nargin<3, outliers=[]; end
    
elseif nargin<5,
    if size(tags,2)+size(timbres,2)==3,
        pieces=tags(:,1);
        timbres=timbres(:,1);
        if size(tags,2)>1, pian=tags(:,2);
        elseif size(timbres,2)>1, pian=timbres(:,2);
        end
    elseif size(tags,2)==2 && size(timbres,2)==2 && ~sum(tags(:,2)~=timbres(:,2)),
        pian=tags(:,2);
        
    else disp('Something is lacking'); runit=0;
    end
    
else pieces=tags(:,1);
    
end

if nargin<3 || (~isempty(outliers) && outliers==0), outliers=[]; end


if length(pieces)~=size(resin,1)-length(outliers),
    disp('Wrong number of piece# tags; shall match the number of performances');
    runit=0;
end

if length(timbres)~=size(resin,1)-length(outliers),
    disp('Wrong number of timbre tags; shall match the number of performances');
    runit=0;
end

if length(pian)==1,
    piano = pian * ones(size(resin,1)-length(outliers),1);
else piano = pian;
    if length(piano)~=size(resin,1)-length(outliers),
        disp('Wrong number of performer tags; shall match the number of performances (or be one and the same for all)');
        runit=0;
    end
end

if runit,
   
    resout(:,1) = resin(:,1); % REC#
    resout(:,desc+1:size(resin,2)+desc-1) = resin(:,2:size(resin,2)); % DATA
    
    for i=1:length(outliers), % Nothing happens if outliers is empty
        resout(find(resout(:,1)==outliers(i),1,'first') ,:) = [];
    end
    
    
    resout(:,2) = pieces;
    resout(:,3) = timbres;
    resout(:,4) = piano;
    
    % Convert SDs to deviation rates
    for i=1:size(resout,1)
        for j=1:size(ref,1), resout(i,SD(j,1):SD(j,2)) = resout(i,SD(j,1):SD(j,2))./resout(i,ref(j,1):ref(j,2)); end
    end
    
end
end