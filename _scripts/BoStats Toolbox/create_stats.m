function [vect vost vfx] = create_stats(matin,pianum,savename)

% Creates all the possible stat outputs from the 'resin' Results matrix (type 'Results_XX_chords_MB')
% INPUT Format: one line per sample; C1= REC#, C2= piece#, C3= timbre, C4:end= vars
% savename : optio nfor designated output name
% pianum : number of pianists simultaneously analyzed

% OUTPUTS
% Stats output from statit.m:
%       statout & 'Stat ***.xls' & 'Sig ***.xls'
%       One line per var; 
%       C1:2:C9= K-S stat for each timbre, 
%       C2:2:C10= K-S p for each timbre,
%       C11:C12= Levene's homogeneity of variance stat + p,
%       C13:C14= ANOVA F + p,
%       C15:C17= Welch stat + df2 + p,
%       C18:20= Brown-Forsythe stat + df2 + p,
%       C21:C22= Kruskal-Wallis chi-square + p
%       C23= Sum of normality of distributions
%       C24= significance score
%
% ZScores output through transform_results.m:
%       ZScores results matrix (each result changed as Zscore)
%       ZScores means and Serrs per timbre
%       All vars sorted per category (columns order modified)
%
% Sig. results selected via sig_results.m:
%       Same format as Results input, with less (only the selectedly sig.) columns
%       + First 2 lines indicating original and sorted ranks
%       + Sig. vars. label output
%
% Plots

[desc factor ngps reorder vars groups colors catfields catlabels fname grname factwo sigthreshold] = stariables();


% --- CREATION ---   ------------------------------------------------------------

if ischar(matin), 
    resin=dlmread(matin);
    resiname=matin;
else
    resin=matin;
    resiname=inputname(1);
end

if length(resiname)>8 && strcmp(resiname(1:8),'Results_'), resoutname=resiname(9:length(resiname));
else resoutname=resiname;
end

if nargin<2, pianum=1; end

% STATS
statoutname = ['Stat_' resoutname];
eval([statoutname ' = statit(resin,''' resiname ''');']);


% ZSCORES
% (category-sorted)
sz=['ZS_' resoutname];
szmean=['ZSmean_' resoutname];
szerr=['ZSerr_' resoutname];

eval(['[' sz ',' szmean ',' szerr '] = transform_results(resin,1);']);



% SIG RESULTS MATRIX
sigi = zeros(size(resin,2)-desc,2);
for i=1:length(sigi), sigi(i,1)=i; end
eval(['sigi(:,2) = ' statoutname '(:,' num2str(ngps*2+14) ');']);
sigi = sortrows(sigi,-2);

sigvars = sigi(1:find(sigi(:,2)>=sigthreshold,1,'last'),:);
sigvars = sortrows(sigvars,1);

signame = ['Sig_' resoutname];
varsig = ['Sig_Vars_' resoutname];

resoutn = resoutname;
eval([resoutn ' = resin;']);

if ~isempty(sigvars),
    eval(['[' signame ',' varsig '] = sig_results(' resoutn ',sigvars(:,1),1);']);
    
    % SIG RESULTS ZSCORES
    szsig=['ZS_' signame];
    szmeansig=['ZSmean_' signame];
    szerrsig=['ZSerr_' signame];
    
    eval(['[' szsig ',' szmeansig ',' szerrsig '] = transform_results(' signame '(3:size(' signame ',1),:)' ');']);
    
end

% PLOTS
eval(['plot_all(' szmean ',' szerr ');']);

if ~isempty(sigvars),
    eval(['plot_select(' szmean ',' szerr ',' signame '(2,' num2str(desc+1) ':size(' signame ',2))' ');']);
    eval(['plot_select(' szmean ',' szerr ',' signame '(2,' num2str(desc+1) ':size(' signame ',2))' ',''sep'');']);
    eval(['boxplot_sig(' signame ');']);
end

% PCA
if pianum==1,
    eval(['pcaXD(' sz ',1,''name'',''' resoutname ''');']);
    if ~isempty(sigvars), eval(['pcaXD(' szsig ',1,''name'',''' signame ''');']); end
else
    eval(['pcaXD(' sz ',1,''name'',''' resoutname ''',''factors'',[3,2,4]);']);
    if ~isempty(sigvars), eval(['pcaXD(' szsig ',1,''name'',''' signame ''',''factors'',[3,2,4]);']); end
end



cleant1=[sz '_cleant'];
eval(['[DimVars DimTags coefs scores variances ' cleant1 ' Vars_cleant] = pcadimit(' sz ');']);
eval(['[DimVarsRedux DimTagsRedux] = pcadimit(' sz ',''redux'');']);
pcasavename1=['PCA_dims ' resoutname];
save(pcasavename1,'DimVars','DimTags','coefs','scores','variances',cleant1,'Vars_cleant','DimVarsRedux','DimTagsRedux',sz);

if ~isempty(sigvars),
    cleant2=[szsig '_cleant'];
    eval(['[DimVarSig DimTagSig coefSig scoreSig varianceSig ' cleant2 ' Sig_Vars_cleant] = pcadimit(' szsig ',''variables'',' varsig ');']);
    pcasavename2=['PCA_dims ' signame];
    save(pcasavename2,'DimVarSig','DimTagSig','coefSig','scoreSig','varianceSig',cleant2,'Sig_Vars_cleant',szsig);
end

% 2-factor ANOVA
eval(['factoranovit(resin,factwo,''name'',''' resoutname ''');']);


% --- SAVE ---   ------------------------------------------------------------

if nargin<3, savename=['Stats ' resoutname]; end
eval([resiname ' =resin;']);
if ~isempty(sigvars),
    save(savename,resiname,statoutname,sz,szmean,szerr,signame,varsig,szsig,szmeansig,szerrsig);
else
    save(savename,resiname,statoutname,sz,szmean,szerr);
end

if nargout>0, % Used for batch processing
    eval(['vect = ' statoutname '(:,' num2str(ngps*2+14) ');']);
end

if nargout>1,
    eval(['vost = ' statoutname '(:,' num2str(ngps*2+18) ':' num2str(ngps*2+17+ngps*(ngps-1)/2) ');']);
end

if nargout>2,
    eval(['vfx = ' statoutname '(:,' num2str(ngps*2+17) ');']);
end

end








