function [cellout res] = manovit(matin,sig)

% MANOVAs for each category
% IN: results matrix (standard) to analyze
%     sig parameter if sig results matrix as input
% OUT: cellout: 4 columns: dimensions of spatial downsizing, matrix of p-values (n-1 values for n groups), struct of additional results, category name
%               1 line per group (11 as standard)
%
% WARNING: specially modified version to avoid reccurring failures in total results (singular SS and CP)


if ischar(matin), resin=dlmread(matin); else resin=matin; end

if nargin<2, sig=0;
elseif strcmp(sig,'sig'), sig=1;
end

[desc factor ngps reorder variables groups colors catfields catelabel] = stariables();

% Categories rank starts
cate = catfields(:,1);
cate(length(cate)+1) = catfields(size(catfields,1),2)+1;

if sig, res = resin;
else [sz szmean szerr res] = transform_results(resin,'sort');
end

% Clean out
j=desc+1;
while j<=size(res,2),
    for i=3:size(res,1),
        if res(i,j)==Inf, res(i,j)=0; end
    end
    if isnan(sum(res(3:size(res,1),j))) || ~sum(abs(res(3:size(res,1),j))),
        res(:,j)=[];
    else j=j+1;
    end
end

dim1=size(res,1);

% MANOVAs
cellout = cell(length(cate)-1,4);

for i=1:length(cate)-1,
%     disp(i);
    if i~=8  && i~=11,
        if ~isempty(res(3:dim1,find(res(2,:)>=cate(i),1,'first'):find(res(2,:)<cate(i+1),1,'last'))),
            [cellout{i,1} cellout{i,2} cellout{i,3}] = manova1(res(3:dim1,find(res(2,:)>=cate(i),1,'first'):find(res(2,:)<cate(i+1),1,'last')), res(3:dim1,factor));
        end
    elseif i==11,
        if ~isempty(res(3:dim1,find(res(2,:)>=cate(i),1,'first'):find(res(2,:)<cate(i+1),1,'last'))),
            [cellout{i,1} cellout{i,2} cellout{i,3}] = manova1(res(3:dim1,[find(res(2,:)>=cate(i),1,'first'):find(res(2,:)==306),find(res(2,:)==310):find(res(2,:)<cate(i+1),1,'last')]), res(3:dim1,factor));
        end
    end
    
    cellout{i,4} = catelabel{i};
end


end
