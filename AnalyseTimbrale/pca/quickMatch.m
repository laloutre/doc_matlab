function out = quickMatch(in)

out = in;

switch out.piece
    case 1
        Nmat = midi2nmat('res\Xpiece_1.mid');
	case 2
        Nmat  = midi2nmat('res\Xpiece_2.mid');
	case 3
        Nmat  = midi2nmat('res\Xpiece_3.mid');
    case 4
        Nmat  = midi2nmat('res\Xpiece_4.mid');
end

out.Pmat{1}= midi2nmat(out.chemin_midi{1});

out.match = dynamicmatch(out.Pmat{1},Nmat,[num2str(out.obs) ' '...
	char(out.pianiste) ' '...
	num2str(out.piece) ' ' ...
	char(out.nuance)]);


%si le matching a un probl�me on passe l'interface graphique
if(sum(ordinal(out.match.M(:,1))~='M')>0)
gmw(out.match);

else
	out = out.match;
end

%clear M;clear global;quickMatch(piece2(21,:));
%[num2str(out.obs) ' ' out.pianiste ' ' num2str(out.piece) ' ' 	out.nuance]
end