%struct = merge_struct(struct, newstruct)
function struct2 = merge_struct(struct, newstruct)

for i=1:length(newstruct)
ind = structfind(struct,'obs',newstruct(i).obs);
struct2(ind) = catstruct(struct(ind),newstruct(i));
end
