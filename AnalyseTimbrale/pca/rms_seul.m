function out = rms_seul(in,start)

mirverbose(0);
mirwaitbar(0);
try

for i = start:length(in)
i
	trim = miraudio(in.chemin(i),'TrimStart','TrimThreshold',0.08);
	
	start_time = get(trim,'Time');
	start_time = start_time{1}{1}(1);	
	
	close all;
	%audio			= mirsegment(trim,in.debfin{i}+start_time)
	
    %segments = mirgetdata(audio);
		
	
	RMS = [];

							
						
	%calcul pour chaque segment
			
	
    RMS = [RMS;mirgetdata(mirrms(trim))];
	out{i}.RMS	=RMS;
	out{i}.tempo =((length(mirgetdata(trim))/44100)/21)*60;	

	assignin('base','out',out);
end

catch err
	erreur = getReport(err);
	disp(erreur);
end
% fclose(f);
% copyfile figures.pdf figures\;
end

%% pour rejouter la sortie au dataset nomm� : piece1
% out = out';
% for i = 1:length(out)
% piece1.bright{i}=out{i,1}.bright;
% piece1.kurt{i}=out{i,1}.kurt;
% piece1.attackt{i}=out{i,1}.attackt;
% piece1.flat{i}=out{i,1}.flat;
% piece1.centroid{i}=out{i,1}.centroid;
% piece1.spread{i}=out{i,1}.spread;
% piece1.entrop{i}=out{i,1}.entrop;
% piece1.attleap{i}=out{i,1}.attleap;
% piece1.skew{i}=out{i,1}.skew;
% piece1.attslp{i}=out{i,1}.attslp;
% piece1.zerocross{i}=out{i,1}.zerocross;
% piece1.rolloff{i}=out{i,1}.rolloff;
% end
