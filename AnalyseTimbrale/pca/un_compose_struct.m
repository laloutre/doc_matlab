function [struct,data] = compose(varargin)
%Usage: [struct,data] = compose()
%compose la structure et le dataset avec les fichiers contenus dans les
%variables environementales SONS et MIDI.
%[struct,data] = compose('chemins') permet de retourner les chemins des fichiers dans le dataset


struct = vachercher(getenv('SONS'),'WAV');

for i=1:length(struct)
split = strsplit(struct(i).chemin,{'/',' ','_','.wav'});

split						= split(5:end-1);
struct(i).obs				= str2double(cell2mat(split(1)));

struct(i).enregistrement	= cell2mat(split(2));
struct(i).enregistrement	= str2double(struct(i).enregistrement(4:end));

struct(i).pianiste			= cell2mat(split(3));

struct(i).piece				= cell2mat(split(4));
struct(i).piece				= str2double(struct(i).piece(2:end));

struct(i).nuance			= cell2mat(split(5));
end


%on les remet dans l'ordre
for i=1:length(struct)
	structout(i) = struct(structfind(struct,'obs',i));
end
struct = structout;

if(length(varargin)>=1)
	struct= compose_midi(struct,varargin{2});
	struct= compose_match(struct,varargin{2});
	struct= nomatch(struct,varargin{2});
	struct = struct(varargin{2});
return;
end

for i=1:length(struct)
	struct = compose_midi(struct,i);
	struct = compose_match(struct,i);
	struct = nomatch(struct,i);
end

data = struct2dataset(struct');
data.pianiste	= nominal(data.pianiste);
data.nuance		= nominal(data.nuance);
data = sortrows(data,'obs');
if isempty(varargin)
data.chemin = [];
data.chemin_midi = [];
data.nom = [];
end

function struct = compose_midi(struct,i)

MIDI = vachercher(getenv('MIDI'),'MID');


nom = strsplit(struct(i).nom,{'_','.wav'});
nom = nom(2);
nom = strrep(nom{1},' ','_');
nom = strcat(nom,'.mid');

ind = structfind(MIDI,'nom',nom);
if length(ind)~=1 
	disp(nom);
else struct(i).chemin_midi = MIDI(ind).chemin;
end



function struct = nomatch(struct,i)
verif = zeros(length(struct),1);
   
    if(strcmp(struct(i).chemin_midi,'inconnu')==0)
        for j=1:length(struct(i).match.M)
            if(strcmp(struct(i).match.M(j,1),'M')==0)
                verif(i,1)= verif(i,1)+1;
            end
        end
        struct(i).noMATCH = verif(i,1);
    else
        verif(i,1)= -1;
        struct(i).noMATCH = verif(i,1);
    end
    

function struct = compose_match(struct,i)
nmat1 = midi2nmat('res/Xpiece_1.mid');
nmat2 = midi2nmat('res/Xpiece_2.mid');
nmat3 = midi2nmat('res/Xpiece_3.mid');
nmat4 = midi2nmat('res/Xpiece_4.mid');
    disp(i);
        
        %on cr�e le pmat de chaque fichier dans la structure
        struct(i).pmat= midi2nmat(struct(i).chemin_midi);
        
        %On supprime le temps avant la premi�re note (les colones 1 et 6 de
        %struct(i).midi sont les temps de tomb�es de chaque notes) 
        struct(i).debut = struct(i).pmat(1,1);
        struct(i).pmat(:,1) = struct(i).pmat(:,1)-struct(i).debut;
        struct(i).pmat(:,6) = struct(i).pmat(:,6)-struct(i).debut;
        
        %Deprecated ;)
        %struct(i) = rmfield(struct(i),'midi');


        %on fait le matching en fonction de la pi�ce
        switch struct(i).piece

            case 1 
                struct(i).match = dynamicmatch(struct(i).pmat,nmat1,sprintf('Observation %d piece %d nuance %s pianiste %s',struct(i).obs,struct(i).piece,struct(i).nuance,struct(i).pianiste));
            case 2 
                struct(i).match = dynamicmatch(struct(i).pmat,nmat2,sprintf('Observation %d piece %d nuance %s pianiste %s',struct(i).obs,struct(i).piece,struct(i).nuance,struct(i).pianiste));
            case 3 
                struct(i).match = dynamicmatch(struct(i).pmat,nmat3,sprintf('Observation %d piece %d nuance %s pianiste %s',struct(i).obs,struct(i).piece,struct(i).nuance,struct(i).pianiste));
            case 4 
                struct(i).match = dynamicmatch(struct(i).pmat,nmat4,sprintf('Observation %d piece %d nuance %s pianiste %s',struct(i).obs,struct(i).piece,struct(i).nuance,struct(i).pianiste));

        end

