function [out2, debfin]= suppr_add(in,minimum)

%fonction qui supprime les notes en trop dans le matching 

out = [];
for i=1:length(in)
		temp = [];
		valeur_precedente = 0;
		for ii=1:length(in.matched{i}.M)
			if((strcmp(in.matched{i}.M(ii,1),'add') && isnan(cell2mat(in.matched{i}.M(ii,2))))...
				|| (strcmp(in.matched{i}.M(ii,1),'del')&& isnan(cell2mat(in.matched{i}.M(ii,2))))...
				|| isnan(cell2mat(in.matched{i}.M(ii,7)))...
				|| cell2mat(in.matched{i}.M(ii,7))-valeur_precedente<minimum)
			continue
			else 
				valeur_precedente = cell2mat(in.matched{i}.M(ii,7));
				temp = [temp;in.matched{i}.M(ii,:)];
			end
out{i} = temp;
		end
end



out = out';

for i =1 : length(out)
temp = out{i};
temp= cell2mat(temp(:,2:end));
temp2 = [];

try	
for ii = 1 :temp(end,1)
	if(~isempty(temp(find(temp(:,1) ==ii),6)))
	ind = find(temp(:,6) ==min(temp(find(temp(:,1) ==ii),6)));
	else continue
	end
	temp2 = [temp2;out{i}(ind,:)];
	end
	out2{i} = temp2;
catch err
disp('hello');
end

end
out2 = out2';
end