function lightUp(n)

global NN Ip In InColor IpColor NN2 Ip2 In2 InColor2 IpColor2
global interface structIn


if(strcmp(get(findobj('Tag','figure1'), 'SelectionType'),'normal') ==1)
%on remet les anciens marqueurs dans leur couleur originale
if exist('Ip')
    if length(Ip)>1
        for ii = 1:length(Ip)
            set(interface.w.Pp(Ip(ii)), 'FaceColor', IpColor{ii})
        end
    else
        set(interface.w.Pp(Ip), 'FaceColor', IpColor)
    end
    if length(In)>1
        for ii = 1:length(In)
            set(interface.w.Pn(In(ii)), 'FaceColor', InColor{ii})
        end
    else
        set(interface.w.Pn(In), 'FaceColor', InColor)
    end
end

%on cherche dans quel accord se trouve le patch cliqu�
[cc tmp] = find(interface.m.GIn == n);

cc = cc; %??
%on cherche les indices des autres notes de l'accord du patch cliqu�
In = interface.m.GIn(cc,find(interface.m.GIn(cc,:) ~= 0));
Ip = interface.m.indP(find(interface.m.indN == cc));

% light up both guys
InColor = get(interface.w.Pn(In), 'FaceColor');
IpColor = get(interface.w.Pp(Ip), 'FaceColor');


set(interface.w.Pn(In), 'FaceColor', 'b')
set(interface.w.Pp(Ip), 'FaceColor', 'b')

NN = cc;


%on r�cup�re le temps de debut de la premiere note de l'accord selectionn�
interface.sel.Tdebut = min(cell2mat(...
	structIn(interface.sel.obs).match.M(Ip,7)) );

interface.sel.Pdebut = n;
end


if(strcmp(get(findobj('Tag','figure1'), 'SelectionType'),'alt') ==1)
	%on remet les anciens marqueurs dans leur couleur originale
if exist('Ip2') && ~isempty(Ip2) && Ip2(end)< length(interface.w.Pp)
    if length(Ip2)>1 
        for ii = 1:length(Ip2)
            set(interface.w.Pp(Ip2(ii)), 'FaceColor', IpColor2{ii})
        end
    else
        set(interface.w.Pp(Ip2), 'FaceColor', IpColor2)
    end
    if length(In2)>1
        for ii = 1:length(In2)
            set(interface.w.Pn(In2(ii)), 'FaceColor', InColor2{ii})
        end
    else
        set(interface.w.Pn(In2), 'FaceColor', InColor2)
    end
end

%on cherche dans quel accord se trouve le patch cliqu�
[cc tmp] = find(interface.m.GIn == n);

cc = cc; %??
%on cherche les indices des autres notes de l'accord du patch cliqu�
In2 = interface.m.GIn(cc,find(interface.m.GIn(cc,:) ~= 0));
Ip2 = interface.m.indP(find(interface.m.indN == cc));

% light up both guys
InColor2 = get(interface.w.Pn(In2), 'FaceColor');
IpColor2 = get(interface.w.Pp(Ip2), 'FaceColor');

set(interface.w.Pn(In2), 'FaceColor', 'g')
set(interface.w.Pp(Ip2), 'FaceColor', 'g')

NN2 = cc;

%on r�cup�re le temps de fin de la derni�re note de l'accord selectionn�
interface.sel.Tfin = max(cell2mat(...
	structIn(interface.sel.obs).match.M(Ip2,7)) ...
	+ cell2mat(...
	structIn(interface.sel.obs).match.M(Ip2,11)));

interface.sel.Pfin = n;
end