function out = datamike(in)



matrice = double(in(:,4:end));

matrice(isnan(matrice)) = 0;
[out.coeff,out.score,out.latent,out.tsquared,out.explained] = pca(zscore(matrice));

in.X = out.score(:,1);
in.Y = out.score(:,2);
in.Z = out.score(:,3);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
TAILLE_GROS_MARQUEURS = 20;
TAILLE_PETITS_MARQUEURS = 10;
couleurs   = [0 0 1; 1 0 0;rgbconv('37892E');rgbconv('00BDBD');1 0 1];
lignes = {'-','--',':','-.','-'};
TAILLE_LIGNES = 2.5;
mark = 'osdvp';

m1 = [];
m2 = [];
m4 = [];

v_coul = double(nominal(in.nuances));
v_mark = double(nominal(in.pianiste));
size(matrice)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%  tracage des points  %%%%%%%%%%%%%%%
	for(i=1:size(matrice))

		if(double(in(i,2)) == 1)
			figure(1);
			hold on;
			plot3(in.X(i),in.Y(i),in.Z(i),...
				'Marker',mark(v_mark(i)),...
				'MarkerSize',TAILLE_PETITS_MARQUEURS,...
				'MarkerFaceColor',[couleurs(v_coul(i),:)],...
				'Color',[couleurs(v_coul(i),:)]);
		
		elseif(double(in(i,2)) == 2)
			figure(2);
			hold on;
			plot3(in.X(i),in.Y(i),in.Z(i),...
				'Marker',mark(v_mark(i)),...
				'MarkerSize',TAILLE_PETITS_MARQUEURS,...
				'MarkerFaceColor',[couleurs(v_coul(i),:)],...
				'Color',[couleurs(v_coul(i),:)]);
		
		elseif(double(in(i,2)) == 3)
		
		elseif(double(in(i,2)) == 4)
			figure(4);
			hold on;
			plot3(in.X(i),in.Y(i),in.Z(i),...
				'Marker',mark(v_mark(i)),...
				'MarkerSize',TAILLE_PETITS_MARQUEURS,...
				'MarkerFaceColor',[couleurs(v_coul(i),:)],...
				'Color',[couleurs(v_coul(i),:)]);
		
		end
		
	end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%  Tracage des croix de moyennes %%%%%%%%%%%%%%%
	
	
	for i=[1 2 4]
		moy{i}.mat = [];
		
		figure(i);
		
		X = mean(double(in(in.nuances=='bri' & in.piece==i,'X')));
		Y = mean(double(in(in.nuances=='bri' & in.piece==i,'Y')));
		Z = mean(double(in(in.nuances=='bri' & in.piece==i,'Z')));
		
		moy{i}.bri = plot3(X,Y,Z,'x','Color',couleurs(1,:),'LineWidth',3,'MarkerSize',15);
		moy{i}.mat = [moy{i}.mat,[X;Y;Z]];
		
		X = mean(double(in(in.nuances=='roun' & in.piece==i,'X')));
		Y = mean(double(in(in.nuances=='roun' & in.piece==i,'Y')));
		Z = mean(double(in(in.nuances=='roun' & in.piece==i,'Z')));
		
		moy{i}.roun = plot3(X,Y,Z,'x','Color',couleurs(2,:),'LineWidth',3,'MarkerSize',15);
		moy{i}.mat = [moy{i}.mat,[X;Y;Z]];
		
		X = mean(double(in(in.nuances=='dry' & in.piece==i,'X')));
		Y = mean(double(in(in.nuances=='dry' & in.piece==i,'Y')));
		Z = mean(double(in(in.nuances=='dry' & in.piece==i,'Z')));
		
		moy{i}.dry = plot3(X,Y,Z,'x','Color',couleurs(3,:),'LineWidth',3,'MarkerSize',15);
		moy{i}.mat = [moy{i}.mat,[X;Y;Z]];
		
		X = mean(double(in(in.nuances=='dar' & in.piece==i,'X')));
		Y = mean(double(in(in.nuances=='dar' & in.piece==i,'Y')));
		Z = mean(double(in(in.nuances=='dar' & in.piece==i,'Z')));
		
		moy{i}.dar = plot3(X,Y,Z,'x','Color',couleurs(4,:),'LineWidth',3,'MarkerSize',15);
		moy{i}.mat = [moy{i}.mat,[X;Y;Z]];
		
		X = mean(double(in(in.nuances=='vel' & in.piece==i,'X')));
		Y = mean(double(in(in.nuances=='vel' & in.piece==i,'Y')));
		Z = mean(double(in(in.nuances=='vel' & in.piece==i,'Z')));
		
		moy{i}.vel = plot3(X,Y,Z,'x','Color',couleurs(5,:),'LineWidth',3,'MarkerSize',15);
		moy{i}.mat = [moy{i}.mat,[X;Y;Z]];
		
		[~,sort_ind] = sort(moy{i}.mat(1,:));
		M_sorted = [moy{i}.mat(:,sort_ind(1)),moy{i}.mat(:,sort_ind(2)),...
			moy{i}.mat(:,sort_ind(3)),moy{i}.mat(:,sort_ind(4)),...
			moy{i}.mat(:,sort_ind(5))];
		M = M_sorted;
		fnplt(cscvn(M(:,[1:end ])),'k',4);

		
		text(moy{i}.mat(1,1)+0.2,moy{i}.mat(2,1)+0.15,moy{i}.mat(3,1)+0.15,'Brillant','FontSize',15);
		text(moy{i}.mat(1,2)+0.2,moy{i}.mat(2,2)+0.15,moy{i}.mat(3,2)+0.15,'rond','FontSize',15);
		text(moy{i}.mat(1,3)+0.2,moy{i}.mat(2,3)+0.15,moy{i}.mat(3,3)+0.15,'sec','FontSize',15);
		text(moy{i}.mat(1,4)+0.2,moy{i}.mat(2,4)+0.15,moy{i}.mat(3,4)+0.15,'sombre','FontSize',15);
		text(moy{i}.mat(1,5)+0.2,moy{i}.mat(2,5)+0.15,moy{i}.mat(3,5)+0.15,'veloute','FontSize',15);
	
		xlabel(strcat('Dimension 1 : ',num2str(out.explained(1)),...
			'% de la variance expliqu�e'),'FontSize',16);
		ylabel(strcat('Dimension 2 : ',num2str(out.explained(2)),...
			'% de la variance expliqu�e'),'FontSize',16);
		zlabel(strcat('Dimension 3 : ',num2str(out.explained(3)),...
			'% de la variance expliqu�e'),'FontSize',16);
	
	end
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%  jonction des points  %%%%%%%%%%%%%%%


for pianiste = unique(in.pianiste)'
	for nuance = unique(in.nuances)'
		for piece = [1 2 4]
		
	figure(piece);
	triplette = in(in.pianiste==pianiste...
	& in.nuances==nuance...
	& in.piece==piece,{'X' 'Y' 'Z'});
		
	triplette = double(triplette);
			line([triplette(1,1) triplette(2,1)],...
				[triplette(1,2) triplette(2,2)],...
				[triplette(1,3) triplette(2,3)],...
				'Color',[couleurs((double(nuance)),:)],...
				'LineWidth',TAILLE_LIGNES,...
				'Linestyle',lignes{double(nuance)});
			
			line([triplette(2,1) triplette(3,1)],...
				[triplette(2,2) triplette(3,2)],...
				[triplette(2,3) triplette(3,3)],...
				'Color',[couleurs((double(nuance)),:)],...
				'LineWidth',TAILLE_LIGNES,...
				'Linestyle',lignes{double(nuance)});
			line([triplette(1,1) triplette(3,1)],...
				[triplette(1,2) triplette(3,2)],...
				[triplette(1,3) triplette(3,3)],...
				'Color',[couleurs((double(nuance)),:)],...
				'LineWidth',TAILLE_LIGNES,...
				'Linestyle',lignes{double(nuance)});
			
		
		
		end
	end
end
	
	
end