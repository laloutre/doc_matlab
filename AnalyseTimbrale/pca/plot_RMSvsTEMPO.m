function plot_RMSvsTEMPO(in)
    

    couleurs   = [0 0 1; 1 0 0; rgbconv('37892E');rgbconv('00BDBD');1 0 1];
    mark = 'osdvp';
    v_mark = double(nominal(in.pianiste));
    v_coul = double(nominal(in.nuance));

for ind_piece = unique(in.piece)'
    figure(ind_piece);
        hold on;
    data_set = in(in.piece==ind_piece,:);
    for ind_nuance = unique(data_set.nuance)'
    data_set2 = data_set(data_set.nuance==ind_nuance,:);
        for i = 1:length(data_set2)
            %sprintf('%f  %f',data_set2.tempo{i}, data_set2.rms{i})
            plot(data_set2.tempo{i},data_set2.rms{i},...
                'Marker',mark(data_set2.pianiste(i)),...
				'MarkerSize',10,...        
                'MarkerFaceColor',[couleurs(ind_nuance,:)],...
                'Color',[couleurs(ind_nuance,:)],...
				'LineWidth',2);
        end
    end
    title(strcat('Piece :',num2str(ind_piece)));set(gcf,'color','w');
    xlabel('Tempo en BPM','FontSize',16);
    ylabel('RMS ','FontSize',16);
    %legend('Brillant','sombre','rond','sec','velout�');
    figure(6)
    set(figure(ind_piece),'units','normalized','outerposition',[0 0 1 1]);
    set(figure(ind_piece),'PaperSize',[24 20],'PaperPosition', [-1 0 26 20])
    print(figure(ind_piece),'-dpdf',strcat('RMSvsTEMPO_p',num2str(ind_piece)));
end
end