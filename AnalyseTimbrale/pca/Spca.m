    
%[coeff,score,latent,tsquared,explained,p2,in] = Spca(dataset)
%affiche une pca du dataset obtenu avec l'interface.
%ET IMPRIME TOUT SEUL
function [out,R]=Spca(in,DIMS)


TAILLE_GROS_MARQUEURS = 20;
TAILLE_PETITS_MARQUEURS = 7;
%couleurs pour (dans l'ordre) : brillant, rond,sec,sombre,veloute
couleurs   = [0 0 1; 1 0 0; rgbconv('37892E');rgbconv('00BDBD');1 0 1];
lignes = {'-','--',':','-.','-'};
TAILLE_LIGNES = 2.5;
mark = 'osdvp';

if(strcmp(DIMS,'1_2')==1)
angle_vue = [180 270];% DImentions 1 et 2

elseif(strcmp(DIMS,'1_3')==1)
angle_vue = [-180 0];% DImentions 1 et 3

elseif(strcmp(DIMS,'2_3')==1)
angle_vue = [90 0];% DImentions 2 et 3
end

LEGEND =0;
AUTOPRINT = 1;
%style = hgexport('readstyle','othr');

%on supprime les infos inutiles pour la PCA
p2 = in;
p2.pianiste=[];
p2.obs =[];
p2.piece=[];
p2.nuance=[];
if(sum(ismember(p2.Properties.VarNames,'chemin_boe')))p2.chemin_boe=[];end
p2.matched=[];
if(sum(ismember(p2.Properties.VarNames,'match')))p2.match=[];end
if(sum(ismember(p2.Properties.VarNames,'pmat')))p2.pmat=[];end
if(sum(ismember(p2.Properties.VarNames,'noMATCH')))p2.noMATCH=[];end
if(sum(ismember(p2.Properties.VarNames,'debfin')))p2.debfin=[];end
if(sum(ismember(p2.Properties.VarNames,'debut')))p2.debut=[];end
if(sum(ismember(p2.Properties.VarNames,'nom')))p2.nom =[];end
if(sum(ismember(p2.Properties.VarNames,'chemin_midi')))p2.chemin_midi =[];end
if(sum(ismember(p2.Properties.VarNames,'chemin')))p2.chemin =[];end
if(sum(ismember(p2.Properties.VarNames,'enregistrement')))p2.enregistrement=[];end
in.nuance = ordinal(in.nuance);
in.pianiste = ordinal(in.pianiste);
%on fait les moyennes de chaque descripteurs :
for i=1:length(p2)
	p2.bright(i) = {mean(p2.bright{i})};
	p2.attackt(i) = {mean(p2.attackt{i})};
	p2.kurt(i) = {mean(p2.kurt{i})};
	p2.flat(i) = {mean(p2.flat{i})};
	p2.centroid(i) = {mean(p2.centroid{i})};
	p2.spread(i) = {mean(p2.spread{i})};
	p2.entrop(i) = {mean(p2.entrop{i})};
	p2.attleap(i) = {mean(p2.attleap{i})};
	p2.skew(i) = {mean(p2.skew{i})};
	p2.attslp(i) = {mean(p2.attslp{i})};
	p2.zerocross(i) = {mean(p2.zerocross{i})};
	p2.rolloff(i) = {mean(p2.rolloff{i})};
	%in.RMS(i) = {mean(in.RMS{i})};
end
p2.bright = cell2mat(p2.bright);
p2.attackt = cell2mat(p2.attackt);
p2.kurt = cell2mat(p2.kurt);
p2.flat = cell2mat(p2.flat);
p2.centroid = cell2mat(p2.centroid);
p2.spread = cell2mat(p2.spread);
p2.entrop = cell2mat(p2.entrop);
p2.attleap = cell2mat(p2.attleap);
p2.skew = cell2mat(p2.skew);
p2.attslp= cell2mat(p2.attslp);
p2.zerocross = cell2mat(p2.zerocross);
p2.rolloff = cell2mat(p2.rolloff);
%in.RMS = cell2mat(in.RMS);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%          descripteurs ot�s du calcul        %%

%p2.entrop=[];
%p2.kurt=[];
%p2.zerocross=[];
%p2.skew=[];


%% 

a  = double(p2);
[coeff,score,latent,tsquared,explained]=pca(zscore(a));

in.X = score(:,1);
in.Y = score(:,2);
in.Z = score(:,3);

v_coul = double(nominal(in.nuance));
v_mark = double(nominal(in.pianiste));
v_lignes = double(nominal(in.pianiste));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FIGURE 1
%repr�sentation des individus sur les dimensions 1 et 2
 
for(i=1:length(in))
		
		if(in.piece(i) == 1)
			figure(1);
			hold on;
			plot3(in.X(i),in.Y(i),in.Z(i),...
				'Marker',mark(v_mark(i)),...
				'MarkerSize',TAILLE_PETITS_MARQUEURS,...
				'MarkerFaceColor',[couleurs(v_coul(i),:)],...
				'Color',[couleurs(v_coul(i),:)]);
		
		elseif(in.piece(i) == 2)
			figure(2);
			hold on;
			plot3(in.X(i),in.Y(i),in.Z(i),...
				'Marker',mark(v_mark(i)),...
				'MarkerSize',TAILLE_PETITS_MARQUEURS,...
				'MarkerFaceColor',[couleurs(v_coul(i),:)],...
				'Color',[couleurs(v_coul(i),:)]);
		
		elseif(in.piece(i) == 3)
		
		elseif(in.piece(i) == 4)
			figure(4);
			hold on;
			plot3(in.X(i),in.Y(i),in.Z(i),...
				'Marker',mark(v_mark(i)),...
				'MarkerSize',TAILLE_PETITS_MARQUEURS,...
				'MarkerFaceColor',[couleurs(v_coul(i),:)],...
				'Color',[couleurs(v_coul(i),:)]);
		
        end
		
        %ici on les affiche toutes !
        figure(6);
			hold on;
			plot3(in.X(i),in.Y(i),in.Z(i),...
				'Marker',mark(v_mark(i)),...
				'MarkerSize',TAILLE_PETITS_MARQUEURS,...
				'MarkerFaceColor',[couleurs(v_coul(i),:)],...
				'Color',[couleurs(v_coul(i),:)]);
        
	end


temp_dataset = in;
temp_OBSNAMES= mat2str(1:length(temp_dataset));
temp_OBSNAMES(1)='';temp_OBSNAMES(end)='';
temp_OBSNAMES = strsplit(temp_OBSNAMES,' ');
temp_dataset.Properties.ObsNames = temp_OBSNAMES;
temp_dataset.nuance = nominal(temp_dataset.nuance);
temp_dataset.pianiste= nominal(temp_dataset.pianiste);
nuances = unique(temp_dataset.nuance);
pianistes =unique(temp_dataset.pianiste);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%  jonction des points  %%%%%%%%%%%%%%%

for i=1 : length(pianistes)
	for ii=1:length(nuances)
		for piece = unique(in.piece)'
		
	figure(piece);
	triplette = temp_dataset(temp_dataset.pianiste==pianistes(i)...
	& temp_dataset.nuance==nuances(ii)...
	& temp_dataset.piece==piece,:).Properties.ObsNames;
		
	triplette = str2double(triplette);
			line([score(triplette(1),1) score(triplette(2),1)],...
				[score(triplette(1),2) score(triplette(2),2)],...
				[score(triplette(1),3) score(triplette(2),3)],...
				'Color',[couleurs((ii),:)],...
				'LineWidth',TAILLE_LIGNES,...
				'Linestyle',lignes{ii});
			line([score(triplette(2),1) score(triplette(3),1)],...
				[score(triplette(2),2) score(triplette(3),2)],...
				[score(triplette(2),3) score(triplette(3),3)],...
				'Color',[couleurs((ii),:)],...
				'LineWidth',TAILLE_LIGNES,...
				'Linestyle',lignes{ii});
			line([score(triplette(1),1) score(triplette(3),1)],...
				[score(triplette(1),2) score(triplette(3),2)],...
				[score(triplette(1),3) score(triplette(3),3)],...
				'Color',[couleurs((ii),:)],...
				'LineWidth',TAILLE_LIGNES,...
				'Linestyle',lignes{ii});
			
		crossX = score(triplette(1),1) + score(triplette(2),1)+ score(triplette(3),1);
		crossY = score(triplette(1),2) + score(triplette(2),2)+ score(triplette(3),2);
		crossZ = score(triplette(1),3) + score(triplette(2),3)+ score(triplette(3),3);

		plot3(crossX/3,crossY/3,crossZ/3,'x');
		end
	end
end
	



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ETOILE pour la moyenne d'une m�me nuance

for i= unique(in.piece)'
		moy{i}.mat  = [];
        
		figure(i);
		
		title(strcat('piece',num2str(i)),'FontSize',14);
		
		X = mean(double(in(in.nuance=='brillant' & in.piece==i,'X')));
		Y = mean(double(in(in.nuance=='brillant' & in.piece==i,'Y')));
		Z = mean(double(in(in.nuance=='brillant' & in.piece==i,'Z')));
		       
		moy{i}.bri = plot3(X,Y,Z,'x',...
			'Color',couleurs(1,:),'LineWidth',3,'MarkerSize',15);
		moy{i}.mat = [moy{i}.mat,[X;Y;Z]];
		        
		X = mean(double(in(in.nuance=='rond' & in.piece==i,'X')));
		Y = mean(double(in(in.nuance=='rond' & in.piece==i,'Y')));
		Z = mean(double(in(in.nuance=='rond' & in.piece==i,'Z')));
		
		moy{i}.roun = plot3(X,Y,Z,'x',...
			'Color',couleurs(2,:),'LineWidth',3,'MarkerSize',15);
		moy{i}.mat = [moy{i}.mat,[X;Y;Z]];
		        
		X = mean(double(in(in.nuance=='sec' & in.piece==i,'X')));
		Y = mean(double(in(in.nuance=='sec' & in.piece==i,'Y')));
		Z = mean(double(in(in.nuance=='sec' & in.piece==i,'Z')));
		
		moy{i}.dry = plot3(X,Y,Z,'x',...
			'Color',couleurs(3,:),'LineWidth',3,'MarkerSize',15);
		moy{i}.mat = [moy{i}.mat,[X;Y;Z]];
		        
		X = mean(double(in(in.nuance=='sombre' & in.piece==i,'X')));
		Y = mean(double(in(in.nuance=='sombre' & in.piece==i,'Y')));
		Z = mean(double(in(in.nuance=='sombre' & in.piece==i,'Z')));
		
		moy{i}.dar = plot3(X,Y,Z,'x',...
			'Color',couleurs(4,:),'LineWidth',3,'MarkerSize',15);
		moy{i}.mat = [moy{i}.mat,[X;Y;Z]];
		        
		X = mean(double(in(in.nuance=='veloute' & in.piece==i,'X')));
		Y = mean(double(in(in.nuance=='veloute' & in.piece==i,'Y')));
		Z = mean(double(in(in.nuance=='veloute' & in.piece==i,'Z')));
		
		moy{i}.vel = plot3(X,Y,Z,'x',...
			'Color',couleurs(5,:),'LineWidth',3,'MarkerSize',15);
		moy{i}.mat = [moy{i}.mat,[X;Y;Z]];
		        
		[~,sort_ind] = sort(moy{i}.mat(1,:));
		M_sorted = [moy{i}.mat(:,sort_ind(1)),moy{i}.mat(:,sort_ind(2)),...
			moy{i}.mat(:,sort_ind(3)),moy{i}.mat(:,sort_ind(4)),...
			moy{i}.mat(:,sort_ind(5))];
		M = M_sorted;
		fnplt(cscvn(M(:,[1:end ])),'k',4);

		
		text(moy{i}.mat(1,1)+0.2,moy{i}.mat(2,1)+0.15,moy{i}.mat(3,1)+0.15,'Brillant','FontSize',15);
		text(moy{i}.mat(1,2)+0.2,moy{i}.mat(2,2)+0.15,moy{i}.mat(3,2)+0.15,'rond','FontSize',15);
		text(moy{i}.mat(1,3)+0.2,moy{i}.mat(2,3)+0.15,moy{i}.mat(3,3)+0.15,'sec','FontSize',15);
		text(moy{i}.mat(1,4)+0.2,moy{i}.mat(2,4)+0.15,moy{i}.mat(3,4)+0.15,'sombre','FontSize',15);
		text(moy{i}.mat(1,5)+0.2,moy{i}.mat(2,5)+0.15,moy{i}.mat(3,5)+0.15,'veloute','FontSize',15);

		view( angle_vue);
		
		legend('Brillant','sombre','rond','sec','velout�');
if(~LEGEND)legend('HIDE');end
xlabel(strcat('Dimension 1 : ',num2str(explained(1)),'% de la variance expliqu�e'),...
	'FontSize',16);
ylabel(strcat('Dimension 2 : ',num2str(explained(2)),'% de la variance expliqu�e'),...
	'FontSize',16);
zlabel(strcat('Dimension 3 : ',num2str(explained(3)),'% de la variance expliqu�e'),...
	'FontSize',16);
box on;
end

    figure(6);
    view( angle_vue);
    moy_globale = (moy{1}.mat+moy{2}.mat+moy{4}.mat)/3;
    for i =1:length(nuances)
    plot3(moy_globale(1,i) ,moy_globale(2,i) ,moy_globale(3,i),...
        'x','Color',couleurs(i,:),'LineWidth',3,'MarkerSize',15);
    text(moy_globale(1,i)+0.2,moy_globale(2,i)+0.15,moy_globale(2,i)+0.15,char(nuances(i)),'FontSize',15);
    end
    
    [~,sort_ind] = sort(moy_globale(1,:));
		M_sorted = [moy_globale(:,sort_ind(1)),moy_globale(:,sort_ind(2)),...
			moy_globale(:,sort_ind(3)),moy_globale(:,sort_ind(4)),...
			moy_globale(:,sort_ind(5))];
		M = M_sorted;
		fnplt(cscvn(M(:,[1:end ])),'k',4);
    legend('Brillant','sombre','rond','sec','velout�');
    if(~LEGEND)legend('HIDE');end
    xlabel(strcat('Dimension 1 : ',num2str(explained(1)),'% de la variance expliqu�e'),...
	'FontSize',16);
    ylabel(strcat('Dimension 2 : ',num2str(explained(2)),'% de la variance expliqu�e'),...
	'FontSize',16);
    zlabel(strcat('Dimension 3 : ',num2str(explained(3)),'% de la variance expliqu�e'),...
	'FontSize',16);
    box on;
    legend('Brillant','sombre','rond','sec','velout�');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for i= unique(in.piece)'
set(figure(i),'units','normalized','outerposition',[0 0 1 1]);
set(figure(i),'PaperSize',[22 20],'PaperPosition', [-1.5 -0.5 28 21]);
print(figure(i),'-dpdf',sprintf('Piece%d',i));
end
%donn�es en sortie
out.coeff=coeff;
out.score=score;
out.latent=latent;
out.tsquared=tsquared;
out.explained=explained;
out.p2=p2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%autres figures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Cercle des variables
figure(5);
title(strcat('Piece ',num2str(in.piece(1))));set(gcf,'color','w');
subplot(2,2,1);hold on;
title(strcat('Piece ',num2str(in.piece(1)),...
' :Contribution des variables sur les dimensions 1 et 2'));
%coefforth = inv(diag(std(double(out.p2))))*out.coeff;
biplot(out.coeff(:,1:3),'Positive',true,'varlabels',out.p2.Properties.VarNames);
grid;
xlabel(strcat('Dim1 : ',num2str(explained(1)),'% expliqu�e'));
ylabel(strcat('Dim2 : ',num2str(explained(2)),'% expliqu�e'));
zlabel(strcat('Dim3 : ',num2str(explained(3)),'% expliqu�e'));
view( angle_vue);
%on trace un cercle
ang=0:0.01:2*pi; 
r=1;
xp=r*cos(ang);
yp=r*sin(ang);
if(strcmp(DIMS,'1_2'))
    plot(0+xp,0+yp);
    clear r;
    set(gca,'XLim',[-1.5 1.5]);
    set(gca,'YLim',[-1 1]);
else 
    plot3(0+xp,zeros(length(yp),1)',0+yp);
    set(gca,'XLim',[-1.5 1.5]);
    set(gca,'ZLim',[-1 1]);
end




subplot(2,2,3);hold on;
title(strcat('Piece ',num2str(in.piece(1)),' :Variance expliqu�e'));
pareto(out.explained);
xlabel('Composantes principales');
ylabel('Variance expliqu�e(%)');


subplot(1,2,2);hold on;
boxplot(zscore(double(out.p2)),'orientation','horizontal',...
'labels',out.p2.Properties.VarNames);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i= unique(in.piece)'
set(figure(i),'units','normalized','outerposition',[0 0 1 1]);
set(figure(i),'PaperSize',[22 20],'PaperPosition', [-1.5 -0.5 28 21]);
print(figure(i),'-dpdf',sprintf('Piece%d',i));
end

set(figure(5),'units','normalized','outerposition',[0 0 1 1]);
set(figure(5),'PaperSize',[34 20],'PaperPosition', [-3 0 40 20]);
print(figure(5),'-dpdf','reste');

figure(6)
set(figure(6),'units','normalized','outerposition',[0 0 1 1]);
set(figure(6),'PaperSize',[24 20],'PaperPosition', [-1 0 26 20]);
print(figure(6),'-dpdf','PCA_GLOBALE');
end

