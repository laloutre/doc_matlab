function out = find_boe(in)

out = dataset();
struct = vachercher('E:\rename\BOE','boe');

for i=1:length(struct)
split = strsplit(struct(i).chemin,{'/','\',' ','_','.'});

piece = strsplit(split{9},'p');
piece = str2double(piece{2});
if(piece==3)continue;end
rec = strsplit(split{5},'REC');
rec = str2double(rec{2});
temp = in(in.pianiste==split{4} & in.nuance==split{10} & in.piece==piece & in.enregistrement == rec,:);
temp.chemin_boe = {struct(i).chemin};

out = [out ;temp];
end


end