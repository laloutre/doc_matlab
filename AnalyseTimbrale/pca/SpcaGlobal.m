
%[coeff,score,latent,tsquared,explained,p2,p1] = Spca(dataset)
%affiche une pca du dataset 
function [out,R]=SpcaGlobal(p1)

PIECE1 = 1;
PIECE2 = 2;
PIECE3 = 4;
%on supprime les infos inutiles pour la PCA
p1.match=[];
p1.pmat=[];
p1.noMATCH=[];
p1.debfin=[];
p1.debut=[];
p1.nom =[];
p1.chemin_midi =[];
p1.chemin =[];
p1.enregistrement =[];


%on fait les moyennes de chaque descripteurs :
for i=1:length(p1)
	p1.bright(i) = {mean(p1.bright{i})};
	p1.attackt(i) = {mean(p1.attackt{i})};
	p1.kurt(i) = {mean(p1.kurt{i})};
	p1.flat(i) = {mean(p1.flat{i})};
	p1.centroid(i) = {mean(p1.centroid{i})};
	p1.spread(i) = {mean(p1.spread{i})};
	p1.entrop(i) = {mean(p1.entrop{i})};
	p1.attleap(i) = {mean(p1.attleap{i})};
	p1.skew(i) = {mean(p1.skew{i})};
	p1.attslp(i) = {mean(p1.attslp{i})};
	p1.zerocross(i) = {mean(p1.zerocross{i})};
	p1.rolloff(i) = {mean(p1.rolloff{i})};
end
p1.bright = cell2mat(p1.bright);
p1.attackt = cell2mat(p1.attackt);
p1.kurt = cell2mat(p1.kurt);
p1.flat = cell2mat(p1.flat);
p1.centroid = cell2mat(p1.centroid);
p1.spread = cell2mat(p1.spread);
p1.entrop = cell2mat(p1.entrop);
p1.attleap = cell2mat(p1.attleap);
p1.skew = cell2mat(p1.skew);
p1.attslp= cell2mat(p1.attslp);
p1.zerocross = cell2mat(p1.zerocross);
p1.rolloff = cell2mat(p1.rolloff);

R= p1;
names = [];
for i=1:length(p1)
names = [names,{strcat(char(p1.pianiste(i)),num2str(p1.piece(i)),...
char(p1.nuance(i)),num2str(p1.obs(i)))}];
end
R.pianiste=[];
R.obs =[];
R.piece=[];
%R.nuance=[];
R.Properties.ObsNames=names;
%export(R,'XLSfile','E:\R-3.1.2\bin\x64\dataR.xlsx');

% p2 = horzcat(p1.bright,p1.attackt,p1.kurt,p1.flat,p1.centroid ...
% 	,p1.spread ,p1.entrop ,p1.attleap ,p1.skew,p1.attslp,p1.zerocross,...
% 	p1.rolloff );
p2 =R;
p2.nuance =[];
a  = double(p2);


%a = zscore(a);%pas besoin car pca fait d�j� la normalisation

%  'VariableWeights' - Variable weights. Choices are:
%            - a vector of length P containing all positive elements.
%            - the string 'variance'. The variable weights are the inverse of
%              sample variance. If 'Centered' is set true at the same time,
%              the data matrix X is centered and standardized. In this case,
%              pca returns the principal components based on the correlation
%              matrix.
[coeff,score,latent,tsquared,explained]=pca(a,...
'VariableWeights','variance');
%test du clustering
k = score(:,1:2);
Kind = kmeans(k,5);

couleurs   = [0 0 1; 1 0 0; 0 1 0;0 1 1;1 0 1];
v_coul = double(nominal(p1.nuance));
mark = 'osv';
v_mark = double(nominal(p1.piece));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FIGURE 1
%repr�sentation des individus sur les dimensions 1 et 2
figure;set(gcf,'color','w');
hold on;
title(strcat('Piece ',num2str(p1.piece(1))...
,' :R�partition des interpr�tations sur les dimensions 1 et 2'),...
'FontName','SansSerif',...
'FontSize',20);
for i =1 :length(score)
    x = [];
    y = [];
    z = [];
    X = score(i,1);
    Y = score(i,2);
    Z = score(i,3);
   %if(p1(i,:).piece==PIECE1||p1(i,:).piece==PIECE2||p1(i,:).piece==PIECE3) 
    %C'est ici qu'on affiche les points
    plot3(X,Y,Z,...
		'Marker',mark(v_mark(i)),...
		'MarkerSize',8,...
		'MarkerFaceColor',[couleurs(v_coul(i),:)],...
		'Color',[couleurs(v_coul(i),:)]);
	%et ici leur l�gendes (numero de pianiste, de piece et couleur pour le
	%timbre.
    text(X+0.03, Y+0.03,Z+0.03,... 
		char(p1.pianiste(i)),...
		'Color',couleurs(v_coul(i),:),...
		'FontSize',8);
	%end
end
legend('Brillant','sombre','rond','sec','velout�');
xlabel(strcat('Dim1 : ',num2str(explained(1)),'% expliqu�e'),...
	'FontSize',16);
ylabel(strcat('Dim2 : ',num2str(explained(2)),'% expliqu�e'),...
	'FontSize',16);
zlabel(strcat('Dim3 : ',num2str(explained(3)),'% expliqu�e'),...
	'FontSize',16);
set(gca,'fontSize',16);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Cluster !
% for i = 1:length(score)
% plot3(score(i,1),score(i,2),score(i,3),...
% 		'Marker',v_mark(Kind(i)),...
% 		'MarkerSize',10,...
% 		'Color',[couleurs(v_coul(i),:)]);
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%puis on va chercher les triplettes pour les relier par des lignes
% for i =1:length(p1)
% 	%if(p1(i,:).piece==PIECE1||p1(i,:).piece==PIECE2||p1(i,:).piece==PIECE3) 
% 	for ii=1:length(p1)
% 		if(strcmp(p1.pianiste{i},p1.pianiste{ii})==1 ...
% 				&& p1.piece(i)==p1.piece(ii)...
% 				&& strcmp(p1.nuance{i},p1.nuance{ii})==1 ...
% 				&& p1.obs(i)~= p1.obs(ii))
% 			line([score(i,1) score(ii,1)],...
% 				[score(i,2) score(ii,2)],[score(i,3) score(ii,3)],...
% 				'Color',couleurs(v_coul(i),:),...
% 				'LineWidth',2.5);
% 		end
% 	end
% 	%end
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%on veut afficher un cercle pour la moyenne d'une m�me nuance
m1 = [0 0 0];m2 = [0 0 0];m3 = [0 0 0];m4 = [0 0 0];m5 = [0 0 0];
n1=0;n2=0;n3=0;n4=0;n5=0;
for i=1:length(p1)
%if(p1(i,:).piece==PIECE1||p1(i,:).piece==PIECE2||p1(i,:).piece==PIECE3) 
	switch v_coul(i)
		case 1
			n1=n1+1;
			m1 = m1+[score(i,1) score(i,2) score(i,3)];
		case 2
			n2=n2+1;
			m2 = m2+[score(i,1) score(i,2) score(i,3)];
		case 3
			n3=n3+1;
			m3 = m3+[score(i,1) score(i,2) score(i,3)];
		case 4
			n4=n4+1;
			m4 = m4+[score(i,1) score(i,2) score(i,3)];
		case 5
			n5=n5+1;
			m5 = m5+[score(i,1) score(i,2) score(i,3)];
	end
%end	
end

m1 = m1/n1;
m2 = m2/n2;
m3 = m3/n3;
m4 = m4/n4;
m5 = m5/n5;

plot3(m1(1),m1(2),m1(3),'p','Markersize',25,'Color',couleurs(1,:),'MarkerFaceColor',couleurs(1,:));
plot3(m2(1),m2(2),m2(3),'p','Markersize',25,'Color',couleurs(2,:),'MarkerFaceColor',couleurs(2,:));
plot3(m3(1),m3(2),m3(3),'p','Markersize',25,'Color',couleurs(3,:),'MarkerFaceColor',couleurs(3,:));
plot3(m4(1),m4(2),m4(3),'p','Markersize',25,'Color',couleurs(4,:),'MarkerFaceColor',couleurs(4,:));
plot3(m5(1),m5(2),m5(3),'p','Markersize',25,'Color',couleurs(5,:),'MarkerFaceColor',couleurs(5,:));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%donn�es en sortie
out.coeff=coeff;
out.score=score;
out.latent=latent;
out.tsquared=tsquared;
out.explained=explained;
out.p2=p2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%autres figures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Cercle des variables
figure;title(strcat('Piece ',num2str(p1.piece(1))));set(gcf,'color','w');
subplot(2,2,1);hold on;
title(strcat('Piece ',num2str(p1.piece(1)),...
' :Contribution des variables sur les dimensions 1 et 2'));
coefforth = inv(diag(std(double(out.p2))))*out.coeff;
biplot(coefforth(:,1:3),'score',out.score(:,1:3),...
'varlabels',out.p2.Properties.VarNames);
grid;
xlabel(strcat('Dim1 : ',num2str(explained(1)),'% expliqu�e'));
ylabel(strcat('Dim2 : ',num2str(explained(2)),'% expliqu�e'));
zlabel(strcat('Dim3 : ',num2str(explained(3)),'% expliqu�e'));
%on trace un cercle
ang=0:0.01:2*pi; 
r=0.5;
xp=r*cos(ang);
yp=r*sin(ang);
plot(0+xp,0+yp);
clear r;
set(gca,'XLim',[-1 1]);
set(gca,'YLim',[-0.5 0.5]);

subplot(2,2,3);hold on;
title(strcat('Piece ',num2str(p1.piece(1)),' :Variance expliqu�e'));
pareto(out.explained);
xlabel('Composantes principales');
ylabel('Variance expliqu�e(%)');


subplot(1,2,2);hold on;
boxplot(zscore(double(out.p2)),'orientation','horizontal',...
'labels',out.p2.Properties.VarNames);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%