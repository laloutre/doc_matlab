function out = recalcul_descripteurs(in,start)

mirverbose(0);
mirwaitbar(0);
try
delete ordre.txt;
delete figures.pdf;
delete figures.ps;
f = fopen('ordre.txt','a');


for i = start:length(in)
i
	trim = miraudio(in.chemin(i),'TrimStart','TrimThreshold',0.08);
	
	start_time = get(trim,'Time');
	start_time = start_time{1}{1}(1);	
	
	close all;
	audio			= mirsegment(trim,in.debfin{i}+start_time)
	
    title(in.nom{i});
	set(figure(1),'Name',in.nom{i});
    print('figures','-dpsc','-append');
	BookMark = sprintf('[/Title (%s) /Page %d /OUT pdfmark',in.nom{i},i);
    fprintf(f,BookMark);
    ghostscript('-q -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=figures.pdf  figures.ps ordre.txt')
	
	segments = mirgetdata(audio);
		
	
	bright =[];
	kurt =[];
	attackt =[];
	flat =[];
	centroid =[];
	spread =[];
	entrop =[];
	attleap =[];
	skew =[];
	attslp =[];
	zerocross =[];
	rolloff =[];
	%RMS = [];

							
						
	%calcul pour chaque segment
	for ii=1:size(segments,2)
ii
		%segmentation
		a = miraudio(segments(:,ii));
		a = miraudio(a,'Trim');
a		
		bright = [bright;mirgetdata(mirbrightness(a))];
		kurt = [kurt;mirgetdata(mirkurtosis(a))];
		flat = [flat;mirgetdata(mirflatness(a))];
		centroid = [centroid;mirgetdata(mircentroid(a))];
		spread = [spread;mirgetdata(mirspread(a))];
		entrop = [entrop;mirgetdata(mirentropy(a))];
		b = mirgetdata(mirattackleap(a));
		attleap = [attleap;b(1)];
		skew = [skew;mirgetdata(mirskewness(a))];
		b = mirgetdata(mirattackslope(a));
		attslp = [attslp;b(1)];
		zerocross = [zerocross;mirgetdata(mirzerocross(a))];
		rolloff = [rolloff;mirgetdata(mirrolloff(a))];
		%RMS = [RMS;mirgetdata(mirrms(a))];
		b = mirgetdata(mirattacktime(a));
			try
			attackt = [attackt;b(1)];
			catch
				disp('attack')
				disp(in)
				disp('chunk')
				disp(ii)
			end
			
close(figure(2));
	end			
	
	out{i}.bright	=bright;
	out{i}.kurt		=kurt;
	out{i}.attackt	=attackt;
	out{i}.flat		=flat;
	out{i}.centroid =centroid;
	out{i}.spread	=spread;
	out{i}.entrop	=entrop;
	out{i}.attleap	=attleap ;
	out{i}.skew		=skew;
	out{i}.attslp	=attslp;
	out{i}.zerocross =zerocross;
	out{i}.rolloff	=rolloff;
		

	assignin('base','out',out);
end

catch err
	erreur = getReport(err);
	disp(erreur);
end
fclose(f);
copyfile figures.pdf figures\;
end

%% pour rejouter la sortie au dataset nomm� : piece1
% out = out';
% for i = 1:length(out)
% piece1.bright{i}=out{i,1}.bright;
% piece1.kurt{i}=out{i,1}.kurt;
% piece1.attackt{i}=out{i,1}.attackt;
% piece1.flat{i}=out{i,1}.flat;
% piece1.centroid{i}=out{i,1}.centroid;
% piece1.spread{i}=out{i,1}.spread;
% piece1.entrop{i}=out{i,1}.entrop;
% piece1.attleap{i}=out{i,1}.attleap;
% piece1.skew{i}=out{i,1}.skew;
% piece1.attslp{i}=out{i,1}.attslp;
% piece1.zerocross{i}=out{i,1}.zerocross;
% piece1.rolloff{i}=out{i,1}.rolloff;
% end
