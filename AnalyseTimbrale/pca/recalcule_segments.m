function out = recalcule_segments(in,X)

%out = recalcule_segments(in,X)
%recalcule les segments. en supprimant les segments inferieurs � X (defaut
%: 0.009
%prends en entr�e un dataset (ou une ligne de dataset)

temp = suppr_add(in,X);


	for i = 1:length(in)
		temp1 = cell2mat(temp{i}(:,7));
		%temp2 = cell2mat(temp{i}(:,11));
		
		temp1 = temp1-temp1(1,1);
		%temp2 = temp2-temp1(1,1)-temp2(1,1);
		
		out{i} = temp1;%+temp2;
	end
out = out';
end