function out = filtre_velocite(in,POURCENTAGE)

for i=1:length(in)
	
	temp = midi2nmat(in.chemin_midi{i});
	
	%tentative 1 => on filtre les plus petites v�locit�es de chaque morceau
	% 	[min_val,index] = sort(temp(:,5),'ascend');
	% 	temp(index(1:2),:)=[];

	%tentative 2 => une fenetre se d�place, si une valeur de v�locit� dans
	%cette fenetre est inf�rieure de X% aux autres valeurs de la fenetre
	%alors on l'enl�ve
	TAILLE_FENETRE	= 6;
	%POURCENTAGE		= 30;
	temp2 = [];
	deja_ajoute=[];
	PAS = 3;
	for ii = 1+TAILLE_FENETRE: PAS :length(temp)
	indices = [ii-TAILLE_FENETRE:ii];
		moyenne = mean(temp(indices,5));
	
		for iii =ii-TAILLE_FENETRE:ii
			if(temp(iii,5)>(moyenne-(POURCENTAGE*moyenne)/100) ...
					&& ~sum(deja_ajoute==iii))
			temp2 = [temp2;temp(iii,:)];
			deja_ajoute = [deja_ajoute;iii];
			end
		end
	end
	out{i} = temp2;
	
end

out = out';

% nmat = midi2nmat('res\Xpiece_2.mid');
% 
% for i=1:length(out)
% dyna{i} = dynamicmatch(out{i},nmat);
% end
% 
% out =[out,dyna'];

end