	% Forme [resultat,objets] = checkTimbre(repertoire)
	% Recoit un répertoire et renvoie l'analyse des .wav contenus
	%
	% Analyse avec Mirtoolbox.
function [resultat,objets] = checkTimbre(in)



	if (isdir(in))
		rep = pwd;
		cd(in)
		else disp('Probleme avec le repertoire d entree');
		end

	resultat = struct();
	objets = struct();

	%cd(in);
	mirverbose(0);
	%***************************************************************%
	%***************************************************************%

	lb = 0;
	a = miraudio('Folder','Label',lb);
	resultat = setfield(resultat,'Noms',get(a,'Label'));
	assignin('base','miraudio',a);
	%***************************************************************%
	f = mirframe(a);
	assignin('base','mirframe',f);
	%***************************************************************%
	e = mirenvelope(a);
	resultat = setfield(resultat,'enveloppe',mirgetdata(e));
	assignin('base','enveloppe',e);
	%***************************************************************%
	%disp('Ascending order of zero crossing...')
	z = mirzerocross(a);
	%mirplay(a,'Increasing',z,'Every',5);
	resultat = setfield(resultat,'zerocross',mirgetdata(z));
	assignin('base','zerocross',z);
	%***************************************************************%
	%disp('Ascending order of low energy rate...')
	l = mirlowenergy(a,'frame');
	%mirplay(a,'Increasing',l,'Every',5);
	resultat = setfield(resultat,'lowenergy',mirgetdata(l));
	assignin('base','lowenergy',l);
	%***************************************************************%
	%disp('Ascending order of spectral centroid...')
	c = mircentroid(a,'frame');
	%mirplay(a,'Increasing',c,'Every',5);
	resultat = setfield(resultat,'centroid',mirgetdata(c)');
	assignin('base','centroid',c);
	%***************************************************************%
	%disp('Ascending order of spectral RMS...')
	r = mirrms(a,'frame');
	resultat = setfield(resultat,'RMS',mirgetdata(r));
	assignin('base','rms',r);
	%***************************************************************%
	%disp('Ascending order of spectral roll-off...')
	r = mirrolloff(f);
	%mirplay(a,'Increasing',r,'Every',5);
	resultat = setfield(resultat,'rolloff',mirgetdata(r));
	assignin('base','rolloff',r);
	%***************************************************************%




	%figure(gcf);
	figure;
	hold on;
	% plot(resultat.RMS(1,1:end), 'DisplayName',...
	%     'resultat.RMS(1,1:end)', 'YDataSource', 'resultat.RMS(1,1:end)')
	% plot(resultat.centroid(1,1:end), 'DisplayName',...
	% 'resultat.centroid(1,1:end)', 'YDataSource', 'resultat.centroid(1,1:end)')

	plot(resultat.RMS(1,1:end));
	plot(resultat.centroid(1,1:end));
	hold off;


	cd(rep);
	close all
end



