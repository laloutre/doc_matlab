function [mondata] = datastart(fichier)
%%
%datastart prends un fichier son en entr�e et retourne des informations, obtenues
%avec MIRTOOLBOX sous forme de DATASET.
%
%Usage :
%data = datastart('audiofile.wav');
clc;

if nargin>1
if isstr(fichier)
    
        
        tempaudio = miraudio(fichier,'Trim');
        temponsets = mironsets(tempaudio,'Attack');

        tempattackpos = get(temponsets, 'AttackPosUnit');tempattackpos =cell2mat( tempattackpos{:}{:});
        tempsegment = mirsegment(tempaudio,tempattackpos); 
        tempframe = mirframe(tempsegment);
        fichier = mirstat(tempframe);
end

        temp1 = dataset(fichier.Std','VarNames','Std');
        temp2 = dataset(fichier.Slope','VarNames','Slope');
        temp3 = dataset(fichier.PeriodFreq','VarNames','PeriodFreq');
        temp4 = dataset(fichier.PeriodAmp','VarNames','PeriodAmp');
        temp5 = dataset(fichier.PeriodEntropy','VarNames','PeriodEntropy');
        temp6 = dataset(fichier.Mean','VarNames','Mean');

mondata = horzcat(temp1,temp2,temp3,temp4,temp5,temp6);

mondata = set(mondata,'DimNames',{'Attaques' 'Variables'});
mondata = set(mondata,'Description',fichier.FileNames);
%mondata = set(mondata,'ObsNames', linspace(1,size(guit,1),size(guit,1)));
%Un peut d'affichage.......
get(mondata);

