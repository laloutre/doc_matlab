%%
%datastart prends un fichier son en entr�e et retourne des informations, obtenues
%avec MIRTOOLBOX sous forme de DATASET. Les fichiers donn�es sont calcul�es
%pour chaque attaque (le fichier est donc d�bit�)
%
%Usage :
%data = datastart('audiofile.wav',type);
%et type peut �tre soit : 
%mirstat
%fichier
%repertoire
function [mondata,tempaudio,temponsets,tempattackpos,tempsegment,tempframe] = datastart(fichier,type)



clc;
disp('ldknf')
if strcmp(type,'mirstat')
    mondata = mafonction(fichier);
elseif strcmp(type ,'fichier')
    
    [mondata0,tempaudio,temponsets,tempattackpos,tempsegment,tempframe,InHarmo] = matransformation(fichier);
    mondata = mafonction(mondata0,InHarmo);
elseif strcmp(type , 'repertoire')
    mondata = [];
    rep = dir('*.wav');
    for i = 1 : size(rep,1)
        disp(rep(i,:).name);
        [mondata0,tempaudio,temponsets,tempattackpos,tempsegment,tempframe] = matransformation(rep(i,:).name);
        temp = mafonction(mondata0);
        mondata = [mondata;temp];
        %mondata = mondata i;
    end
%     cellfun(@num2str,num2cell(linspace(1,10,10)),'UniformOutput',false)
%     cellfun(@num2str,ans,'UniformOutput',false);
end



end

function mondata = mafonction(fichier,InHarmo)

disp('Mafonction');
        temp1 = dataset(fichier.Std','VarNames','Std');
        temp2 = dataset(fichier.Slope','VarNames','Slope');
        temp3 = dataset(fichier.PeriodFreq','VarNames','PeriodFreq');
        temp4 = dataset(fichier.PeriodAmp','VarNames','PeriodAmp');
        temp5 = dataset(fichier.PeriodEntropy','VarNames','PeriodEntropy');
        temp6 = dataset(fichier.Mean','VarNames','Mean');
        numero = num2cell(linspace(1,size(fichier.Mean,2),size(fichier.Mean,2)));
        temp7 = dataset(numero','VarNames','Placedesattaques');
        %temp7.Placedesattaques = nominal(temp7.Placedesattaques,numero');
        temp8 = dataset(InHarmo','VarNames','InHarmo');
        
mondata = horzcat(temp7,temp1,temp2,temp3,temp4,temp5,temp6);    
mondata = set(mondata,'DimNames',{'Attaques' 'Variables'});
mondata = set(mondata,'Description',fichier.FileNames);
%Un peut d'affichage.......
get(mondata);

end

function [transfo,tempaudio,temponsets,tempattackpos,tempsegment,tempframe,InHarmo] = matransformation(fichier)
       
        tempaudio = miraudio(fichier,'Trim');
        temponsets = mironsets(tempaudio,'Attack');

        tempattackpos = get(temponsets, 'AttackPosUnit');tempattackpos =cell2mat( tempattackpos{:}{:});
        tempsegment = mirsegment(tempaudio,tempattackpos); 
        tempframe = mirframe(tempsegment);
        transfo = mirstat(tempframe);
        tempspectrum = mirspectrum(tempsegment);
        %InHarmo = 
end
